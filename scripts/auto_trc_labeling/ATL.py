#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: opensim parser compatible with numpy
       :platform: ubuntu
       :synopsis: parse OpenSim motion files into numpy matrices
.. moduleauthor:: Galo Maldonado <galo_xav@hotmail.com>
"""

## TODO : 
## - return all .trc files in folder
## - search if a .trc folder is valid : must have : 
##      - a rmqs.txt file with : 
##          * For each .trc file of the folder which do NOT have a .tak file with the same name as it,
##          a value for CoR
##  - For each experiment folder : 
##      Take each calibration frame for each folder name through rmqs.txt
##      For each .trc file : 
##        * modify trc_file.yaml file with the name of the trc file
##        * launch roslaunch file (through a .bash)
##              ==> subprocess.call(self.absolute_path+"rtab_scripts/run_rtab.sh")
##        * takes the max time of the .trc file
##            - add topic between optitrack_full and this node, in repeat case only
##            - this info will be used to be sure that .trc file is ready in optitrack_full
##        * Takes the optitrack frame corresponding to the calibration frame, turn it in time
##        (through parameter given at begin of node)
##        * Set time through rosservice /to_time at calibration time - 0.5 s
##          - For next step to work, /to_time need to ONLY call pause_continue = true, and NOT std::cin
##        * Call rosservice call /mocap_expe/calibration 1.0 2 False 70 170.0
##          - Need to set pause_continue = false at this step in optitrack_full to work
##        * <At this step, you may still need to check if everything is all right, no solution possible :( >
##        * Get confirm that process is done through another topic that also gives the max duration of the file
##              - after this confirm, get /mocap_expe/optitrack_markers/header/seq (to get begin index); then, 
##              after computing how many frames long the file is, check that we reached this amount with this topic
##        * Call a save of the .trc file
##           - rosservice call /mocap_expe/save_trc '{pathToFolder: "<path to the folder of the .trc file>",data: [""], typeSave: 2}'
##              ==> add poss to directly rename OR mark it as markers.trc
##        * Kill optitrack_full node
##        * GOTO next .trc file
##      When all .trc of folder done : 
##          Copy folder, put it in addbiomechanics format
##          Apply localBiomechanics on it (https://gitlab.inria.fr/auctus-team/people/gautierlaisne/public/addbiomechanicslocal.git)
##          Take resulting .mot files
##          Apply rula_eval on it
##
##
##
##
##
##

import os 

from threading import Thread

import copy

import numpy as np

from sensor_msgs.msg import PointCloud2

from std_srvs.srv import SetBool, SetBoolRequest

from std_msgs.msg import String, MultiArrayDimension, Bool, Float64MultiArray, Int64, Int64MultiArray

from optitrack_full.srv import timeRequest, timeResponse, time, calibration, calibrationRequest, calibrationResponse, saveTxtRequest, saveTxt, saveTxtResponse, trcParametersRequest, trcParameters, listString, listStringRequest, SetFloat
from pathlib import Path

import yaml

import glob

import csv

import tf.transformations

import rospy

import subprocess

import shutil

from auto_trc_labeling.srv import TRCInfos, TRCInfosRequest

from datetime import datetime

def func(pt):
    os.system(pt)

class ATL():

    def getAllTRCFolders(self, path):
        """!
        https://stackoverflow.com/questions/3964681/find-all-files-in-a-directory-with-extension-txt-in-python
        
        """

        allFolders = []


        for root, dirs, files in os.walk(path):

            for file in files:
              
                #print(file)
                if file.endswith(".trc"):
                    L = root.split("/")[:-1]
                    s = ""
                    for x in L:
                        s+=x+"/"
                    if s not in self.allTRCFolders:
                        self.allTRCFolders.append(s)



    def getCalibFramesOfFolder(self, path): 
        D_trc_calib_frame = {}
        D_trc_folders = {}
        D_trc_params = {}

        self.height = -1
        self.sex = -1
        self.mass = -1

        if os.path.exists(path+"rmqs.txt"):

            ## check .txt file
            with open(path+"rmqs.txt") as f:
                lines = f.readlines()
                for line in lines:
                    if len(line) > 3:
                        line = line[:-1]
                        details = line.split(":")
                        folder = details[0].replace(" ","")
                        if os.path.exists(path+folder):
                            
                            ## count .trc files in this particular folder
                            trc_files = []
                            trc_folders = []
                            for root, dirs, files in os.walk(path+folder):

                                for file in files:
                                
                                    #print(file)
                                    if file.endswith(".trc"):
                                        filePath = os.path.join(root,file)

                                        size = os.path.getsize(filePath)
                                        # print(size)
                                        # print(filePath)
                                        if size > 1e6:
                                            ## size must be superior to 1Mb to consider
                                            ## .trc file as valid
                                            trc_files.append(filePath)
                                            trc_folders.append(root + "/")


                            calib_frames = details[1].replace(" ","").split("/")

                            print("calib_frames : ", calib_frames)
                            # print(details[1])


                            if len(calib_frames) == len(trc_files):
                                for k in range(len(calib_frames)):
                                    # ## verification step : check if labelized file already exists. If it's the case, don't bother
                                    # ## to compute it (made later)
                                    # localPathToFile=""
                                    # L = trc_folders[k].split("/")                                    
                                    # for l in range(-4,-1):
                                    #     localPathToFile += L[l] + "/"

                                    # labelizedTRCPathFile = self.labelTRCPath+localPathToFile
                                    # ## Check if labelizedPath exists. If it's the case, it means that we already treated this file : don't bother to recompute it
                                    # if not os.path.exists(labelizedTRCPathFile):   
                                    D_trc_calib_frame[trc_files[k]] =  int(calib_frames[k])
                                    D_trc_folders[trc_files[k]] = trc_folders[k]

                                    D_trc_params[trc_files[k]] = {
                                        "heightM": 1.65,  # default: 1.6                                        
                                        "massKg": 65,  # default: 68.0

                                        "sex": 0  # default: unknown (0); male : 1; female : 2

                                    }

                            else:
                                rospy.logwarn("BUG FOR PATH %s : ", path+folder)
                                print(calib_frames)
                                print(trc_files)
                        elif folder == "infos" or folder=="height":
                            if len(details) >= 2:
                                self.height = float(details[1])
                                self.height = self.height/100
                            if len(details) >=3:
                                if len(details[2]) != 0:
                                    self.sex = int(details[2])
                                else:
                                    self.sex = 0 ##unknown case

                            if len(details) >=4:
                                self.mass = float(details[3])
            if self.height !=-1:       
                rospy.loginfo("Found height : %3.3f"%self.height)
                for keys in D_trc_params:
                    D_trc_params[keys]["heightM"] = self.height
            else:
                rospy.logwarn("ERROR : HEIGHT NOT FOUND FOR SUBJECT")

            if self.sex !=-1:       
                rospy.loginfo("Found sex : %d"%self.sex)
                for keys in D_trc_params:
                    D_trc_params[keys]["sex"] = self.sex
            else:
                rospy.logwarn("ERROR : SEX NOT FOUND FOR SUBJECT")   

            if self.mass !=-1:       
                rospy.loginfo("Found mass : %3.4f"%self.mass)
                for keys in D_trc_params:
                    D_trc_params[keys]["massKg"] = self.mass
            else:
                rospy.logwarn("ERROR : MASS NOT FOUND FOR SUBJECT")  


        return(D_trc_calib_frame, D_trc_folders, D_trc_params)

    def manageTRCFolder(self,path):
        
        error = False

        ##  --- get all calib frames
        rospy.loginfo("Check Path : %s", path)

        D_trc_calib_frames, D_trc_folders, D_trc_params = self.getCalibFramesOfFolder(path)

        if len(list(D_trc_calib_frames.keys())) > 0:

            computedFiles = 0

            

            subjectID = ""

            i = 1
            trcMsg = TRCInfosRequest()
            dimInt = MultiArrayDimension()
            dimFloat = MultiArrayDimension()
            trcMsgForCoR = TRCInfosRequest()


                

            #print(D_trc_folders)

            ## check si osim_result folder already exists for those .trc files. If it is the case, stop treatment; otherwise,
            ## do it.
            firstTRC = list( D_trc_calib_frames.keys())[0]
            trc_folder = D_trc_folders[firstTRC]
            L = trc_folder.split("/")

            subjectID = L[-4]

            if not os.path.exists(self.addBioPath + "data/"+ subjectID+ "/osim_results/IK/%s_result.csv"%(self.ergoType)) or self.ergoOnly or self.SAOnly:
                
                if os.path.exists(self.addBioPath + "data/"+ subjectID+ "/osim_results/IK/%s_result.csv"%(self.ergoType)):
                    ## copy file, to be sure to keep it
                    now = datetime.now()

                    strTime = now.strftime("%Y%m%d_%H%M")


                    shutil.copy2(self.addBioPath + "data/"+ subjectID+ "/osim_results/IK/%s_result.csv"%(self.ergoType),
                                 self.addBioPath + "data/"+ subjectID+ "/osim_results/IK/%s_result_%s.csv"%(self.ergoType,strTime))
                
                    #print("OK!")
                
                # else:
                #     print("BUG")
                # exit()
                    
                    

                dimInt.size = 0
                dimFloat.size = 0
                
                # print(subjectID)
                # print(L)
                # exit()
                create = False
       
                ## dayMode : do labelization
                if os.path.exists(self.addBioPath + "data/"+ subjectID+ "/trials"):
                    if not self.ergoOnly and not self.SAOnly:
                        ## clear everything. Too much issues otherwise.
                        shutil.rmtree(self.addBioPath + "data/"+ subjectID+ "/trials")
                        os.system("rm -rf "+self.addBioPath + "data/"+ subjectID+ "/trials")
                        create = True

                elif not self.dayMode and not os.path.exists(self.addBioPath + "data/"+ subjectID+ "/trials"):
                    create = True

                if create:
                    path = Path(self.addBioPath + "data/"+ subjectID+ "/trials")
                    path.mkdir(parents=True,exist_ok=True)

                    ## add .osim model, rename it as unscaled_generic.osim
                    shutil.copy2(self.osimPath,self.addBioPath + "data/"+ subjectID+ "/unscaled_generic.osim" )

                L_keys = ["heightM", "massKg", "sex"]
                
                for j in range(len(L_keys)):
                    trcMsg.subjectParams.data.append(D_trc_params[firstTRC][L_keys[j]])
                    dimFloat.size += 1
                trcMsg.subjectParams.layout.dim.append(dimFloat)

                ## debug purpose : we would like .trc files to be read in a certain order
                ## order is : CoR, direct, security, computer_no_int, computer_int, computer_tele
                L_trc_order = ["CoR", "direct","security", "computer_no_int", "computer_int", "computer_tele"]

                for k in range(len(L_trc_order)):

                    ## manage keeping of local positions for next trc files
                    ## (to avoid to always have to check for calibration)
                    if (computedFiles == 0):
                        keepLocalPos = 0
                    else:
                        keepLocalPos = 1
                    j = 0
                    L_used_trc = []
                    #used_trc = ""
                    numOfSameKeys = 0
                    currentType = ""
                    listKey = list( D_trc_calib_frames.keys())
                    while j < len(listKey):
                        if L_trc_order[k] in listKey[j]:
                            #used_trc = listKey[j]
                            L_used_trc.append(listKey[j])
                            currentType = L_trc_order[k]
                            numOfSameKeys+=1
                            #j = len(listKey)
                        j+=1
                    
                    # if (numOfSameKeys> 1):
                    #     ## add other elements to assure treatment
                    #     for v in range(1,numOfSameKeys):
                    #         L_trc_order = L_trc_order[:k]+currentType + L_trc_order[k:]
                    treatDone = False              
                    ## if dayMode : because we suppressed trials folder, we need to redo operation on all .trc files

                    if (len(L_used_trc) > 0):

                        s = ""
                        for x in range(len(L_used_trc)):
                            s+= L_used_trc[x] + ", "
                        rospy.loginfo("Files for type %s : %s", currentType, s)

                        for cnt_trc in range(len(L_used_trc)):
                            used_trc = L_used_trc[cnt_trc]
                            
                            rospy.loginfo("Treated file : %s", used_trc)

                            if not rospy.is_shutdown():
                                localPathToFile=""
                                trc_folder = D_trc_folders[used_trc]
                                L = trc_folder.split("/")
                                ## Create addBiomechanics folder
                                
                                calib_frame = D_trc_calib_frames[used_trc]

                                ## Copy folder into labelized files
                                for l in range(-4,-1):
                                    localPathToFile += L[l] + "/"
                                if (numOfSameKeys > 1):
                                    ## we have more than one trc file for this test (for example, because of bug in recording) :
                                    ## special treatment to deal with it
                                    localPathToFile+="file_%i/"%i
                                labelizedTRCPathFile = self.labelTRCPath+localPathToFile

                                ## Create trial folder
                                path = Path(self.addBioPath + "data/"+ subjectID+  "/trials/trial_%i"%i)
                                path.mkdir(parents=True,exist_ok=True)        
                                
                                ## Check if labelizedPath exists. If it's the case, it means that we already treated this file : don't bother to recompute it
                                ## HOWEVER, we will use the old created file for addBiomechanics
                                if not os.path.exists(labelizedTRCPathFile):             


                                    if (self.byBash):
                                        ## * modify trc_file.yaml file with the name of the trc file
                                        D = dict(

                                            optitrack_config=dict(
                                            trc_file=used_trc,
                                            auto_code=True
                                            )
                                        )

                                        with open(
                                            self.labelizing_package
                                            + "/config/trc_file.yaml",
                                            "w",
                                        ) as file:
                                            yaml.dump(D, file, default_flow_style=False)
                                        ##        * launch roslaunch file (through a .bash)
                                        ##              ==> subprocess.call(self.absolute_path+"rtab_scripts/run_rtab.sh")


                                        self.gotTRCParameters = False
                                        s = self.script_path + 'launch_labelization.sh'
                                        proc = subprocess.Popen(s)
                                    
                                        # thread = Thread( target=func, args=(s))
                                        # thread.daemon = True
                                        # #os.system(self.script_path + "launch_labelization.sh &")
                                        # thread.start()
                                        ##subprocess.call(self.script_path + "launch_labelization.sh &")
                                        ## waits for the max time + dataframe of .trc file
                                        ##        * takes the max time of the .trc file

                                        # while not rospy.is_shutdown() and not self.gotTRCParameters:
                                            
                                        #     pass
                                    else:
                                        ## just send to service the new .trc file
                                        msg = saveTxtRequest("",used_trc,keepLocalPos)

                                        suc = False
                                        while not rospy.is_shutdown() and not suc:
                                            suc,resp,ret = self.call_service("to_set_trc", self.to_set_trc , msg, verbose=True)
                                        
                                        if (suc):
                                            ## wait for state = 0 (== end of reboot)
                                            self.state = 2
                                            while not rospy.is_shutdown() and self.state == 2:
                                                self.r.sleep()
                                                #rospy.loginfo("Wait for end of reboot")

                                    suc = False
                                    resp = None
                                    msg = trcParametersRequest()
                                    while not rospy.is_shutdown() and not suc:
                                        suc,resp,ret = self.call_service("ask_trc_params", self.to_trc_parameters , msg)
                                    if suc:
                                        self.gotTRCParameters = suc            
                                        self.dataframe = ret.dataFrame
                                        self.trcDuration = ret.maxTime
                                        self.trcDurInFrames = int(self.trcDuration*self.dataframe)                
                                    if self.gotTRCParameters:
                                        rospy.loginfo("GOT TRC PARAMETERS")
                                    ##        * Takes the optitrack frame corresponding to the calibration frame, turn it in time
                                    ##      

                                        

            
                                        time_frame = max(calib_frame/self.dataframe - 0.5, 0.0)

                                        msg = timeRequest(time_frame,1)

                                        suc = False
                                        while not rospy.is_shutdown() and not suc:
                                            suc,resp,ret = self.call_service("to_time", self.to_time_client , msg, verbose=True)
                                        
                                        ## sleep for 1 sec
                                        ts = rospy.Time.now().to_sec()
                                        while (rospy.Time.now().to_sec()- ts) < 1.0:                 
                                            self.r.sleep()
                                        # print(rospy.Time.now().to_sec())

                                        ## Call rosservice call /mocap_expe/calibration 1.0 2 False 70 170.0

                                        msg = calibrationRequest(1.0,2, False, 70.0, 170.0 )

                                        suc = False
                                        while not rospy.is_shutdown() and not suc:
                                            suc,resp,ret = self.call_service("to_calib", self.to_calib_client , msg)

                                        ## * Get confirm that process is done through another topic that also gives the max duration of the file
                                        ##  - after this confirm, get /mocap_expe/optitrack_markers/header/seq (to get begin index); then, 
                                        ##   after computing how many frames long the file is, check that we reached this amount with this topic
                                        self.state = 1
                                        while not rospy.is_shutdown() and self.state == 1:
                                            self.r.sleep()

                                        rospy.loginfo("Confirmed that we have noticed the end of calibration")

                                        ## to be sure that we go back to time where there is info about calibration, BUT
                                        ## we do not want any pause
                                        msg = timeRequest( time_frame,-1)

                                        suc = False
                                        while not rospy.is_shutdown() and not suc:
                                            suc,resp,ret = self.call_service("to_time", self.to_time_client , msg, verbose=True)
                                        

                                        self.ori_id_pc = self.id_pc

                                        self.lastCnt = 0
                                        howMany = self.dataframe*10

                                        waitFrames = self.numWait*(self.trcDurInFrames+ self.dataframe)
                                        reachMid = False

                                        while not rospy.is_shutdown() and (self.id_pc - self.ori_id_pc) < waitFrames:
                                            valuesUntilEnd =  waitFrames - (self.id_pc - self.ori_id_pc)
                                            if (self.id_pc - self.ori_id_pc) > howMany*self.lastCnt:
                                                rospy.logwarn("Number of frames before stop for file %s : %i", used_trc ,valuesUntilEnd)
                                                self.lastCnt+=1
                                            if not reachMid and (self.id_pc - self.ori_id_pc) > waitFrames/2:
                                                rospy.logwarn("====== REACHED HALF OF THE FRAMES BEFORE STOP ====== (last : %i frames)" ,valuesUntilEnd)
                                                reachMid = True
                                            
                                            if (self.needClean):
                                                suc = False
                                                while not rospy.is_shutdown() and not suc:
                                                    suc,resp,ret = self.call_service("clear_assos", self.clear_assos_client , self.cleanReq)

                                                if suc:
                                                    self.cleanReq = None
                                                    self.needClean = False
                                                    self.ori_id_pc = self.id_pc - self.dataframe
                                                    waitFrames = self.numWait*(self.trcDurInFrames+ self.dataframe)
                                                    self.lastCnt = 0
                                                    reachMid = False

                                            self.r.sleep()

                                        
                                        ##   * Call a save of the .trc file
                                        ##     - rosservice call /mocap_expe/save_trc '{pathToFolder: "<path to the folder of the .trc file>",data: [""], typeSave: 3}'
                                        ##     

                                        if not rospy.is_shutdown():

                                            ## if not shutdown, creates folder
                                            path = Path(labelizedTRCPathFile)
                                            path.mkdir(parents=True,exist_ok=True)        

                                            msg = saveTxtRequest("",labelizedTRCPathFile,3)

                                            suc = False
                                            while not rospy.is_shutdown() and not suc:
                                                suc,resp,ret = self.call_service("to_save_trc", self.to_save_trc , msg)
                                            
                                            ## reset speed to 1.0 (for security)
                                            suc = False
                                            while not rospy.is_shutdown() and not suc:
                                                suc,resp,ret = self.call_service("speed reset", self.reading_speed_client , 1.0)                                                                                                
                                            if (self.byBash):
                                                ## Kill optitrack_full node
                                                subprocess.call(self.script_path + "stop_labelization.sh")
                                                proc.terminate()
                                                #thread.join()

                                                ## Check for stop 
                                                self.stopCheck()

                                            # ts = rospy.Time.now().to_sec()
                                            # while (rospy.Time.now().to_sec()- ts) > 3.0:
                                            #     pass

                                            ## Copy markers.trc into addbiomechanics folder
                                            # L = used_trc.split("/")
                                            # used_trc_mark = "/"
                                            # for k in range(len(L)-1):
                                            #     used_trc_mark+= L[k]+"/"
                                            # used_trc_mark+="markers.trc"

                                            treatDone = True
                                            computedFiles+=1

                                    else:
                                        if (self.byBash):
                                            ## Kill optitrack_full node
                                            subprocess.call(self.script_path + "stop_labelization.sh")
                                            proc.terminate()
                                            ## Check for stop 
                                            self.stopCheck()

                                else:
                                    treatDone = True
                                    
                                if not rospy.is_shutdown() and treatDone:

                                    used_trc_mark= labelizedTRCPathFile+"/markers.trc"
                                    
                                    shutil.copy2(used_trc_mark,self.addBioPath + "data/"+ subjectID+ "/trials/trial_%i/markers.trc"%i )

                                    ## add infos to TRCInfosRequest
                                    trcMsg.trcFilesPath.append(self.addBioPath + "data/"+ subjectID+"/trials/trial_%i/markers.trc"%i)
                                    trcMsg.calibFrames.data.append(calib_frame)
                                    dimInt.size += 1


                                    if self.tryCoRAlone and len(trcMsgForCoR.trcFilesPath) == 0:
                                    ##if self.tryCoRAlone and L_trc_order[k] == "CoR":  
                                        ## suppressed, otherwise bug when CoR file was missing
                                                                                

                                        if os.path.exists(self.addBioPath + "data/CoRCompute"):
                                            ## clear everything. Too much issues otherwise.
                                            shutil.rmtree(self.addBioPath + "data/CoRCompute")
                                            os.system("rm -rf "+self.addBioPath + "data/CoRCompute")
                                        
                                        path = Path(self.addBioPath + "data/CoRCompute"+"/trials/trial_1/")
                                        path.mkdir(parents=True,exist_ok=True)

                                        ## add .osim model, rename it as unscaled_generic.osim
                                        shutil.copy2(self.osimPath,self.addBioPath + "data/CoRCompute"+ "/unscaled_generic.osim" )

                                        ## copy first trc file
                                        CoRFileTRC = self.addBioPath + "data/CoRCompute"+ "/trials/trial_1/markers.trc"
                                        shutil.copy2(used_trc_mark, CoRFileTRC )

                                        ## finish message
                                        trcMsgForCoR = copy.deepcopy(trcMsg)
                                        trcMsgForCoR.trcFilesPath = []
                                        trcMsgForCoR.trcFilesPath.append(CoRFileTRC)
                                        trcMsgForCoR.calibFrames.layout.dim.append(dimInt)
                                        trcMsgForCoR.doErgo = False
                                        trcMsgForCoR.doIK = True
                                        trcMsgForCoR.doSA = False

                                        rospy.loginfo("CoR will be set with file of type %s", L_trc_order[k])

                                    dimFloat = MultiArrayDimension()
                                    ## to be sure of the parameters order in the list



                                i+=1

                    else:
                        rospy.logerr("WARNING : FILE FOR %s IS MISSING", L_trc_order[k])
                        print( list( D_trc_calib_frames.keys()) )
                # if computedFiles>0:
                ## (for debug purpose) pause_continue = true
                # pause = SetBoolRequest(True)
                # suc = False
                # while not rospy.is_shutdown() and not suc:
                #     suc,resp,ret = self.call_service("pause_continue", self.to_pause_continue ,pause)
                
                if not self.dayMode:
                    ## a essayer sinon : 
                    ## - on applique addbiomechanics juste avec trial_1, en mettant dans dossier "TRY"
                    ## - on recupère modèle généré, on l'appelle unscaled
                    ## - on applique addbiomechanics à l'ensemble des essais
                    ## - check si meilleurs résultats ou non




                    ## night mode : do computing of addbiomechanicslocal
                    ## Check if addbiomechanics is available. If not, waits
                    ## When available, send all informations to node : 
                    ## Path, mass (kg), height (m), sex, D_trc_calib_frames
                    if not rospy.is_shutdown():
                        trcMsg.calibFrames.layout.dim.append(dimInt)
                        trcMsg.doErgo = (self.SAOnly == False)
                        trcMsg.typeErgo = self.ergoType
                        trcMsg.doIK = (self.ergoOnly == False and self.SAOnly == False)
                        trcMsg.doSA = True

                        if (self.tryCoRAlone):

                            ## 1st :try with only CoR (already prepared)
                            suc = False
                            while not rospy.is_shutdown() and not suc:
                                suc,resp,ret = self.call_service("addBio_server (CoR Case)", self.to_addBio ,trcMsgForCoR, 1000, verbose=True)
                                if not suc:
                                    self.r.sleep()               
                                         
                                self.addBioState = "Busy"
                                ## wait for treatment of CoR to end
                                while not rospy.is_shutdown() and self.addBioState != "IDLE":
                                    self.r.sleep()

                                ## displace final.osim to folder
                                if not rospy.is_shutdown():
                                    shutil.copy2(self.addBioPath + "data/CoRCompute/"+"osim_results/Models/final.osim",self.addBioPath + "data/"+ subjectID+ "/unscaled_generic.osim" )
                                    
                                ## finally, call trcMsg for all


                        suc = False
                        while not rospy.is_shutdown() and not suc:
                            suc,resp,ret = self.call_service("addBio_server", self.to_addBio ,trcMsg, 10, verbose=False)
                            if not suc:
                                self.r.sleep()

                        self.addBioState = "Busy"
                        ## wait for treatment of CoR to end
                        while not rospy.is_shutdown() and self.addBioState != "IDLE":
                            self.r.sleep()                                
                ## (for debug purpose) pause_continue = false
                # pause = SetBoolRequest(False)
                # suc = False
                # while not rospy.is_shutdown() and not suc:
                #     suc,resp,ret = self.call_service("pause_continue", self.to_pause_continue ,pause)
                

                    ## GOTO next folder; all next treatments will be done in other node


    def stopCheck(self):
        """
        
        Check if everything is stopped by calling to_time service
        
        !"""
        result = 0
        i = 0
        while not rospy.is_shutdown() and result != None :
            try : 
                result = None
                msg = timeRequest(-1.0,False)
                ret = self.to_time_client(msg)
                if ret != None :
                    i += 1
                    if (i%1000 == 0):
                        print("Error for the test : mocap_expe still activated")
                    subprocess.call(self.script_path + "stop_labelization.sh")
                
            except rospy.ServiceException as e :
                result = None

        if result == None:
            print("mocap_expe stopped")            


            

    def call_service(self,service_name,service, request, vmax=1000,verbose = False):
        v = 0
        resp1 = 0
        success = False
        ret = None
        if verbose:
            rospy.loginfo("Begin call to %s..."%service_name)
        while (not rospy.is_shutdown() ) and (resp1 == 0):
            try:
                ret = service(request)
                try:
                    success = ret.success
                except Exception:
                    success = True
                resp1 = 1

                if (success and verbose):
                    rospy.loginfo("Call to %s successful!"%service_name)
                elif not success and vmax >= 1000:
                    rospy.loginfo("Call to %s worked, but result failed."%service_name)
            except Exception as e :
                #rospy.loginfo("HERE")
                v+=1
                
                if (v%100) == 0:
                    if (verbose):
                        rospy.loginfo("Still try to call %s..."%service_name)
                if v> vmax:
                    rospy.loginfo("Error : too many failures into calling service %s. Please retry."%service_name)
                    resp1 = -1
                    success = False
            # except Exception:
            #     rospy.loginfo("HERE")
            #     resp1 = 0
                
        return(success,resp1,ret)




    def get_trc_parameters(self,msg):
        rospy.loginfo("Got msg")
        if not self.gotTRCParameters:
            self.dataframe = msg.data[0]
            self.trcDuration = msg.data[1]
            self.trcDurInFrames = int(self.trcDuration*self.dataframe)
            self.gotTRCParameters = True

    def get_state(self,msg):
        self.state = msg.data

    def get_pc(self,msg):
        """https://answers.ros.org/question/344096/subscribe-pointcloud-and-convert-it-to-numpy-in-python/
        !"""
        self.id_pc = msg.header.seq
        
    
    def manageClear(self,msg):
        
        self.cleanReq = listStringRequest()

        L = []

        for k in range(len(msg.data)):
            if (msg.data[k][0] == "_"):
                ## special command for here
                cmd = msg.data[k][1:]
                #print(cmd)
                L2 = cmd.split("=")
                #print(L2)
                if L2[0] == "NR":
                    self.numWait = float(L2[1])
                    print(self.numWait)
                
            else:
                L.append(msg.data[k])

        self.cleanReq.data = L
        self.needClean = True

        return(True,"CleanUp taken into account; it will be sent to optitrack_full")

    def getAddBioState(self,msg):
        self.addBioState = msg.data


    def __init__(self):
        rospy.init_node('atl',
                    anonymous=True)
        nodeName = rospy.get_name()
        nodeName+="/"
        self.r = rospy.Rate(10.0)        
        ## get parameters : 



        ## Path to current package
        self.absolute_path = rospy.get_param(nodeName + "absolutePath")

        if self.absolute_path[-1] != "/":
            self.absolute_path+="/"

        ### Path to scripts
        self.script_path = self.absolute_path+"bash_scripts/"

        self.numWait = 2


        ## Path to optitrack_full package
        self.labelizing_package = rospy.get_param(nodeName + "labelizingPackagePath")
        if len(self.labelizing_package) > 1 and self.labelizing_package[-1] != "/":
            self.labelizing_package+="/"
        ## Path to addbiomechanics package

        self.addBioPath = rospy.get_param(nodeName + "addBioPackagePath")
        if len(self.addBioPath) > 1 and self.addBioPath[-1] != "/":
            self.addBioPath+="/"


        ## Path to unlabelized trc files

        self.trcPath  =  rospy.get_param(nodeName + "TRCPath")
        if len(self.trcPath) > 1 and self.trcPath[-1] != "/":
            self.trcPath+="/"

        ## Path where to save labelized trc files

        self.labelTRCPath = rospy.get_param(nodeName + "labelTRCPath")
        if len(self.labelTRCPath) > 1 and self.labelTRCPath[-1] != "/":
            self.labelTRCPath+="/"


        ## Path to opensim model used
        self.osimPath  =  rospy.get_param(nodeName + "osimPath")

        


        self.byBash = rospy.get_param(nodeName + "byBash")

        ## Day or night mode
        ## Day mode : Do labelization on unlabelized files. Do not
        ## use addbiomechanics local as it conducts to a big loss in
        ## labelization performances
        ## Night mode : Use addbiomechanicslocal on all concerned files, as it can be executed
        ## automatically.
        self.dayMode = rospy.get_param(nodeName + "dayMode")

        ### (Night mode only) If we should try first to scale model
        ## on CoR datas
        self.tryCoRAlone = rospy.get_param(nodeName + "tryCoRAlone")

        ### (Night mode only) If we should only do RULA
        self.ergoOnly = rospy.get_param(nodeName + "ergoOnly")

        ### (Night mode only) If we should only do Speed Acc
        self.SAOnly = rospy.get_param(nodeName + "SAOnly")

        ### (Night mode only) type of ergo (REBA, RULA)
        self.ergoType = rospy.get_param(nodeName + "ergoType")

        ## special case : RULA Only ==> no CORAlone
        if self.ergoOnly or self.SAOnly:
            self.tryCoRAlone = False




        ## Variables

        ### Dataframe (normally, 100 Hz)
        self.dataframe = -1
        self.trcDuration = -1
        self.trcDurInFrames = -1

        ### Publishers

        ### Subscribers
        self.trc_sub = rospy.Subscriber('/trc_parameters',Float64MultiArray, self.get_trc_parameters)

        self.state_sub =  rospy.Subscriber('/mocap_expe/state',Int64, self.get_state)

        self.pc_sub = rospy.Subscriber("/mocap_expe/optitrack_markers", PointCloud2, self.get_pc)

        self.addbioState_sub = rospy.Subscriber("/addBio_state",String, self.getAddBioState)

        self.addBioState = "IDLE"

        self.state = -1

        self.id_pc = -1

        self.state_poss = ["Classic", "Calibration"]

        self.ori_id_pc = -1
        ## Clients
        self.to_time_client = rospy.ServiceProxy("/mocap_expe/to_time", time)

        self.to_calib_client = rospy.ServiceProxy("/mocap_expe/calibration", calibration)

        self.to_save_trc = rospy.ServiceProxy("/mocap_expe/save_trc", saveTxt)

        self.to_set_trc =  rospy.ServiceProxy("/mocap_expe/set_trc", saveTxt)

        self.to_trc_parameters = rospy.ServiceProxy("/mocap_expe/trc_params_server",trcParameters)

        self.to_addBio = rospy.ServiceProxy("/addBio_server", TRCInfos)

        self.to_pause_continue = rospy.ServiceProxy("/mocap_expe/pause_continue", SetBool)

        self.clear_assos_client = rospy.ServiceProxy("/mocap_expe/clear_assos",listString)
        
        self.reading_speed_client = rospy.ServiceProxy("/mocap_expe/reading_speed",SetFloat)

        ## Server


        self.to_clean_assos = rospy.Service("/to_clear_assos",listString, self.manageClear)

        self.needClean = False
        self.cleanReq = None


        self.gotTRCParameters = False

        self.lastVal = -1
        self.lastCnt = 0

        if not self.dayMode:
            rospy.logwarn("WARNING : If you just finished the labelization of your .trc, you might want to be sure that they are clean (with roslaunch human_control clean_trc.launch, which might takes some time). Otherwise, you might have bad addbiomechanics data in output.")
            rospy.logwarn("Please press any touch to confirm.")
            input("")



        ## - return all .trc files in folder
        self.allTRCFolders = []
        self.getAllTRCFolders(self.trcPath)

        print(self.allTRCFolders)

        if not self.byBash:
            ## sleep for 1 sec
            ts = rospy.Time.now().to_sec()
            while (rospy.Time.now().to_sec()- ts) < 1.0:                 
                self.r.sleep()            

        for folder in self.allTRCFolders:
            if not rospy.is_shutdown() :
                self.manageTRCFolder(folder)

        ## (for debug purpose) pause_continue = true
        pause = SetBoolRequest(True)
        suc = False
        while self.dayMode and not rospy.is_shutdown() and not suc:
            suc,resp,ret = self.call_service("pause_continue", self.to_pause_continue ,pause)
        

        subprocess.call(self.script_path + "stop_labelization.sh")
        
        #rospy.signal_shutdown("Code done with success.")
if __name__ == "__main__":

    ATL()

