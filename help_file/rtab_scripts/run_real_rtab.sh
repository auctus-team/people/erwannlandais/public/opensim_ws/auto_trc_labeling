#!/bin/bash

# Launch orb and gmapping at the same time.

sudo killall gzserver
sudo killall gzclient
sudo killall rviz

cd ..
source container_variables.sh
WORLD_NAME="real_orange_labs"
# currents world_name : maze_world | rosbot_world | test_orb2 | test_orb_dc | willowgarage_world | test_orb_real_dc
mkdir rosbags
mkdir rosbags/rts
cd rosbags/rts
mkdir $WORLD_NAME
cd ../..
source devel/setup.bash
#docker start ${rtab_slam_container}
#docker exec -dit ${rtab_slam_container} /home/launch_rts_real.sh
#rosrun image_view image_view image:=/camera/rgb/image_rect_color _image_transport:=compressed &
#rosrun image_view image_view image:=/camera/depth/image_raw _image_transport:=compressedDepth &
roslaunch rosbot_description rosbot_rts.launch type_cam:=0 world_name:=$WORLD_NAME simu:=0 navig:=false visu:=true record_raw:=1 activate_mb:=false activate_gazebo:=true
#docker stop ${rtab_slam_container}
