#!/bin/bash

cd "/home/tnmx0798/Documents/ROSBot_simu"
source container_variables.sh
source ${path_global}"/src/rosbot_description/src/rosbot_gazebo/src/orb_node/temp.sh"
rm -r ${path_to_database}"/last_database"
mkdir ${path_to_database}"/last_database"
docker cp ${rtab_slam_container}:/root/.ros/rtabmap${index_rtab}.db ${path_to_database}"/last_database"
rm ${path_global}"/src/rosbot_description/src/rosbot_gazebo/src/orb_node/temp.sh"
docker stop ${rtab_slam_container}
