#!/bin/bash

# Launch orb and gmapping at the same time.

sudo killall gzserver
sudo killall gzclient
sudo killall rviz

cd ..
source container_variables.sh
WORLD_NAME="geko_datacenter_easy_simulation"
# currents world_name : maze_world | rosbot_world | test_orb2 | test_orb_dc | willowgarage_world | geko_datacenter_easy_simulation | geko_datacenter_diff_simulation |  geko_datacenter_simulation | geko_lu | turtlebot3_world
mkdir rosbags
mkdir rosbags/gmap
cd rosbags/gmap
mkdir $WORLD_NAME
cd ../..
source devel/setup.bash
#docker start ${rtab_slam_container}
#docker exec -dit ${rtab_slam_container} /home/launch_rts.sh
#rosrun image_view image_view image:=/camera/rgb/image_raw &
roslaunch rosbot_description rosbot_gmap.launch type_cam:=0 world_name:=$WORLD_NAME simu:=1 navig:=true record:=0 record_raw:=0 raw_or_amcl:=1 map:=$WORLD_NAME activate_mb:=true 
#docker stop ${rtab_slam_container} 
