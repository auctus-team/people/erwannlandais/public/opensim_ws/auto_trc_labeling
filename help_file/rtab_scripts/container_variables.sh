#!/bin/bash

# Variables of the docker containers.

# for orb_slam2

export orb_slam2_container="cos2"
export record=0

# for rtab_slam

export rtab_slam_container="rts2"
export record=0
export path_global="/home/tnmx0798/Documents/ROSBot_simu"
export rosbag_name=${path_global}"/rosbags/rts/geko_datacenter_easy_simulation/raw_recorded_data_2020-07-30-11-01-09_0.bag" #raw_recorded_data_2020-07-29-15-04-19_0.bag" #raw_recorded_data_2020-07-21-13-43-52_0.bag" #raw_recorded_data_2020-07-01-17-47-29_0.bag" #raw_recorded_data_2020-07-01-17-47-29_0.bag" # test.bag"
export path_to_launchfile=${path_global}"/src/rosbot_description/src/rosbot_gazebo/src/orb_node/rtabmap_auto.launch"
export path_to_database=${path_global}"/data/Rts_Datas"
