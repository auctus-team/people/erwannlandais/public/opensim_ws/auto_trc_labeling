#!/bin/bash

# Launch orb and gmapping at the same time.

sudo killall gzserver
sudo killall gzclient
sudo killall rviz

rosparam set use_sim_time true
cd ..
source container_variables.sh
source devel/setup.bash
roslaunch rosbot_description rosbot_rts_rosbag.launch simu:=1 nb_evaluate:=1
