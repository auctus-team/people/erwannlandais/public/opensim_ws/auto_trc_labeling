#!/bin/bash

cd "/home/tnmx0798/Documents/ROSBot_simu"
source container_variables.sh
docker start ${rtab_slam_container}
docker cp ${path_to_launchfile} ${rtab_slam_container}:/opt/ros/kinetic/share/rtabmap_ros
