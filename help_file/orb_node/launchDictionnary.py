#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:04:42 2020

@author: tnmx0798
"""

import subprocess

from datetime import datetime,timedelta

import os

import csv

import numpy as np

import matplotlib.pyplot as plt

class Parameter():
    
    def __init__(self,name,typ,values,all_values,already_checked = False):
        """
    

        Parameters
        ----------
        name : string
            Name associated with the parameter. (ex : Marker/VarianceLinear)
        typ : string
            Type of the parameter.
        all_values : boolean
            Indicate whether the object values design all the values available for the parameter.
        values : list
            Indicate whether all the values available for the parameter, or the : [ max_value, min_value, value_at_beginning]
        current_values : list
            Indicate the values of the parameter. 

        Returns
        -------
        None.

        """
        self.name = name
        self.type = typ
        self.all_values = all_values
        self.ori_values = values
        if (self.all_values):
            self.current_values = values[0]
        else:
            self.current_values = values
        self.already_checked = already_checked


class launchDictionnary():
          
    def save_dictionnary(self,f):
        
        f.write(" ---- Dictionnary Parameters ---- \n \n")
        
        for i in range(len(self.L_dics)):
            f.write("Type of parameters : "+ self.L_dics[i][-2][1] + " \n" )
            for j in range(len(self.L_dics[i]) - len(self.L_info_class)):
                param = self.L_dics[i][j][1]
                line = 'Name : "' + param.name + '" ; Type : ' + param.type + " ; Current_Values : ["
                if (param.all_values == False):
                    for j in range(len(param.current_values)-1):
                        line += str(param.current_values[j]) + " | "
                    line+= str(param.current_values[len(param.current_values)-1]) + "]"
                else:
                    try:
                        line += str(param.current_values[0]) + "]"
                    except Exception : 
                        line += str(param.current_values) + "]"
                line += " ; All_values : " + str(param.all_values) +  " ; Already_checked : " + str(param.already_checked) +" \n"
                f.write(line)
            # for j in range(len(self.L_info_class)):
                
            f.write("\n")
            
        
    def load_dictionnary(self,name,global_load):

        cur_index_class = -1
        cur_index_param = -1
        use_param = True
        
        search_last_value = False
        
        with open(name,"r") as f:
            Lines = f.readlines()
            for i in range(1,len(Lines)):
                line = Lines[i]
                #print(line)
                if line != "":
                    line = line.split("\n")[0]
                    
                    if line[0:9] == "Precedent" and search_last_value == True:
                        Lline = line.split(" : ")
                        self.trans_err = float(Lline[2].split(" (")[0] )
                        self.trans_err_std = float(Lline[3].split(")")[0] )
                        self.rot_err = float(Lline[4].split(" (")[0] )
                        self.rot_err_std = float(Lline[5].split(")")[0] )
                        self.pos_LC = float(Lline[6].split(" ; ")[0] )
                        self.neg_LC = float(Lline[7] )
                        i = len(Lines)
                        break

                    if line[0:5] == "Start":
                        search_last_value = True
                    elif line == '':
                        pass
                    
                    elif search_last_value == False:
                        param_line = line.split(";")
                        #print(param_line)
                        if len(param_line) == 1:
                            cur_index_class = -1
                            param_name = param_line[0].split(":")
                            
                            if (len(param_name) > 1):
                                param_name = param_line[0].split(":")[1][1:-1]
                            #print("Search : ", param_name)
                            for l in range(len(self.L_dics)):
                                #print(self.L_dics[l][-2][1])
                                if self.L_dics[l][-2][1] == param_name:
                                    cur_index_class = l
                                    break
                            if cur_index_class == -1:
                                use_param = False
                            else:
                                use_param = True
                        else :
                            if use_param:
                                # check concerned line
                                #print("into use_param")
                                #print( param_line[0].split(":"))
                                name_par = param_line[0].split(":")[1][2:-2]
                                cur_index_param = -1
                                for l in range(len(self.L_dics[cur_index_class])):
                                    if self.L_dics[cur_index_class][l][0] == name_par:
                                        cur_index_param = l
                                        break
                                if cur_index_param != -1:
                                    #print("check param")
                                    #param = self.L_dics[cur_index_class][cur_index_param][1]
                                    type_par = param_line[1].split(":")[1][1:-1]
                                    self.L_dics[cur_index_class][cur_index_param][1].type = type_par
                                    
                                    cur_val = param_line[2].split(":")[1][2:-2].split(" | ")
                                    L = []
                                    #print(cur_val)
                                    # cur_val[0] = cur_val[0][1:]
                                    # cur_val[-1]  = cur_val[-1][:-1]
                                    # print(cur_val)
                                    
                                    
                                    for l in range(len(cur_val)):
                                       # print("For %i : %s"%(l,cur_val[l]))
                                        L.append(float(cur_val[l]))
                                    if len(L) == 3 and global_load==False:
                                        L[0] = self.L_dics[cur_index_class][cur_index_param][1].ori_values[0]
                                        L[1] = self.L_dics[cur_index_class][cur_index_param][1].ori_values[1]

                                    type_all_values = (param_line[3].split(":")[1][1:-1] == "True")

                                    if type_all_values== True:
                                        self.L_dics[cur_index_class][cur_index_param][1].current_values = L[0]
                                        #print(self.L_dics[cur_index_class][cur_index_param][1].current_values)
                                    else : 
                                        self.L_dics[cur_index_class][cur_index_param][1].current_values = L
                                    self.L_dics[cur_index_class][cur_index_param][1].all_values = type_all_values
                                    type_already_checked = (param_line[4].split(":")[1][1:-1] == "True")

                                    if (global_load):
                                        self.L_dics[cur_index_class][cur_index_param][1].already_checked = type_already_checked
                                    if param_name in self.L_nopt:
                                        self.L_dics[cur_index_class][cur_index_param][1].already_checked = True
                            
                              
                            
            
    
    def add_namespace(self,f,k,all_same):
        """
        

        Parameters
        ----------
        f : file
            Fichier launchfile en cours de réalisation.
        k : int
            Numéro namespace.

        Returns
        -------
        None.

        """
        deb = self.begin_namespace+str(k) + '">'
        f.write(deb)
        with open(self.pwd_begin_namespace,"r+") as f2:
            beg_content = f2.read()
        f.write(beg_content)
        for j in range(len(self.L_dics)):
            actual_class = self.L_dics[j]
            typ_of_class = actual_class[-2][1]
            change = False
            if (typ_of_class == self.parameter[0]):
                change = True
                #print("type of class : ", typ_of_class)
            
            parameters_of_class = [actual_class[l][0] for l in range(len(actual_class)) if  (actual_class[l][0] not in self.L_info_class) ]
            if_cond = actual_class[-1][1]

            l = -1
            if change: 
                l = parameters_of_class.index(self.parameter[1])
            
            i = 0
            while i < (len(parameters_of_class) ):
                if i == l:
                    if all_same==-1:
                        value = self.actual_variation[k-1]
                    else:
                        value = self.actual_variation[all_same]
                else:
                    if actual_class[i][1].all_values :
                        value = actual_class[i][1].current_values
                    else:
                        value = actual_class[i][1].current_values[2]
                        
                line = "<param " + if_cond + ' name="' + parameters_of_class[i] 
                line = line + '"             type="string" value="' + str(value) +'" /> \n'
                f.write(line)
                i+=1
                
            f.write(" \n")
            
        l1 = ' <param name="map_frame_id"         type="string" value="map%i"/> \n'%k
        l2 = ' <param name="database_path"        type="string" value="$(arg database_path_%i)"/> \n'%k
        f.write(l1)
        f.write(l2)
        f.write(self.end_namespace)
    
    
    def create_bash_stop_rtabmap(self):
        if os.path.exists(self.path + "stop_rtabmap.sh") :
            os.remove(self.path + "stop_rtabmap.sh")
        with open(self.path +"stop_rtabmap.sh","a+") as f:
            f.write("#!/bin/bash \n \n \n")
            line = "rosnode kill "
            for k in range(1,self.nb_diff+1):
                line += " /rtabmap%i/rtabmap "%k
            line += " /rgbd_sync \n"
            f.write(line)
        cmd = 'chmod +x '+self.path + "stop_rtabmap.sh"
        os.system(cmd)
    
    
    def create_launchfile(self,all_same=-1):
        
        with open(self.path +"rtabmap_auto.launch","a+") as f:
            with open(self.pwd_begin_launchfile,"r+") as f2:
                beg_content = f2.read()
            f.write(beg_content)
            for k in range(1,self.nb_diff+1):
                self.add_namespace(f,k,all_same)
            f.write(self.end_launchfile)
            
    def destroy_launchfile(self):
        nme = self.path+"rtabmap_auto.launch"
        if os.path.exists(nme):
            os.remove(nme)
        else:
            print("File not found")

    def load_from_txt(self,name):
        L_mean_t = []
        L_mean_t_std = []
        L_mean_r = []
        L_mean_r_std = []    
        LC_pos = []
        LC_neg = []
        
        
        Rank = []
        
        typename = ''
        
        with open(name,"r") as f:
            Lines = f.readlines()
            
            typename = Lines[0]
            #print(typename)
            
            params = Lines[2].split(' : ')[1].split(' | ')[1:-1]
            #print(params)
            Lparams = [ float(params[k]) for k in range(len(params))]
            
            #print(Lparams)
            
            i = 4+3*len(self.L_dics)
            
            while i <  len(Lines):
                line = Lines[i]
                #print(line)
                #print(line[0:7])
                if line[0:5] == "Start":
                    Lmt_temp = []
                    Lmts_temp = []
                    Lmr_temp = []
                    Lmrs_temp = []
                    LCp_temp = []
                    LCn_temp = []
                    linet = Lines[i+2]
                    
                    trans = linet.split('|')
                    
                    Lmt_temp = [float(trans[k].split('(std : ')[0]) for k in range(1,len(trans)-1)]
                    Lmts_temp = [float(trans[k].split('(std : ')[1][1:-2]) for k in range(1,len(trans)-1)]              
                    
                    liner = Lines[i+4]
                    
                    rot = liner.split('|')
                    
                    Lmr_temp = [float(rot[k].split('(std : ')[0]) for k in range(1,len(rot)-1)]
                    Lmrs_temp = [float(rot[k].split('(std : ')[1][1:-2]) for k in range(1,len(rot)-1)] 
                    
                    lineLC = Lines[i+6]
                    
                    LC = lineLC.split('|')
                    
                    LCp_temp = [float(LC[k].split(' ; ')[0]) for k in range(1,len(LC)-1)]
                    LCn_temp = [float(LC[k].split(' ; ')[1][:-1]) for k in range(1,len(LC)-1)] 
                    
                    L_mean_t.append(Lmt_temp)
                    L_mean_t_std.append(Lmts_temp)
                    L_mean_r.append(Lmr_temp)
                    L_mean_r_std.append(Lmrs_temp)
                    LC_pos.append(LCp_temp)
                    LC_neg.append(LCn_temp)
                    i+= 6
                
                elif line[0:7] == "Ranking":
                    
                    liner = line.split("[")[1][:-3]
                    ranks = liner.split(",")
                    
                    Rank = [float(ranks[k]) for k in range(len(ranks))]
                    i+=4

                else:
                    i+=1
                                           
            #print(L_mean_t)  
            if typename == " ----- Values with checks (all same for each test) ----- \n":
                #print("Allsame")
                #print(np.array(L_mean_t))
                M_mean_t = np.array(L_mean_t).T
                M_mean_t_std = np.array(L_mean_t_std).T
                M_mean_r = np.array(L_mean_r).T
                M_mean_r_std = np.array(L_mean_r_std).T
                M_LC_pos = np.array(LC_pos).T
                M_LC_neg = np.array(LC_neg).T              
                
            else:
                M_mean_t = np.array(L_mean_t)
                M_mean_t_std = np.array(L_mean_t_std)
                M_mean_r = np.array(L_mean_r)
                M_mean_r_std = np.array(L_mean_r_std)
                M_LC_pos = np.array(LC_pos)
                M_LC_neg = np.array(LC_neg)
                # print(M_LC_pos)
                # print(Lparams)
                
            return(M_mean_t, M_mean_t_std, M_mean_r, M_mean_r_std, M_LC_pos, M_LC_neg,typename,Lparams,Rank)

    def display_graphics(self,name):
        M_mean_t, M_mean_t_std, M_mean_r, M_mean_r_std, M_LC_pos, M_LC_neg, typename,Lparams,Rank = self.load_from_txt(name)
        
        colors = ['R','G','B','orange','black','pink','purple',
                      "magenta","cyan","crimson","royalblue","gold","firebrick",
                      "slategrey","chocolate","steelblue","navy","olive","deepskyblue",'yellow']
        
        fig,axs = plt.subplots(2, 1)
            #ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            # ori_time = self.L_time_synch[0]
            # Ltime = [self.L_time_synch[l]-ori_time for l in range(length)]
        k = 0
        col = M_mean_t.shape[0]
        #print(M_LC_pos)
        while k < len(Lparams):
            i = 0
            while (k+i+1)< len(Lparams) and Lparams[k+i] == Lparams[k+i+1]:
                i+=1
            Lparams = Lparams[:k]+ Lparams[k+i:] 
            # print(np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1) )
            # print( np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1)[0])
            # # print(np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1).shape )
            # print(M_LC_pos[:,:k])
            # # print(M_LC_pos[:,:k].shape)
            # print(M_LC_pos[:,:k][0])
            if k==0:
                # print(k,k+i)
                meanCpos = np.mean(M_LC_pos[:,k:k+i+1], axis = 1).reshape(col,1) 
                M_LC_pos = np.hstack( (meanCpos,M_LC_pos[:,k+i+1:]) )
                meanCneg = np.mean(M_LC_neg[:,k:k+i+1], axis = 1).reshape(col,1) 
                M_LC_neg = np.hstack( (meanCneg,M_LC_neg[:,k+i+1:]) )
            elif k+i == M_LC_pos.shape[1]:
                pass
            
            else : #and np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1)[0] != []:
                # print(k)
                # print(k+i)
                meanCpos = np.mean(M_LC_pos[:,k:k+i+1], axis = 1).reshape(col,1) 
                M_LC_pos2 = np.hstack( (M_LC_pos[:,:k],meanCpos) )
                M_LC_pos = np.hstack( (M_LC_pos2,M_LC_pos[:,k+i+1:]) )
                
                meanCneg = np.mean(M_LC_neg[:,k:k+i], axis = 1).reshape(col,1) 
                M_LC_neg2 = np.hstack( (M_LC_neg[:,:k],meanCneg) )
                M_LC_neg = np.hstack( (M_LC_neg2,M_LC_neg[:,k+i+1:]) )

            k+=1
        #     print(M_LC_pos.shape)
        
        # print(Lparams)
        # print(M_LC_pos)
        # print(M_LC_pos.shape)


        nb_diff = M_LC_pos.shape[0]

        

        for k in range(nb_diff):
                #Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                # print(k)
                # print(len(Lind))
                # print(len(self.L_all_trans_err[k][:length]))
                
                #print(self.L_time[:length])

                
                #print(colors[k])
            axs[0,].plot(Lparams,M_LC_pos[k,:], label="Instance nbe %i"%k, color = colors[k])
            axs[1,].plot(Lparams,M_LC_neg[k,:], label="Instance nbe %i"%k, color = colors[k])
            # ok
        axs[0,].set_xlabel("Value of parameters")
        axs[0,].set_ylabel("Number of Positive Loop Closure")
        axs[0,].set_title("Number of Positive Loop Closure according to the value of the parameters")
        axs[0,].legend()
        axs[1,].set_xlabel("Value of parameters")
        axs[1,].set_ylabel("Number of Negative Loop Closure")
        axs[1,].set_title("Number of Negative Loop Closure according to the value of the parameters")
        axs[1,].legend()
        plt.show()


    def txt_to_csv(self,name,name_csv):

            
            M_mean_t, M_mean_t_std, M_mean_r, M_mean_r_std, M_LC_pos, M_LC_neg, typename,Lparams,Rank = self.load_from_txt(name)
            
            
            with open(name_csv,'w') as f2:
                writer = csv.writer(f2,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
                
                nb_diff = len(Lparams)
                
                space= [" " for k in range(nb_diff)]
                
                writer.writerow([typename]+[" " for k in range(nb_diff-1)])
                for l in range(2):
                    writer.writerow(space)
                    
                writer.writerow(["Values of parameters"]+[" " for k in range(nb_diff-1)])
                writer.writerow(Lparams)
                for l in range(2):
                    writer.writerow(space)                
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Trans_err_%i"%k]
                writer.writerow(fnames)
                #print(M_mean_t.shape[0])
                for l in range(M_mean_t.shape[0]):
                    writer.writerow(M_mean_t[l,:])
                
                for l in range(5):
                    writer.writerow(space)
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Rot_err_%i"%k] 
                
                writer.writerow(fnames)
                for l in range(M_mean_r.shape[0]):
                    writer.writerow(M_mean_r[l,:])
                
                for l in range(5):
                    writer.writerow(space)
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Trans_err_std_%i"%k] 
                writer.writerow(fnames)
                for l in range(M_mean_t_std.shape[0]):
                    writer.writerow(M_mean_t_std[l,:])
                    
                for l in range(5):
                    writer.writerow(space)
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Rot_err_std_%i"%k] 
                writer.writerow(fnames)
                for l in range(M_mean_r_std.shape[0]):
                    writer.writerow(M_mean_r_std[l,:])
                for l in range(5):
                    writer.writerow(space)
                
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Positive_detection_%i"%k] 
                writer.writerow(fnames)
                for l in range(M_LC_pos.shape[0]):
                    writer.writerow(M_LC_pos[l,:])
                for l in range(5):
                    writer.writerow(space)
                
                fnames = []
                for k in range(1,nb_diff+1):
                    fnames += ["Negative_detection_%i"%k] 
                writer.writerow(fnames)
                for l in range(M_LC_neg.shape[0]):
                    writer.writerow(M_LC_neg[l,:])                
                for l in range(5):
                    writer.writerow(space)

                writer.writerow(["Rank"]+[" " for k in range(nb_diff-1)])
                writer.writerow(Rank)
                
                

    def __init__(self,rate,nb_diff,data_path,navig):

        self.L_info_class= ["Param_type","if_condition"]        

        self.actual_variation = []
        self.parameter = [None,None]
        self.nb_diff = nb_diff
        self.data_path = data_path
        self.navig = navig
        
        self.path = "/home/tnmx0798/Documents/ROSBot_simu/src/rosbot_description/src/rosbot_gazebo/src/orb_node/"
        
        self.odom_dic = [ 
                          ["Odom/FilteringStrategy",Parameter("Odom/FilteringStrategy","int",[0,1,2],True)],
                          ["Odom/KalmanProcessNoise",Parameter("Odom/KalmanProcessNoise","float",[1e4,1e-4,0.001],False)],
                          ["Odom/KalmanMeasurementNoise",Parameter("Odom/KalmanMeasurementNoise","float",[1e4,1e-4,0.01],False)],
                          ["Odom/ParticleSize",Parameter("Odom/ParticleSize","int",[1e6,10,400],False)],
                          ["Odom/ParticleNoiseT",Parameter("Odom/ParticleNoiseT","float",[1e4,1e-4,0.002],False)],
                          ["Odom/ParticleLambdaT", Parameter("Odom/ParticleLambdaT","float",[1e4,1e-4,100],False)],
                          ["Odom/ParticleNoiseR", Parameter("Odom/ParticleNoiseR","float",[1e4,1e-4,0.002],False)],
                          ["Odom/ParticleLambdaR" , Parameter("Odom/ParticleLambdaR","float",[1e4,1e-4,100],False)],
                          ["Param_type","Odom"],
                          ["if_condition"," "]            
            ]
        
        self.imu_dic = [ ["ImuFilter/MadgwickGain",Parameter("ImuFilter/MadgwickGain","float",[1,0,0.001],False)],
                        ["ImuFilter/MadgwickZeta",Parameter("ImuFilter/MadgwickZeta","float",[1,-1,0.0],False)],
                        ["ImuFilter/ComplementaryGainAcc",Parameter("ImuFilter/ComplementaryGainAcc","float",[1,0,0.01],False)],
                        ["ImuFilter/ComplementaryBiasAlpha",Parameter("ImuFilter/ComplementaryBiasAlpha","float",[1,0,0.01],False)],
                        ["Param_type","ImuFilter"],
                        ["if_condition"," "]
            ]

        self.marker_dic = [
                        ["Marker/CornerRefinementMethod",Parameter("Marker/CornerRefinementMethod","int",[0,1,2],True)],
                        ["Marker/MaxDistanceDetection",Parameter("Marker/MaxDistanceDetection","float",[8.0,1.0,2.8],False)],
                        ["Marker/VarianceLinear",Parameter("Marker/VarianceLinear","float",[1e3,1e-6,0.001],False)],
                          ["Marker/VarianceAngular",Parameter("Marker/VarianceAngular","float",[1e3,1e-6,100],False)],
                          ["Param_type","Marker"],
                          ["if_condition",' if="$(arg intern_tag_detection)"']
            ]
        
        self.vis_dic = [ ["Vis/InlierDistance",Parameter("Vis/InlierDistance","float",[1,1e-6,0.1],False)],
                          ["Vis/MinInliers",Parameter("Vis/MinInliers","int",[100,1,20],False)],
                          ["Param_type","Vis"],
                          ["if_condition"," "]
            ]        
        
        # mod : Icp/VoxelSize, Icp/MaxCorrespondanceDistance
        self.icp_dic = [  ["Icp/VoxelSize",Parameter( "Icp/VoxelSize","float",[1,1e-6,0.05],False)],
                          ["Icp/MaxCorrespondenceDistance",Parameter("Icp/MaxCorrespondenceDistance","float",[1,1e-6,0.1],False)],
                          ["Icp/CorrespondenceRatio",Parameter("Icp/CorrespondenceRatio","float",[1,1e-6,0.3],False)],
                         [ "Icp/MaxTranslation",Parameter("Icp/MaxTranslation","float",[1,1e-6,0.235],False)],                   
                         [  "Param_type" , "Icp"], 
                         [ "if_condition"," "]            
            ]   

        self.grid_dic = [
                         [ "GridGlobal/OccupancyThr", Parameter("GridGlobal/OccupancyThr","float",[1,1e-6,0.5],False)],
                         [ "GridGlobal/ProbHit" , Parameter("GridGlobal/ProbHit","float",[1,1e-6,0.7],False,True)],
                         [ "GridGlobal/ProbMiss" , Parameter("GridGlobal/ProbMiss","float",[1,1e-6,0.4],False)],
                         [ "GridGlobal/ProbClampingMin" , Parameter("GridGlobal/ProbClampingMin","float",[1,1e-6,0.1192],False)],
                         [ "GridGlobal/ProbClampingMax", Parameter("GridGlobal/ProbClampingMax","float",[1,1e-6,0.971],False)],                        
                         [  "Param_type" , "GridGlobal"], 
                         [ "if_condition"," "]                    
            ]
        
        self.rgbd_dic = [ ["RGBD/OptimizeMaxError",Parameter("RGBD/OptimizeMaxError","float",[100,1,10],False)],
                        [  "Param_type" , "RGBD"], 
                         ["if_condition"," " ]  
            
            ]
        
        self.rgbd_dic_navig = [ ["RGBD/OptimizeMaxError",Parameter("RGBD/OptimizeMaxError","float",[100,1,10],False)],
                               ["RGBD/MaxOdomCacheSize",Parameter("RGBD/MaxOdomCacheSize","int",[100,1,5],False)],
                        [  "Param_type" , "RGBD"], 
                         ["if_condition"," " ]  
            
            ]        
        
        if self.navig : 
            self.L_nopt = ["GridGlobal","ImuFilter"]
            self.L_dics =  [self.odom_dic,self.vis_dic,self.icp_dic, self.rgbd_dic_navig , self.grid_dic, self.imu_dic]
          
        else:
            self.L_nopt = []
            self.L_dics =  [self.odom_dic,self.vis_dic,self.icp_dic, self.rgbd_dic , self.grid_dic, self.imu_dic]
                
        #[self.marker_dic,self.vis_dic,self.icp_dic, self.rgbd_dic , self.imu_dic]
        
        #[self.marker_dic,self.vis_dic,self.icp_dic, self.rgbd_dic , self.grid_dic, self.imu_dic,self.odom_dic]
        
        #[self.rgbd_dic,self.imu_dic,self.marker_dic,self.vis_dic,self.icp_dic,self.odom_dic]
        #
        
        self.pwd_begin_launchfile = "/home/tnmx0798/Documents/ROSBot_simu/src/rosbot_description/src/rosbot_gazebo/src/orb_node/begin_launcher.txt"
        self.pwd_begin_namespace = "/home/tnmx0798/Documents/ROSBot_simu/src/rosbot_description/src/rosbot_gazebo/src/orb_node/begin_namespace.txt"
        
        self.begin_namespace = '\n \n <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ New test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ --> \n \n  <group ns="rtabmap'
        
        self.end_namespace = "\n    </node> \n  </group> "
        self.end_launchfile = "\n </launch>"
        
        
        self.trans_err = 9999
        self.rot_err = 9999
        self.trans_err_std = 9999
        self.rot_err_std = 9999
        self.pos_LC = 0
        self.neg_LC = 9999
        
    


        return(None)


if __name__ == "__main__":
    nb_diff = 10
    cm = launchDictionnary(1.0,nb_diff,"")
    maxi = 10
    mini = 1

    cm.actual_variation = [((maxi-mini)*k +  (nb_diff-1)*mini)/(nb_diff-1) for k in range(nb_diff)]
    cm.parameter = ["RGBD","RGBD/OptimizeMaxError"]
    
    path = "/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Marker/MaxDistanceDetection" + "/"
    
    path_txt = path+"check_Marker_MaxDistanceDetection_param_save_2020-07-17-9-37.txt"
    
    path_csv = path_txt[:-3]+"csv"
    

    cm.txt_to_csv(path_txt,path_csv)
    #cm.display_graphics(path_txt)