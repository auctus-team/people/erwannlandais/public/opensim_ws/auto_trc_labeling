#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt


from sensor_msgs.msg import Image as ImageMSG
from sensor_msgs.msg import CameraInfo as CamInfoMSG
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2
import image_geometry
import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped

from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry

import time
import geometry_msgs.msg

import tf

import tf.transformations

from tf.msg import tfMessage

from dynamic_reconfigure.msg import Config as ConfigMsg
from dynamic_reconfigure.msg import ConfigDescription as ConfigDescrMsg


from gazebo_msgs.msg import LinkStates
from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp, comp_rts, comp_odom

from rosbot_gazebo.srv import boolean, save_name

import matplotlib.pyplot as plt

import csv

class transformStampedEuler():
    
    def __init__(self):
        self.time = 0
        self.pos = None
        self.rot = None
    
    def __repr__(self):
        return(self.__str__() )
    
    def __str__(self):
        
        return(" Stamp : %f \n pos : [%f,%f,%f] \n rot : [%f,%f,%f] \n"%(self.time,self.pos[0],self.pos[1],self.pos[2],self.rot[0],self.rot[1],self.rot[2]))


class evaluateNodes():

    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec < prev_ec:
                i -= 1
                # if (abs(i) < len(L_time)):
                #     i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
        
    def check_synch_bl(self,buf,t):
        bl_synch = []
        i = -1
        if (len(buf) != self.len_buf_bl):
            L_time = [ buf[k].header.stamp.to_sec() for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_bl+self.tol*(self.rate_bl/self.rate_rts):
                self.err_bl = False
                bl_synch.append(buf[i])
            else:
                print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_bl or self.err_bl:
                    # No matching possible, cancel for this topic
                    self.map_time_synch = -1
                    if (self.err_bl):
                        print("Stop bl blocking!")
                    self.err_bl = False
                else:
                    self.err_bl = True
                self.len_buf_bl = len(buf)

        return(bl_synch,i)

    def check_synch_bl_L(self,buf,Lt):
        bl_synch = []
        imin = 999
        #print("Lt : ", Lt)
        if (len(buf) != self.len_buf_bl):
            for j in range(len(Lt)):
                t = Lt[j]
                i = -1            
                L_time = [ buf[k].header.stamp.to_sec() for k in range(len(buf))]
                (i,ec) = self.find_closest_time_match(t,L_time)
                if ec < self.rate_bl/2:
                    bl_synch.append(buf[i])
                    if i < imin:
                        imin = i
                else:
                    print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                    
                    self.len_buf_bl = len(buf)

                    #print(buf[-4:])
                    #print(self.buf_all_tf_map)
                    bl_synch = []

                    break
        # else:
        #     print("bl : No new at : ",rospy.get_time())
        return(bl_synch,imin)

    def check_synch_odom(self,buf,t):
        odom_synch = []
        i = -1
        if (len(buf) != self.len_buf_true):
            L_time = [ buf[k].time for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_rts):
                odom_synch.append(buf[i])
                self.err_odom = False
            else:
                print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_odom or self.err_odom:
                    # No matching is then possible : cancel the synchronization for this topic
                    self.map_time_synch = -1
                    if self.err_odom:
                        print("Stop odom blocking!")
                    self.err_odom = False
                else:
                    self.err_odom = True
                self.len_buf_true = len(buf)
        return(odom_synch,i)        

    def check_synch_odom_L(self,buf,Lt):
        odom_synch = []
        imin = 999
        if (len(buf) != self.len_buf_true):
            for j in range(len(Lt)):
                t = Lt[j]
                i = -1
    
                L_time = [ buf[k].time for k in range(len(buf))]
                (i,ec) = self.find_closest_time_match(t,L_time)
                if ec < self.rate_odom/2 :# self.rate_odom+self.rate_bl*3/2:
                    odom_synch.append(buf[i])
                    if i < imin : 
                        imin = i
                else:
                    print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                    #print(buf)
                    self.len_buf_true = len(buf)
                    odom_synch = []

                    break
        #else:
            #print("odom : No new at : ",rospy.get_time())
            # if j == len(Lt)-1:
            #     Ltmos = [odom_synch[k].time for k in range(len(odom_synch))]
            #     tMOS = min(Ltmos)
            #     MOS =odom_synch[ Ltmos.index(tMOS) ]
            #     odom_synch2 = [ MOS for k in range(len(odom_synch))]
                
        return(odom_synch,imin)               

    def check_synch_map(self):
        """
        Sycnhronisation effectuée par rapport à la dernière tf map[nb_diff/2] --> odom.
        Recherche des tfs mapi --> odom les plus proches temporellement parmi ceux disponibles.
        Principe : celui au milieu devrait avoir écart moyen le plus faible avec les autres, ce
        qui devrait faciliter synchronisation.
        

        Returns
        -------
        None.

        """
        
        # if (self.int_synch_map == -1):
        #     # def de indice int_synch_map, à partir duquel synchro sera effectué
        #     int_synch = 0
        #     Lalltime = [self.buf_all_tf_map[k][0].header.stamp.to_sec()  for k in range(self.nb_diff) ]
        #     #print(Lalltime)
        #     Ltime = sorted(Lalltime)
        #     #print(self.ref_i_map)
        #     t_mid = Ltime[self.ref_i_map]
        #     #print(Ltime)
        #     #print(t_mid)
        #     self.int_synch_map = Lalltime.index(t_mid)
        #     print("********* Maps will be synchronized with respect to : %i . **********"%self.int_synch_map)
        #time_synch_map = self.buf_all_tf_map[self.int_synch_map][self.icheck].header.stamp.to_sec()
        if (self.map_time_synch == -1):
            Lalltime = [self.buf_all_tf_map[k][-1].header.stamp.to_sec()  for k in range(self.nb_diff) ]
            time_synch_map = min(Lalltime)
            self.icheck = Lalltime.index(time_synch_map)
        else:
            time_synch_map = self.map_time_synch
        # icheck = self.buf_all_tf_map[self.int_synch_map].index(self.buf_all_tf_map[self.int_synch_map][self.icheck])
        # if icheck != self.icheck:
        #     print("icheck : ",icheck)
        #     #print(self.buf_all_tf_map[self.int_synch_map][self.icheck], self.buf_all_tf_map[self.int_synch_map][icheck])
        #     self.icheck = icheck
        #print("timesmap : ", time_synch_map  
        if (len(self.L_time_synch) > 0 ) :
            if (self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec() - self.L_time_synch[-1])  < self.rate_rts: # -self.tol : 
                l = 2*self.nb_diff
                if (self.dispm1 != self.pcheck):
                    print("Err : not enough time difference between last accepted miminum and last occurence of this minimum : %f ; for map : %i (last accepted min time : %f )"%((self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec() - self.L_time_synch[-1]), self.pcheck, self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec()))    
                    self.dispm1 = self.pcheck
                self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                
            else:
                self.dispm1 = self.nb_diff
                if (time_synch_map -self.L_all_time[self.icheck][-1]) < self.rate_rts : #-self.tol : 
                    l = 2*self.nb_diff
                    if self.dispm2 != self.icheck:
                        print("Err : not enough time difference between actual minimum and last occurence of this minimum : %f ; for map : %i (actual min time : %f )"%((time_synch_map -self.L_all_time[self.icheck][-1]), self.icheck, time_synch_map))    
                        self.dispm2 = self.icheck
                    self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                else:
                    self.dispm2 = self.nb_diff
                    if (time_synch_map -self.L_time_synch[-1]) < self.rate_rts : 
                        l = 2*self.nb_diff
                        if self.dispm3 != self.icheck:
                            self.dispm3= self.icheck
                            print("Err : not enough time difference between actual minimum and last synchronized time : %f ; for map : %i (actual min time : %f )"%((time_synch_map -self.L_time_synch[-1]), self.icheck, time_synch_map))    
                        self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                    else:
                        self.dispm3 = self.nb_diff
                        if self.dispok!= self.icheck :
                            self.dispok = self.icheck
                            print("Synchronisation completed with map : %i (actual min time : %f )"%(self.icheck, time_synch_map))    
                        l = 0
        else:
            l=0
        L_sync = []
        L_map_synch = []
        while l < self.nb_diff:
            L_time = [ self.buf_all_tf_map[l][k].header.stamp.to_sec() for k in range(len(self.buf_all_tf_map[l])) ]
            (i,ec) = self.find_closest_time_match(time_synch_map,L_time)
            if (ec < self.rate_rts):
                L_sync.append(i)
                l+=1
            else:
                print("Err map synch : min time difference : ",ec, " for time : ", time_synch_map, " with map : ", l)
                self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                self.dispok = self.nb_diff
                break                
            
            
            # if len(self.L_all_time[l]) > 0:
            #     #if ec < self.desynch_lim and (L_time[i] - self.L_all_time[l][-1]) > (self.rate_rts-self.nb_diff*0.01): #(self.rate_rts - self.desynch_lim)
            #     if ec < self.desynch_lim and (L_time[i] - self.L_all_time[l][-1]) > (self.rate_rts-self.rate_bl*3/2): #(self.rate_rts - self.desynch_lim)
            #         L_sync.append(i)
            #         l+=1
            #     else:
            #         if ec > self.desynch_lim:
            #             print("Err map synch : min time difference : ",ec, " for time : ", time_synch_map, " with map : ", l)
            #             self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
            #             break
            #         elif (L_time[i] - self.L_all_time[l][-1]) <  (self.rate_rts-self.rate_bl*3/2) :
            #             if (L_time[i] -self.L_all_time[l][-1]) > 0:
            #                 print("Err : not enough time difference with last one : %f ; for map : %i"%((L_time[i] - self.L_all_time[l][-1]), l))
            #             self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
            #             break
            # else:
            #     if ec < self.desynch_lim :
            #         L_sync.append(i)
            #         l+=1
            #     else:
            #         print("Err map synch : min time difference : ",ec, " for time : ", time_synch_map, " with map : ", l)
            #         self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
            #         break                

        LMT = []
        if l== self.nb_diff:
           
            #print(L_sync)
            #print([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map)) ])
            L_map_synch = [ self.buf_all_tf_map[j][ L_sync[j] ] for j in range(len(L_sync))  ]
            self.map_time_synch = time_synch_map
            
            # try : 
            #     L_map_synch = [ self.buf_all_tf_map[j][ L_sync[j] ] for j in range(len(L_sync))  ]
            #     LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
            #     stime = np.mean(LMT)
            # # if (self.map_time_synch != -1.0 and (self.map_time_synch - stime) > 0):
            # #     self.rate_rts = self.map_time_synch - stime
            # #     print("New rate_rts : ", self.rate_rts)
            #     if stime - self.prev_map_synch_success >= self.rate_rts-3*self.rate_bl/2 :#desynch_lim #rate_rts
            #         #print("********* Maps will be synchronized with respect to : %f . **********"%stime)
            #         self.map_time_synch = stime

            #     else:
            #         print("Not enough time diff : stime : %f ; prev_synch : %f"%(stime,self.prev_map_synch_success))
            #         if self.buf_access ==True:
            #             self.icheck = -1
            #         L_map_synch = []
            # except Exception:
            #     print("Exeption in check_map!")
            #     #self.icheck = -1
            #     L_map_synch = []
        
        return(L_map_synch,L_sync,LMT)
       
    
    def callback_tf(self,msg):
        #print(msg.transforms[0])
        #print(self.buf_access_map)
        if (self.buf_access_map == True or self.temp_buf_access ==True):
            for k in range(len(msg.transforms)):
                name = msg.transforms[k].header.frame_id
                if name == "odom":
                    if (self.buf_access):
                        self.buf_tf_bl.append(msg.transforms[k])
                        # if (len(self.buf_tf_bl) ) > 1:
                        #     self.rate_bl = self.buf_tf_bl[-1].header.stamp.to_sec()- self.buf_tf_bl[-2].header.stamp.to_sec()
                    elif (self.temp_buf_access):
                        self.temp_buf_tf_bl.append(msg.transforms[k])
                        
                elif name in self.L_map_name:
                    i = int(name[-1])-1            
                    
                    # not temp buffer
                    if (self.buf_access and len(self.buf_all_tf_map[i] ) == 0 ):
                        self.buf_all_tf_map[i].append(msg.transforms[k])
                        
                    elif (self.temp_buf_access and len(self.temp_buf_all_tf_map[i] ) == 0 ):
                        self.temp_buf_all_tf_map[i].append(msg.transforms[k])    
                        
                    
                    else:
                        true_pos= msg.transforms[k].transform.translation
                        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
                        b_pos = true_pos
                        if (self.buf_access):
                            try:
                                b_pos = self.buf_all_tf_map[i][-1].transform.translation
                            except Exception : 
                                pass
                        elif (self.temp_buf_access):
                            try : 
                                b_pos = self.temp_buf_all_tf_map[i][-1].transform.translation
                            except Exception : 
                                pass
                        old_pose = np.array([b_pos.x, b_pos.y, b_pos.z])
                        if (np.all(new_pose==old_pose) == False):
                            #print("pose : ", new_pose,old_pose)
                            if (self.buf_access):
                                self.buf_all_tf_map[i].append(msg.transforms[k])
                            #     l = 0
                            #     new_time_tranf = msg.transforms[k].header.stamp.to_sec()
                            # #print("ntt : ", new_time_tranf)
                            #     self.nmap = 1
                                # while l < self.nb_diff:
                                #     if l != i:
                                #         old_time_tranf = self.buf_all_tf_map[l][-1].header.stamp.to_sec()
                                #     #print("ott : ", old_time_tranf)
                                #         if abs(new_time_tranf-old_time_tranf) > self.desynch_lim:
                                #             break
                                #         else:
                                #             self.nmap += 1
                                #     l+=1
                                # if (l != self.nb_diff):
                                #     self.nmap = 0     

                                # else:
                                #     print("-------------------------")
                                #     print(self.buf_all_tf_map)
                                
                            elif (self.temp_buf_access):
                                self.temp_buf_all_tf_map[i].append(msg.transforms[k])
                                
            # and (self.nmap >= self.nb_diff or self.icheck != -1)
            lms = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
            lentot = np.min(lms)
            # if lentot >0 and (self.buf_all_tf_map[0][-1].header.stamp.to_sec() > 26123):
            #     print(self.buf_all_tf_map)
            #print(lentot)
            #print(self.buf_access ==True and lentot > 0 and len(self.buf_tf_bl) > 0 and len(self.buf_tf_true) > 0)
            if (self.buf_access ==True and lentot > 0 and len(self.buf_tf_bl) > 0 and len(self.buf_tf_true) > 0):
                self.buf_access = False
                self.buf_access_map = False
                self.temp_buf_access = True
                L_map_synch = []
                #print("inside")
                if (np.all(lms == self.prev_map_synch_size) == False):
                    #print("Check map ...")
                    L_map_synch,L_synch,LMT = self.check_synch_map()
                if (len(L_map_synch) == self.nb_diff):
                    #print("Map ok!")
                    self.prev_map_synch_success = self.map_time_synch
                    self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
                    #LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
                    
                    # synchro avec base_link
                    # print("----------NEW TEST-----------")
                    # print(L_map_synch)
                    # print("-------------------------")
                    # print(self.buf_tf_bl[-5:])
                    bl_synch, odom_synch = [],[]
                    bl_check, odom_check = [],[]
                    
                    odom_synch,odom_i = self.check_synch_odom(self.buf_tf_true,self.map_time_synch)
                    
                    #odom_synch,odom_i = self.check_synch_odom_L(self.buf_tf_true,LMT)
                    #odom_s,odom_i = self.check_synch_odom(self.buf_tf_true,self.map_time_synch)
                   
                    if (len(odom_synch) ) > 0:
                        
                    # if (len(odom_synch) ) == self.nb_diff:
                        #odom_synch = [odom_s[-1] for k in range(self.nb_diff)]
                        #print("Odom ok!")
                        self.len_buf_true = -1
                        odom_check = odom_synch
                    # synchro avec odom
                        bl_synch,bl_i = self.check_synch_bl(self.buf_tf_bl,self.map_time_synch)
                        #bl_synch,bl_i = self.check_synch_bl_L(self.buf_tf_bl,LMT)
                        if (len(bl_synch) ) > 0:
                        #print("blsy : ", bl_synch)
                        #print(bl_synch)
                        # if (len(bl_synch) ) == self.nb_diff:
                            #print("BL ok!")
                            self.len_buf_bl = -1
                            bl_check = bl_synch
                            #print(len(bl_check),len(odom_check))
                        else : 
                            self.prev_map_synch_success = -1
                    else:
                        self.prev_map_synch_success = -1
                        
                    if (min(len(bl_synch),len(odom_synch)) > 0):
                    #print(len(bl_check) , len(odom_check) )
                    #if (min(len(bl_check),len(odom_check)) == self.nb_diff):
                        # publish synchronized topics
                        #self.pub_rts_err_L(L_map_synch,bl_check,odom_check)
                        self.pub_rts_err(L_map_synch,bl_synch,odom_synch)
                        if (len(self.L_time_synch) > 1):
                            tol = (self.L_time_synch[-1] - self.L_time_synch[-2])%self.rate_rts
                            if (tol > self.rate_rts/2): 
                                tol = abs(tol - self.rate_rts)
                            self.tol = (self.tol*len(self.L_time_synch)+tol)/(len(self.L_time_synch)+1)
                            print(self.tol)
                        #self.tol = 0.0
                        print("------ Publication at synchronized time : ",self.map_time_synch, " (sychronized with %i)"%self.icheck ," completed! ----------")
                        ic = self.icheck
                        self.pcheck = ic
                        self.icheck = -1
                        self.send = True
                        self.dispok = self.nb_diff
                        #clean up buffers
                        for k in range(self.nb_diff):
                            #print("Before : ", self.buf_all_tf_map[k])
                            self.buf_all_tf_map[k] = self.buf_all_tf_map[k][L_synch[k]:]
                            #print("After : ", self.buf_all_tf_map[k])
                        self.buf_tf_bl = self.buf_tf_bl[bl_i:]
                        self.buf_tf_true = self.buf_tf_true[odom_i:]
                        self.nmap = 0  
                        self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
                        self.map_time_synch = -1
                self.temp_buf_access = False
                #add up temporary buffers
                for k in range(self.nb_diff):
                    if (len(self.buf_all_tf_map[k]) > 0):
                        test = False
                        while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                            try :
                                #print("Treating suppression")
                                p1 = self.temp_buf_all_tf_map[k][0].transform.translation    
                                nw_pose = np.array([p1.x, p1.y, p1.z])
                                p2 = self.buf_all_tf_map[k][-1].transform.translation
                                od_pose = np.array([p2.x, p2.y, p2.z])
                                if (np.all(nw_pose==od_pose) == True):
                                    #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                                    #print("Because of : ",  self.buf_all_tf_map[k][-1])
                                    del(self.temp_buf_all_tf_map[k][0])
                                else:
                                    test = True
                        # while len(self.temp_buf_all_tf_map[k]) > 0 and  abs( self.temp_buf_all_tf_map[k][0].header.stamp.to_sec() - self.buf_all_tf_map[k][-1].header.stamp.to_sec()) < self.rate_rts :
                        #     try :
                        #         #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #         #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        #         del(self.temp_buf_all_tf_map[k][0])
                    # if (len(self.temp_buf_all_tf_map[k]) > 0):
                    #     while len(self.buf_all_tf_map[k]) > 0 and  abs( self.temp_buf_all_tf_map[k][0].header.stamp.to_sec() - self.buf_all_tf_map[k][-1].header.stamp.to_sec()) < self.rate_rts :
                    #         try :
                    #             #print("Suppress : ", self.buf_all_tf_map[k][-1])
                    #             #print("Because of : ",  self.temp_buf_all_tf_map[k][0])
                    #             del(self.buf_all_tf_map[k][-1])

                            except Exception:
                                test = True
                                print("Exeption in clean_map!")
                                pass
                            #     test = True
                            #     pass
                    self.buf_all_tf_map[k] += self.temp_buf_all_tf_map[k]
                    self.temp_buf_all_tf_map[k] = []
                    if (self.send):
                        #print("Bufffer after send : ", self.buf_all_tf_map)
                        self.send = False
                self.buf_access_map = True
                if len(self.buf_tf_bl) > 0:
                    test= False
                    while len(self.temp_buf_tf_bl) > 0 and test == False:
                        p1 = self.temp_buf_tf_bl[0].transform.translation     
                        n_pose = np.array([p1.x, p1.y, p1.z])
                        p2 = self.buf_tf_bl[-1].transform.translation
                        o_pose = np.array([p2.x, p2.y, p2.z])
                        if (np.all(n_pose==o_pose) == True):
                            #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                            #print("Because of : ",  self.buf_all_tf_map[k][-1])
                            del(self.temp_buf_tf_bl[0])
                        else:
                            test= True
                    
                self.buf_tf_bl += self.temp_buf_tf_bl
                self.temp_buf_tf_bl  = []
                if len(self.buf_tf_true) > 0:
                    test= False
                    while len(self.temp_buf_tf_true) > 0 and test == False:
                        p1 = self.temp_buf_tf_true[0].pos
                        n_pose = p1
                        p2 = self.buf_tf_true[-1].pos
                        o_pose = p2
                        if (np.all(n_pose==o_pose) == True):
                            #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                            #print("Because of : ",  self.buf_all_tf_map[k][-1])
                            del(self.temp_buf_tf_true[0])
                        else:
                            test= True
                    
                    
                    # while len(self.temp_buf_tf_true) > 0 and abs( self.temp_buf_tf_true[0].time - self.buf_tf_true[-1].time) < self.rate_odom:
                    #     del(self.temp_buf_tf_true[0])
                self.buf_tf_true += self.temp_buf_tf_true
                self.temp_buf_tf_true = []
                
                self.buf_access = True

        return(None)

    def pub_rts_err(self,L_map_synch,bl_synch,odom_synch):
        true_pos = odom_synch[-1].pos
        true_rot = odom_synch[-1].rot
        pose_bl = bl_synch[-1]
        self.L_time_synch.append(self.map_time_synch)
        for k in range(self.nb_diff):
            tf_rts = geometry_msgs.msg.TransformStamped()
            tf_tranf = L_map_synch[k]
            self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
            tf_rts.header.stamp = tf_tranf.header.stamp
            tf_rts.transform.translation.x = tf_tranf.transform.translation.x + pose_bl.transform.translation.x-0.03
            tf_rts.transform.translation.y = tf_tranf.transform.translation.y + pose_bl.transform.translation.y
            tf_rts.transform.translation.z = tf_tranf.transform.translation.z + pose_bl.transform.translation.z+0.18
                    
            x0,y0,z0,w0 = tf_tranf.transform.rotation.x,tf_tranf.transform.rotation.y,tf_tranf.transform.rotation.z,tf_tranf.transform.rotation.w
            x1,y1,z1,w1 = pose_bl.transform.rotation.x,pose_bl.transform.rotation.y,pose_bl.transform.rotation.z,pose_bl.transform.rotation.w
                    
            M_quat = np.array((
                x1*w0 + y1*z0 - z1*y0 + w1*x0,
                -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                x1*y0 - y1*x0 + z1*w0 + w1*z0,
                -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)
                    
            tf_rts.transform.rotation.x = M_quat[0]
            tf_rts.transform.rotation.y = M_quat[1]
            tf_rts.transform.rotation.z = M_quat[2]
            tf_rts.transform.rotation.w = M_quat[3]
            self.evaluateProximityToTruth_rts2(true_pos,true_rot,tf_rts,k)

    def pub_rts_err_L(self,L_map_synch,Lbl_synch,Lodom_synch):

        for k in range(self.nb_diff):
            true_pos = Lodom_synch[k].pos
            true_rot = Lodom_synch[k].rot
            pose_bl = Lbl_synch[k]
            tf_rts = geometry_msgs.msg.TransformStamped()
            tf_tranf = L_map_synch[k]
            self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
            tf_rts.header.stamp = tf_tranf.header.stamp
            tf_rts.transform.translation.x = tf_tranf.transform.translation.x + pose_bl.transform.translation.x-0.03
            tf_rts.transform.translation.y = tf_tranf.transform.translation.y + pose_bl.transform.translation.y
            tf_rts.transform.translation.z = tf_tranf.transform.translation.z + pose_bl.transform.translation.z+0.18
                    
            x0,y0,z0,w0 = tf_tranf.transform.rotation.x,tf_tranf.transform.rotation.y,tf_tranf.transform.rotation.z,tf_tranf.transform.rotation.w
            x1,y1,z1,w1 = pose_bl.transform.rotation.x,pose_bl.transform.rotation.y,pose_bl.transform.rotation.z,pose_bl.transform.rotation.w
                    
            M_quat = np.array((
                x1*w0 + y1*z0 - z1*y0 + w1*x0,
                -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                x1*y0 - y1*x0 + z1*w0 + w1*z0,
                -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)
                    
            tf_rts.transform.rotation.x = M_quat[0]
            tf_rts.transform.rotation.y = M_quat[1]
            tf_rts.transform.rotation.z = M_quat[2]
            tf_rts.transform.rotation.w = M_quat[3]
            self.evaluateProximityToTruth_rts2(true_pos,true_rot,tf_rts,k)


    def check_jump(self,current_lin_speed,current_ang_speed,
                   prev_lin_speed,prev_ang_speed,
                   current_lin_pos,current_ang_pos,
                   prev_lin_pos,prev_ang_pos,
                   current_time,prev_time,current_detect_jump):
        """
        
        Principe : 
            - prendre vitesse (current_lin_speed, current_ang_speed) et position odométrique actuelle (current_lin_pos,current_ang_pos)
            - obtenir la vitesse (angulaire et linéaire) estimée, en comparant la position précédente
            à la position actuelle. (inutile maintenant)
            Cela permet d'avoir une estimation de la vitesse réelle estimée du robot.
            - obtenir la position (angulaire et linéaire) estimée, à partir de la vitesse précédente
            et la position réelle précédente. 
            - Evaluer la différence entre les positions estimées à partir des mesures précédentes et les positions actuelles.
            Si trop importante, veut dire qu'il y a une erreur : position actuelle ne colle
            pas par rapport à estimation de la vitesse et position précédente. 
        
        """
        detect_jump = False
        if current_detect_jump == True:
            return(current_detect_jump)
        
        if (type(prev_time) != type(None)):
            delay = current_time - prev_time
            estimated_speed = (current_lin_pos - prev_lin_pos)/delay
            estimated_ang = (current_ang_pos-prev_ang_pos)/delay
            
            estimated_pose = current_lin_speed*delay + prev_lin_pos
            # suppress z component if inferior to threshold
            if estimated_pose[2] < self.th_tlp_pos:
                estimated_pose[2] = 0
            estimated_rot = current_ang_speed*delay + prev_ang_pos
            
            rot_diff  = np.linalg.norm(estimated_rot-current_ang_pos) 
            if (rot_diff > np.pi):
                if (rot_diff//(2*np.pi)==0 ):
                    rot_diff =abs( rot_diff - 2*np.pi)
                else:
                    rot_diff = rot_diff%(2*np.pi)

            if (np.linalg.norm(estimated_pose - current_lin_pos) > self.th_tlp_pos  
                or rot_diff > self.th_tlp_rot):
                detect_jump = True
                # print("speed : ", np.linalg.norm(new_speed - estimated_speed ))
                # print("angular : ", np.linalg.norm(new_ang-estimated_ang))
                #print("speed : ", estimated_speed )
                #print("angular : ", estimated_ang)
                print("diff_pose : ", np.linalg.norm( current_lin_pos - estimated_pose ))
                #print("pose : ",  estimated_pose )
                #print("rotation : ", estimated_rot )      
                print("diff_rot : ", rot_diff )
        
        return(detect_jump)

    def callback_odom_jump(self,msg):
        # ancienne méthode pour estimer les téléportations à partir de l'odométrie. N'est plus utilisée

        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        new_speed = np.array([msg.twist.twist.linear.x, msg.twist.twist.linear.y, msg.twist.twist.linear.z])
        new_ang = np.array([msg.twist.twist.angular.x,msg.twist.twist.angular.y,msg.twist.twist.angular.z])
        timens = msg.header.stamp.to_sec()

        self.odom_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_odom_speed,self.prev_odom_ang,
                                           new_pose,new_rot,
                                           self.prev_odom_pos,self.prev_odom_rot,
                                           timens,self.prev_odom_time,self.odom_jump)

        self.prev_odom_pos = new_pose
        self.prev_odom_rot = new_rot
        self.prev_odom_speed = new_speed
        self.prev_odom_ang = new_ang
        self.prev_odom_time = timens
    
    def callback_initialpose_jump(self,msg):
        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        if (type(self.prev_initialpose_pos) != type(None) and (np.linalg.norm(self.prev_initialpose_pos-new_pose ) > 0 or self.linalg.norm(self.prev_initialpose_rot-new_rot) >0) ):
            if (self.pose_jump == False):
                self.pose_jump = True
        elif type(self.prev_initialpose_pos) == type(None):
            self.pose_jump = True
        self.prev_initialpose_pos = new_pose
        self.prev_initialpose_rot = new_rot

    
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation

        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        true_speed = msg.twist[self.num_rosbot].linear
        true_ang = msg.twist[self.num_rosbot].angular
        new_speed = np.array([true_speed.x, true_speed.y, true_speed.z])
        new_ang = np.array([true_ang.x,true_ang.y,true_ang.z])
        timens = rospy.get_time()
        
        self.gazebo_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_gaz_speed,self.prev_gaz_ang,
                                           new_pose,new_rot,
                                           self.prev_gaz_pos,self.prev_gaz_rot,
                                           timens,self.prev_gaz_time,self.gazebo_jump)
        
        self.prev_gaz_pos = new_pose
        self.prev_gaz_rot = new_rot
        self.prev_gaz_speed = new_speed
        self.prev_gaz_ang = new_ang
        self.prev_gaz_time = timens
        
        
        #print(rospy.get_time() - self.last_time_pose)
        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            if (self.type_slam=="orb"):
                self.evaluateProximityToTruth_orb(true_pos,true_rot)
            elif(self.type_slam=="rts"):
                self.evaluateProximityToTruth_rts(true_pos,true_rot)
            elif(self.type_slam=="None"):
                self.evaluateProximityToTruth_odom(true_pos,true_rot)
            self.last_time_pose = rospy.get_time()
        
        
        return(None)
    
    def callback_get_last_truth(self,msg):

        true_pos = msg.trans_tr
        true_rot = msg.rot_tr      

        if (self.type_slam=="orb"):
            self.evaluateProximityToTruth_orb(true_pos,true_rot)
        elif(self.type_slam=="rts"):
            actual_time = rospy.Time(0)
            time = rospy.get_time()
            for k in range(self.nb_diff):
                self.evaluateProximityToTruth_rts(true_pos,true_rot,k,actual_time) #actual_time
            # check if a new value has been save for every test
            size = len(self.L_all_trans_err[0])
            check_sizes = True
            for i in range(1, self.nb_diff):
                if len(self.L_all_trans_err[i]) != size:
                    print("Not the same sizes!")
                    check_sizes = False
            # if every list have same size --> compare by saving time
            if check_sizes:
                self.L_time.append(time)
            else:
               length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
               for i in range(self.nb_diff): 
                   self.L_all_rot_err[i] = self.L_all_rot_err[i][:length]
                   self.L_all_trans_err[i] = self.L_all_trans_err[i][:length]
            
        elif(self.type_slam=="None"):
            self.evaluateProximityToTruth_odom(true_pos,true_rot)

        
        return(None)    

# tester plusieurs threads 
    def callback_get_last_truth2(self,msg):
        time = rospy.get_time()
        actual_time = rospy.Time.from_sec(time)
        true_pos = msg.trans_tr
        true_rot = msg.rot_tr      
        
        ntry = self.ntry
        
        

        if (self.type_slam=="orb"):
            self.evaluateProximityToTruth_orb(true_pos,true_rot)
        elif(self.type_slam=="rts"):
            print("---------------------")
             #rospy.Time(time)
            #actual_time = rospy.Time(0)
            Lpose =  []
            time2 = rospy.Time.from_sec(time) # rospy.Time(0)
            firstfind = True
            k = 0
            ech = 0.5
            while k < (self.nb_diff):
                nr = 0

                transformStamped_rts = geometry_msgs.msg.TransformStamped()
                # get transform between origin (by rts) and pose of camera for rts
                while not rospy.is_shutdown() and nr < ntry:
                    try:
                        if (nr > 0 and time2.to_sec() > 0):
                            time2 = rospy.Time.from_sec(time2.to_sec() - ech)
                        print(k, " ; ",actual_time.to_sec(), " ; " ,time2.to_sec() ) #, " ; ", time2)
                        #transformStamped_rts = self.buffer.lookup_transform(self.L_map_name[k], 'camera_link2', actual_time, rospy.Duration(2.0) ) # 2.0/(self.ntry*self.nb_diff)
                        
                        #Ok ms bugs
                        #transformStamped_rts = self.buffer.lookup_transform_full(self.L_map_name[k], actual_time, 'camera_link2', time2, self.L_map_name[k], rospy.Duration(0.1) ) # 2.0/(self.ntry*self.nb_diff)
                        
                        transformStamped_rts = self.buffer.lookup_transform_full(self.L_map_name[k], actual_time, 'camera_link2', time2, self.L_map_name[k], rospy.Duration(0.1) ) # 2.0/(self.ntry*self.nb_diff)

                        break
                    except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                            #rospy.loginfo("Still searching for rts")
                        nr+=1
                        pass
    
                if (nr==ntry and firstfind == False):
                    print("First Time not found. Stop.")
                    break
    
                if (nr==ntry):
                    print("TF rts not found for %i. Stop."%k)
                    break
                       
                else:
                    if (firstfind ==True):
                        k+=1                       
                        Lpose.append(transformStamped_rts)
                    
                    if (firstfind ==False):
                        firstfind = True
                    ech = ech/2
                    time2 = rospy.Time.from_sec(time2.to_sec() +ech*(self.ntry/2) )
                    
                    
            check_sizes = True
            if (len(Lpose)!=self.nb_diff):
                    check_sizes = False

            if check_sizes:
                self.L_time.append(time)
                for k in range(self.nb_diff):
                     self.evaluateProximityToTruth_rts2(true_pos,true_rot,Lpose[k],k)
            
        elif(self.type_slam=="None"):
            self.evaluateProximityToTruth_odom(true_pos,true_rot)

        
        return(None)    

    def callback_get_last_truth3(self,msg):
        
        time = rospy.get_time()
        true_pos = msg.trans_tr
        true_rot = msg.rot_tr    
        if (self.buf_access):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            tf_true.time = time
            self.buf_tf_true.append(tf_true)
            # if (len(self.buf_tf_true) > 1):
            #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
    
        elif (self.temp_buf_access):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            tf_true.time = time
            self.temp_buf_tf_true.append(tf_true)

        
        if (self.type_slam=="orb"):
            self.evaluateProximityToTruth_orb(true_pos,true_rot)
            
        elif(self.type_slam=="None"):
            self.evaluateProximityToTruth_odom(true_pos,true_rot)

        
        return(None)    


	    
    def evaluateProximityToTruth_orb(self,true_pos,true_rot):     
        #rospy.loginfo("New pose detected, goto tf search")
        ng = 0
        no = 0
        # get transform between origin (by gmap) and pose of camera for gmap
        while not rospy.is_shutdown() and ng < self.ntry:
            try:
                transformStamped_gmap = geometry_msgs.msg.TransformStamped()
                transformStamped_gmap = self.buffer.lookup_transform('map2', 'camera_link2', rospy.Time(0), rospy.Duration(0.5) )
                time_gmap = transformStamped_gmap.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for gmap")
                ng+=1
                pass
        # get transform between origin (by gmap) and pose of camera for orb_slam
        while not rospy.is_shutdown() and no < self.ntry and ng < self.ntry:
            try: 
                transformStamped_orb = geometry_msgs.msg.TransformStamped()
                transformStamped_orb = self.buffer.lookup_transform('map2', 'camera_link', time_gmap, rospy.Duration(0.5) )
                break              
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                rospy.loginfo("Still searching for orb")
                no +=1
                pass
        if (ng==self.ntry):
            #pass
            print("TF gmap not found. Retry.")

        elif (no==self.ntry):
            print("TF orb not found. Retry.")        
        else:
            
            # obtain the norm of translation and rotation
            euler_gm = tf.transformations.euler_from_quaternion((transformStamped_gmap.transform.rotation.x,
                                                                 transformStamped_gmap.transform.rotation.y,
                                                                 transformStamped_gmap.transform.rotation.z,
                                                                 transformStamped_gmap.transform.rotation.w))
            roll_gm, pitch_gm, yaw_gm= euler_gm[0],euler_gm[1],euler_gm[2]
            
            euler_orb = tf.transformations.euler_from_quaternion((transformStamped_orb.transform.rotation.x,
                                                                                     transformStamped_orb.transform.rotation.y,
                                                                                     transformStamped_orb.transform.rotation.z,
                                                                                     transformStamped_orb.transform.rotation.w))
            roll_orb,pitch_orb,yaw_orb = euler_orb[0],euler_orb[1],euler_orb[2]
            
            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            
            trans_gm = np.array([transformStamped_gmap.transform.translation.x,transformStamped_gmap.transform.translation.y,transformStamped_gmap.transform.translation.z])
            trans_orb = np.array([transformStamped_orb.transform.translation.x,transformStamped_orb.transform.translation.y,transformStamped_orb.transform.translation.z])
            trans_tr = np.array([true_pos.x-0.03,true_pos.y,true_pos.z+0.18])
            
            rot_gm = np.array([roll_gm,pitch_gm,yaw_gm])
            rot_orb = np.array([roll_orb,pitch_orb,yaw_orb])
            rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
            
            trans_gmTr = trans_tr-trans_gm
            trans_orbTr = trans_tr-trans_orb
            
            rot_gmTr = rot_tr-rot_gm
            rot_orbTr = rot_tr-rot_orb
            
            if np.linalg.norm(rot_gmTr) > np.linalg.norm(rot_orbTr):
                closest_rot = "orb"
            else:
                closest_rot = "gmap"
            if np.linalg.norm(trans_gmTr) > np.linalg.norm(trans_orbTr):
                closest_trans = "orb"
            else:
                closest_trans = "gmap"            
            
            
            # publish it
            msg = comp()
            msg.trans_gm = list(trans_gm)
            msg.trans_orb= list(trans_orb)
            msg.trans_tr = list(trans_tr)
            msg.rot_gm  = list(rot_gm)
            msg.rot_orb  = list(rot_orb)
            msg.rot_tr  = list(rot_tr)
            msg.closest_rot = closest_rot
            msg.closest_trans = closest_trans
            self.pub_comp.publish(msg)        

        return(None)


    def evaluateProximityToTruth_rts(self,true_pos,true_rot,k,actual_time):  
        """
        Ici, on prend le lien map_gazebo --> base_link (exprimé par true_pos et true_rot), et on y rajoute
        le lien base_link --> camera2 pour pouvoir le comparer avec la transformation map --> camera_link2
        
        k : transformation rtabmap k testée
        """
        #rospy.loginfo("New pose detected, goto tf search")
        nr = 0
        # get transform between origin (by rts) and pose of camera for rts
        while not rospy.is_shutdown() and nr < self.ntry:
            try:
                transformStamped_rts = geometry_msgs.msg.TransformStamped()
                transformStamped_rts = self.buffer.lookup_transform(self.L_map_name[k], 'camera_link2', actual_time, rospy.Duration(0.5) )
                time_rts = transformStamped_rts.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for rts")
                nr+=1
                pass

        if (nr==self.ntry):
            pass
            print("TF rts not found for %i. Retry."%k)

        else:
            if (self.type_data == "raw"):
                trans_tr = np.array([true_pos.x-0.03,true_pos.y,true_pos.z+0.18]) #+0.14
                euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
                roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
                true_trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
                rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
                
            elif (self.type_data == "record"):
                 trans_tr = np.array([true_pos[0]-0.03,true_pos[1],true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
                 true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
                 rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
            # obtain the norm of translation and rotation
            euler_rts = tf.transformations.euler_from_quaternion((transformStamped_rts.transform.rotation.x,
                                                                 transformStamped_rts.transform.rotation.y,
                                                                 transformStamped_rts.transform.rotation.z,
                                                                 transformStamped_rts.transform.rotation.w))
            roll_rts, pitch_rts, yaw_rts= euler_rts[0],euler_rts[1],euler_rts[2]
                        

            
            trans_rts = np.array([transformStamped_rts.transform.translation.x,transformStamped_rts.transform.translation.y,transformStamped_rts.transform.translation.z])           
            rot_rts = np.array([roll_rts,pitch_rts,yaw_rts])

            trans_rtsTr = trans_tr-trans_rts
            rot_rtsTr = rot_tr-rot_rts

            diff_norm_trans = np.linalg.norm(trans_rtsTr)
            diff_norm_rot = np.linalg.norm(rot_rtsTr)
            
            
            
            # publish it
            msg = comp_rts()
            msg.trans_rts = list(trans_rts)
            msg.trans_tr = list(trans_tr)
            msg.rot_rts = list(rot_rts)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot
            msg.pose_jump = self.pose_jump
            msg.gazebo_jump = self.gazebo_jump
            self.L_pub_comp[k].publish(msg)
            
            self.L_all_trans_err[k].append(diff_norm_trans)
            self.L_all_rot_err[k].append(diff_norm_rot)

            self.pose_jump = False
            self.prev_gaz_pos  = true_trans_tr
            self.prev_gaz_rot = rot_tr
            self.gazebo_jump = False
            self.odom_jump = False

        return(None)


    def evaluateProximityToTruth_rts2(self,true_pos,true_rot,transformStamped_rts,k):  
        """
        Ici, on prend le lien map_gazebo --> base_link (exprimé par true_pos et true_rot), et on y rajoute
        le lien base_link --> camera2 pour pouvoir le comparer avec la transformation map --> camera_link2
        
        k : transformation rtabmap k testée
        """
        if True:
            if (self.type_data == "raw"):
                trans_tr = np.array([true_pos.x-0.03,true_pos.y,true_pos.z+0.18]) #+0.14
                euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
                roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]

                true_trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
                rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
                
            elif (self.type_data == "record"):
                 trans_tr = np.array([true_pos[0]-0.03,true_pos[1],true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
                 true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
                 rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
                 
            # obtain the norm of translation and rotation
            euler_rts = tf.transformations.euler_from_quaternion((transformStamped_rts.transform.rotation.x,
                                                                 transformStamped_rts.transform.rotation.y,
                                                                 transformStamped_rts.transform.rotation.z,
                                                                 transformStamped_rts.transform.rotation.w))
            roll_rts, pitch_rts, yaw_rts= euler_rts[0],euler_rts[1],euler_rts[2]
            

                

            
            trans_rts = np.array([transformStamped_rts.transform.translation.x,transformStamped_rts.transform.translation.y,transformStamped_rts.transform.translation.z])           
            rot_rts = np.array([roll_rts,pitch_rts,yaw_rts])

            trans_rtsTr = trans_tr-trans_rts
            
            rot_rtsTr = rot_tr-rot_rts
            for l in range(3):  
                if (rot_tr[l]*rot_rts[l]<0):
                    rot_rtsTr[l] = min([abs(rot_rts[l]-rot_tr[l]),abs(rot_rts[l]+rot_tr[l]),abs(rot_tr[l]-rot_rts[l]),abs(rot_tr[l]+rot_rts[l])])

            diff_norm_trans = np.linalg.norm(trans_rtsTr)
            diff_norm_rot = np.linalg.norm(rot_rtsTr)
            
            
            
            # publish it
            msg = comp_rts()
            msg.stamp = transformStamped_rts.header.stamp
            msg.trans_rts = list(trans_rts)
            msg.trans_tr = list(trans_tr)
            msg.rot_rts = list(rot_rts)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot
            msg.pose_jump = self.pose_jump
            msg.gazebo_jump = self.gazebo_jump
            self.L_pub_comp[k].publish(msg)
            
            self.L_all_trans_err[k].append(diff_norm_trans)
            self.L_all_rot_err[k].append(diff_norm_rot)

            self.pose_jump = False
            self.prev_gaz_pos  = true_trans_tr
            self.prev_gaz_rot = rot_tr
            self.gazebo_jump = False
            self.odom_jump = False

        return(None)

    def evaluateProximityToTruth_odom(self,true_pos,true_rot):     

        nr = 0
        # get transform between origin (by rts) and pose of camera for rts
        while not rospy.is_shutdown() and nr < self.ntry:
            try:
                transformStamped_odom = geometry_msgs.msg.TransformStamped()
                transformStamped_odom = self.buffer.lookup_transform('odom', 'base_link', rospy.Time(0), rospy.Duration(0.5) )
                time_odom = transformStamped_odom.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for odom")
                nr+=1
                pass

        if (nr==self.ntry):
            pass
            print("TF odom not found. Retry.")

        else:
            
            euler_odom = tf.transformations.euler_from_quaternion((transformStamped_odom.transform.rotation.x,
                                                                 transformStamped_odom.transform.rotation.y,
                                                                 transformStamped_odom.transform.rotation.z,
                                                                 transformStamped_odom.transform.rotation.w))
            roll_odom, pitch_odom, yaw_odom= euler_odom[0],euler_odom[1],euler_odom[2]
                        
            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            
            trans_odom = np.array([transformStamped_odom.transform.translation.x,transformStamped_odom.transform.translation.y,transformStamped_odom.transform.translation.z])           
            rot_odom = np.array([roll_odom,pitch_odom,yaw_odom])
            
            
            
            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
            
            trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
            
            trans_odomTr = trans_tr-trans_odom
            rot_odomTr = rot_tr-rot_odom

            diff_norm_trans = np.linalg.norm(trans_odomTr)
            diff_norm_rot = np.linalg.norm(rot_odomTr)
            
            # publish it
            msg = comp_odom()
            msg.trans_odom = list(trans_odom)
            msg.trans_tr = list(trans_tr)
            msg.rot_odom = list(rot_odom)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot

            self.pub_comp_odom.publish(msg)        


        return(None)
    
    
    def display_errors(self,msg):
        if (msg.true_false):
            colors = ['R','G','B','orange','black','yellow','pink','purple']
            fig,axs = plt.subplots(2, 1)
            print([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            #ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            ori_time = self.L_time_synch[0]
            Ltime = [self.L_time_synch[l]-ori_time for l in range(length)]
            for k in range(self.nb_diff):
                #Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                # print(self.L_all_trans_err[k][:length])
                # print(self.L_time[:length])
                # print(colors[k])
                axs[0,].plot(Ltime,self.L_all_trans_err[k][:length], label="test nbe %i"%k, color = colors[k])
                axs[1,].plot(Ltime,self.L_all_rot_err[k][:length],  label="test nbe %i"%k, color = colors[k])
            # ok
            axs[0,].set_xlabel("temps (s)")
            axs[0,].set_ylabel("diff_trans_total (m)")
            axs[0,].set_title("Translational error in time")
            axs[0,].legend()
            axs[1,].set_xlabel("temps (s)")
            axs[1,].set_ylabel("diff_rot_total (rad)")
            axs[1,].set_title("Rotational error in time")
            axs[1,].legend()
            plt.show()
        return(True)

    def display_max_errors(self,msg):
        if (msg.true_false):
            
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            L_means_trans = []
            L_stds_trans = []
            L_means_rot = []
            L_stds_rot = []   
            L_mins_rot = []
            L_mins_trans = []
            L_maxs_rot = []
            L_maxs_trans = []
            

            
            for k in range(self.nb_diff):
                Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                L_means_trans.append(np.mean(self.L_all_trans_err[k]) )
                L_stds_trans.append(np.std(self.L_all_trans_err[k]))
                L_means_rot.append(np.mean(self.L_all_rot_err[k]) )
                L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
                min_rot_k = np.min(self.L_all_rot_err[k])
                t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
                L_mins_rot.append([min_rot_k,t_min_rot_k])
                
                max_rot_k = np.max(self.L_all_rot_err[k])
                t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
                L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
                min_trans_k = np.min(self.L_all_trans_err[k])
                t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
                L_mins_trans.append([min_trans_k,t_min_trans_k])
                
                max_trans_k = np.max(self.L_all_trans_err[k])
                t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
                L_maxs_trans.append([max_trans_k,t_max_trans_k])       
           
            L_class_mean_trans = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_trans)):
                val_clas = L_means_trans[i]
                clas = 0
                for j in range(len(L_means_trans)):
                    if val_clas > L_means_trans[j]:
                        clas+=1
                L_class_mean_trans[i] = clas
                
            L_class_mean_rot = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_rot)):
                val_clas = L_means_rot[i]
                clas = 0
                for j in range(len(L_means_rot)):
                    if val_clas > L_means_rot[j]:
                        clas+=1
                L_class_mean_rot[i] = clas                
               
            print("------------ Max errors -------------")
            print(" Translation Max Error Statistics ")
            Lmt2 = [ L_mins_trans[k][0] for k in range(len(L_mins_trans))]
            LMt2 = [ L_maxs_trans[k][0] for k in range(len(L_maxs_trans))]
            print("By mean : %f | By max : %f | By min : %f "%(max(L_means_trans) - min(L_means_trans) , max(LMt2) - min(LMt2) , max(Lmt2) - min(Lmt2)  ) )
            print("********************************")
            Lmr2 = [ L_mins_rot[k][0] for k in range(len(L_mins_rot))]
            LMr2 = [ L_maxs_rot[k][0] for k in range(len(L_maxs_rot))]
            print(" Rotational Max Error Statistics ")
            print("By mean : %f | By max : %f | By min : %f "%(max(L_means_rot) - min(L_means_rot) , max(LMr2) - min(LMr2) , max(Lmr2) - min(Lmr2)             ) )
               
            

        return(True)   


    def display_statistics(self,msg):
        if (msg.true_false):
            
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            L_means_trans = []
            L_stds_trans = []
            L_means_rot = []
            L_stds_rot = []   
            L_mins_rot = []
            L_mins_trans = []
            L_maxs_rot = []
            L_maxs_trans = []
            
            for k in range(self.nb_diff):
                Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                L_means_trans.append(np.mean(self.L_all_trans_err[k]) )
                L_stds_trans.append(np.std(self.L_all_trans_err[k]))
                L_means_rot.append(np.mean(self.L_all_rot_err[k]) )
                L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
                min_rot_k = np.min(self.L_all_rot_err[k])
                t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
                L_mins_rot.append([min_rot_k,t_min_rot_k])
                
                max_rot_k = np.max(self.L_all_rot_err[k])
                t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
                L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
                min_trans_k = np.min(self.L_all_trans_err[k])
                t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
                L_mins_trans.append([min_trans_k,t_min_trans_k])
                
                max_trans_k = np.max(self.L_all_trans_err[k])
                t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
                L_maxs_trans.append([max_trans_k,t_max_trans_k])       
           
            L_class_mean_trans = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_trans)):
                val_clas = L_means_trans[i]
                clas = 0
                for j in range(len(L_means_trans)):
                    if val_clas > L_means_trans[j]:
                        clas+=1
                L_class_mean_trans[i] = clas
                
            L_class_mean_rot = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_rot)):
                val_clas = L_means_rot[i]
                clas = 0
                for j in range(len(L_means_rot)):
                    if val_clas > L_means_rot[j]:
                        clas+=1
                L_class_mean_rot[i] = clas                
               
            print("------------ Statistics -------------")
            print(" Translation Error Statistics ")
            for k in range(self.nb_diff):
                print("Test nbe : %i | Global Mean Translational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,L_means_trans[k],L_stds_trans[k],L_mins_trans[k][0],L_mins_trans[k][1],L_maxs_trans[k][0],L_maxs_trans[k][1],L_class_mean_trans[k]) )
            print("********************************")
            print(" Rotational Error Statistics ")
            for k in range(self.nb_diff):
                print("Test nbe : %i | Global Mean Rotational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,L_means_rot[k],L_stds_rot[k],L_mins_rot[k][0],L_mins_rot[k][1],L_maxs_rot[k][0],L_maxs_rot[k][1],L_class_mean_rot[k]) )
                
            

        return(True)    
    
    def save_data(self,msg):
        name = msg.path + "/" + msg.map_name + ".csv"
        f = open(name,'w')
        with f:
            fnames = ["Trans_err","Rot_err","Associated_time"]
            writer = csv.DictWriter(f,fieldnames=fnames)
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            for k in range(self.nb_diff):
                writer.writerow({"Trans_err" : "Test nbe %i"%k, "Rot_err" : " ", "Associated_time" : " " })
                L_trans_k = self.L_all_trans_err[k]
                L_rot_k = self.L_all_rot_err[k]
                L_time = self.L_all_time[k]
                for l in range(length):
                    writer.writerow({"Trans_err" : L_trans_k[l], "Rot_err" : L_rot_k[l], "Associated_time" : L_time[l]})
                writer.writerow({"Trans_err" : " ", "Rot_err" : " ", "Associated_time" : " "})

        return(True)
    


    
    def __init__(self,rate):

        """
        
        Constructor of the CrackMap class. 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        Parameters : 
            
            PicID : int, associated with the ID of the last depth message
            recorded.
            
            cur_PicID : int, associated with the ID of the currently processed
            depth message (by the checkCracks function)
            
            CIFrontColor : Camera Info object, containing all the intrinsic parameters
            of the camera.
            
            depthMsg : Image Message : ROS message containing the last message published by 
            the topic /phantomx/crack_image (a depth picture containing the depth of 
            all the detected cracks on a picture).
            
            depthPic : OpenCV Matrix, containing the last depth picture published by
            the topic /phantomx/crack_image.
            
            allCracksInCave : list, containing all the cracks detected since the beginning
            of the simulation.
            
            nb_cracks : int, containing the number of cracks detected since the beginning of the
            simulation.
            
            dist_center_cracks : float, indicating the minimum 3D distance between the barycenters
            of the cracks.
            
            R_rpic2rcam : rotationnal matrix from the picture frame to the camera sensor frame
            
            R_rcam2rpic : rotationnal matrix from the camera sensor frame to the picture frame
            
            buffer : BufferInterface object, stores the last transformation matrices between frames for
            a certain time (60 seconds here).
            
            listener : TransformListener object, used to express the coordinates of a pixel from the 
            camera sensor frame into the cave frame.
            
        """
        rospy.loginfo("INIT")

        self.period = 1.0/rate


        self.type_slam= rospy.get_param("~type_slam")
        self.type_data= rospy.get_param("~type_data")
        self.nb_diff =rospy.get_param("~nb_diff")

        self.display_service = rospy.Service('display_errors', boolean, self.display_errors)
        self.display_max_err = rospy.Service('display_max_errors', boolean, self.display_max_errors)
        self.display_stats = rospy.Service('display_statistics', boolean, self.display_statistics)
        self.save_dta = rospy.Service('save_data',save_name , self.save_data)
        
        if self.type_data == "raw":            
            self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
        elif self.type_data == "record":
            self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth3)

        self.initialPoseTopic = rospy.Subscriber("/rtabmap/initialpose", PoseWithCovarianceStamped, self.callback_initialpose_jump)

        ## Parameters for callback_gazebo
        self.detect_name = False
        self.num_rosbot = -1
        self.prev_gaz_pos = None
        self.prev_gaz_rot = None
        self.prev_gaz_speed = None
        self.prev_gaz_ang = None
        self.prev_gaz_time = None
        
        ## Parameters for callback_initialpose_jump      
        self.prev_initialpose_pos = None
        self.prev_initialpose_rot = None   
        self.pose_jump = False

        ## Parameters for check_jump  
        self.th_tlp_pos = 0.175
        self.th_tlp_rot = 0.055

            
        # Parameters for evaluateProximityToTruth_rts
        self.odom_jump = False
        self.gazebo_jump = False


        # Parameters for buffer and buffer access
        self.buf_access = True
        self.buf_access_map = True
        self.temp_buf_access = False


        self.buf_tf_true = []
        self.buf_all_tf_map = []
        self.buf_tf_bl = []
        
        self.temp_buf_tf_true = []
        self.temp_buf_all_tf_map = []
        self.temp_buf_tf_bl = []        
        
        self.L_pub_comp = []
        self.L_map_name = []
      
        # Parameters to save datas 
        
        self.L_all_trans_err = []
        self.L_all_rot_err = []
        self.L_all_time = []
        self.L_time_synch = []
        self.pcheck = None
        
        # Parameters to manage the publication of rts data

        self.ref_i_map = -1
        if (self.nb_diff%2) == 0:
            self.ref_i_map = self.nb_diff//2-1
        else:
            self.ref_i_map = self.nb_diff//2
        self.int_synch_map = -1

        self.nmap = 0

        self.map_time_synch = -1
        self.rate_rts = 2.0
        self.rate_odom = 1.0+1e-3
        self.rate_bl = 0.1+1e-3
        
        self.len_buf_true = -1
        self.len_buf_bl = -1
        #self.desynch_lim = (self.rate_rts/10)*self.nb_diff# ecart temporel max pour synchro des topics map
        self.tol = 0.0

        # Parameters to manage the progression of the synchrnization process
        
        self.icheck = None
        self.map_time_synch = -1
        self.prev_map_synch_success = -1
        self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])

        self.err_odom = False
        self.err_bl = False
        

        # Parameters to manage the publication of informations abut the synchronization process
        
        self.dispok = self.nb_diff
        self.dispm1 = self.nb_diff
        self.dispm2 = self.nb_diff
        self.dispm3 = self.nb_diff
        self.send = False

        self.sub_tf = rospy.Subscriber("/tf",tfMessage,self.callback_tf)
        for k in range(1,self.nb_diff+1):
            self.L_map_name.append("map%i"%k)
            self.buf_all_tf_map.append([])
            self.temp_buf_all_tf_map.append([])               
            self.L_all_trans_err.append([])
            self.L_all_time.append([])
            self.L_all_rot_err.append([])
            self.L_pub_comp.append(rospy.Publisher("comp_rts_%i"%(k), comp_rts) )

        self.rate = rospy.Rate(rate)
        
        self.ntry =4
        
        rospy.spin()


        

        
        
        # self.L_time = []
        
        # self.tol = 0.0
        

        # self.rate_rts = 2.0
        # self.rate_odom = 1.0+1e-3
        # self.rate_bl = 0.1+1e-3
        
        # self.desynch_lim =1.0#0.1+1e-3# (self.rate_odom)/4 #0.1*(self.nb_diff)+1e-3 #1.0+1e-3 #(self.rate_rts)/2 # 2.0 # (self.rate_rts/20)*self.nb_diff# ecart temporel max pour synchro des topics map
        # self.ref_i_map = -1


        


        return None



if __name__ == "__main__":
    rospy.init_node("evaluateNodes", anonymous=False, log_level=rospy.DEBUG)
    cm = evaluateNodes(1.0)