#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 09:55:33 2020

@author: tnmx0798
"""


import rospy
from rosbot_gazebo.srv import boolean,save_name, save_nameResponse


class TestServer():

    def true_false(self,mes):
        print(mes.true_false)
        return(True)

    def name(self,mes):
        print(mes.path)
        print(mes.map_name)
        print(mes.save_or_load)
        if mes.save_or_load == 0:
            # save
            name = mes.map_name
            f = open(mes.path+"/"+name+".txt","w+")
            f.write(str([0,1,2,3,4,5,6]) )
            f.close()
        elif mes.save_or_load == 1:
            # load
            f = open(mes.path+"/"+mes.map_name+".txt","r+")
            fl = f.readlines()
            data = fl[0]
            data = data[1:-1].split(',')
            data = [float(data[k]) for k in range(len(data))]
            print(data)
            print(type(data))
        obj = save_nameResponse()
        obj.success= True
        
        return(obj)
        
    
    def __init__(self,rate = 0.5):
        rospy.init_node('test_server')
        start_stop_service = rospy.Service('true_false', boolean, self.true_false)
        start_stop_service2 = rospy.Service('name', save_name, self.name)
        self.rate = rospy.Rate(rate)
        rospy.spin()
        
if __name__ == "__main__":
    TestServer()


