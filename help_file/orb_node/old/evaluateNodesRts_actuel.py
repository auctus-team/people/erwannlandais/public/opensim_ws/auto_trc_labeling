#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np

import matplotlib.pyplot as plt


import image_geometry
import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped

from geometry_msgs.msg import PoseWithCovarianceStamped

import time
import geometry_msgs.msg

import tf

import tf.transformations

from tf.msg import tfMessage

from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp_rts, comp_odom

from rosbot_gazebo.srv import boolean, save_name

from rtabmap_ros.msg import MapGraph

from nav_msgs.msg import Odometry

import csv

class transformStampedEuler():
    
    def __init__(self):
        self.time = 0
        self.pos = None
        self.rot = None
    
    def __repr__(self):
        return(self.__str__() )
    
    def __str__(self):
        
        return(" Stamp : %f \n pos : [%f,%f,%f] \n rot : [%f,%f,%f] \n"%(self.time,self.pos[0],self.pos[1],self.pos[2],self.rot[0],self.rot[1],self.rot[2]))


class evaluateNodesRts():

    def callback_tf2(self,msg):
        lentot = 0   
        
        if (self.buf_access_map ==True and self.buf_access_bl == True and self.buf_access_truth == True):
            lms = [len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))]
            lentot1 = min(lms)
            vs = lms.index(lentot1)
                
            lentot2 = len(self.buf_all_tf_bl[vs])
            lentot3 = len(self.buf_all_tf_true[vs])
                
            lentot = min(lentot1,lentot2,lentot3)
        if (lentot > 0 ):
            self.prepare_to_send()

    def callback_rtab(self,msg):
        name = msg.header.frame_id
        i = int(name[-1])-1        
        
        if (self.buf_access_map): # and len( self.buf_all_tf_map[i]) == 0 ):
            self.buf_all_tf_map[i].append(msg)
            if len(self.buf_all_tf_map[i] > self.qs_rts):
                del(self.buf_all_tf_map[i][0])
                        
        elif (self.temp_buf_access_map): # and len( self.temp_buf_all_tf_map[i]) == 0):
            self.temp_buf_all_tf_map[i].append(msg)    
            if len(self.temp_buf_all_tf_map[i] > self.qs_rts):
                del(self.temp_buf_all_tf_map[i][0])        
        #print(self.buf_all_tf_map)
        
        # if (self.buf_access_map and self.L_prev_poses[i][-1] == None and len( self.buf_all_tf_map[i]) == 0 ):
        #     self.buf_all_tf_map[i].append(msg)
                        
        # elif (self.temp_buf_access_map and self.L_prev_poses[i][-1] == None and len( self.temp_buf_all_tf_map[i]) == 0):
        #     self.temp_buf_all_tf_map[i].append(msg)    
                        
                    
        # elif self.prev_pose_access and self.L_prev_poses[i][-1] != None :
        #     true_pos= msg.mapToOdom.translation
        #     new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])

        #     p_pos = self.L_prev_poses[i][-1].mapToOdom.translation
        #     old_pose = np.array([p_pos.x, p_pos.y, p_pos.z])
        #     if (self.prev_pose_access and np.all(new_pose==old_pose) == False):
                            
        #             # p_pos = self.L_prev_poses[i][-1].transform.translation
        #             # old_pose = np.array([p_pos.x, p_pos.y, p_pos.z])
        #             if (self.buf_access_map and len( self.buf_all_tf_map[i]) == 0  ): # and np.all(new_pose==old_pose) == False ):
        #                         # if (i == 6):
        #                         #     print("prev_pose : ", self.L_prev_poses[i][-1])
        #                         #     print("new_pose : ",msg.mapToOdom)
        #                 self.buf_all_tf_map[i].append(msg)
        #                         #print("Add then : ", msg.mapToOdom)
                        
        #             elif (self.temp_buf_access_map and len( self.temp_buf_all_tf_map[i]) == 0 ): # and  np.all(new_pose==old_pose) == False):
        #                 self.temp_buf_all_tf_map[i].append(msg)    
                            
        #                     #elif np.all(new_pose==old_pose) == False :
        #             else:
        #                 b_pos = true_pos
        #                 if (self.buf_access_map):
        #                     try:
        #                         b_pos = self.buf_all_tf_map[i][-1].mapToOdom.translation
        #                     except Exception : 
        #                         pass
        #                 elif (self.temp_buf_access_map):
        #                     try : 
        #                         b_pos = self.temp_buf_all_tf_map[i][-1].mapToOdom.translation
        #                     except Exception : 
        #                         pass
        #                 old_pose2 = np.array([b_pos.x, b_pos.y, b_pos.z])
        #                 #print("pose : ", new_pose,old_pose)
        #                 if (np.all(new_pose==old_pose2) == False):
        #                     if (self.buf_access_map):
        #                         self.buf_all_tf_map[i].append(msg)
                                        
        #                     elif (self.temp_buf_access_map):
        #                         self.temp_buf_all_tf_map[i].append(msg)
        


    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec < prev_ec:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
    
    def find_tol_closest_time_match(self,time,L_time,tol):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec < prev_ec:
                i -= 1
                prev_ec = ec
                if prev_ec < tol:
                    break
            else:
                break
        return(i,prev_ec)
    
    
    def find_rel_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = (L_time[i-1]-time)
            if ec < prev_ec and ec >= 0:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
        
    def check_synch_bl(self,buf,t):
        bl_synch = []
        i = -1
        if (len(buf) != self.len_buf_bl):
            L_time = [ buf[k].header.stamp.to_sec() for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_bl+self.tol*(self.rate_bl/self.rate_rts):
                self.err_bl = False
                bl_synch.append(buf[i])
            else:
                print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_bl or self.err_bl:
                    # No matching possible, cancel for this topic
                    self.map_time_synch = -1
                    if (self.err_bl):
                        print("Stop bl blocking!")
                    self.err_bl = False
                else:
                    self.err_bl = True
                self.len_buf_bl = len(buf)

        return(bl_synch,i)

    def check_synch_bl_L(self,buf,Lt):
        bl_synch = []
        imin = 999
        #print("Lt : ", Lt)
        if (len(buf) != self.len_buf_bl):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t = Lt[j] #-self.rate_rts
                    i = -1            
                    L_time = [ buf[k].header.stamp.to_sec() for k in range(len(buf))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    #(i,ec) = self.find_tol_closest_time_match(t,L_time, self.rate_bl+self.tol*(self.rate_bl/self.rate_rts))
                    if ec < self.rate_bl/2+self.tol*(self.rate_bl/self.rate_rts):
                        #self.err_bl = False
                        bl_synch.append(buf[i])
                        self.L_ec_bl[j] = -1
                        self.L_disp_bl[j] = True                        
                        if i < imin:
                            imin = i
                    else:
                        if self.L_disp_bl[j]:
                            print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            self.L_disp_bl[j] = False
                            self.L_ec_bl[j] = ec
                        bl_synch.append(None)
                            # self.len_buf_bl = len(buf)
                            # #print(buf[-4:])
                            # #print(self.buf_all_tf_map)
                            # bl_synch = []
                            # break
                else:
                    bl_synch.append(None)
            # else:
            #     print("bl : No new at : ",rospy.get_time())
        return(bl_synch,imin)

    def check_synch_bl_L2(self,buf,Lt):
        bl_synch = []
        Lbl_i = []
        imin = 999
        lms = np.array([len(buf[l]) for l in range(len(buf))])
        #print("Lt : ", Lt)
        if (np.all(lms == self.len_buf_bl) == False):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t = Lt[j] #-self.rate_rts
                    i = -1            
                    L_time = [ buf[j][k].time for k in range(len(buf[j]))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    #(i,ec) = self.find_tol_closest_time_match(t,L_time, self.rate_bl+self.tol*(self.rate_bl/self.rate_rts))
                    if ec < self.rate_bl/2+self.tol*(self.rate_bl/self.rate_rts):
                        #self.err_bl = False
                        bl_synch.append(buf[j][i])
                        self.L_ec_bl[j] = -1
                        self.L_disp_bl[j] = True                        
                        Lbl_i.append(i)
                    else:
                        if self.L_disp_bl[j]:
                            print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            self.L_disp_bl[j] = False
                            self.L_ec_bl[j] = ec
                        bl_synch.append(None)
                        Lbl_i.append(len(buf[j]))
                            # self.len_buf_bl = len(buf)
                            # #print(buf[-4:])
                            # #print(self.buf_all_tf_map)
                            # bl_synch = []
                            # break
                else:
                    bl_synch.append(None)
                    Lbl_i.append(len(buf[j]))
            # else:
            #     print("bl : No new at : ",rospy.get_time())
        return(bl_synch,Lbl_i)


    def check_synch_odom(self,buf,t):
        odom_synch = []
        i = -1
        if (len(buf) != self.len_buf_true):
            L_time = [ buf[k].time for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_rts):
                odom_synch.append(buf[i])
                self.err_odom = False
            else:
                print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_odom or self.err_odom:
                    # No matching is then possible : cancel the synchronization for this topic
                    self.map_time_synch = -1
                    if self.err_odom:
                        print("Stop odom blocking!")
                    self.err_odom = False
                else:
                    self.err_odom = True
                self.len_buf_true = len(buf)
        return(odom_synch,i)        

    def check_synch_odom_L(self,buf,Lt):
        odom_synch = []
        imin = 999
        if (len(buf) != self.len_buf_true):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t =Lt[j] # Lt[j]-self.rate_rts
                    i = -1
        
                    L_time = [ buf[k].time for k in range(len(buf))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    #(i,ec) = self.find_tol_closest_time_match(t,L_time, self.rate_odom+self.tol*(self.rate_odom/self.rate_rts))
                    if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_rts):
                        odom_synch.append(buf[i])
                        self.L_ec_odom[j] = -1
                        self.L_disp_odom[j] = True
                        if i < imin : 
                            imin = i
                    else:
                        #if self.L_disp_odom[j] == False and L_ec_odom[j] != ec:
                        
                        if self.L_disp_odom[j]:
                            print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            self.L_disp_odom[j] = False
                            self.L_ec_odom[j] = ec
                        
                        #print(buf)
                        odom_synch.append(None)
                        
                        # self.len_buf_true = len(buf)
                        # odom_synch = []
                        #break
                else:
                    odom_synch.append(None)
        #else:
            #print("odom : No new at : ",rospy.get_time())
        return(odom_synch,imin)        

    def check_synch_odom_L2(self,buf,Lt):
        odom_synch = []
        Lodom_i = []
        imin = 999
        lms = np.array([len(buf[l]) for l in range(len(buf))])
        
        if (np.all(lms == self.len_buf_true) == False):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t =Lt[j] # Lt[j]-self.rate_rts
                    i = -1
        
                    L_time = [ buf[j][k].time for k in range(len(buf[j]))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    #(i,ec) = self.find_tol_closest_time_match(t,L_time, self.rate_odom+self.tol*(self.rate_odom/self.rate_rts))
                    if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_rts):
                        odom_synch.append(buf[j][i])
                        self.L_ec_odom[j] = -1
                        self.L_disp_odom[j] = True
                        Lodom_i.append(i)
                    else:
                        #if self.L_disp_odom[j] == False and L_ec_odom[j] != ec:
                        
                        if self.L_disp_odom[j]:
                            print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            self.L_disp_odom[j] = False
                            self.L_ec_odom[j] = ec
                        
                        #print(buf)
                        odom_synch.append(None)
                        Lodom_i.append(len(buf[j]))
                        
                        # self.len_buf_true = len(buf)
                        # odom_synch = []
                        #break
                else:
                    odom_synch.append(None)
                    Lodom_i.append(len(buf[j]))
        return(odom_synch,Lodom_i)

            
            
    def check_synch_map3(self,lms):
        """
        No check of synchronization at all. Only publish if there is a new object.
        """
        l = 0
        L_sync = []
        L_map_synch = []
        LMT = []

        buf_max = max(lms)
        # print("Buf max : ", buf_max)

        if buf_max > 1:
            Lalltime = [self.buf_all_tf_map[k][0].header.stamp.to_sec()  for k in range(self.nb_diff) ]
            time_synch_map = max(Lalltime)
            while l < self.nb_diff:
                L_time = [ self.buf_all_tf_map[l][k].header.stamp.to_sec() for k in range(len(self.buf_all_tf_map[l])) ]
                           
                (i,ec) = self.find_closest_time_match(time_synch_map,L_time)
                if (ec < self.rate_rts-self.tol) and  (self.buf_all_tf_map[l][i].header.stamp.to_sec() - self.L_all_time[l][-1] > self.rate_rts/2-self.tol):
                    L_sync.append(i)
                    L_map_synch.append(self.buf_all_tf_map[l][i])
                    LMT.append(L_map_synch[-1].header.stamp.to_sec())  

                else:
                    L_sync.append(0)
                    L_map_synch.append(self.buf_all_tf_map[l][0])
                    LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                l+=1
                
            
        else:
            while l < self.nb_diff:
                
                if len(self.buf_all_tf_map[l]) > 0:
                        L_sync.append(0)
                        L_map_synch.append(self.buf_all_tf_map[l][0])
                        LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                else:
                        L_sync.append(None)
                        L_map_synch.append(None)
                        LMT.append(-1)
                l+=1

        
        return(L_map_synch,L_sync,LMT)
 

    def check_synch_map(self):
        """
        Sycnhronisation effectuée par rapport à la dernière tf map[nb_diff/2] --> odom.
        Recherche des tfs mapi --> odom les plus proches temporellement parmi ceux disponibles.
        Principe : celui au milieu devrait avoir écart moyen le plus faible avec les autres, ce
        qui devrait faciliter synchronisation.
        

        Returns
        -------
        None.

        """
        
        # if (self.int_synch_map == -1):
        #     # def de indice int_synch_map, à partir duquel synchro sera effectué
        #     int_synch = 0
        #     Lalltime = [self.buf_all_tf_map[k][0].header.stamp.to_sec()  for k in range(self.nb_diff) ]
        #     #print(Lalltime)
        #     Ltime = sorted(Lalltime)
        #     #print(self.ref_i_map)
        #     t_mid = Ltime[self.ref_i_map]
        #     #print(Ltime)
        #     #print(t_mid)
        #     self.int_synch_map = Lalltime.index(t_mid)
        #     print("********* Maps will be synchronized with respect to : %i . **********"%self.int_synch_map)
        #time_synch_map = self.buf_all_tf_map[self.int_synch_map][self.icheck].header.stamp.to_sec()

        # icheck = self.buf_all_tf_map[self.int_synch_map].index(self.buf_all_tf_map[self.int_synch_map][self.icheck])
        # if icheck != self.icheck:
        #     print("icheck : ",icheck)
        #     #print(self.buf_all_tf_map[self.int_synch_map][self.icheck], self.buf_all_tf_map[self.int_synch_map][icheck])
        #     self.icheck = icheck
        #print("timesmap : ", time_synch_map  
        L_sync = []
        L_map_synch = []
        
        
        if (len(self.L_time_synch) > 0 ) and (min([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))]) > 0) :
            
            if ( (self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec() - self.L_time_synch[-1]) > self.rate_rts*3/4-self.tol ) or (self.desynch<2) :            
                self.dispm3 = self.nb_diff
                if (self.map_time_synch == -1): 
                    Lalltime = [self.buf_all_tf_map[k][-1].header.stamp.to_sec()  for k in range(self.nb_diff) ]
                    time_synch_map = min(Lalltime)
                    self.icheck = Lalltime.index(time_synch_map)

                else:
                    time_synch_map = self.map_time_synch
                
                if ( (time_synch_map -self.L_all_time[self.icheck][-1]) > self.rate_rts/2- self.tol) or (self.desynch<2): #- self.tol : #-self.tol : 
                    
                    
                    # necessaire pour bloquer :
                        # //////////Synchronisation accepted for map : 1 (actual min time : 26131.101678 ) \\\\\\
# ('Time of maps : ', [26131.151971628, 26131.101678225, 26131.101678225, 26131.151971628, 26131.101678225, 26131.151971628, 26131.151971628, 26131.151971628])
# ('------ Publication at synchronized time : ', 26131.101678225, ' (sychronized with 1)', ' completed! ----------')
# ('Updated tolerance : ', 0.07419240576457335)
# Err : not enough time difference between last accepted miminum and last occurence of this minimum : 0.000000 ; for map : 1 (last accepted min time : 26131.101678 )
# //////////Synchronisation accepted for map : 2 (actual min time : 26131.101678 ) \\\\\\
# ('Time of maps : ', [26131.151971628, 26131.101678225, 26131.101678225, 26131.151971628, 26131.101678225, 26131.151971628, 26131.151971628, 26131.151971628])
# ('------ Publication at synchronized time : ', 26131.101678225, ' (sychronized with 2)', ' completed! ----------')


                    self.dispm2 = self.nb_diff

                    l= 0
                    L_sync = []
                    L_map_synch = []
                    while l < self.nb_diff and (min([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))]) > 0):
                        L_time = [ self.buf_all_tf_map[l][k].header.stamp.to_sec() for k in range(len(self.buf_all_tf_map[l])) ]
                           
                        (i,ec) = self.find_rel_closest_time_match(time_synch_map,L_time)
                        if (ec < self.rate_rts-self.tol) and ( (self.buf_all_tf_map[l][i].header.stamp.to_sec() - self.L_all_time[l][-1] > self.rate_rts/2-self.tol) or self.desynch < 2 ):
                                #print("Sucessful ec : ", ec)
                            L_sync.append(i)
                            l+=1
                        else:
                            if self.dispm1 != l:
                                self.dispm1= l
                                if (ec == 9999):
                                    print("Err map synch (LT) : map %i is not synchronized yet with the given time %f (no values before this minima)"%(l,time_synch_map) )
                                elif (self.buf_all_tf_map[l][i].header.stamp.to_sec() - self.L_all_time[l][-1] <= self.rate_rts/2 - self.tol):
                                    print("Err map synch (LT) : not sure that %i has a new element to compare (according to difference : %f)"%(l,self.buf_all_tf_map[l][i].header.stamp.to_sec() - self.L_all_time[l][-1]))
                                    
                                elif (ec >= self.rate_rts-self.tol) :
                                    print("Err map synch (LT) : min time difference : ",ec, " for time : ", time_synch_map, " with map : ", l,"(minimum time found : %f )"%(L_time[i]))
                                else:
                                    print("Unknown error in map")
                            self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                            
                            break            
                else:
                    l = 2*self.nb_diff
                    if self.dispm2 != self.icheck:
                        print("Err : not enough time difference between actual minimum and last occurence of this minimum : %f ; for map : %i (actual min time : %f )"%((time_synch_map -self.L_all_time[self.icheck][-1]), self.icheck, time_synch_map))    
                        self.dispm2 = self.icheck
                    self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
        
            else:
                l = 2*self.nb_diff
                if self.dispm3 != self.icheck:
                    self.dispm3= self.icheck
                    print("Err : not enough time difference between last accepted miminum and last occurence of this minimum : %f ; for map : %i (last accepted min time : %f )"%((self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec() - self.L_time_synch[-1]), self.pcheck, self.buf_all_tf_map[self.pcheck][-1].header.stamp.to_sec()))    
                self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                
                
        elif min([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))]) > 0:
            
            if (self.map_time_synch == -1):
                Lalltime = [self.buf_all_tf_map[k][-1].header.stamp.to_sec()  for k in range(self.nb_diff) ]
                time_synch_map = min(Lalltime)
                self.icheck = Lalltime.index(time_synch_map)
            else:
                time_synch_map = self.map_time_synch
            
            l=0    
            L_sync = []
            L_map_synch = []
            while l < self.nb_diff:
                L_time = [ self.buf_all_tf_map[l][k].header.stamp.to_sec() for k in range(len(self.buf_all_tf_map[l])) ]

                (i,ec) = self.find_rel_closest_time_match(time_synch_map,L_time)
                if (ec < self.rate_rts-self.tol):
                    L_sync.append(i)
                    l+=1
                else:
                    if (ec == 9999):
                        print("Err map synch (no LT) : map %i is not synchronized yet with the given time %f (no values before this minima)"%(l,time_synch_map) )
                    else:
                        print("Err map synch (no LT) : min time difference : ",ec, " with time of synchronized map : ", time_synch_map, " with map : ", l, "(minimum time found : %f )"%(L_time[i]))
                    self.prev_map_synch_size = np.array([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map))])
                    break                
            
           

        else:
            l = 0

        LMT = []
        if l== self.nb_diff:
            self.dispm1 = self.nb_diff
            
            L_map_synch = [ self.buf_all_tf_map[j][ L_sync[j] ] for j in range(len(L_sync))  ]
            LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
            if ( max(LMT)-min(LMT)) > self.rate_rts/2 : #0.06:
                    print("Attention : huge desynchronization. Max difference between times : ", max(LMT)-min(LMT))
                    
                    # if (self.desynch < 2):
                    #     print("Buff with desynch : ", self.buf_all_tf_map)
                    #     print("Selected : ", L_map_synch)
                    #     print("L_prev_poses : ", self.L_prev_poses)
                        
                    # print("LMT : ", LMT)
                    # print("Time sync : ",time_synch_map)
                    # print("L_sync : ", L_sync)
                    
                    
                    L_map_synch = []
                    #self.temp_buf_access = False
                    self.prev_poses_access = False
                    self.temp_buf_access = False
                    for k in range(self.nb_diff):
                                                
                        # print("Bef : ", self.buf_all_tf_map[k])
                        # print("Ls : ", L_sync[k])
                        if (L_sync[k] != -1):
                            del(self.buf_all_tf_map[k][L_sync[k]])
                        del(self.buf_all_tf_map[k][-1])
                        while len(self.buf_all_tf_map[k]) >0 and (self.buf_all_tf_map[k][-1].header.stamp.to_sec() - self.L_all_time[k][-1])>=0:
                            del(self.buf_all_tf_map[k][-1])
                        self.L_prev_poses[k][-1] = None
                        self.temp_buf_all_tf_map[k] = []
                        # print("Aft : ", self.buf_all_tf_map[k])
                        # while len(self.buf_all_tf_map) > 1 and (self.buf_all_tf_map[k][-1].header.stamp.to_sec()-self.buf_all_tf_map[k][-2].header.stamp.to_sec()) ==0.0: 
                        #     del(self.buf_all_tf_map[k][-2])
                        #     #print("Before : ", self.buf_all_tf_map[k])
                    self.prev_poses_access = True
                    self.desynch = 0
                    #print("Buffer after desynch : ", self.buf_all_tf_map)
                        
                    #self.temp_buf_access = True
            #self.dispok = self.nb_diff
            #print(L_sync)
            #print([len(self.buf_all_tf_map[k]) for k in range(len(self.buf_all_tf_map)) ])
            else:
                self.map_time_synch = time_synch_map
            
            if (len(L_map_synch) != 0):
                if (self.dispok != self.icheck):
                    self.dispok = self.icheck
                    LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
                    print("--- Synchronisation accepted for map : %i (actual min time : %f ) ---"%(self.icheck, time_synch_map))    
                    print("Time of maps : ", LMT)

            # try : 
            #     L_map_synch = [ self.buf_all_tf_map[j][ L_sync[j] ] for j in range(len(L_sync))  ]
            #     LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
            #     stime = np.mean(LMT)
            # # if (self.map_time_synch != -1.0 and (self.map_time_synch - stime) > 0):
            # #     self.rate_rts = self.map_time_synch - stime
            # #     print("New rate_rts : ", self.rate_rts)
            #     if stime - self.prev_map_synch_success >= self.rate_rts-3*self.rate_bl/2 :#desynch_lim #rate_rts
            #         #print("********* Maps will be synchronized with respect to : %f . **********"%stime)
            #         self.map_time_synch = stime

            #     else:
            #         print("Not enough time diff : stime : %f ; prev_synch : %f"%(stime,self.prev_map_synch_success))
            #         if self.buf_access ==True:
            #             self.icheck = -1
            #         L_map_synch = []
            # except Exception:
            #     print("Exeption in check_map!")
            #     #self.icheck = -1
            #     L_map_synch = []
            
        
        return(L_map_synch,L_sync,LMT)

        
    def callback_odom(self,msg):
        
        if (self.buf_access_bl == True or self.temp_buf_access_bl ==True):
            new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
            new_quat = msg.pose.pose.orientation
            euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
            roll,pitch,yaw = euler[0],euler[1],euler[2]
            new_rot = np.array([roll,pitch,yaw])
            
            bl = transformStampedEuler()
            bl.pos = new_pose
            bl.rot = new_rot
            bl.time = msg.header.stamp.to_sec()
            
            if (self.buf_access_bl):
                for k in range(self.nb_diff):
                    self.buf_all_tf_bl[k].append(bl)
                    
                #self.buf_tf_bl.append(bl)
            elif (self.temp_buf_access_bl):
                for k in range(self.nb_diff):
                    self.temp_buf_all_tf_bl[k].append(bl)
                #self.temp_buf_tf_bl.append(bl)      

        # lentot = 0   
        
        # if (self.buf_access_map ==True and self.buf_access_bl == True and self.buf_access_truth == True):
        #     lms = [len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))]
        #     lentot1 = min(lms)
        #     vs = lms.index(lentot1)
                
        #     lentot2 = len(self.buf_all_tf_bl[vs])
        #     lentot3 = len(self.buf_all_tf_true[vs])
                
        #     lentot = min(lentot1,lentot2,lentot3)
        # if (lentot > 0 ):
        #     self.prepare_to_send()
        return(None)


    def callback_odom2(self,msg):
        
        if (self.buf_access_bl == True or self.temp_buf_access_bl ==True):
            new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
            new_quat = msg.pose.pose.orientation
            euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
            roll,pitch,yaw = euler[0],euler[1],euler[2]
            new_rot = np.array([roll,pitch,yaw])
            
            bl = transformStampedEuler()
            bl.pos = new_pose
            bl.rot = new_rot
            bl.time = msg.header.stamp.to_sec()
            
            if (self.buf_access_bl):
                self.buf_tf_bl.append(bl)
                if len(self.buf_tl_bl) > self.qs_bl:
                    del(self.buf_tf_bl[0])
                    
            elif (self.temp_buf_access_bl):
                self.temp_buf_tf_bl.append(bl)
                if len(self.temp_buf_tl_bl) > self.qs_bl:
                    del(self.temp_buf_tf_bl[0])

      
    def pub_rts_err(self,L_map_synch,bl_synch,odom_synch):
        true_pos = odom_synch[-1].pos
        true_rot = odom_synch[-1].rot
        pose_bl = bl_synch[-1]
        self.L_time_synch.append(self.map_time_synch)
        for k in range(self.nb_diff):
            tf_rts = geometry_msgs.msg.TransformStamped()
            tf_tranf = L_map_synch[k]
            self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
            tf_rts.header.stamp = tf_tranf.header.stamp
            tf_rts.transform.translation.x = tf_tranf.transform.translation.x + pose_bl.transform.translation.x-0.03
            tf_rts.transform.translation.y = tf_tranf.transform.translation.y + pose_bl.transform.translation.y
            tf_rts.transform.translation.z = tf_tranf.transform.translation.z + pose_bl.transform.translation.z+0.18
                    
            x0,y0,z0,w0 = tf_tranf.transform.rotation.x,tf_tranf.transform.rotation.y,tf_tranf.transform.rotation.z,tf_tranf.transform.rotation.w
            x1,y1,z1,w1 = pose_bl.transform.rotation.x,pose_bl.transform.rotation.y,pose_bl.transform.rotation.z,pose_bl.transform.rotation.w
                    
            M_quat = np.array((
                x1*w0 + y1*z0 - z1*y0 + w1*x0,
                -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                x1*y0 - y1*x0 + z1*w0 + w1*z0,
                -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)
                    
            tf_rts.transform.rotation.x = M_quat[0]
            tf_rts.transform.rotation.y = M_quat[1]
            tf_rts.transform.rotation.z = M_quat[2]
            tf_rts.transform.rotation.w = M_quat[3]
            self.evaluateProximityToTruth_rts(true_pos,true_rot,tf_rts,k)


    def pub_rts_err_L2(self,L_map_synch,Lbl_synch,Lodom_synch):
        
        Lind = []
        for k in range(self.nb_diff):
            if (Lbl_synch[k] != None and Lodom_synch[k] != None ):
                Lind.append(k)        

        L_real_odom = [Lodom_synch[Lind[j]] for j in range(len(Lind))]
        L_real_bl = [Lbl_synch[Lind[j]] for j in range(len(Lind))]
        L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # mini = min(Lrt)
        try :
            print("BL : ", [L_real_bl[k].time for k in range(len(L_real_bl))])
            print("Odom : ", [L_real_odom[k].time for k in range(len(L_real_odom))])
            print("With times : ", Lrt)
        except AttributeError:
            print("AE!")
            print("BL  : ", L_real_odom)
            print("Odom :", L_real_bl)
            pass
        ########## TRY TWO
        
        
        # ZO = transformStampedEuler()
        # ZO.pos = np.array([0,0,0])
        # ZO.rot = np.array([0,0,0])

        
        # for k in range(self.nb_diff):
        #     if (Lbl_synch[k]!=None):
        #         Lodom_synch[k] = [ZO]

        
        
        for k in range(self.nb_diff):
            if (L_map_synch[k] != None and Lbl_synch[k] != None and Lodom_synch[k] != None):
                true_pos = Lodom_synch[k].pos
                true_rot = Lodom_synch[k].rot
                pose_bl = Lbl_synch[k]
                tf_rts = geometry_msgs.msg.TransformStamped()
                tf_tranf = L_map_synch[k]
                self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
                tf_rts.header.stamp = tf_tranf.header.stamp
                tf_rts.transform.translation.x = tf_tranf.mapToOdom.translation.x + pose_bl.pos[0]-0.03
                tf_rts.transform.translation.y = tf_tranf.mapToOdom.translation.y + pose_bl.pos[1]
                tf_rts.transform.translation.z = tf_tranf.mapToOdom.translation.z + pose_bl.pos[2]+0.18
                        
                x0,y0,z0,w0 = tf_tranf.mapToOdom.rotation.x,tf_tranf.mapToOdom.rotation.y,tf_tranf.mapToOdom.rotation.z,tf_tranf.mapToOdom.rotation.w
                
                q = tf.transformations.quaternion_from_euler(pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2])
                
                x1,y1,z1,w1 = q[0],q[1],q[2],q[3]
                        
                M_quat = np.array((
                    x1*w0 + y1*z0 - z1*y0 + w1*x0,
                    -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                    x1*y0 - y1*x0 + z1*w0 + w1*z0,
                    -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)
                        
                tf_rts.transform.rotation.x = M_quat[0]
                tf_rts.transform.rotation.y = M_quat[1]
                tf_rts.transform.rotation.z = M_quat[2]
                tf_rts.transform.rotation.w = M_quat[3]
                self.evaluateProximityToTruth_rts(true_pos,true_rot,tf_rts,k)



    def pub_rts_err_L(self,L_map_synch,Lbl_synch,Lodom_synch):
        ######## TRY ONE
        
        # Lind = [k for k in range(self.nb_diff) if Lbl_synch[k]!= None]

        # L_real_odom = [Lodom_synch[Lind[j]] for j in range(len(Lind))]
        # L_real_bl = [Lbl_synch[Lind[j]] for j in range(len(Lind))]
        # L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        # Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # mini = min(Lrt)
        # print("Before : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])
        # print("With times : ", Lrt)
        
        # Li = []
        # i = 1
        # Lrttemp = [ m for m in Lrt]
        # mini = min(Lrt)
        # while len(Li) != len(Lind):

        #     Loi = [Lrttemp[j] - mini -i*self.rate_rts/2 for j in range(len(Lrt))]
        #     Loi_ok = [Loi.index(a) for a in Loi if a < 0]
        #     if len(Loi_ok) > 0:
        #         Lttemp = [ Lrt[a] for a in Loi_ok]
        #         vs = Lrt.index(min(Lttemp))
        #         for l in Loi_ok:
        #             print(L_real_odom[l].time,l,vs,L_real_odom[vs].time)
        #             L_real_odom[l] = L_real_odom[vs]
        #             Li.append(l)
        #             Lrttemp[l] = max(Lrttemp) + (i+1)*self.rate_rts/2
        #     i+=1
                
        # print(Lrttemp)
        # print(  Lrt)
            
        # Li = []
        # i = 1
        # Lrttemp2 = [ m for m in Lrt]
        # #print(Lrttemp2)
        # while len(Li) != len(Lind):
        #     Loi = [Lrttemp2[j] - mini -i*self.rate_rts/2 for j in range(len(Lrt))]
        #     Loi_ok = [Loi.index(a) for a in Loi if a < 0]
        #     if len(Loi_ok) > 0:
        #         #print(Loi)
        #         #print(Loi_ok)
        #         Lttemp = [ Lrt[a] for a in Loi_ok]
        #         vs = Lrt.index(min(Lttemp))
        #         for l in Loi_ok:
        #             L_real_bl[l] = L_real_bl[vs]
        #             #print(L_real_bl[l].header.stamp.to_sec(),l,vs,L_real_bl[vs].header.stamp.to_sec())
        #             Li.append(l)
        #             Lrttemp2[l] = max(Lrttemp2) + (i+1)*self.rate_rts/2
        #     #print("i+1")
        #     i+=1                
        #print("After : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])        
        
        # for k in range(self.nb_diff):
        #     if k in Lind:
        #         Lbl_synch[k] = L_real_bl[k]
        #         Lodom_synch[k] = L_real_odom[k]
                
        #print("After : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])
        
        ########## TRY TWO
        
        # ZBL = geometry_msgs.msg.TransformStamped()
        # ZBL.transform.translation.x = 0
        # ZBL.transform.translation.y = 0
        # ZBL.transform.translation.z = 0
        
        # ZBL.transform.rotation.x = 0
        # ZBL.transform.rotation.y = 0
        # ZBL.transform.rotation.z = 0
        # ZBL.transform.rotation.w = 1
        
        # ZO = transformStampedEuler()
        # ZO.pos = np.array([0,0,0])
        # ZO.rot = np.array([0,0,0])
        
        # Lind = [k for k in range(self.nb_diff) if Lbl_synch[k]!= None]
        # L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        # Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # vs = Lrt.index(min(Lrt))
        # blmin = Lbl_synch[vs]
        
        # for k in range(self.nb_diff):
        #     if (Lbl_synch[k]!=None):
        #         Lodom_synch[k] = ZO
        #         Lbl_synch[k] = blmin
        
        Lind = []
        for k in range(self.nb_diff):
            if (Lbl_synch[k] != None and Lodom_synch[k] != None ):
                Lind.append(k)        

        L_real_odom = [Lodom_synch[Lind[j]] for j in range(len(Lind))]
        L_real_bl = [Lbl_synch[Lind[j]] for j in range(len(Lind))]
        L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # mini = min(Lrt)
        try :
            print("BL : ", [L_real_bl[k].time for k in range(len(L_real_bl))])
            print("Odom : ", [L_real_odom[k].time for k in range(len(L_real_odom))])
            print("With times : ", Lrt)
        except AttributeError:
            print("AE!")
            print("BL  : ", L_real_odom)
            print("Odom :", L_real_bl)
            pass
        
        for k in range(self.nb_diff):
            if (L_map_synch[k] != None and Lbl_synch[k] != None and Lodom_synch != None):
                true_pos = Lodom_synch[k].pos
                true_rot = Lodom_synch[k].rot
                pose_bl = Lbl_synch[k]
                tf_rts = geometry_msgs.msg.TransformStamped()
                tf_tranf = L_map_synch[k]
                self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
                tf_rts.header.stamp = tf_tranf.header.stamp
                tf_rts.transform.translation.x = tf_tranf.transform.translation.x + pose_bl.transform.translation.x-0.03
                tf_rts.transform.translation.y = tf_tranf.transform.translation.y + pose_bl.transform.translation.y
                tf_rts.transform.translation.z = tf_tranf.transform.translation.z + pose_bl.transform.translation.z+0.18
                        
                x0,y0,z0,w0 = tf_tranf.transform.rotation.x,tf_tranf.transform.rotation.y,tf_tranf.transform.rotation.z,tf_tranf.transform.rotation.w
                x1,y1,z1,w1 = pose_bl.transform.rotation.x,pose_bl.transform.rotation.y,pose_bl.transform.rotation.z,pose_bl.transform.rotation.w
                        
                M_quat = np.array((
                    x1*w0 + y1*z0 - z1*y0 + w1*x0,
                    -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                    x1*y0 - y1*x0 + z1*w0 + w1*z0,
                    -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)
                        
                tf_rts.transform.rotation.x = M_quat[0]
                tf_rts.transform.rotation.y = M_quat[1]
                tf_rts.transform.rotation.z = M_quat[2]
                tf_rts.transform.rotation.w = M_quat[3]
                self.evaluateProximityToTruth_rts(true_pos,true_rot,tf_rts,k)

    def check_jump(self,current_lin_speed,current_ang_speed,
                   prev_lin_speed,prev_ang_speed,
                   current_lin_pos,current_ang_pos,
                   prev_lin_pos,prev_ang_pos,
                   current_time,prev_time,current_detect_jump):
        """
        
        Principe : 
            - prendre vitesse (current_lin_speed, current_ang_speed) et position odométrique actuelle (current_lin_pos,current_ang_pos)
            - obtenir la vitesse (angulaire et linéaire) estimée, en comparant la position précédente
            à la position actuelle. (inutile maintenant)
            Cela permet d'avoir une estimation de la vitesse réelle estimée du robot.
            - obtenir la position (angulaire et linéaire) estimée, à partir de la vitesse précédente
            et la position réelle précédente. 
            - Evaluer la différence entre les positions estimées à partir des mesures précédentes et les positions actuelles.
            Si trop importante, veut dire qu'il y a une erreur : position actuelle ne colle
            pas par rapport à estimation de la vitesse et position précédente. 
        
        """
        detect_jump = False
        if current_detect_jump == True:
            return(current_detect_jump)
        
        if (type(prev_time) != type(None)):
            delay = current_time - prev_time
            estimated_speed = (current_lin_pos - prev_lin_pos)/delay
            estimated_ang = (current_ang_pos-prev_ang_pos)/delay
            
            estimated_pose = current_lin_speed*delay + prev_lin_pos
            # suppress z component if inferior to threshold
            if estimated_pose[2] < self.th_tlp_pos:
                estimated_pose[2] = 0
            estimated_rot = current_ang_speed*delay + prev_ang_pos
            
            rot_diff  = np.linalg.norm(estimated_rot-current_ang_pos) 
            if (rot_diff > np.pi):
                if (rot_diff//(2*np.pi)==0 ):
                    rot_diff =abs( rot_diff - 2*np.pi)
                else:
                    rot_diff = rot_diff%(2*np.pi)

            if (np.linalg.norm(estimated_pose - current_lin_pos) > self.th_tlp_pos  
                or rot_diff > self.th_tlp_rot):
                detect_jump = True
                # print("speed : ", np.linalg.norm(new_speed - estimated_speed ))
                # print("angular : ", np.linalg.norm(new_ang-estimated_ang))
                #print("speed : ", estimated_speed )
                #print("angular : ", estimated_ang)
                print("diff_pose : ", np.linalg.norm( current_lin_pos - estimated_pose ))
                #print("pose : ",  estimated_pose )
                #print("rotation : ", estimated_rot )      
                print("diff_rot : ", rot_diff )
        
        return(detect_jump)

    def callback_odom_jump(self,msg):
        # ancienne méthode pour estimer les téléportations à partir de l'odométrie. N'est plus utilisée

        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        new_speed = np.array([msg.twist.twist.linear.x, msg.twist.twist.linear.y, msg.twist.twist.linear.z])
        new_ang = np.array([msg.twist.twist.angular.x,msg.twist.twist.angular.y,msg.twist.twist.angular.z])
        timens = msg.header.stamp.to_sec()

        self.odom_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_odom_speed,self.prev_odom_ang,
                                           new_pose,new_rot,
                                           self.prev_odom_pos,self.prev_odom_rot,
                                           timens,self.prev_odom_time,self.odom_jump)

        self.prev_odom_pos = new_pose
        self.prev_odom_rot = new_rot
        self.prev_odom_speed = new_speed
        self.prev_odom_ang = new_ang
        self.prev_odom_time = timens
    
    def callback_initialpose_jump(self,msg):
        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        if (type(self.prev_initialpose_pos) != type(None) and (np.linalg.norm(self.prev_initialpose_pos-new_pose ) > 0 or self.linalg.norm(self.prev_initialpose_rot-new_rot) >0) ):
            if (self.pose_jump == False):
                self.pose_jump = True
        elif type(self.prev_initialpose_pos) == type(None):
            self.pose_jump = True
        self.prev_initialpose_pos = new_pose
        self.prev_initialpose_rot = new_rot

    
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation

        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        true_speed = msg.twist[self.num_rosbot].linear
        true_ang = msg.twist[self.num_rosbot].angular
        new_speed = np.array([true_speed.x, true_speed.y, true_speed.z])
        new_ang = np.array([true_ang.x,true_ang.y,true_ang.z])
        timens = rospy.get_time()
        
        self.gazebo_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_gaz_speed,self.prev_gaz_ang,
                                           new_pose,new_rot,
                                           self.prev_gaz_pos,self.prev_gaz_rot,
                                           timens,self.prev_gaz_time,self.gazebo_jump)
        
        self.prev_gaz_pos = new_pose
        self.prev_gaz_rot = new_rot
        self.prev_gaz_speed = new_speed
        self.prev_gaz_ang = new_ang
        self.prev_gaz_time = timens
        
        
        #print(rospy.get_time() - self.last_time_pose)
        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            
            self.last_time_pose = rospy.get_time()
            time = self.last_time_pose
            if (self.buf_access):
                tf_true = transformStampedEuler()
                tf_true.pos = true_pos
                tf_true.rot = true_rot
                tf_true.time = time
                self.buf_tf_true.append(tf_true)
                # if (len(self.buf_tf_true) > 1):
                #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
        
            elif (self.temp_buf_access):
                tf_true = transformStampedEuler()
                tf_true.pos = true_pos
                tf_true.rot = true_rot
                tf_true.time = time
                self.temp_buf_tf_true.append(tf_true)
            
        
        return(None)


    def callback_get_last_truth(self,msg):
        
        time = rospy.get_time()
        true_pos = msg.trans_tr
        true_rot = msg.rot_tr    
        if (self.buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            try:
                tf_true.time = msg.stamp.to_sec()
            except Exception:
                tf_true.time = time
            for k in range(self.nb_diff):
                self.buf_all_tf_true[k].append(tf_true)
            #self.buf_tf_true.append(tf_true)
            # if (len(self.buf_tf_true) > 1):
            #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
    
        elif (self.temp_buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            try:
                tf_true.time = msg.stamp.to_sec()
            except Exception:
                tf_true.time = time
            for k in range(self.nb_diff):
                self.temp_buf_all_tf_true[k].append(tf_true)
            #self.temp_buf_tf_true.append(tf_true)

        
        return(None)    

    def callback_get_last_truth2(self,msg):
        
        time = rospy.get_time()
        true_pos = msg.trans_tr
        true_rot = msg.rot_tr    
        if (self.buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            try:
                tf_true.time = msg.stamp.to_sec()
            except Exception:
                tf_true.time = time
            self.buf_tf_true.append(tf_true)
            if (len(self.buf_tf_true) > self.qs_odom):
                del(self.buf_tf_true[0])

            #self.buf_tf_true.append(tf_true)
            # if (len(self.buf_tf_true) > 1):
            #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
    
        elif (self.temp_buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = true_pos
            tf_true.rot = true_rot
            try:
                tf_true.time = msg.stamp.to_sec()
            except Exception:
                tf_true.time = time
            self.temp_buf_tf_true.append(tf_true)
            if (len(self.temp_buf_tf_true) > self.qs_odom):
                del(self.temp_buf_tf_true[0])

        
        return(None)    


    def prepare_to_send(self):
        
        
        self.buf_access_map = False
        self.temp_buf_access_map = True
        self.buf_access_bl = False
        self.temp_buf_access_bl = True
        self.buf_access_true = False
        self.temp_buf_access_true = True
        # ind_deb = self.ind
        
        lms = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
        L_map_synch = []
                #print("inside")
        if True : # (np.all(lms == self.prev_map_synch_size) == False):
                    #print("Check map ...")
            L_map_synch,L_synch,LMT = self.check_synch_map3(lms)
        if True : #(len(L_map_synch) == self.nb_diff):

                self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
                    #LMT = [L_map_synch[j].header.stamp.to_sec() for j in range(self.nb_diff)]
                    
                    # synchro avec base_link
                    # print("----------NEW TEST-----------")
                    # print(L_map_synch)
                    # print("-------------------------")
                    # print(self.buf_tf_bl[-5:])
                bl_synch, odom_synch = [],[]
                bl_check, odom_check = [],[]
                    
                #odom_synch,odom_i = self.check_synch_odom(self.buf_tf_true,self.map_time_synch)
                odom_synch,odom_i = self.check_synch_odom_L(self.buf_all_tf_true,LMT)
                #odom_synch,Lodom_i = self.check_synch_odom_L2(self.buf_all_tf_true,LMT)
                    #odom_s,odom_i = self.check_synch_odom(self.buf_tf_true,self.map_time_synch)
                #print("Odom :", odom_synch)
                   
                if np.all([odom_synch[k] != None for k in range(len(odom_synch))]):
                        
                    # if (len(odom_synch) ) == self.nb_diff:
                        #odom_synch = [odom_s[-1] for k in range(self.nb_diff)]
                        #print("Odom ok!")
                    #self.len_buf_true = np.array([-1 for k in range(self.nb_diff)])
                    odom_check = odom_synch
                    # synchro avec odom
                        #bl_synch,bl_i = self.check_synch_bl(self.buf_tf_bl,self.map_time_synch)
                    bl_synch,bl_i = self.check_synch_bl_L(self.buf_all_tf_bl,LMT)
                    #bl_synch,Lbl_i = self.check_synch_bl_L2(self.buf_all_tf_bl,LMT)
                    if np.all([bl_synch[k] != None for k in range(len(bl_synch))]) :
                        #print("blsy : ", bl_synch)
                        #print(bl_synch)
                        # if (len(bl_synch) ) == self.nb_diff:
                            #print("BL ok!")
                        #self.len_buf_bl = np.array([-1 for k in range(self.nb_diff)])
                        bl_check = bl_synch
                            #print(len(bl_check),len(odom_check))
                        # else : 
                        #     self.prev_map_synch_success = -1
                    # else:
                    #     self.prev_map_synch_success = -1
                        
                if (min(len(bl_check),len(odom_check)) > 0 and min([ len(self.buf_all_tf_map[k]) for k in range(self.nb_diff)]) > 0):
                    #print(len(bl_check) , len(odom_check) )
                    #if (min(len(bl_check),len(odom_check)) == self.nb_diff):
                        # publish synchronized topics
                        # print(bl_check)
                        # print(odom_check)
                    self.pub_rts_err_L(L_map_synch,bl_check,odom_check)
                    #self.pub_rts_err_L2(L_map_synch,bl_check,odom_check)
                        #self.pub_rts_err(L_map_synch,bl_synch,odom_synch)
                    l = 0
                    stol = 0
                    Lt = []
                    for k in range(self.nb_diff):
                        if (len(self.L_all_time[k]) > 1 and LMT[k]!= -1):
                            l+=1
                            tol = (self.L_all_time[k][-1] - self.L_all_time[k][-2])%self.rate_rts
                            if (tol > self.rate_rts/2): 
                                tol = abs(tol - self.rate_rts)
                            stol += tol
                                
                        if LMT[k] != -1:
                            Lt.append(LMT[k])
                                
                    ltot = sum([len(self.L_all_time[k]) for k in range(self.nb_diff)])
                    if ltot!= 0:
                        self.tol = (self.tol*(ltot-l)+stol)/(ltot)
                            
                    mtime = min(Lt)
                    print("////////////// Publication at time : ",mtime," completed! Indice : %i \\\\\\\\\\\\\\\\\\"%self.ind)
                    self.ind += 1
                    print("Updated tolerance : ", self.tol)
                    #self.tol = 0.0
                        
                    ic = self.icheck
                    self.pcheck = ic
                    self.icheck = -1
                    self.send = True
                    self.dispok = self.nb_diff
                        #clean up buffers
                    # if (self.desynch)<2:
                    #     self.desynch +=1
                        
                    # self.prev_pose_access = False
                    for k in range(self.nb_diff):
                            #print("Before : ", self.buf_all_tf_map[k])
                        val = L_synch[k]
                        if val!= None and bl_check[k] != None and odom_check[k] != None:
                                #print("Delete %i !"%k)
                            try : 
                                #self.L_prev_poses[k][-1] = self.buf_all_tf_map[k][val]
                                    
                                self.buf_all_tf_map[k] = self.buf_all_tf_map[k][val:]
                                if val <= 0:
                                    del(self.buf_all_tf_map[k][val])
                            except IndexError:
                                print("Error Index : ", L_synch[k], len(self.buf_all_tf_map[k]), k)
                                if len(self.buf_all_tf_map[k]) == 0:
                                    pass
                                
                            except Exception:
                                print("Other type of error!")
                            
                            #print("After : ", self.buf_all_tf_map[k])
                    self.prev_pose_access = True
                    
                    # for k in range(self.nb_diff):
                    #     if bl_check[k]!=None :
                    #         bl_i = Lbl_i[k]
                    #         self.buf_all_tf_bl[k] = self.buf_all_tf_bl[k][bl_i:]
                    #     if odom_check[k] != None:
                    #         odom_i = Lodom_i[k]
                    #         self.buf_all_tf_true[k] = self.buf_all_tf_true[k][odom_i:]
                        #self.nmap = 0  
                    self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
                    self.map_time_synch = -1
                        
            
        self.temp_buf_access_map = False
        # if ind_deb == self.ind:
        #     self.tol+= 0.1
        
                #add up temporary buffers
        for k in range(self.nb_diff):
            if (len(self.buf_all_tf_map[k]) > 0):
                test = False
                while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                                #print("Treating suppression")
                    p1 = self.temp_buf_all_tf_map[k][0].mapToOdom.translation    
                    nw_pose = np.array([p1.x, p1.y, p1.z])
                    p2 = self.buf_all_tf_map[k][-1].mapToOdom.translation
                    od_pose = np.array([p2.x, p2.y, p2.z])
                    if (self.L_prev_poses[k][-1] != None):
                        p3 = self.L_prev_poses[k][-1].mapToOdom.translation
                        od_pose2 =  np.array([p3.x, p3.y, p3.z])
                    else:
                        od_pose2 = nw_pose - 1
                    if (np.all(nw_pose==od_pose) == True) or (np.all(nw_pose == od_pose2) == True):
                                    #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                                    #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_all_tf_map[k][0])
                    else:
                        test = True
            elif (self.L_prev_poses[k][-1] != None):
                test = False
                while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                    p1 = self.temp_buf_all_tf_map[k][0].mapToOdom.translation    
                    nw_pose = np.array([p1.x, p1.y, p1.z])   
                    p3 = self.L_prev_poses[k][-1].mapToOdom.translation
                    od_pose2 =  np.array([p3.x, p3.y, p3.z])
                    if  (np.all(nw_pose == od_pose2) == True):
                                    #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                                    #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_all_tf_map[k][0])
                    else:
                        test = True

            len_temp_map = len(self.buf_all_tf_map[k])
            self.buf_all_tf_map[k] = self.buf_all_tf_map[k][len_temp_map:]
            self.buf_all_tf_map[k] += self.temp_buf_all_tf_map[k]
            self.temp_buf_all_tf_map[k] = []
                    
                # if (self.send):
                #     print("Bufffer after send : ", self.buf_all_tf_map)
                        
                #     if(self.desynch == 1):
                           
                #         print("L_prev_poses (D=1) : ", self.L_prev_poses)
                    #self.send = False
        self.buf_access_map = True
        self.temp_buf_access_bl = False
        if len(self.temp_buf_tf_bl) > 0:
            test= False
            while len(self.temp_buf_tf_bl) > 0 and test == False:
                p1 = self.temp_buf_all_tf_bl[0].pos
                n_pose = p1
                p2 = self.buf_all_tf_bl[-1].pos
                o_pose = p2
                if (np.all(n_pose==o_pose) == True):
                                #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                                #print("Because of : ",  self.buf_all_tf_map[k][-1])
                    del(self.temp_buf_tf_bl[0])
                else:
                    test= True
        len_temp_bl = len(self.buf_tf_bl)  
        self.buf_tf_bl = self.buf_tf_bl[len_temp_bl:]        
        self.buf_tf_bl += self.temp_buf_tf_bl
        self.temp_buf_tf_bl  = []

           
        self.buf_access_bl = True
            
        self.temp_buf_access_true = False
        if len(self.temp_buf_tf_true) > 0:
            test= False
            while len(self.temp_buf_tf_true) > 0 and test == False:
                p1 = self.temp_buf_all_tf_true[k][0].pos
                n_pose = p1
                p2 = self.buf_tf_true[k][-1].pos
                o_pose = p2
                if (np.all(n_pose==o_pose) == True):
                                #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                                #print("Because of : ",  self.buf_all_tf_map[k][-1])
                    del(self.temp_buf_all_tf_true[k][0])
                else:
                    test= True
                        
                        
        len_temp_true = len(self.buf_tf_true)  
        self.buf_tf_true = self.buf_tf_true[len_temp_true:]
        self.buf_tf_true += self.temp_buf_tf_true
        self.temp_buf_tf_true = []

        
        
        self.buf_access_true = True
        


    def evaluateProximityToTruth_rts(self,true_pos,true_rot,transformStamped_rts,k):  
        """
        Ici, on prend le lien map_gazebo --> base_link (exprimé par true_pos et true_rot), et on y rajoute
        le lien base_link --> camera2 pour pouvoir le comparer avec la transformation map --> camera_link2
        
        k : transformation rtabmap k testée
        """
        if True:
            if (self.type_data == "raw"):
                trans_tr = np.array([true_pos.x-0.03,true_pos.y,true_pos.z+0.18]) #+0.14
                euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
                roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]

                true_trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
                rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
                
            elif (self.type_data == "record"):
                 trans_tr = np.array([true_pos[0]-0.03,true_pos[1],true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
                 true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
                 rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
                 
            # obtain the norm of translation and rotation
            euler_rts = tf.transformations.euler_from_quaternion((transformStamped_rts.transform.rotation.x,
                                                                 transformStamped_rts.transform.rotation.y,
                                                                 transformStamped_rts.transform.rotation.z,
                                                                 transformStamped_rts.transform.rotation.w))
            roll_rts, pitch_rts, yaw_rts= euler_rts[0],euler_rts[1],euler_rts[2]
            

                

            
            trans_rts = np.array([transformStamped_rts.transform.translation.x,transformStamped_rts.transform.translation.y,transformStamped_rts.transform.translation.z])           
            rot_rts = np.array([roll_rts,pitch_rts,yaw_rts])

            trans_rtsTr = trans_tr-trans_rts
            
            rot_rtsTr = rot_tr-rot_rts
            for l in range(3):  
                if (rot_tr[l]*rot_rts[l]<0):
                    rot_rtsTr[l] = min([abs(rot_rts[l]-rot_tr[l]),abs(rot_rts[l]+rot_tr[l]),abs(rot_tr[l]-rot_rts[l]),abs(rot_tr[l]+rot_rts[l])])

            diff_norm_trans = np.linalg.norm(trans_rtsTr)
            diff_norm_rot = np.linalg.norm(rot_rtsTr)
            
            
            
            # publish it
            msg = comp_rts()
            msg.stamp = transformStamped_rts.header.stamp
            msg.trans_rts = list(trans_rts)
            msg.trans_tr = list(trans_tr)
            msg.rot_rts = list(rot_rts)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot
            msg.pose_jump = self.pose_jump
            msg.gazebo_jump = self.gazebo_jump
            self.L_pub_comp[k].publish(msg)
            
            self.L_all_trans_err[k].append(diff_norm_trans)
            self.L_all_rot_err[k].append(diff_norm_rot)

            self.pose_jump = False
            self.prev_gaz_pos  = true_trans_tr
            self.prev_gaz_rot = rot_tr
            self.gazebo_jump = False
            self.odom_jump = False

        return(None)
    
    def display_errors(self,msg):
        if (msg.true_false):
            colors = ['R','G','B','orange','black','yellow','pink','purple']
            fig,axs = plt.subplots(2, 1)
            print([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            #ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            # ori_time = self.L_time_synch[0]
            # Ltime = [self.L_time_synch[l]-ori_time for l in range(length)]
            Lind = [l for l in range(length)]
            for k in range(self.nb_diff):
                #Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                # print(self.L_all_trans_err[k][:length])
                # print(self.L_time[:length])
                # print(colors[k])
                axs[0,].plot(Lind,self.L_all_trans_err[k][:length], label="test nbe %i"%k, color = colors[k])
                axs[1,].plot(Lind,self.L_all_rot_err[k][:length],  label="test nbe %i"%k, color = colors[k])
            # ok
            axs[0,].set_xlabel("Pose index")
            axs[0,].set_ylabel("diff_trans_total (m)")
            axs[0,].set_title("Translational error in time")
            axs[0,].legend()
            axs[1,].set_xlabel("Pose index")
            axs[1,].set_ylabel("diff_rot_total (rad)")
            axs[1,].set_title("Rotational error in time")
            axs[1,].legend()
            plt.show()
        return(True)

    def display_max_errors(self,msg):
        if (msg.true_false):
            
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            L_means_trans = []
            L_stds_trans = []
            L_means_rot = []
            L_stds_rot = []   
            L_mins_rot = []
            L_mins_trans = []
            L_maxs_rot = []
            L_maxs_trans = []
            

            
            for k in range(self.nb_diff):
                Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                L_means_trans.append(np.mean(self.L_all_trans_err[k]) )
                L_stds_trans.append(np.std(self.L_all_trans_err[k]))
                L_means_rot.append(np.mean(self.L_all_rot_err[k]) )
                L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
                min_rot_k = np.min(self.L_all_rot_err[k])
                t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
                L_mins_rot.append([min_rot_k,t_min_rot_k])
                
                max_rot_k = np.max(self.L_all_rot_err[k])
                t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
                L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
                min_trans_k = np.min(self.L_all_trans_err[k])
                t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
                L_mins_trans.append([min_trans_k,t_min_trans_k])
                
                max_trans_k = np.max(self.L_all_trans_err[k])
                t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
                L_maxs_trans.append([max_trans_k,t_max_trans_k])       
           
            L_class_mean_trans = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_trans)):
                val_clas = L_means_trans[i]
                clas = 0
                for j in range(len(L_means_trans)):
                    if val_clas > L_means_trans[j]:
                        clas+=1
                L_class_mean_trans[i] = clas
                
            L_class_mean_rot = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_rot)):
                val_clas = L_means_rot[i]
                clas = 0
                for j in range(len(L_means_rot)):
                    if val_clas > L_means_rot[j]:
                        clas+=1
                L_class_mean_rot[i] = clas                
               
            print("------------ Max errors -------------")
            print(" Translation Max Error Statistics ")
            Lmt2 = [ L_mins_trans[k][0] for k in range(len(L_mins_trans))]
            LMt2 = [ L_maxs_trans[k][0] for k in range(len(L_maxs_trans))]
            print("By mean : %f | By max : %f | By min : %f "%(max(L_means_trans) - min(L_means_trans) , max(LMt2) - min(LMt2) , max(Lmt2) - min(Lmt2)  ) )
            print("********************************")
            Lmr2 = [ L_mins_rot[k][0] for k in range(len(L_mins_rot))]
            LMr2 = [ L_maxs_rot[k][0] for k in range(len(L_maxs_rot))]
            print(" Rotational Max Error Statistics ")
            print("By mean : %f | By max : %f | By min : %f "%(max(L_means_rot) - min(L_means_rot) , max(LMr2) - min(LMr2) , max(Lmr2) - min(Lmr2)             ) )
               
        return(True)   


    def display_statistics(self,msg):
        if (msg.true_false):
            
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            L_means_trans = []
            L_stds_trans = []
            L_means_rot = []
            L_stds_rot = []   
            L_mins_rot = []
            L_mins_trans = []
            L_maxs_rot = []
            L_maxs_trans = []
            
            for k in range(self.nb_diff):
                Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                L_means_trans.append(np.mean(self.L_all_trans_err[k]) )
                L_stds_trans.append(np.std(self.L_all_trans_err[k]))
                L_means_rot.append(np.mean(self.L_all_rot_err[k]) )
                L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
                min_rot_k = np.min(self.L_all_rot_err[k])
                t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
                L_mins_rot.append([min_rot_k,t_min_rot_k])
                
                max_rot_k = np.max(self.L_all_rot_err[k])
                t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
                L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
                min_trans_k = np.min(self.L_all_trans_err[k])
                t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
                L_mins_trans.append([min_trans_k,t_min_trans_k])
                
                max_trans_k = np.max(self.L_all_trans_err[k])
                t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
                L_maxs_trans.append([max_trans_k,t_max_trans_k])       
           
            L_class_mean_trans = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_trans)):
                val_clas = L_means_trans[i]
                clas = 0
                for j in range(len(L_means_trans)):
                    if val_clas > L_means_trans[j]:
                        clas+=1
                L_class_mean_trans[i] = clas
                
            L_class_mean_rot = [0 for k in range(self.nb_diff)]
            for i in range(len(L_means_rot)):
                val_clas = L_means_rot[i]
                clas = 0
                for j in range(len(L_means_rot)):
                    if val_clas > L_means_rot[j]:
                        clas+=1
                L_class_mean_rot[i] = clas                
               
            print("------------ Statistics -------------")
            print(" Translation Error Statistics ")
            for k in range(self.nb_diff):
                print("Test nbe : %i | Global Mean Translational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,L_means_trans[k],L_stds_trans[k],L_mins_trans[k][0],L_mins_trans[k][1],L_maxs_trans[k][0],L_maxs_trans[k][1],L_class_mean_trans[k]) )
            print("********************************")
            print(" Rotational Error Statistics ")
            for k in range(self.nb_diff):
                print("Test nbe : %i | Global Mean Rotational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,L_means_rot[k],L_stds_rot[k],L_mins_rot[k][0],L_mins_rot[k][1],L_maxs_rot[k][0],L_maxs_rot[k][1],L_class_mean_rot[k]) )

        return(True)    
    
    def save_data(self,msg):
        name = msg.path + "/" + msg.map_name + ".csv"
        f = open(name,'w')
        with f:
            fnames = ["Trans_err","Rot_err","Associated_time"]
            writer = csv.DictWriter(f,fieldnames=fnames)
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            for k in range(self.nb_diff):
                writer.writerow({"Trans_err" : "Test nbe %i"%k, "Rot_err" : " ", "Associated_time" : " " })
                L_trans_k = self.L_all_trans_err[k]
                L_rot_k = self.L_all_rot_err[k]
                L_time = self.L_all_time[k]
                for l in range(length):
                    writer.writerow({"Trans_err" : L_trans_k[l], "Rot_err" : L_rot_k[l], "Associated_time" : L_time[l]})
                writer.writerow({"Trans_err" : " ", "Rot_err" : " ", "Associated_time" : " "})

        return(True)    

    def save_data2(self,msg):
        name = msg.path + "/" + msg.map_name + ".csv"
        
            
        f = open(name,'w')
        with f:
            
            fnames = []
            for k in range(1,self.nb_diff+1):
                fnames += ["Trans_err_%i"%k,"Rot_err_%i"%k,"Associated_time_%i"%k," "]
            fnames += ["Max difference in time between everyone"]
            
            
            writer = csv.writer(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
            writer.writerow(fnames)
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            for l in range(length):
                Lt = [ self.L_all_time[i][l] for i in range(len(self.L_all_time))]
                diff = max(Lt)-min(Lt)
                L = []
                for k in range(self.nb_diff):
                    L += [self.L_all_trans_err[k][l],self.L_all_rot_err[k][l],self.L_all_time[k][l]," "]
                L+= [diff]
                writer.writerow(L)
            
        return(True)        


    def __init__(self,rate):

        """
        
        Constructor of the evaluateNodesRts class 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        """
        rospy.loginfo("INIT")

        self.period = 1.0/rate

        self.type_data= rospy.get_param("~type_data")
        self.nb_diff =rospy.get_param("~nb_diff")

        self.display_service = rospy.Service('display_errors', boolean, self.display_errors)
        self.display_max_err = rospy.Service('display_max_errors', boolean, self.display_max_errors)
        self.display_stats = rospy.Service('display_statistics', boolean, self.display_statistics)
        self.save_dta = rospy.Service('save_data',save_name , self.save_data)
        self.save_dta5 = rospy.Service('save_data2',save_name , self.save_data2)
        
        if self.type_data == "raw":            
            self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
        elif self.type_data == "record":
            self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth)

        self.initialPoseTopic = rospy.Subscriber("/rtabmap/initialpose", PoseWithCovarianceStamped, self.callback_initialpose_jump)

        self.odom_topic = rospy.Subscriber("/odom",Odometry, self.callback_odom)

        

        ## Parameters for callback_gazebo
        self.detect_name = False
        self.num_rosbot = -1
        self.prev_gaz_pos = None
        self.prev_gaz_rot = None
        self.prev_gaz_speed = None
        self.prev_gaz_ang = None
        self.prev_gaz_time = None
        
        ## Parameters for callback_initialpose_jump      
        self.prev_initialpose_pos = None
        self.prev_initialpose_rot = None   
        self.pose_jump = False

        ## Parameters for check_jump  
        self.th_tlp_pos = 0.175
        self.th_tlp_rot = 0.055

        # Parameters for evaluateProximityToTruth_rts
        self.odom_jump = False
        self.gazebo_jump = False

        # Parameters for buffer and buffer access
        self.buf_access = True
        self.temp_buf_access = False
        
        self.buf_access_map = True
        self.temp_buf_access_map = False

        self.buf_access_truth = True
        self.temp_buf_access_truth = False

        self.buf_access_bl = True
        self.temp_buf_access_bl = False

        self.buf_tf_true = []
        self.buf_all_tf_map = []
        self.buf_tf_bl = []
        
        self.temp_buf_tf_true = []
        self.temp_buf_all_tf_map = []
        self.temp_buf_tf_bl = []        

        self.buf_all_tf_true = []
        self.buf_all_tf_bl = []
        
        self.temp_buf_all_tf_true = []
        self.temp_buf_all_tf_bl = []        
        
        self.L_pub_comp = []
        self.L_map_name = []
        
        self.L_sub_rtab = []
        
        # Parameters to save datas 
        
        self.L_all_trans_err = []
        self.L_all_rot_err = []
        self.L_all_time = []
        self.L_time_synch = []
        self.pcheck = None
        
        # Parameters to manage the publication of rts data

        # self.ref_i_map = -1
        # if (self.nb_diff%2) == 0:
        #     self.ref_i_map = self.nb_diff//2-1
        # else:
        #     self.ref_i_map = self.nb_diff//2
        # self.int_synch_map = -1

        #self.nmap = 0

        self.rate_rts = 2.0
        self.rate_odom = 1.0+1e-3
        self.rate_bl = 0.1+1e-3
        self.tol = 0.0
        
        self.qs_bl = int((self.nb_diff + self.rate_rts)/(self.rate_bl))
        self.qs_odom = int((self.nb_diff + self.rate_rts)/(self.rate_odom))
        self.qs_rts = int((self.nb_diff + self.rate_rts)/self.rate_rts)

        self.buf_tf_true = []#[None for k in range(self.qs_odom)]
        self.buf_tf_bl = []#[None for k in range(self.qs_bl)]
        
        


        #self.desynch_lim = (self.rate_rts/10)*self.nb_diff# ecart temporel max pour synchro des topics map
        

        # Parameters to manage the progression of the synchrnization process
        
        self.icheck = None
        self.map_time_synch = -1
        # self.prev_map_synch_success = -1
        self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
        self.len_buf_true = -1
        self.len_buf_bl = -1
        
        self.err_odom = False
        self.err_bl = False
        

        # Parameters to manage the publication of informations abut the synchronization process
        
        self.dispok = self.nb_diff
        self.dispm1 = self.nb_diff
        self.dispm2 = self.nb_diff
        self.dispm3 = self.nb_diff
        self.send = False
        
        self.L_prev_poses = []
        
        self.L_disp_odom = []
        self.L_disp_bl = []
        self.L_ec_odom = []
        self.L_ec_bl = []
        self.sub_tf = rospy.Subscriber("/tf",tfMessage,self.callback_tf2)
        for k in range(1,self.nb_diff+1):
            self.L_map_name.append("map%i"%k)
            # self.buf_all_tf_map.append([])
            self.buf_all_tf_map.append([])
            self.temp_buf_all_tf_map.append([])              
            self.buf_all_tf_bl.append([])
            self.temp_buf_all_tf_bl.append([])     
            self.buf_all_tf_true.append([])
            self.temp_buf_all_tf_true.append([])    
            self.L_all_trans_err.append([])
            self.L_all_time.append([])
            self.L_all_rot_err.append([])
            self.L_prev_poses.append([None])
            self.L_disp_odom.append(True)
            self.L_disp_bl.append(True)
            self.L_ec_odom.append(-1)
            self.L_ec_bl.append(-1)
            self.L_pub_comp.append(rospy.Publisher("comp_rts_%i"%(k), comp_rts) )
            self.L_sub_rtab.append(rospy.Subscriber("rtabmap%i/mapGraph"%k,MapGraph,self.callback_rtab))
        self.rate = rospy.Rate(rate)
        
        self.desynch = 2
        self.prev_pose_access = True
        
        self.ind = 0
        
        self.ori_time = 0


        self.L_rel_synch = [[]]

        rospy.spin()
        # while not rospy.is_shutdown() : 
        #     lentot = 0   
        
        #     if (self.buf_access_map ==True and self.buf_access_bl == True and self.buf_access_truth == True):
        #         lms = [len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))]
        #         lentot1 = min(lms)
        #         vs = lms.index(lentot1)
                
        #         lentot2 = len(self.buf_all_tf_bl[vs])
        #         lentot3 = len(self.buf_all_tf_true[vs])
                
        #         lentot = min(lentot1,lentot2,lentot3)
        #     if (lentot > 0 ):
        #         self.prepare_to_send()
        #     else:
        #         pass
        #     #rospy.spinOnce()
            #rospy.sleep(self.rate_bl/2)
            # t0 = rospy.get_time()
            # while rospy.get_time() -t0 < self.rate_bl/2:
            #     #print(rospy.get_time() -t0 )
            #     pass
            

        return None



if __name__ == "__main__":
    rospy.init_node("evaluateNodesRts", anonymous=False, log_level=rospy.DEBUG)
    cm = evaluateNodesRts(1.0)