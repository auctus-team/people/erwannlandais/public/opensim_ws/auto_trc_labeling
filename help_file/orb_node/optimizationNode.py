#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:04:42 2020

@author: tnmx0798
"""

import os

import rospy

from datetime import datetime,timedelta

from rosbot_gazebo.msg import comp_rts, comp_odom

from rosbot_gazebo.srv import boolean, save_name, differences

from nodelet.srv import NodeletList

from std_srvs.srv import Empty

from launchDictionnary import launchDictionnary

import time

import numpy as np

import subprocess

class optimizationNode():
 
    
    def classEvaluation(self,L):
        Lclass = [0 for k in range(len(L))]
        for i in range(len(L)):
            val_clas = L[i]
            clas = 0
            for j in range(len(L)):
                if val_clas > L[j]:
                    clas+=1
            Lclass[i] = clas
        return(Lclass)
    
    
    def classEvaluation2(self,L,n):

        mini = np.min(L)
        Lclass = [ L[k] - mini for k in range(n) ]
        
        return(Lclass)
        
    
    def manageOptimizationTest(self):
        
        for i in range(len(self.ld.L_dics)):
            current_class = self.ld.L_dics[i]
            name_param = current_class[-2][1]
            if name_param == "Odom":
                basic_param = current_class[0]
                
                #1e test : comparaison 0 - 1 - 2
                
                # Pr 10 : 0 : rtab 1 - 2 - 3 ; 1 : rtab 4 - 5 -6 ; 2 : rtab 7 -8 -9 -10
                self.ld.actual_variation = [0 for k in range(self.nb_diff//3)]+[1 for k in range(self.nb_diff//3, 2*self.nb_diff//3)]+[2 for k in range( 2*self.nb_diff//3, self.nb_diff)]
                self.ld.parameter = ["Odom","Odom/FilteringStrategy"]            
                continue_test = self.globalOptimizationTest(i,0,True)
                if continue_test !=2:
                    basic_param_value = self.ld.L_dics[i][0][1].current_values
                    
                    if (basic_param_value == 1):
                        for j in range(1,3):
                            continue_test = self.globalOptimizationTest(i,j,False)
                            if continue_test == 2:
                                break
                  
                    elif (basic_param_value == 2):
                        for j in range(3,8):
                            continue_test = self.globalOptimizationTest(i,j,False)
                            if continue_test == 2:
                                break

            else: 
            
                
                for j in range(len(current_class)):
                    current_parameter = current_class[j]

                    if current_parameter[0] not in self.ld.L_info_class:
                        continue_test = self.globalOptimizationTest(i,j,current_parameter[1].all_values)
                        if continue_test == 2:
                            break
            if continue_test ==2:
                break
        if continue_test == 2:
            print("Maximun optimization obtained!")
                            
             
    
    def globalOptimizationTest(self,i,j,check_all_values):
        parameter = self.ld.L_dics[i][j]
        self.ld.parameter = [self.ld.L_dics[i][-2][1],parameter[1].name]
        print("Let's check parameter : ", parameter[1].name)
        continue_test = 1
        self.iter_opti = 1
        if self.ld.L_dics[i][j][1].already_checked == False:
            
            if check_all_values : 
                nb_diff = self.nb_diff - self.nb_same
                # if True:
                if nb_diff == 0:
                    length = len(parameter[1].ori_values)
                    self.ld.actual_variation = [parameter[1].ori_values[l] for l in range(length)]
                    continue_test = self.oneOptimizationTest_allsame(i,j, 1)                
                else:
                    length = len(parameter[1].ori_values)
                    self.ld.actual_variation = []
                    for k in range(length):
                        self.ld.actual_variation += [parameter[1].ori_values[k] for l in range(self.nb_diff*k//length,self.nb_diff*(k+1)//length)]
                    continue_test = self.oneOptimizationTest(i,j, max(self.n_repeat//2,1))
                                
                
            else:
                nb_diff = self.nb_diff - self.nb_same
                while not rospy.is_shutdown() and continue_test==1 :
                    parameter = self.ld.L_dics[i][j]
                    values = parameter[1].current_values
                    maxv = values[0]
                    minv = values[1]
                    actual = values[2]
                    print(maxv,minv,actual)
                    if nb_diff == 0:
                        self.ld.actual_variation =  [((maxv-minv)*k +  (self.nb_div-1)*minv)*1.0/(self.nb_div-1) for k in range(self.nb_div)]
                        #self.ld.actual_variation =  [((maxv-minv)*k +  (self.nb_diff-1)*minv)*1.0/(self.nb_diff-1) for k in range(self.nb_diff)]
                        print(self.ld.actual_variation)
                        # début des tests
                        continue_test = self.oneOptimizationTest_allsame(i,j,1)
                    else:
                        self.ld.actual_variation = [ actual for k in range(self.nb_same)] +  [((maxv-minv)*k +  (nb_diff-1)*minv)*1.0/(nb_diff-1) for k in range(nb_diff)]
                        print(self.ld.actual_variation)
                        # début des tests
                        continue_test = self.oneOptimizationTest(i,j,self.n_repeat)
                    
                    self.iter_opti +=1
                    if (self.iter_opti) > self.max_iter_opti:
                        continue_test = 0
                        self.iter_opti = 1
            self.ld.L_dics[i][j][1].already_checked = True
        return(continue_test)
                


    def oneOptimizationTest(self,i,j,repeat):
        continue_test = 1
        # créer dossier dans lequel doivent être enregistrés data rtabmap
        # nom dossier = nom paramètre
        # juste au dessus : dossier last_database, contenant dernier .db (vidé après chaque oneOpitmizationTest, avant nouveau enregistrement)
        # dans dossier : un autre dossier database, dans lequel se trouve dernier .db (dossier vidé )
        try:
            os.mkdir(self.data_path+self.ld.parameter[0])
        except OSError:
            print("Folder"+self.data_path+self.ld.parameter[0] +" already existing.")
        try:
            os.mkdir(self.data_path+self.ld.parameter[1])
        except OSError:
            print("Folder"+self.data_path+self.ld.parameter[1] +" already existing.")
        
        temp_data_path = self.data_path+self.ld.parameter[1] 
        
        # créer launchfile
        self.ld.destroy_launchfile()
        self.ld.create_launchfile()

        # transférer launch file vers docker
        subprocess.call(self.absolute_path+"rtab_scripts/send_launcher.sh")
        # préparer test paramètres
        
        M_all_rot = np.array([])
        M_all_rot_std = np.array([])
        M_all_trans = np.array([])
        M_all_trans_std = np.array([])     
        M_all_pos_dec = np.array([])        
        M_all_neg_dec = np.array([])
        
        npam = self.ld.parameter[1].split("/")[1]
        tdate = datetime.utcnow()+timedelta(hours=2)
        ndate = str(tdate.date())+ "-" + str(tdate.hour) + "-"+ str(tdate.minute)
        savename = temp_data_path + "/check_" + self.ld.parameter[0]+"_"+npam +  "_param_save_" + ndate + ".txt"
        resp1 = False
        
        with open(savename,"a+") as f:
            if self.nb_same == 0:
                f.write(" ----- Values with checks (all different for each line) ----- \n")
            else:
                f.write(" ----- Values with checks (some same, some different) ----- \n")
            
            
            line = "Parameter considered : " + self.ld.parameter[1]  + "\n"
            f.write(line)
            f.write("Values of parameters :  ")
            line = " | "
            for l in range(len(self.ld.actual_variation)):
                line += str(self.ld.actual_variation[l]) + " | "
            line += "\n"
            f.write(line) 
            self.ld.save_dictionnary(f)
    
            for k in range(repeat):
                    # bien verif que tout est desactive
                self.stopCheck()
                    # lancer rtabmap
                subprocess.call(self.absolute_path+"rtab_scripts/run_rtab.sh")
                    #check si opérationnel
                self.operationalCheck()
                    
                date = (datetime.utcnow()+timedelta(hours=2)).isoformat(sep="-").split(".")[0]
                f.write("Start time : " + date + " \n")
                    # lancer rosbag
                    
                    # d = rospy.Duration(5, 0)
                    # rospy.sleep(d)
                    
                    
                    #print("Before : " ,time.time())
                    #time.sleep(15)
                t0 = time.time()
                while not rospy.is_shutdown() and time.time()- t0 < 5:
                    pass
                    #print("After : ", time.time() )
                    
                subprocess.call(self.absolute_path+"rtab_scripts/run_rosbag.sh")
                    # attendre durée rosbag
    
                    # récupérer résultats

                tst= rospy.ServiceProxy('get_statistics', differences)
                L_medians_trans, L_medians_trans_std, L_medians_rot, L_medians_rot_std = [],[],[],[]
                v= 0 
                resp1 = None
                while not rospy.is_shutdown() and resp1 == None and min(len(L_medians_trans),len(L_medians_trans_std),len(L_medians_rot),len(L_medians_rot_std) ) == 0 :
                    try : 
                        msg = tst(self.median)
                        L_medians_trans = msg.all_means_diff_trans
                        L_medians_trans_std = msg.all_means_diff_trans_std
                        L_medians_rot = msg.all_means_diff_rot
                        L_medians_rot_std = msg.all_means_diff_rot_std
                        L_pos_dec = msg.all_positive_detection
                        L_neg_dec = msg.all_negative_detection
                        number_node = msg.number_node
                        resp1 = True
                            
                    except rospy.ServiceException as e :
                        print("Service call failed : %s"%e)
                        if (v) > 10:
                            print("Something's wrong...")
                            resp1 = False
                        v+=1
                    
                if resp1 == False:
                    break
                else : 
                    if self.median : 
                        f.write("Median Translational Error (with std) : \n")
                    else:
                        f.write("Mean Translational Error (with std) : \n")
                    line = " | "
                    for k in range(len(L_medians_trans)):
                        line += str(L_medians_trans[k]) + "(std : " + str(L_medians_trans_std[k]) + " ) |"
                    line += "\n"
                    f.write(line)                    
                    if self.median : 
                        f.write("Median Rotational Error (with std) : \n")
                    else:
                        f.write("Mean Rotational Error (with std) : \n")
                    line = " | "
                    for k in range(len(L_medians_rot)):
                        line += str(L_medians_rot[k]) + "(std : " + str(L_medians_rot_std[k]) + " ) |"
                    line += "\n"
                    f.write(line)    
                    
                    f.write("Positive and negative LC detection : \n")
                    line = " | "
                    for k in range(len(L_pos_dec)):
                        line += str(L_pos_dec[k]) + " ; " + str(L_neg_dec[k]) + " |"
                    line += "\n \n"
                    f.write(line)                    
                    
                        
                    if M_all_trans.shape != (0,):
                        M_all_trans = np.vstack( (M_all_trans,np.array(L_medians_trans)) )
                    else:
                        M_all_trans = np.array([L_medians_trans])
                            
                    if M_all_trans_std.shape != (0,):
                        M_all_trans_std = np.vstack( (M_all_trans_std,np.array(L_medians_trans_std)) )
                    else:
                        M_all_trans_std= np.array([L_medians_trans_std])
                            
                    if M_all_rot.shape != (0,):
                        M_all_rot = np.vstack( (M_all_rot,np.array(L_medians_rot)) )
                    else:
                        M_all_rot = np.array([L_medians_rot])
                            
                    if M_all_rot_std.shape != (0,):
                        M_all_rot_std = np.vstack( (M_all_rot_std,np.array(L_medians_rot_std)) )
                    else:
                        M_all_rot_std = np.array([L_medians_rot_std])
                        
                    if M_all_pos_dec.shape != (0,):
                        M_all_pos_dec = np.vstack( (M_all_pos_dec,np.array(L_pos_dec)) )
                    else:
                        M_all_pos_dec = np.array([L_pos_dec]) 
                        
                    if M_all_neg_dec.shape != (0,):
                        M_all_neg_dec = np.vstack( (M_all_neg_dec,np.array(L_neg_dec)) )
                    else:
                        M_all_neg_dec = np.array([L_neg_dec])                        
                        
                        # stopper noeuds rtabmap
                        # resp1 = None
                        # v =0
                        # for k in range(1,self.nb_diff+1):
                        #     while not rospy.is_shutdown() and resp1 == None : 
                        #         try : 
                        #             tst = rospy.ServiceProxy('/rtabmap%i/reset'%k, Empty)
                        #             resp1 = tst()
                        #             resp1 = "OK"
                        #         except rospy.ServiceException as e :
                        #             print("Reset : service call failed : %s"%e)
                        #     print("Rtabmap nbe %i reset."%k)
                        #     resp1= None
                        
                        
                        
                    subprocess.call(self.ld.path+"stop_rtabmap.sh")
                    self.stopCheck()
                    time.sleep(5)
        
                        # reboot
                    resp1 = None
                    v =0
                    while not rospy.is_shutdown() and resp1 == None : 
                        try : 
                            tst = rospy.ServiceProxy('reboot', save_name)
                            resp1 = tst("check_" + self.ld.parameter[0]+"_"+date,temp_data_path,1)
                        except rospy.ServiceException as e :
                            print("Reboot : service call failed : %s"%e)
                            if (v) > 10:
                                print("Something's wrong...")
                                resp1 = False
                            v+=1
                                
                
            if resp1 == False:
                print("Something's wrong. Stop algorithm.")
                continue_test = 2
            
            else:
                continue_test = self.evaluateTest(f,M_all_trans,M_all_trans_std,
                                                  M_all_rot,M_all_rot_std,
                                                  M_all_pos_dec,M_all_neg_dec,
                                                  i,j)     

                
        return(continue_test)

    def oneOptimizationTest_allsame(self,i,j,repeat):

        # créer dossier dans lequel doivent être enregistrés data rtabmap
        # nom dossier = nom paramètre
        # juste au dessus : dossier last_database, contenant dernier .db (vidé après chaque oneOpitmizationTest, avant nouveau enregistrement)
        # dans dossier : un autre dossier database, dans lequel se trouve dernier .db (dossier vidé )
        try:
            os.mkdir(self.data_path+self.ld.parameter[0])
        except OSError:
            print("Folder"+self.data_path+self.ld.parameter[0] +" already existing.")
        try:
            os.mkdir(self.data_path+self.ld.parameter[1])
        except OSError:
            print("Folder"+self.data_path+self.ld.parameter[1] +" already existing.")
        
        temp_data_path = self.data_path+self.ld.parameter[1] 
        

        M_all_rot = np.array([])
        M_all_rot_std = np.array([])
        M_all_trans = np.array([])
        M_all_trans_std = np.array([])        
        M_all_pos_dec = np.array([])        
        M_all_neg_dec = np.array([])        
        
        param = self.ld.L_dics[i][j][1]
        if param.all_values == True:
            nb_div = len(self.ld.actual_variation)
        else:
            nb_div = self.nb_div
        
        continue_test = 1
        ind = 0
        npam = self.ld.parameter[1].split("/")[-1]

        tdate = (datetime.utcnow()+timedelta(hours=2))
        ndate = str(tdate.date())+ "-" + str(tdate.hour) + "-"+ str(tdate.minute)
        savename = temp_data_path + "/check_" + self.ld.parameter[0]+"_"+npam +  "_param_save_" + ndate + ".txt"
        
        
        
        with open(savename,"a+") as f:
            f.write(" ----- Values with checks (all same for each test) ----- \n")

            line = "Parameter considered : " + self.ld.parameter[1]  + "\n"
            f.write(line)
            f.write("Values of parameters :  ")
            line = " | "
            
            for l in range(len(self.ld.actual_variation)):
                line += str(self.ld.actual_variation[l]) + " | "
            line += "\n"
            f.write(line) 
            self.ld.save_dictionnary(f)
            while not rospy.is_shutdown() and  ind < (nb_div) and continue_test == 1:
            #while not rospy.is_shutdown() and  ind < (self.nb_same) and continue_test == 1:
                # créer launchfile
                    
                self.ld.destroy_launchfile()
                self.ld.create_launchfile(ind)
    
                # transférer launch file vers docker
                subprocess.call(self.absolute_path+"rtab_scripts/send_launcher.sh")
                # préparer test paramètres
    
  
        
                for rep in range(repeat):

                    tst = False
                    
                    while tst == False:
                        # bien verif que tout est desactive
                        self.stopCheck()
                        # lancer rtabmap
                        subprocess.call(self.absolute_path+"rtab_scripts/run_rtab.sh")
                        #check si opérationnel
                        tst = self.operationalCheck()
                        
                    date = (datetime.utcnow()+timedelta(hours=2)).isoformat(sep="-").split(".")[0]
                    # lancer rosbag
                        
                    # d = rospy.Duration(5, 0)
                    # rospy.sleep(d)
                        
                        
                    #print("Before : " ,time.time())
                    #time.sleep(15)
                    t0 = time.time()
                    while not rospy.is_shutdown() and time.time()- t0 < 5:
                        pass
                        #print("After : ", time.time() )
                    t0 = time.time()   
                    subprocess.call(self.absolute_path+"rtab_scripts/run_rosbag.sh")
                    t1 = time.time()
                    if rep ==0 and ind == 0:
                        self.number_node = int((t1-t0)*self.acc//self.rate_rts)
                        
                        print(self.number_node)
                        # attendre durée rosbag
        
                        # récupérer résultats
                    print("Bef : %i , %i"%(rep,ind) )
                    tst= rospy.ServiceProxy('get_statistics', differences)
                    L_medians_trans, L_medians_trans_std, L_medians_rot, L_medians_rot_std = [],[],[],[]
                    L_pos_dec = []
                    L_neg_dec = []
                    L_param = []
                    number_node = 0
                    v= 0 
                    resp1 = None
                    while not rospy.is_shutdown() and resp1 == None and min(len(L_medians_trans),len(L_medians_trans_std),len(L_medians_rot),len(L_medians_rot_std) ) == 0 :
                        try : 
                            msg = tst(self.median)
                            L_medians_trans = msg.all_means_diff_trans
                            L_medians_trans_std = msg.all_means_diff_trans_std
                            L_medians_rot = msg.all_means_diff_rot
                            L_medians_rot_std = msg.all_means_diff_rot_std
                            L_pos_dec = msg.all_positive_detection
                            L_neg_dec = msg.all_negative_detection      
                            L_param = msg.parameters
                            number_node = msg.number_node
                            resp1 = True
                                
                        except rospy.ServiceException as e :
                            print("Service call failed : %s"%e)
                            if (v) > 10:
                                print("Something's wrong...")
                                resp1 = False
                            v+=1
                      

                    if resp1 == False:
                        continue_test = 2
                        break

                    
                    elif self.number_node != -1 and number_node < self.number_node*0.75 :
                        # something went wrong; we must do the test again...
                        f.write("Test at time : " + date + " for value of parameter : " + str(self.ld.actual_variation[ind]) + ", but something went wrong (not enough nodes recorded : %f on %f expected)... \n"%(len(L_medians_trans),self.number_node))
                        ind -=1
                        
                    
                    else : 
                        f.write("Start time : " + date + " for value of parameter : " + str(self.ld.actual_variation[ind]) + " \n")

                        # if self.number_node == -1:
                        #     self.number_node = len(L_medians_trans)
                        
                        if self.median : 
                            f.write("Median Translational Error (with std) : \n")
                        else:
                            f.write("Mean Translational Error (with std) : \n")
                        line = " | "
                        for k in range(len(L_medians_trans)):
                            line += str(L_medians_trans[k]) + "(std : " + str(L_medians_trans_std[k]) + " ) |"
                        line += "\n"
                        f.write(line)            
                        
                        if self.median : 
                            f.write("Median Rotational Error (with std) : \n")
                        else:
                            f.write("Mean Rotational Error (with std) : \n")
                        line = " | "
                        for k in range(len(L_medians_rot)):
                            line += str(L_medians_rot[k]) + "(std : " + str(L_medians_rot_std[k]) + " ) |"
                        line += "\n"
                        f.write(line)             
                        
                        f.write("Positive and negative LC detection  (parameters : translational tolerance : %f ; rotational tolerance : %f): \n"%(L_param[0],L_param[1]))
                        line = " | "
                        for k in range(len(L_pos_dec)):
                            line += str(L_pos_dec[k]) + " ; " + str(L_neg_dec[k]) + " |"
                        line += "\n \n"
                        f.write(line)                    
                            
                        if M_all_trans.shape != (0,):
                            M_all_trans = np.vstack( (M_all_trans,np.array(L_medians_trans)) )
                        else:
                            M_all_trans = np.array([L_medians_trans])
                                
                        if M_all_trans_std.shape != (0,):
                            M_all_trans_std = np.vstack( (M_all_trans_std,np.array(L_medians_trans_std)) )
                        else:
                            M_all_trans_std= np.array([L_medians_trans_std])
                                
                        if M_all_rot.shape != (0,):
                            M_all_rot = np.vstack( (M_all_rot,np.array(L_medians_rot)) )
                        else:
                            M_all_rot = np.array([L_medians_rot])
                                
                        if M_all_rot_std.shape != (0,):
                            M_all_rot_std = np.vstack( (M_all_rot_std,np.array(L_medians_rot_std)) )
                        else:
                            M_all_rot_std = np.array([L_medians_rot_std])
                        if M_all_pos_dec.shape != (0,):
                            M_all_pos_dec = np.vstack( (M_all_pos_dec,np.array(L_pos_dec)) )
                        else:
                            M_all_pos_dec = np.array([L_pos_dec]) 
                            
                        if M_all_neg_dec.shape != (0,):
                            M_all_neg_dec = np.vstack( (M_all_neg_dec,np.array(L_neg_dec)) )
                        else:
                            M_all_neg_dec = np.array([L_neg_dec])                                  
                            # stopper noeuds rtabmap
                            # resp1 = None
                            # v =0
                            # for k in range(1,self.nb_diff+1):
                            #     while not rospy.is_shutdown() and resp1 == None : 
                            #         try : 
                            #             tst = rospy.ServiceProxy('/rtabmap%i/reset'%k, Empty)
                            #             resp1 = tst()
                            #             resp1 = "OK"
                            #         except rospy.ServiceException as e :
                            #             print("Reset : service call failed : %s"%e)
                            #     print("Rtabmap nbe %i reset."%k)
                            #     resp1= None
                            
                            
                            
                    subprocess.call(self.ld.path+"stop_rtabmap.sh")
                    self.stopCheck()
                    time.sleep(5)
            
                    # reboot
                    resp1 = None
                    v =0
                    while not rospy.is_shutdown() and resp1 == None : 
                        try : 
                            tst = rospy.ServiceProxy('reboot', save_name)
                            print("Number node : ", number_node)
                            if number_node < self.number_node*0.75:
                                resp1 = tst("check_" + self.ld.parameter[0]+"_"+date,temp_data_path,1)
                            else:
                                resp1 = tst("check_" + self.ld.parameter[0]+"_"+date,temp_data_path,0)
                        except rospy.ServiceException as e :
                            print("Reboot : service call failed : %s"%e)
                            if (v) > 10:
                                print("Something's wrong...")
                                resp1 = False
                            v+=1
                                    
                    
                        if resp1 == False:
                            continue_test = 2
                    ind+=1
                    print("Aft : %i , %i"%(rep,ind) )
                
                
            print("Continue_test : ", continue_test)   
            if continue_test != 2:
                continue_test = self.evaluateTest_allsame(f,M_all_trans,M_all_trans_std,
                                                          M_all_rot,M_all_rot_std,
                                                          M_all_pos_dec,M_all_neg_dec,
                                                          i,j,repeat)     
                
        self.number_node = -1
        subprocess.call(self.absolute_path +"rtab_scripts/purge_all.sh")                 
        return(continue_test)


    def saveDatabase(self):
        with open(self.ld.path+"temp.sh","w+") as f2:
            f2.write("#!/bin/bash \n \n \n")
            f2.write("export index_rtab=" + str(self.last_best_index+1))
        subprocess.call(self.absolute_path +"rtab_scripts/get_database.sh") 



    def evaluateTest(self,f,M_all_trans,M_all_trans_std,M_all_rot,M_all_rot_std,M_all_pos_dec,M_all_neg_dec,i,j):
        param = self.ld.L_dics[i][j][1]        
        if param.all_values == True :
            continue_test = 0
            self.evaluateBestDiff(f,M_all_trans,
                                  M_all_trans_std,
                                  M_all_rot,
                                  M_all_rot_std,
                                  M_all_pos_dec,
                                  M_all_neg_dec,
                                  i,j)
            
        else:
            if self.stop_gradient_from_precedent : 
                continue_test = self.evaluateBestDiff(f,M_all_trans,
                                      M_all_trans_std,
                                      M_all_rot,
                                      M_all_rot_std,
                                      M_all_pos_dec,
                                      M_all_neg_dec,
                                      i,j)
            else : 
                if self.nb_same == 0:
                    continue_test = self.evaluateSameAndDiff_nosame(f,M_all_trans,M_all_rot,
                                                                    M_all_trans_std,M_all_rot_std,
                                                                    self.nb_diff) 
                else:
                    continue_test = self.evaluateSameAndDiff(f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std, M_all_pos_dec,M_all_neg_dec) 

                print("CT : ", continue_test)
                if continue_test == 1 :
                    self.evaluateBestDiff(f,M_all_trans,
                                          M_all_trans_std,
                                          M_all_rot,
                                          M_all_rot_std,
                                          M_all_pos_dec,
                                          M_all_neg_dec,
                                          i,j)
        
        if continue_test != 0:
            self.saveDatabase()

        return(continue_test)

    def evaluateTest_allsame(self,f,M_all_trans,M_all_trans_std,M_all_rot,M_all_rot_std, M_all_pos_dec,M_all_neg_dec,i,j,repeat):
        param = self.ld.L_dics[i][j][1]
        continue_test= 1
        if self.stop_gradient_from_precedent :
            continue_test = self.evaluateBestDiff_allsame(f,M_all_trans,
                                          M_all_trans_std,
                                          M_all_rot,
                                          M_all_rot_std,
                                          M_all_pos_dec,M_all_neg_dec,i,j,repeat)
        else:
            continue_test = self.evaluateSameAndDiff_allsame(f,M_all_trans,
                                                             M_all_rot,M_all_trans_std,M_all_rot_std,
                                                             M_all_pos_dec,M_all_neg_dec,
                                                             repeat)
            if continue_test ==1:
                self.evaluateBestDiff_allsame(f,M_all_trans,
                                          M_all_trans_std,
                                          M_all_rot,
                                          M_all_rot_std,
                                          M_all_pos_dec,M_all_neg_dec,i,j,repeat)

        if continue_test !=0:
            self.saveDatabase()

        return(continue_test)
    


# ----- BEST DIFF


    def evaluateBestdiff_global(self,f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec):
        class_final = np.zeros( (1,len(L_means_trans)) )
        
        Lcmt = self.classEvaluation(L_means_trans)

        class_final = np.vstack((class_final,Lcmt))

        Lcmr = self.classEvaluation(L_means_rot)
        
        class_final = np.vstack((class_final, Lcmr))

        L_means_trans_std = [ L_means_trans[k] + all_means_std_t[k] for k in range(len(L_means_trans))]             
        
        Lcmrs = self.classEvaluation(L_means_trans_std)
        # class_final = np.vstack((class_final, Lcmrs))
        
        L_means_rot_std = [ L_means_rot[k] + all_means_std_r[k] for k in range(len(L_means_rot))]             
        
        Lcmts = self.classEvaluation(L_means_rot_std)
        # class_final = np.vstack((class_final, Lcmts))
        
        Lcnd = self.classEvaluation(all_neg_dec)
        #Lcnd = [Lcnd[k]*5 for k in range(len(Lcnd))]
        class_final = np.vstack((class_final, Lcnd))
        
        apdc = [-all_pos_dec[k] for k in range(len(all_pos_dec))]
        
        Lcpd = self.classEvaluation(apdc)
        #Lcpd = [Lcpd[k]*5 for k in range(len(Lcpd))]
        class_final = np.vstack((class_final, Lcpd))
        

        print(class_final)
        
        result = list(np.sum(class_final, axis = 0) )
        
        f.write("Ranking result : %s \n"%str(result))
        r2 = [result[l] for l in range(len(result)) ]
        L_i_r = []
        tst = False
        while tst == False:
            try :
                index_result = r2.index(min(result))
                L_i_r.append(index_result)
                del(r2[index_result])
            except Exception:
                tst = True
        if len(L_i_r) > 1:
            clas = [0 for k in range(len(L_i_r))]
            # 1e check : prendre d'abord ceux différent de actual
            for k in range(len(L_i_r)):
                if L_i_r[k] < self.nb_same:
                    clas[k]+=1
            Mclass = np.array([clas])
            #2e check : 
            #LcmtLir = [Lcmt[k] for k in L_i_r]
            
            LcpdLir = [Lcpd[l] for l in L_i_r]
            LcndLir = [Lcnd[l] for l in L_i_r]           
            Mclass = np.vstack((Mclass,LcndLir))
            Mclass = np.vstack((Mclass,LcpdLir))
            
            print("L_i_r > 1 : Mclass : ", Mclass)
            
            result2 = list(np.sum(Mclass, axis = 0) )
            index_result = L_i_r[ result2.index(min(result2)) ]
            
            
            
        else:
            index_result = L_i_r[0]
        return(index_result)

    def evaluateBestDiff_allsame(self,f,M_all_trans,M_all_trans_std,M_all_rot,M_all_rot_std,M_all_pos_dec,M_all_neg_dec,i,j,repeat):
        # print(M_all_trans)
        # print( np.mean(M_all_trans,axis = 0))
        Mt1,Mr1 = np.mean(M_all_trans,axis = 1).flatten().tolist() ,  np.mean(M_all_rot,axis = 1).flatten().tolist()
        Mt1s,Mr1s = np.mean(M_all_trans_std,axis = 1).flatten().tolist() ,  np.mean(M_all_rot_std,axis = 1).flatten().tolist()
        apd1, and1 = np.mean(M_all_pos_dec,axis =1).flatten().tolist(), np.mean(M_all_neg_dec,axis = 1).flatten().tolist()
        
        L_means_trans = [ np.mean(Mt1[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]
        L_means_rot = [ np.mean(Mr1[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]
        all_means_std_t = [ np.mean(Mt1s[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]
        all_means_std_r = [ np.mean(Mr1s[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]        
        all_pos_dec = [ np.mean(apd1[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]  
        all_neg_dec = [ np.mean(and1[k*repeat:(k+1)*repeat]) for k in range( (len(Mt1))//repeat)  ]  
        
        # print(L_means_trans)
        index_result = self.evaluateBestdiff_global(f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec)
        
        continue_test = 1
        
        if self.stop_gradient_from_precedent:
            continue_test = self.evaluateSameAndDiffFromPrecedent_global(f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec,index_result)
        
        if continue_test !=0:
            self.last_best_index = index_result
                
            
            f.write("Current minima : %i \n"%(index_result))
            f.write("Translational error : %f (with std : %f ) | Rotational error : %f (with std : %f ) \n"%(L_means_trans[index_result],all_means_std_t[index_result],L_means_rot[index_result],all_means_std_r[index_result]))
            f.write("Number of positive detection : %i | Number of negative detection : %i \n"%(all_pos_dec[index_result],all_neg_dec[index_result]))
            
            param = self.ld.L_dics[i][j][1]
            best_val = self.ld.actual_variation[index_result]
            if param.all_values == True:
                self.ld.L_dics[i][j][1].current_values = best_val
                continue_test = 0
            else:
                #nb_diff = self.nb_diff
                nb_diff = self.nb_div
                maxi = param.current_values[0]
                mini = param.current_values[1]
                pas = (maxi-mini)*1.0/(nb_diff-1)
                print("pas : ",pas)
                self.ld.L_dics[i][j][1].current_values[2] = best_val
                self.ld.L_dics[i][j][1].current_values[1] = max(best_val-pas,self.ld.L_dics[i][j][1].ori_values[1])
                self.ld.L_dics[i][j][1].current_values[0] = min(best_val + pas, self.ld.L_dics[i][j][1].ori_values[0])
        return(continue_test)
               
    def evaluateBestDiff(self,f,M_all_trans,M_all_trans_std,M_all_rot,M_all_rot_std, M_all_pos_dec,M_all_neg_dec,i,j):
        # print(M_all_trans)
        # print( np.mean(M_all_trans,axis = 0))
        L_means_trans, L_means_rot= np.mean(M_all_trans,axis = 0).flatten().tolist(), np.mean(M_all_rot,axis = 0).flatten().tolist()
        all_means_std_t, all_means_std_r = np.mean(M_all_trans_std,axis = 0).flatten().tolist(), np.mean(M_all_rot_std,axis = 0).flatten().tolist()
        all_pos_dec, all_neg_dec = np.mean(M_all_pos_dec,axis = 0).flatten().tolist(), np.mean(M_all_neg_dec,axis = 0).flatten().tolist()

        index_result = self.evaluateBestdiff_global(f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec) 
        continue_test = 1
        
        if self.stop_gradient_from_precedent:
            continue_test = self.evaluateSameAndDiffFromPrecedent_global(f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec,index_result)
        
        if continue_test != 0:
            self.last_best_index = index_result
    
            f.write("Current minima : %i \n"%(index_result))
            f.write("Translational error : %f (with std : %f ) | Rotational error : %f (with std : %f ) \n"%(L_means_trans[index_result],all_means_std_t[index_result],L_means_rot[index_result],all_means_std_r[index_result]))
            f.write("Number of positive detection : %f | Number of negative detection : %f \n"%(all_pos_dec[index_result],all_neg_dec[index_result]))
            
            param = self.ld.L_dics[i][j][1]
            best_val = self.ld.actual_variation[index_result]
            if param.all_values == True:
                self.ld.L_dics[i][j][1].current_values = best_val
            else:
                nb_diff = self.nb_diff - self.nb_same
                maxi = param.current_values[0]
                mini = param.current_values[1]
                pas = (maxi-mini)*1.0/(nb_diff-1)
                print("pas : ",pas)
                self.ld.L_dics[i][j][1].current_values[2] = best_val
                self.ld.L_dics[i][j][1].current_values[1] = max(best_val-pas,self.ld.L_dics[i][j][1].ori_values[1])
                self.ld.L_dics[i][j][1].current_values[0] = min(best_val + pas, self.ld.L_dics[i][j][1].ori_values[0])
        return(continue_test)


# --- SAME AND DIFF
    def evaluateSameAndDiffFromPrecedent_global(self,f,L_means_trans, L_means_rot, all_means_std_t, all_means_std_r, all_pos_dec, all_neg_dec,index_result):
        
        Mti = L_means_trans[index_result]
        Mri = L_means_rot[index_result]
        Mtis = all_means_std_t[index_result]
        Mris = all_means_std_r[index_result]
        Mtpd = all_pos_dec[index_result]
        Mrnd = all_neg_dec[index_result]
        
        pts_id = 0
        pts_diff = 0
        
        if self.trans_err - Mti > self.gradient_tol_trans :
            pts_id += 1
        else:
            pts_diff +=1
            
        if self.rot_err - Mri > self.gradient_tol_rot :
            pts_id += 1
        else:
            pts_diff +=1
            
        if self.neg_LC - Mrnd > self.gradient_tol_neg_LC :
            pts_id += 2
        else:
            pts_diff +=2
            
        if self.pos_LC - Mtpd < self.gradient_tol_pos_LC :
            pts_id += 1
        else:
            pts_diff +=1
        
        
        continue_test = 1
        
        if Mti < self.max_trans_err and Mri < self.max_rot_err : 
            f.write("Precision limit reached! (Parameter : %f) \n"%(self.ld.actual_variation[index_result] ) )
            f.write("Mean Translational Difference : " +  str(Mti) + " with std : " + str(Mtis) + " \n")
            f.write("Mean Rotational Difference : " +  str(Mri) + " with std : " + str(Mris) + " \n") 
            continue_test = 2
            self.trans_err = Mti
            self.rot_err = Mri
            self.trans_err_std = Mtis
            self.rot_err_std = Mris
            self.neg_LC = Mrnd
            self.pos_LC = Mtpd
            
        elif pts_id > pts_diff:
            # from tests, we choose to continue
            f.write("Results between previous optimization and actual optimization varied enough to continue experimentation. \n")
            f.write("Precedent : Trans_error : %f (std : %f) ; Rot_error : %f (std : %f) ; Pos LC : %f ; Neg LC : %f \n"%(self.trans_err,self.trans_err_std, self.rot_err, self.rot_err_std,self.pos_LC,self.neg_LC )      )
            f.write("Actual (Parameter : %f) : Trans_error : %f (std : %f) ; Rot_error : %f (std : %f) ; Pos LC : %f ; Neg LC : %f \n"%(self.ld.actual_variation[index_result],Mti,Mtis,Mri,Mris,Mtpd,Mrnd )      )
            continue_test = 1
            self.trans_err = Mti
            self.rot_err = Mri
            self.trans_err_std = Mtis
            self.rot_err_std = Mris
            self.neg_LC = Mrnd
            self.pos_LC = Mtpd
            
        else:
            f.write("Results between previous optimization and actual optimization not different enough to continue experimentation. \n")
            f.write("Precedent : Trans_error : %f (std : %f) ; Rot_error : %f (std : %f) ; Pos LC : %f ; Neg LC : %f \n"%(self.trans_err,self.trans_err_std, self.rot_err, self.rot_err_std,self.pos_LC,self.neg_LC )      )
            f.write("Actual (Parameter : %f) : Trans_error : %f (std : %f) ; Rot_error : %f (std : %f) ; Pos LC : %f ; Neg LC : %f \n"%(self.ld.actual_variation[index_result],Mti,Mtis,Mri,Mris,Mtpd,Mrnd )      )
            continue_test = 0
   
        return(continue_test)
            
    def evaluateSameAndDiff_global(self,f, M_all_trans, M_all_rot, M_all_trans_std,M_all_rot_std):
        pts_id = 0
        pts_diff = 0
        # poss 1 : comparer des pourcentages, entre la dispersion des moyennes de chaque paramètre
        # ( disp moy des identiques) et la dispersion des moyennes de l'ensemble des paramètres.
       
        # minit = np.min(M_all_trans,axis=0)
        # perc =  [ M_all_trans[:,k:k+1]/minit[k:k+1] for k in range(self.nb_diff)]
        # disp = [np.std(perc[k]) for k in range(self.nb_diff)]
        # mdp_t = np.mean(disp) # disp moy des identiques
        
        # m_t = np.mean(M_all_trans,axis=0)
        # mind = np.min(m_t)
        # perc2 =  [ m_t[k]/mind for k in range(self.nb_diff)]
        # disp_ens_t = np.std(perc2) # disp des moyennes des identiques
        
        # if disp_ens_t <= mdp_t:
        #     pts_diff +=2
        # else:
        #     pts_id+=2
            
            
        # minir = np.min(M_all_rot,axis=0)
        # perc =  [ M_all_rot[:,k:k+1]/minir[k:k+1] for k in range(self.nb_diff)]
        # disp = [np.std(perc[k]) for k in range(self.nb_diff)]
        # mdp_r = np.mean(disp)
        
        # m_r = np.mean(M_all_rot,axis=0)
        # mind = np.min(m_r)
        # perc2 =  [ m_r[k]/mind for k in range(self.nb_diff)]
        # disp_ens_r = np.std(perc2)
        
        # if disp_ens_r <= mdp_r:
        #     pts_diff +=1
        # else:
        #     pts_id+=1
        
        # minit = np.min(M_all_trans,axis=0)
        # maxi = np.max(M_all_trans,axis=0) 
        # maxti = np.mean([maxi[k] - minit[k] for k in range(self.nb_diff)])
        
        # m_t = np.mean(M_all_trans,axis=0)
        # mind = np.min(m_t)
        # maxd = np.max(m_t)
        # maxtd = maxd-mind
        
        # if maxti <= maxtd:
        #     pts_id+= 1
        # else:
        #     pts_diff +=1
                   
        # minir = np.min(M_all_rot,axis=0)
        # maxi = np.max(M_all_rot,axis=0) 
        # maxri = np.mean([maxi[k] - minir[k] for k in range(self.nb_diff)])
        
        # m_r = np.mean(M_all_rot,axis=0)
        # mind = np.min(m_r)
        # maxd = np.max(m_r)
        # maxrd = maxd-mind
        
        # if maxri <= maxrd:
        #     pts_id+= 1
        # else:
        #     pts_diff +=1
        
        # poss 2 : 
        #   Pour les 10 paramètres : 
        #   - prendre std de chaque instance entre moyenne des 10 paramètres
        #   - prendre médiane de cet std : sera med_diff_[r/t]
        #
        #   Pour chaque paramètre : 
        #   - prendre std des moyennes de chacun des 10 tests
        #   - prendre médiane de ces std : sera med_same_[r/t]
        #
        #   Celui avec la médiane la plus haute doit être favorisé.
        #   (Donc celui avec médiane la plus faible prend points).

        std_diff_t = np.std(M_all_trans,axis=1)
        med_diff_t = np.median(std_diff_t)
        
        std_same_t = np.std(M_all_trans,axis=0)
        med_same_t = np.median(std_same_t)

        if med_same_t >= med_diff_t:
            pts_diff +=2
        else:
            pts_id+=2
            
            
        std_diff_r = np.std(M_all_rot,axis=1)
        med_diff_r = np.median(std_diff_r)
        
        std_same_r = np.std(M_all_rot,axis=0)
        med_same_r = np.median(std_same_r)

        if med_same_r >= med_diff_r:
            pts_diff +=1
        else:
            pts_id+=1
        
        
        minit = np.min(M_all_trans,axis=0)
        maxi = np.max(M_all_trans,axis=0) 
        maxti = np.mean([maxi[k] - minit[k] for k in range(len(maxi))])
        
        mindt = np.min(M_all_trans,axis=1)
        maxdt = np.max(M_all_trans,axis=1) 
        maxtd = np.mean([maxdt[k] - mindt[k] for k in range(len(maxdt))])
        
        if maxti <= maxtd:
            pts_id+= 1
        else:
            pts_diff +=1
                   
        minir = np.min(M_all_rot,axis=0)
        maxi = np.max(M_all_rot,axis=0) 
        maxri = np.mean([maxi[k] - minir[k] for k in range(len(minir))])
        
        mindr = np.min(M_all_rot,axis=1)
        maxdr = np.max(M_all_rot,axis=1) 
        maxrd = np.mean([maxdr[k] - mindr[k] for k in range(len(mindr))])
        
        if maxri <= maxrd:
            pts_id+= 1
        else:
            pts_diff +=1
            
        # if pts_id > pts_diff:
        #     # from tests, we choose to continue
        #     f.write("Results between identicals rtabmap and different rtabmap varied enough to continue experimentation. \n")
        #     f.write("Dispersion with translation : " +  str(disp_ens_t) + " with difference : "+ str(maxtd)+" \n")
        #     f.write("Dispersion with rotation : " +  str(disp_ens_r) + " with difference : " + str(maxrd) + " \n")            
        #     return(1)
        # else:
        #     f.write("Results between identicals rtabmap and different rtabmap not different enough to continue experimentation. \n")
        #     f.write("Dispersion with translation : " +  str(mdp_t) + " with difference : "+ str(maxti)+" \n")
        #     f.write("Dispersion with rotation : " +  str(mdp_r) + " with difference : " + str(maxri) + " \n")            
        #     return(0)  

        if pts_id > pts_diff:
            # from tests, we choose to continue
            f.write("Results between identicals rtabmap and different rtabmap varied enough to continue experimentation. \n")
            f.write("Dispersion with translation : " +  str(med_diff_t) + " with difference : "+ str(maxtd)+" \n")
            f.write("Dispersion with rotation : " +  str(med_diff_r) + " with difference : " + str(maxrd) + " \n")            
            return(1)
        else:
            f.write("Results between identicals rtabmap and different rtabmap not different enough to continue experimentation. \n")
            f.write("Dispersion with translation : " +  str(med_same_t) + " with difference : "+ str(maxti)+" \n")
            f.write("Dispersion with rotation : " +  str(med_same_r) + " with difference : " + str(maxri) + " \n")            
            return(0)             
                   
    def evaluateSameAndDiff_allsame(self,f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std,M_all_pos_dec,M_all_neg_dec,nbe):
        # first check difference between identicals and differences
        
        # goal : have smallest points
        col = M_all_trans.shape[1]
        
        M_all_trans = M_all_trans.reshape(self.nb_same,nbe*col).T
        M_all_rot = M_all_rot.reshape(self.nb_same,nbe*col).T
        M_all_trans_std = M_all_trans_std.reshape(self.nb_same,nbe*col).T
        M_all_rot_std = M_all_rot_std.reshape(self.nb_same,nbe*col).T

        continue_test = self.evaluateSameAndDiff_global(f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std)
        return(continue_test)
       
    def evaluateSameAndDiff_nosame(self,f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std,M_all_pos_dec,M_all_neg_dec,nbe):
        # just in case the user wants to try an other mean of evaluation for this particular case.
        continue_test = self.evaluateSameAndDiff_global(f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std)
        return(continue_test)        

    def evaluateSameAndDiff(self,f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std,M_all_pos_dec,M_all_neg_dec):
        # case where some identical and different are used
        # ex : |0 | 0 |0 | 0.25 | 0.5 | 0.75| 1 |
        #       |0 | 0 |0 | 0.25 | 0.5 | 0.75| 1 |
        #       |0 | 0 |0 | 0.25 | 0.5 | 0.75| 1 |
        #       |0 | 0 |0 | 0.25 | 0.5 | 0.75| 1 |
        #       |0 | 0 |0 | 0.25 | 0.5 | 0.75| 1 |
        
        # goal : have smallest points
        if False :
            continue_test = self.evaluateSameAndDiffFromPrecedent_global(f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std,M_all_pos_dec,M_all_neg_dec)
        else:
            line = M_all_trans.shape[0]
            
            k=0
            
            while k < len(self.ld.actual_variation):
                i = 0
                while (k+i+1)< len(self.ld.actual_variation) and self.ld.actual_variation[k+i] == self.ld.actual_variation[k+i+1]:
                    i+=1
    
                # print(np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1) )
                # print( np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1)[0])
                # # print(np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1).shape )
                # print(M_LC_pos[:,:k])
                # # print(M_LC_pos[:,:k].shape)
                # print(M_LC_pos[:,:k][0])
                if k==0:
    
                    meanCtrans = np.mean(M_all_trans[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_trans = np.hstack( (meanCtrans,M_all_trans[:,k+i+1:]) )
                    
                    meanCrot= np.mean(M_all_rot[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_rot = np.hstack( (meanCrot,M_all_rot[:,k+i+1:]) )
                    
                    meanCtrans_std = np.mean(M_all_trans_std[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_trans_std = np.hstack( (meanCtrans_std,M_all_trans_std[:,k+i+1:]) )
                    
                    meanCrot_std= np.mean(M_all_rot_std[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_rot_std = np.hstack( (meanCrot_std,M_all_rot_std[:,k+i+1:]) )
    
                    meanCpos_dec= np.mean(M_all_pos_dec[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_pos_dec = np.hstack( (meanCpos_dec,M_all_pos_dec[:,k+i+1:]) )
    
                    meanCneg_dec= np.mean(M_all_neg_dec[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_neg_dec = np.hstack( (meanCneg_dec,M_all_neg_dec[:,k+i+1:]) )
    
                    
                elif k+i == M_all_trans.shape[1]:
                    pass
                
                else : #and np.mean(M_LC_pos[:,k+i:], axis = 1).reshape(col,1)[0] != []:
                    # print(k)
                    # print(k+i)
                    meanCtrans = np.mean(M_all_trans[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_trans2 = np.hstack( (M_all_trans[:,:k],meanCtrans) )
                    M_all_trans = np.hstack( (meanCtrans,M_all_trans[:,k+i+1:]) )
                    
                    meanCrot = np.mean(M_all_rot[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_rot2 = np.hstack( (M_all_rot[:,:k],meanCrot) )
                    M_all_rot = np.hstack( (meanCrot,M_all_rot[:,k+i+1:]) )
                    
                    meanCtrans_std = np.mean(M_all_trans_std[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_trans_std2 = np.hstack( (M_all_trans_std[:,:k],meanCtrans_std) )
                    M_all_trans_std = np.hstack( (meanCtrans_std,M_all_trans_std[:,k+i+1:]) )
                    
                    meanCrot_std = np.mean(M_all_rot_std[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_rot_std2 = np.hstack( (M_all_rot_std[:,:k],meanCrot_std) )
                    M_all_rot_std = np.hstack( (meanCrot_std,M_all_rot_std[:,k+i+1:]) )
                    
                    meanCpos_dec = np.mean(M_all_pos_dec[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_pos_dec2 = np.hstack( (M_all_pos_dec[:,:k],meanCpos_dec) )
                    M_all_pos_dec = np.hstack( (meanCpos_dec,M_all_pos_dec[:,k+i+1:]) )
    
                    meanCneg_dec = np.mean(M_all_neg_dec[:,k:k+i+1], axis = 1).reshape(line,1) 
                    M_all_neg_dec2 = np.hstack( (M_all_neg_dec[:,:k],meanCneg_dec) )
                    M_all_neg_dec = np.hstack( (meanCneg_dec,M_all_neg_dec[:,k+i+1:]) )

                k+=1

            continue_test = self.evaluateSameAndDiff_global(f,M_all_trans,M_all_rot,M_all_trans_std,M_all_rot_std)

        return(continue_test)
    
    def operationalCheck(self):
        L_rosservice = [ "/rtabmap%i/rtabmap/list"%k for k in range(1,self.nb_diff+1)]
        
        #print(L_rosservice)
        for k in range(len(L_rosservice)):
            result = None
            i = 0
            while not rospy.is_shutdown() and result == None and i < 1e6:
                try : 
                    tst = rospy.ServiceProxy(L_rosservice[k], NodeletList)
                    result = tst()
                    
                except rospy.ServiceException as e :
                    i += 1
                    if (i%1000 == 0):
                        print("Service call failed : %s ; for the test : %i"%(e,k))
            if result != None:
                print("Success of operationnal check  for %i"%(k+1))
            else:
                return(False)
        return(True)
                
                
                
    def stopCheck(self):
        L_rosservice = [ "/rtabmap%i/rtabmap/list"%k for k in range(1,self.nb_diff+1)]

        #print(L_rosservice)
        for k in range(len(L_rosservice)):
            result = 0
            i = 0
            while not rospy.is_shutdown() and result != None :
                try : 
                    result = None
                    tst = rospy.ServiceProxy(L_rosservice[k], NodeletList)
                    result = tst()
                    if result != None :
                        i += 1
                        if (i%1000 == 0):
                            print("Error for the test : %i, rtabmap still activated"%k)
                        subprocess.call(self.ld.path+"stop_rtabmap.sh")
                    
                except rospy.ServiceException as e :

                    result = None
            if result == None:
                print("Rtabmap stoppped for %i"%(k+1))            
    
    
    def __init__(self,rate,path_load,global_load):
        """
        

        Parameters
        ----------
        allsame : si nb_diff == nb_same.
        Situation dans laquelle les valeurs du paramètre testé, lors de l'exécution de chaque
        ensemble d'instance de rtabmap, doivent être identique. 
        
        nosame : si nb_same == 0 
        
        
        Ex : pour les valeurs : \ 0 \ 1 \ 2 \ 3 \
            Cas allsame : Instance rtabmap 1 : \ 0 \ 0 \ 0 \ 0
                            Instance rtabmap 2 : \ 1 \ 1 \ 1 \ 1
                            Instance rtabmap 3 : \ 2 \ 2 \ 2 \ 2       
                            Instance rtabmap 4 : \ 3 \ 3 \ 3 \ 3    
                            ( s'exécutera forcément 4 fois actuellemt)
            
            Cas nosame : Instance rtabmap 1 : \ 0 \ 1 \ 2 \ 3 \  
                            Instance rtabmap 2 : \ 0 \ 1 \ 2 \ 3 \
                            Instance rtabmap 3 : \ 0 \ 1 \ 2 \ 3 \     
                            Instance rtabmap 4 : \ 0 \ 1 \ 2 \ 3 \
                            (avec n_repeat = 4; mais la valeur peut changer)

        rate : TYPE
            DESCRIPTION.
        nb_diff : TYPE
            DESCRIPTION.
        path_load : TYPE
            DESCRIPTION.
        global_load : boolean
            Determine whether we want a global load or not (which means to take already_checked or not during the loading).
            Consequence : restart from exact last point, or do another round of optimization starting from last middle.
            
            Ex : True : take already_checked parameter saved, and take the current values
                False : do not take already_checked parameter saved. All parameters will then be considered
                as if they were never optimized. However, change current value as : [ ori_max, ori_min, last_optimized_value]
        Returns
        -------
        None.

        """

        self.test_all_errT = []        
        self.test_all_errR = []
        self.min_trans_errT = [None,None] # numéro, erreur
        self.min_trans_errR = [None,None]
        
        self.first_test = True
        

        self.iter_opti = 1
        self.max_iter_opti = 6 #1 #8        


        nb_diff = rospy.get_param("~nb_diff")
        self.navig = rospy.get_param("~navig")
        
        self.nb_same =  nb_diff#0 #3
        
        self.nb_div = 10
        
        self.n_repeat = 1  #10 #3       

        self.n_repeat = max(self.n_repeat,1)

        self.rate = rospy.Rate(rate)
        
        self.absolute_path = "/home/tnmx0798/Documents/ROSBot_simu/"
        if self.navig:
            self.data_path = self.absolute_path+ 'data/Rts_Datas_navig/'
        else:
            self.data_path = self.absolute_path+ 'data/Rts_Datas/'
        
        self.median = False
        
        self.nb_diff = nb_diff
        self.ld = launchDictionnary(rate,nb_diff,self.data_path,self.navig)
        maxi = 9.5
        mini = 1


        self.ld.actual_variation = [((maxi-mini)*k +  (nb_diff-1)*mini)/(nb_diff-1) for k in range(nb_diff)]
        self.ld.parameter = ["RGBD","RGBD/OptimizeMaxError"]
        
        self.ld.create_bash_stop_rtabmap()
        # if choice == 0:
        #     self.ld.create_launchfile()
        # if choice == 1:
        #     self.ld.destroy_launchfile()
        self.rate_rts = 2.0
        self.acc = 3.0
        self.number_node = -1

        
        
        
        self.stop_gradient_from_precedent = True
        self.gradient_tol_trans = 0.0 # 0.001
        self.gradient_tol_rot = 0.0 #0.1*(np.pi/180)
        self.gradient_tol_pos_LC = 0
        self.gradient_tol_neg_LC = 0

        self.max_trans_err = 0.001
        self.max_rot_err = 0.1*(np.pi/180)
        
        self.last_best_index = self.nb_diff
        
        

        


        
        if path_load!=None:
            print("Working! Path :", path_load)            
            self.ld.load_dictionnary(path_load,global_load)
        
        
        self.trans_err = self.ld.trans_err
        self.rot_err = self.ld.rot_err
        self.trans_err_std = self.ld.trans_err_std
        self.rot_err_std = self.ld.rot_err_std
        self.pos_LC = self.ld.pos_LC
        self.neg_LC = self.ld.neg_LC
        
        
        
        print("Precedent : Trans_error : %f (std : %f) ; Rot_error : %f (std : %f) ; Pos LC : %f ; Neg LC : %f \n"%(self.trans_err,self.trans_err_std, self.rot_err, self.rot_err_std,self.pos_LC,self.neg_LC )      )

        self.manageOptimizationTest()
        
        print("Final Translationnal Error : %f ; Final Rotationnal Error : %f \n"%(self.trans_err,self.rot_err) )
        
        tdate = (datetime.utcnow()+timedelta(hours=2))
        ndate = str(tdate.date())+ "-" + str(tdate.hour) + "-"+ str(tdate.minute)
        savename = self.data_path + "/Final_optimized_parameters_" + ndate  + ".txt"
        
        with open(savename,"a+") as f:
            self.ld.save_dictionnary(f)
        return(None)


if __name__ == "__main__":
    rospy.init_node("optimizationNode", anonymous=False, log_level=rospy.DEBUG)
    
    #path = "/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/ImuFilter/MadgwickZeta/check_ImuFilter_MadgwickZeta_param_save_2020-07-06-13-59.txt"
    path = None
    # "/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/GridGlobal/ProbHit/last_recall.txt"
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Icp/MaxCorrespondenceDistance/check_Icp_MaxCorrespondenceDistance_param_save_2020-08-01-7-27.txt"
    
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Odom/ParticleSize/check_Odom_ParticleSize_param_save_2020-07-29-17-30.txt"
    
    #None
    
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Icp/CorrespondenceRatio/check_Icp_CorrespondenceRatio_param_save_2020-07-28-1-49.txt"
    
    #None
    
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Vis/MinInliers/check_Vis_MinInliers_param_save_2020-07-24-22-46.txt"
    
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Marker/MaxDistanceDetection/check_Marker_MaxDistanceDetection_param_save_2020-07-23-12-41.txt"
    
    #None
    #"/home/tnmx0798/Documents/ROSBot_simu/data/Rts_Datas/Marker/MaxDistanceDetection/check_Marker_MaxDistanceDetection_param_save_2020-07-16-15-20.txt"
    
    
    cm = optimizationNode(1.0,path,True)
    