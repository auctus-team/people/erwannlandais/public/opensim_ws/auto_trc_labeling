#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np
import matplotlib.pyplot as plt

from std_msgs.msg import String

import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped

import time
import geometry_msgs.msg


import tf

import tf.transformations

from gazebo_msgs.msg import LinkStates
from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp_odom

from nav_msgs.msg import Odometry


class transformStampedEuler():
    
    def __init__(self):
        self.time = 0
        self.pos = None
        self.rot = None
    
    def __repr__(self):
        return(self.__str__() )
    
    def __str__(self):
        
        return(" Stamp : %f \n pos : [%f,%f,%f] \n rot : [%f,%f,%f] \n"%(self.time,self.pos[0],self.pos[1],self.pos[2],self.rot[0],self.rot[1],self.rot[2]))



class evaluateNodesRaw():

    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec <= prev_ec:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
    
    def callback_kidnap(self,msg):
        if self.gazebo_jump == False:
            self.gazebo_jump = True
    
    def check_jump(self,current_lin_speed,current_ang_speed,
                   prev_lin_speed,prev_ang_speed,
                   current_lin_pos,current_ang_pos,
                   prev_lin_pos,prev_ang_pos,
                   current_time,prev_time,current_detect_jump):
        """
        
        Principe : 
            - prendre vitesse (current_lin_speed, current_ang_speed) et position odométrique actuelle (current_lin_pos,current_ang_pos)
            - obtenir la vitesse (angulaire et linéaire) estimée, en comparant la position précédente
            à la position actuelle. (inutile maintenant)
            Cela permet d'avoir une estimation de la vitesse réelle estimée du robot.
            - obtenir la position (angulaire et linéaire) estimée, à partir de la vitesse précédente
            et la position réelle précédente. 
            - Evaluer la différence entre les positions estimées à partir des mesures précédentes et les positions actuelles.
            Si trop importante, veut dire qu'il y a une erreur : position actuelle ne colle
            pas par rapport à estimation de la vitesse et position précédente. 
        
        """
        detect_jump = False
        if current_detect_jump == True:
            return(current_detect_jump)
        
        if (type(prev_time) != type(None)):
            delay = current_time - prev_time
            estimated_speed = (current_lin_pos - prev_lin_pos)/delay
            estimated_ang = (current_ang_pos-prev_ang_pos)/delay
            
            estimated_pose = current_lin_speed*delay + prev_lin_pos
            # suppress z component if inferior to threshold
            if estimated_pose[2] < self.th_tlp_pos:
                estimated_pose[2] = 0
            estimated_rot = current_ang_speed*delay + prev_ang_pos
            
            rot_diff  = np.linalg.norm(estimated_rot-current_ang_pos) 
            if (rot_diff > np.pi):
                if (rot_diff//(2*np.pi)==0 ):
                    rot_diff =abs( rot_diff - 2*np.pi)
                else:
                    rot_diff = rot_diff%(2*np.pi)

            if (np.linalg.norm(estimated_pose - current_lin_pos) > self.th_tlp_pos  
                or rot_diff > self.th_tlp_rot):
                detect_jump = True
                # print("speed : ", np.linalg.norm(new_speed - estimated_speed ))
                # print("angular : ", np.linalg.norm(new_ang-estimated_ang))
                #print("speed : ", estimated_speed )
                #print("angular : ", estimated_ang)
                print("diff_pose : ", np.linalg.norm( current_lin_pos - estimated_pose ))
                #print("pose : ",  estimated_pose )
                #print("rotation : ", estimated_rot )      
                print("diff_rot : ", rot_diff )
        
        return(detect_jump)
        
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation

        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        true_speed = msg.twist[self.num_rosbot].linear
        true_ang = msg.twist[self.num_rosbot].angular
        new_speed = np.array([true_speed.x, true_speed.y, true_speed.z])
        new_ang = np.array([true_ang.x,true_ang.y,true_ang.z])
        timens = rospy.get_time()
        
        self.gazebo_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_gaz_speed,self.prev_gaz_ang,
                                           new_pose,new_rot,
                                           self.prev_gaz_pos,self.prev_gaz_rot,
                                           timens,self.prev_gaz_time,self.gazebo_jump)
        
        self.prev_gaz_pos = new_pose
        self.prev_gaz_rot = new_rot
        self.prev_gaz_speed = new_speed
        self.prev_gaz_ang = new_ang
        self.prev_gaz_time = timens
        
        
        #print(rospy.get_time() - self.last_time_pose)
        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            self.evaluateProximityToTruth_odom(true_pos,true_rot)
            #self.prepare_to_send2(timens,true_pos,true_rot)
            self.last_time_pose = rospy.get_time()
        
        
        return(None)

    def check_synch_bl(self,buf,t):
        bl_synch = []
        i = -1
        if (len(buf) != self.len_buf_bl):
            L_time = [ buf[k].time.to_sec() for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_bl+self.tol:
                self.err_bl = False
                bl_synch.append(buf[i])
            else:
                print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_bl or self.err_bl:
                    # No matching possible, cancel for this topic
                    self.map_time_synch = -1
                    if (self.err_bl):
                        print("Stop bl blocking!")
                    self.err_bl = False
                else:
                    self.err_bl = True
                self.len_buf_bl = len(buf)

        return(bl_synch,i)

    def prepare_to_send2(self,time_gaz,true_pos,true_rot):
        
        
        # self.temp_buf_access_map = True
        # self.buf_access_bl = False
        # self.temp_buf_access_bl = True
        # self.buf_access_truth = False
        # self.temp_buf_access_truth = True

        lentot = len(self.buf_tf_bl)
        
        if lentot > 0 and self.buf_access_bl== True:

            self.buf_access_bl = False
            self.temp_buf_access_bl = True


            bl_synch= []
            bl_synch,bl_i = self.check_synch_bl(self.buf_tf_bl,time_gaz)
                    
            if len(bl_synch) > 0 :
                bl_check = bl_synch
    
                self.evaluateProximityToTruth_odom2(true_pos,true_rot,bl_check[0])

 
            #add up temporary buffer (for tf /odom --> /bl)
            self.temp_buf_access_bl = False
            if len(self.temp_buf_tf_bl) > 0:
                test= False
                while len(self.temp_buf_tf_bl) > 0 and test == False:
                    p1 = self.temp_buf_tf_bl[0].pos
                    n_pose = p1
                    p2 = self.buf_tf_bl[-1].pos
                    o_pose = p2
                    if (np.all(n_pose==o_pose) == True):
                        #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_tf_bl[0])
                    else:
                        test= True
            len_temp_bl = len(self.temp_buf_tf_bl)  
            self.buf_tf_bl = self.buf_tf_bl[len_temp_bl:]        
            self.buf_tf_bl += self.temp_buf_tf_bl
            self.temp_buf_tf_bl  = []
    
            self.buf_access_bl = True
            lentot = len(self.buf_tf_bl)
            

                

    def evaluateProximityToTruth_odom2(self,true_pos,true_rot,bl):     
            
        time_odom = bl.time
        trans_odom = bl.pos
        rot_odom = bl.rot

        euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
        roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
        rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
        trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
            
        # here, we'll guess that odometry on z is perfect. 
        trans_odom[2] = trans_tr[2]
        
        trans_odomTr = trans_tr-trans_odom
        rot_odomTr = rot_tr-rot_odom

        diff_norm_trans = np.linalg.norm(trans_odomTr)
        diff_norm_rot = np.linalg.norm(rot_odomTr)
            
        # publish it
        msg = comp_odom()
        msg.stamp = time_odom
        msg.trans_odom = list(trans_odom)
        msg.trans_tr = list(trans_tr)
        msg.rot_odom = list(rot_odom)
        msg.rot_tr  = list(rot_tr)
        msg.diff_norm_trans = diff_norm_trans
        msg.diff_norm_rot =  diff_norm_rot
        msg.gazebo_jump = self.gazebo_jump

        self.pub_comp_odom.publish(msg)        
        self.gazebo_jump = False

        return(None)



    def evaluateProximityToTruth_odom(self,true_pos,true_rot):     

        nr = 0
        # get transform between origin (by rts) and pose of camera for rts
        while not rospy.is_shutdown() and nr < self.ntry:
            try:
                transformStamped_odom = geometry_msgs.msg.TransformStamped()
                transformStamped_odom = self.buffer.lookup_transform('odom_noise', 'base_link', rospy.Time(0), rospy.Duration(0.5) )
                # odom_noise
                time_odom = transformStamped_odom.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for odom")
                nr+=1
                pass

        if (nr==self.ntry):
            pass
            print("TF odom not found. Retry.")

        else:
            
            euler_odom = tf.transformations.euler_from_quaternion((transformStamped_odom.transform.rotation.x,
                                                                 transformStamped_odom.transform.rotation.y,
                                                                 transformStamped_odom.transform.rotation.z,
                                                                 transformStamped_odom.transform.rotation.w))
            roll_odom, pitch_odom, yaw_odom= euler_odom[0],euler_odom[1],euler_odom[2]
                        
            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            
            trans_odom = np.array([transformStamped_odom.transform.translation.x,transformStamped_odom.transform.translation.y,transformStamped_odom.transform.translation.z])           
            rot_odom = np.array([roll_odom,pitch_odom,yaw_odom])

            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
            
            trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
            
            trans_odomTr = trans_tr-trans_odom
            rot_odomTr = rot_tr-rot_odom

            for l in range(3):  
                if (rot_tr[l]*rot_odom[l]<0):
                    rot_odomTr[l] = min([abs(rot_odom[l]-rot_tr[l]),abs(rot_odom[l]+rot_tr[l]),abs(rot_tr[l]-rot_odom[l]),abs(rot_tr[l]+rot_odom[l])])

            diff_norm_trans = np.linalg.norm(trans_odomTr)
            diff_norm_rot = np.linalg.norm(rot_odomTr)
            
            # publish it
            msg = comp_odom()
            msg.stamp = time_odom
            msg.trans_odom = list(trans_odom)
            msg.trans_tr = list(trans_tr)
            msg.rot_odom = list(rot_odom)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot
            msg.gazebo_jump = self.gazebo_jump

            self.pub_comp_odom.publish(msg)        
            self.gazebo_jump = False

        return(None)

    def callback_odom2(self,msg):
        
        if (self.buf_access_bl == True or self.temp_buf_access_bl ==True):
            new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
            new_quat = msg.pose.pose.orientation
            euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
            roll,pitch,yaw = euler[0],euler[1],euler[2]
            new_rot = np.array([roll,pitch,yaw])
            
            bl = transformStampedEuler()
            bl.pos = new_pose
            bl.rot = new_rot
            bl.time = msg.header.stamp
            
            if (self.buf_access_bl):
                self.buf_tf_bl.append(bl)
                if len(self.buf_tf_bl) > self.qs_bl:
                    del(self.buf_tf_bl[0])
                    
            elif (self.temp_buf_access_bl):
                self.temp_buf_tf_bl.append(bl)
                if len(self.temp_buf_tf_bl) > self.qs_bl:
                    del(self.temp_buf_tf_bl[0])
    

    def __init__(self,rate):

        """
        
        Constructor of the CrackMap class. 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        Parameters : 
            
            
        """
        rospy.loginfo("INIT")
        

        self.last_time_pose = -1

        self.period = 1.0/rate
                
        self.type_data= rospy.get_param("~type_data")

        if self.type_data == "raw":            
            self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
            self.odom_topic = rospy.Subscriber("/odom_noise",Odometry, self.callback_odom2)
            self.kidnap_topic = rospy.Subscriber("/default/kidnap_event", String, self.callback_kidnap)

            
        self.detect_name = False
        self.num_rosbot = -1
        
        self.ntry =4 #3

        self.prev_gaz_pos = None
        self.prev_gaz_rot = None
        self.prev_gaz_speed = None
        self.prev_gaz_ang = None
        self.prev_gaz_time = None
        
        self.th_tlp_pos = 100#0.175
        self.th_tlp_rot = 100#0.1396


        self.gazebo_jump = False
        
        
        self.rate_bl = 0.1+1e-3
        self.tol = self.rate_bl/4
        self.qs_bl = int((4+self.period)/(self.rate_bl))
        
        self.buf_access_bl = True
        self.temp_buf_access_bl = False
        self.buf_tf_bl = []
        self.temp_buf_tf_bl = []
        
        self.len_buf_bl = -1
        
        self.buffer = tf2.Buffer(rospy.Duration(60)) # prend 60s de tf 
        self.listenertf = tf2.TransformListener(self.buffer)

        self.pub_comp_odom = rospy.Publisher("comp_odom", comp_odom)           
        
        self.rate = rospy.Rate(rate)
        
        rospy.spin()
        
        return None


if __name__ == "__main__":
    rospy.init_node("evaluateNodesRaw", anonymous=False, log_level=rospy.DEBUG)
    cm = evaluateNodesRaw(10.0)