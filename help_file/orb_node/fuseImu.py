#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 17:11:49 2020

@author: tnmx0798
"""


import rospy
import rospkg
import numpy as np

import matplotlib.pyplot as plt

import image_geometry
import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped


import tf2_geometry_msgs

from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Imu


import time
import geometry_msgs.msg

import tf

import tf.transformations

from tf.msg import tfMessage

import csv

import os

from datetime import datetime


class fuseImu():
    
    def __init__(self,rate):
        self.buf_cam_imu = []
        self.temp_buf_cam_imu = []
        
        self.base_frame = 'depthcamera_link'
        self.cov_or = 0.1
        
        self.rate_geko = 1/20.0
        self.rate_cam_imu = 1/20.0
        self.qs_geko = int((6+self.rate_geko)/(self.rate_geko))
        self.qs_cam_imu = int((6+self.rate_cam_imu)/(self.rate_geko)) 
        
        self.buf_tf_cam_imu = []
        self.temp_buf_tf_cam_imu = []

        self.buf_tf_geko = []
        self.temp_buf_tf_geko = []   
        
        self.buf_access_geko = True
        self.buf_access_cam_imu = True
        self.temp_buf_access_geko = False
        self.temp_buf_access_cam_imu = False
        
	self.err_cam_imu = False
        
        self.lt_buf_cam_imu = -1
        
        self.sub_cam_imu = rospy.Subscriber("/camera/imu", Imu, self.callback_cam_imu)

        self.sub_geko_orientation = rospy.Subscriber("/r2p/imu", Vector3, self.callback_geko_orientation)        
        
        
	self.tol = 0.5

        self.sub_tf = rospy.Subscriber("/tf", tfMessage, self.callback_tf)
        
        self.pub_fuse_imu = rospy.Publisher("fuse_imu", Imu)
        self.rate = rospy.Rate(rate)
        rospy.spin()
    
    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec <= prev_ec:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)    

        
    def callback_cam_imu(self,msg):
        if (self.buf_access_cam_imu):
            angv = msg.angular_velocity
            angv_cov = msg.angular_velocity_covariance
            
            lacc = msg.linear_acceleration
            lacc_cov = msg.linear_acceleration_covariance
            time = msg.header.stamp
            tf_cam_imu = [angv,angv_cov,lacc,lacc_cov,time]
            
            self.buf_tf_cam_imu.append(tf_cam_imu)
            if (len(self.buf_tf_cam_imu) > self.qs_cam_imu):
                del(self.buf_tf_cam_imu[0])

        elif (self.temp_buf_access_cam_imu):
            angv = msg.angular_velocity
            angv_cov = msg.angular_velocity_covariance
            
            lacc = msg.linear_acceleration
            lacc_cov = msg.linear_acceleration_covariance
            time = msg.header.stamp
            tf_cam_imu = [angv,angv_cov,lacc,lacc_cov,time]
            
            self.temp_buf_tf_cam_imu.append(tf_cam_imu)
            if (len(self.temp_buf_tf_cam_imu) > self.qs_cam_imu):
                del(self.temp_buf_tf_cam_imu[0])

    def callback_geko_orientation(self,msg):
        if (self.buf_access_geko):
            ang = msg.z
            time = rospy.get_time()
            tf_geko = [ang,time]
            
            self.buf_tf_geko.append(tf_geko)
            if (len(self.buf_tf_geko) > self.qs_geko):
                del(self.buf_tf_geko[0])

        elif (self.temp_buf_access_geko):
            ang = msg.z
            time = rospy.get_time()
            tf_geko = [ang,time]
            
            self.temp_buf_tf_geko.append(tf_geko)
            if (len(self.temp_buf_tf_geko) > self.qs_geko):
                del(self.temp_buf_tf_geko[0])
        
    
    
    def callback_tf(self,msg):
        lentot = 0   
        
        if (self.buf_access_geko ==True and self.buf_access_cam_imu == True):
            
            lentot = min(len(self.buf_tf_geko),len(self.buf_tf_cam_imu) )

        if (lentot > 0 and self.buf_access_geko ==True):
            self.buf_access_geko = False
            self.temp_buf_access_geko = True
            #print("Here_fuse_imu")
            self.prepare_to_send()    

    def check_synch_cam_imu(self,buf,t):
        """
        

        Parameters
        ----------
        buf : TYPE
            Type : [ [angv, angv_cov, lacc, lacc_cov,time.stamp]  ]
        t : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        cam_imu_synch = []
        i = -1
        if (buf[-1][-1].to_sec() != self.lt_buf_cam_imu):
            L_time = [ buf[k][-1].to_sec() for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_cam_imu/2+self.tol*(self.rate_cam_imu/self.rate_geko):
                cam_imu_synch.append(buf[i])
                self.err_cam_imu = False
            else:
                print("Err true_cam_imu synch : min time difference : ",ec, " for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_cam_imu or self.err_cam_imu:
                    # No matching is then possible : cancel the synchronization for this topic
                    self.map_time_synch = -1
                    if self.err_cam_imu:
                        print("Stop cam_imu blocking!")
                        self.len_buf_true = buf[-1].time
                    self.err_cam_imu = False
                else:
                    self.err_cam_imu = True
                self.lt_buf_cam_imu = buf[-1][-1].to_sec()
        return(cam_imu_synch,i)       
    

    def prepare_to_send(self):
        lentot = min(len(self.buf_tf_geko),len(self.buf_tf_cam_imu) )
        
        if not rospy.is_shutdown() and lentot > 0 and self.buf_access_cam_imu == True:
            self.buf_access_geko = False
            self.temp_buf_access_geko = True
            self.buf_access_cam_imu = False
            self.temp_buf_access_cam_imu = True

            L_geko_synch = [self.buf_tf_geko[0] ]

            true_synch= []
	    try:
                true_synch,true_i = self.check_synch_cam_imu(self.buf_tf_cam_imu,L_geko_synch[0][-1]  )
	    except Exception:
	        pass
                    
            if len(true_synch) > 0 :
                true_check = true_synch
    
                self.fuse_all_imu(true_check[0],L_geko_synch[0])

            else:
                print("fuseImu : No synch with geko")
            self.temp_buf_access_geko = False
    
            #add up temporary buffers (for geko)
            if (len(self.buf_tf_geko) > 0):
                test = False
                while len(self.temp_buf_tf_geko) > 0 and  test == False :
                    #print("Treating suppression")
                    p1 = self.temp_buf_tf_geko[0][-1]

                    nw_pose = p1
                    p2 = self.buf_tf_geko[-1][-1]
                    od_pose = p2
                    if ( nw_pose == od_pose) :
                            #print("Suppress : ", self.temp_buf_tf_geko[k][0])
                            #print("Because of : ",  self.buf_tf_geko[k][-1])
                        del(self.temp_buf_tf_geko[0])
                    else:
                        test = True
    
            len_temp_geko = len(self.buf_tf_geko)
            self.buf_tf_geko = self.buf_tf_geko[len_temp_geko:]
            self.buf_tf_geko += self.temp_buf_tf_geko
            self.temp_buf_tf_geko = []
                        
            # if (self.send):
            #   print("Bufffer after send : ", self.buf_tf_geko)
                #self.send = False
            self.buf_access_geko = True
            
            
                
            #add up temporary buffer (for true position)
            # print("Bef temp : ", [self.temp_buf_tf_cam_imu[k].time for k in range(len(self.temp_buf_tf_cam_imu))])
            # print("Bef curr : ", [self.buf_tf_cam_imu[k].time for k in range(len(self.buf_tf_cam_imu))])
            self.temp_buf_access_cam_imu = False

            if len(self.temp_buf_tf_cam_imu) > 0:
                test= False
                while len(self.temp_buf_tf_cam_imu) > 0 and test == False:
                    p1 = self.temp_buf_tf_cam_imu[0][0]
                    n_pose = np.array([p1.x, p1.y, p1.z])
                    p2 = self.buf_tf_cam_imu[-1][0]
                    o_pose = np.array([p2.x, p2.y, p2.z])
                    if (np.all(n_pose==o_pose) == True):
                        #print("Suppress : ", self.temp_buf_tf_geko[k][0])
                        #print("Because of : ",  self.buf_tf_geko[k][-1])
                        del(self.temp_buf_tf_cam_imu[0])
                    else:
                        test= True
                            
                            
            len_temp_cam_imu = len(self.temp_buf_tf_cam_imu)  
            self.buf_tf_cam_imu = self.buf_tf_cam_imu[len_temp_cam_imu:]
            self.buf_tf_cam_imu += self.temp_buf_tf_cam_imu
            self.temp_buf_tf_cam_imu = []
    
            self.buf_access_cam_imu = True

            lentot = min(len(self.buf_tf_geko),len(self.buf_tf_cam_imu) )        
        
    def fuse_all_imu(self,cam_imu,geko_imu):
        # obtain the norm of translation and rotation
            
        #print(true_check)
        angv = cam_imu[0]
        angv_cov = cam_imu[1]        
        lacc = cam_imu[2]
        lacc_cov = cam_imu[3]        
        time = cam_imu[4]
        
        euler_orient = geko_imu[0]
        
        Lorientation = tf.transformations.quaternion_from_euler(0,0,euler_orient)
        orientation = Quaternion()
	orientation.x = Lorientation[0]
	orientation.y = Lorientation[1]
	orientation.z = Lorientation[2]
	orientation.w = Lorientation[3]

        msg = Imu()
        msg.header.stamp = time
        msg.header.frame_id = self.base_frame
	
        msg.orientation = orientation
        msg.orientation_covariance = [self.cov_or,0.0,0.0, 0.0,self.cov_or,0.0, 0.0,0.0,self.cov_or]
        
        msg.angular_velocity = angv
        msg.angular_velocity_covariance = angv_cov
        
        msg.linear_acceleration = lacc
        msg.linear_acceleration_covariance = lacc_cov
        
        
        self.pub_fuse_imu.publish(msg)        
  
        


if __name__ == "__main__":    
    rospy.init_node("fuseImu", anonymous=False, log_level=rospy.DEBUG)
    fi = fuseImu(1.0)
