#!/usr/bin/env python
# coding: utf-8

import rospy
import actionlib
import sys
from std_msgs.msg import Bool
from std_srvs.srv import Empty
import numpy as np
from std_msgs.msg import Float64
from geometry_msgs.msg import Point, Quaternion, PoseStamped, PoseWithCovarianceStamped, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from tf.transformations import quaternion_from_euler, euler_from_quaternion, euler_matrix, euler_from_matrix

from rosbot_gazebo.msg import comp_rts, comp_odom
from tf.msg import tfMessage

from rosbot_gazebo.srv import transfo,boolean

import csv
import random

import os
from datetime import datetime,timedelta

class bareboneWaypointsManager():    
    """
    Class managing the sending of waypoints to the robot, the beginnings of the recovery_behaviour
    from rtabmap, and the savings of data which would be useful for statistics. 
    """

    def __init__(self,waypoints,data_path,repeat=1,metric_eval = "relocalization",number_eval_required = 100,ntry = 3):
        """
        Constructor of the class bareboneWaypointsManager.

        Parameters
        ----------
        waypoints : List
            List of the waypoints to be reached by the robot.
        data_path : String
            Path to the data folder.
        repeat : int, optional
            Number of time the path to all the waypoints should be repeated. The default is 1.
        number_eval_required : int, optional
            Parameter used for statistics. Fix the number of kidnapping needed to stop the
            algorithm. The default is 100.
        ntry : int, optional
            Number of attempts to reach a waypoint by the algorithm. The default is 3.

        Returns
        -------
        None.

        """
        
        rospy.init_node('bareboneWaypointsManager', anonymous=True)
        
        self.try_transf = rospy.get_param("~try_transf")
        
        self.metric_eval = metric_eval


        # Parameters used for the covariance relocalization trigger
        # If the covariance of AMCL pose is superior to the value cov_trigger
        # and if the last covariance relocalization occured after a duration time_cov_trigger, 
        # then the relocalization is activated.
        
        self.time_cov_trigger = 10.0 # np.inf #10.0
        self.cov_trigger = 1.0

        # Parameters used for the no movement relocalization trigger        
        # If the robot has not moved for a time superior to the duration time_no_move_trigger,
        # (detected by the publication of /amcl_pose; this publication also depends on the variation
        # of the position and of the orientation of the robot since the last publicated pose),
        # then the relocalization is activated.
        
        self.time_no_move_trigger = 6.0 # np.inf #6.0        
        self.last_time_amcl = -1
        self.nomove_amcl = True
        
        # Parameters used to save datas       
        
        self.time_save_pose_trigger = 0.5
        self.last_time_save_pose = -1
        
        self.L_diff_norm_rot_rts = []
        self.L_diff_norm_trans_rts = []
        self.L_time_rts = []
        self.L_time_amcl = []
        self.L_time_odom = []
        self.L_diff_norm_rot_amcl = []
        self.L_diff_norm_trans_amcl = []
        self.L_diff_norm_rot_odom= []
        self.L_diff_norm_trans_odom = []        
        self.L_gazebo_jump = []
        self.L_relocation_event = []
        self.L_true_trans=  []
        self.L_true_rot = []    

        self.relocation_event = " "
        
        self.diff_norm_rot_rts = -1
        self.diff_norm_trans_rts = -1

        self.diff_norm_rot_amcl = -1
        self.diff_norm_trans_amcl = -1       
        
        self.diff_norm_rot_odom = -1
        self.diff_norm_trans_odom = -1     
        self.time_odom= -1         

        self.actual_pose = np.array([0,0,0])
        self.actual_rot = np.array([0,0,0])
        
        self.true_pose = np.array([0,0,0])
        self.true_rot = np.array([0,0,0])
        
        self.final = " "
        
        # Parameters used to manage the publication of datas
        self.record_poses = True
        self.gazebo_jump = False
        self.data_path = data_path        
        
        # Parameters used to manage the publication of goals

        self.ntry = ntry
        self.number_eval_required = number_eval_required
        self.repeat = repeat
        self.waypoints = waypoints
        
        # Parameters used to manage the relocalization process

        self.recov_rts= False
        self.rot_speed_recovery = 1.0
        self.pub_amcl = rospy.Publisher("/amcl/initialpose", PoseWithCovarianceStamped)
        self.x_amcl,self.y_amcl,self.tht_amcl = 0,0,0
        self.record_rts = True
        self.rtab_cov = 0.1
        self.rtab_time = -1
        self.M_gmap2rtab = np.eye(4,4)
        self.M_rtab2gmap = np.eye(4,4)



        self.move_base = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
        rospy.loginfo("--------------------Waiting for move_base action server... ------------------------")
        # Wait a huge time for the action server to become available
        activate = self.move_base.wait_for_server(rospy.Duration(1000000))
        if activate:
            rospy.loginfo("-------Connected to move base server -----------------")
            
            self.number_eval = 0
            self.sub_odom = rospy.Subscriber("/comp_odom",comp_odom,self.callback_odom)
            self.sub_rtab = rospy.Subscriber("/comp_rts_1",comp_rts,self.callback_rtabmap)
            self.sub_AMCL_err = rospy.Subscriber("/comp_amcl",comp_rts,self.callback_amcl_err)
            self.pub_cmd = rospy.Publisher("/cmd_vel", Twist)
            
            rospy.loginfo("Starting move base goals smoother")            
            self.main()
        else:
            rospy.loginfo("---------- No connection available. Stop. ---------------------")
        return(None)

    def callback_odom(self,msg):
        """
        Callback for the subscriber to the /comp_odom topic. (comparaison topic between
        the odometry topic and the true pose of the robot)

        Parameters
        ----------
        msg : comp_odom.msg
            Message containing the odometry pose, the true pose, and the error of the
            odometry pose (compared to the true pose)
        Returns
        -------
        None.
        
        """
        if self.record_poses : 
            self.diff_norm_trans_odom = msg.diff_norm_trans
            self.diff_norm_rot_odom = msg.diff_norm_rot       
            self.time_odom = msg.stamp.to_sec()
            self.true_pose = np.array([msg.trans_tr[0],msg.trans_tr[1],msg.trans_tr[2]])
            self.true_rot = np.array([msg.rot_tr[0],msg.rot_tr[1],msg.rot_tr[2]])
            if self.gazebo_jump == False:
                self.gazebo_jump = msg.gazebo_jump

    def callback_tf(self,msg):
        """
        Callback for the subscriber to the /tf topic. 
        Here, this is used as some kind of "while" loop, which depends on the publication
        of data (and therefore can be stopped and then restarted depending on the state of
        the Gazebo simulation or the rosbag).
        
        Here, it is used to :
            - save the poses and the deviations from the truth of the odometry, AMCL
            and Rtabmap poses, and other informations
            - monitor the movement of the robot, to activate the no_movement trigger
        
        Parameters
        ----------
        msg : tf.msg
            Message containing the transformations to every frame declared.
        Returns
        -------
        None.
        
        """
        time = msg.transforms[0].header.stamp.to_sec()
        
        if (  self.last_time_amcl != -1 and self.rtab_time.to_sec() != -1 and self.time_odom != -1) and self.record_poses == True and time - self.last_time_save_pose > self.time_save_pose_trigger:
            self.record_poses = False
            self.save_poses()
            self.last_time_save_pose = time
            self.record_poses = True

        if  self.last_time_amcl != -1 and self.record_rts == True and time - self.last_time_amcl > self.time_no_move_trigger:
            self.nomove_amcl = True
        else:
            self.nomove_amcl = False

        if  self.nomove_amcl== True :
            self.last_time_amcl = -1
            if self.recov_rts == False:
                rospy.loginfo("Relocalisation since the robot has not moved for a time superior to %fs",self.time_no_move_trigger)
                self.rtabmap_relocalization("Relocalisation since the robot has not moved for a time superior to %fs"%self.time_no_move_trigger)

    def save_poses(self):
        """
        Save :
            * The poses and the deviations from the truth of the odometry, AMCL
            and Rtabmap poses
            * If a kidnapping occurred (via gazebo_jump)
            * If a relocalization event occured (via relocation_event)
        
        Into lists.
            
        Parameters
        ----------
        None

        Returns
        -------
        None.
        
        """        
        self.L_diff_norm_rot_rts.append(self.diff_norm_rot_rts)
        self.L_diff_norm_trans_rts.append(self.diff_norm_trans_rts)
        self.L_time_rts.append(self.rtab_time.to_sec())
        self.L_diff_norm_rot_amcl.append(self.diff_norm_rot_amcl)
        self.L_diff_norm_trans_amcl.append(self.diff_norm_trans_amcl)
        self.L_time_amcl.append(self.last_time_amcl)
        self.L_gazebo_jump.append(self.gazebo_jump)
        self.L_diff_norm_rot_odom.append(self.diff_norm_rot_odom)
        self.L_diff_norm_trans_odom.append(self.diff_norm_trans_odom)
        
        self.L_time_odom.append(self.time_odom)
        
        self.L_relocation_event.append(self.relocation_event)
        
        self.L_true_trans.append(self.true_pose)
        self.L_true_rot.append(self.true_rot)
        
        if self.gazebo_jump == True and self.metric_eval == "gazebo_jump":
           self.number_eval +=1
           rospy.loginfo("Number of gazebo jumps recorded : %i"%self.number_eval)
        
        self.gazebo_jump = False
        self.relocation_event = " "

    def callback_amcl_err(self,msg):
        """
        Callback for the subscriber to the /comp_amcl topic. (comparaison topic between
        the amcl_pose topic and the true pose of the robot)

        Parameters
        ----------
        msg : comp_rts.msg
            Message containing the AMCL pose, the true pose, and the error of the
            AMCL pose (compared to the true pose)
        
        Returns
        -------
        None.
        
        """
        if self.record_poses :
            self.diff_norm_rot_amcl = msg.diff_norm_rot
            self.diff_norm_trans_amcl = msg.diff_norm_trans
            if self.gazebo_jump == False:
                self.gazebo_jump = msg.gazebo_jump

    def callback_amcl_pose(self,msg):
        """
        Callback for the subscriber of the /amcl_pose topic. (topic used to send
        the pose of the robot )
        It is also used as a monitor to activate the covariance_relocalization trigger.

        Parameters
        ----------
        msg : PoseWithCovarianceStamped
            Message containing the pose and orientation of the robot for the AMCL
            algorithm, and the covariance of this pose.
        
        Returns
        -------
        None.
        
        """        
        if self.record_poses:    
            self.last_time_amcl = msg.header.stamp.to_sec()
            Lcov = [msg.pose.covariance[0],msg.pose.covariance[7],msg.pose.covariance[35]]
            
            self.x_amcl = msg.pose.pose.position.x
            self.y_amcl = msg.pose.pose.position.y
            
            q = (msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w)
            
            self.tht_amcl= euler_from_quaternion(q)[2]

            if max(Lcov) > self.cov_trigger and (rospy.Time.now().to_sec() - self.begin) > self.time_cov_trigger:
                if max(Lcov) == msg.pose.covariance[0]:
                    err = "x axis"
                    value = msg.pose.covariance[0]
                if max(Lcov) == msg.pose.covariance[7]:
                    err = "y axis"
                    value = msg.pose.covariance[7]
                if max(Lcov) == msg.pose.covariance[35]:
                    err = "theta value"
                    value = msg.pose.covariance[35] 
                if self.recov_rts == False:
                    rospy.loginfo("Relocalisation as the robot has a huge covariance on %s (value : %f)",err,value)
                    self.rtabmap_relocalization("Relocalisation as the robot has a huge covariance on %s (value : %f)"%(err,value))
                self.begin = rospy.Time.now().to_sec()

    def rad2deg(self,ang):
        """
        
        Turn an angle expressed as [-pi ; pi] radians into [0 ; 360] degrees
        

        Parameters
        ----------
        ang : float
            An angle expressed on [-pi; pi].

        Returns
        -------
        deg : float
            The same angle expressed on [0; 360] degrees.

        """
        
        # -pi ; pi to 0 ; 2*pi
        if ang <0:
            ang = 2*np.pi+ang
            
        deg = ang*180/np.pi
        return(deg)

    def cmd_oneTurn(self):
        """
        Send to the robot the order to do one turn on himself.

        Returns
        -------
        None.

        """
        beg_rot = self.rad2deg(self.actual_rot[2])
        actual_rot = beg_rot
        prev_rot = beg_rot
        nturn = 0
        
        msg = Twist()
        msg.linear.x = 0.0
        msg.linear.y=0.0
        msg.linear.z=  0.0
        msg.angular.x= 0.0
        msg.angular.y= 0.0
        msg.angular.z= self.rot_speed_recovery
        
        print("Beg angle : ", beg_rot)
        
        i = 0
        
        while not rospy.is_shutdown() and actual_rot-beg_rot < 360.0:
            actual_rot = self.rad2deg(self.actual_rot[2])+nturn*360.0
            if prev_rot- actual_rot > 180:
                nturn+=1
                
                # print("Prev angle : ", prev_rot)
                # print("Actual angle in rad : ", self.actual_rot[2])
                # print("Actual angle : ", actual_rot)
            prev_rot = actual_rot
            self.pub_cmd.publish(msg)
            #i+=1
            # if i%10000 == 0:
            #     print("Actual angle in rad : ", self.actual_rot[2])
            #     print("Actual rot in degrees : ", prev_rot)
        
        print("Angle made : ", actual_rot - beg_rot)
        
        msg = Twist()
        msg.linear.x = 0.0
        msg.linear.y=0.0
        msg.linear.z=  0.0
        msg.angular.x= 0.0
        msg.angular.y= 0.0
        msg.angular.z=0.0
        self.pub_cmd.publish(msg)
        
    def rtabmap_relocalization(self,reloc_event):
        """
        Function managing the relocalization process with Rtabmap.
        Works as follow : 
            * [ can be added : start rtabmap]
            * Order to the robot to do one turn on himself, in order to recognize
            his location.
            * Use the rtabmap pose. 
            * Clear the costmaps of the robot.
            * [ can be added : stop rtabmap]

        Parameters
        ----------
        reloc_event : string
            Description of the trigger of the relocalization process.
            As the relocalization process can be a little bit long (which
            means that new poses could be recorded during this process), indicates
            first when the relocalization starts, then indicates the moment when it
            actually happens.

        Returns
        -------
        None.

        """
        self.recov_rts = True
        self.relocation_event = "Start of relocalization. " + reloc_event
        self.cmd_oneTurn()
        self.record_rts = False
        self.relocation_event = reloc_event        
        self.use_rtabmap_pose()
        self.record_rts = True
        self.clear_costmap()   
        if self.metric_eval == "relocalization":
            self.number_eval+=1
            rospy.loginfo("Number of relocalizations recorded : %i"%self.number_eval)
        self.recov_rts = False

    def callback_rtabmap(self,msg):
        """
        Callback for the subscriber of the /comp_rts_1 topic. (comparaison topic between
        the /rtabmap1/MapGraph topic and the true pose of the robot)

        Parameters
        ----------
        msg : comp_rts.msg
            Message containing the Rtabmap pose, the true pose, and the error of the
            Rtabmap pose (compared to the true pose)
        
        Returns
        -------
        None.
        
        """        
        if self.record_rts and self.record_poses:
            self.actual_pose = np.array([ msg.trans_rts[0], msg.trans_rts[1], msg.trans_rts[2]] )
            self.actual_rot = np.array([ msg.rot_rts[0], msg.rot_rts[1], msg.rot_rts[2]] )
            self.rtab_time = msg.stamp
            self.diff_norm_rot_rts = msg.diff_norm_rot
            self.diff_norm_trans_rts = msg.diff_norm_trans
            if self.gazebo_jump == False:
                self.gazebo_jump = msg.gazebo_jump
        
    def use_rtabmap_pose(self):
        """
        
        Method used to send the Rtabmap pose to the /amcl/initial_pose topic. (topic used
        to send the "true" pose of the robot to the AMCL algorithm)
        The message sent is a PoseWithCovarianceStamped. The covariance is determined 
        by the difference between the AMCL pose and the Rtabmap pose, restricted by 
        an assumption on the covariance of the Rtabmap algorithm (fixed by rtab_cov).

        Parameters
        ----------
        None.
        
        Returns
        -------
        None.
        
        """
        # do the transformation between the pose into the rtabmap referential and
        # the pose into the AMCL referential.

        print("Rtab2gmap : trans : ", self.M_rtab2gmap[:3,3])
        print("Rtab2gmap : euler angles : ", euler_from_matrix(self.M_rtab2gmap))        

        actual_pose = np.dot(self.actual_pose,self.M_rtab2gmap[:3,:3].T)
        actual_pose += self.M_rtab2gmap[:3,3]
        
        # M_rot = euler_matrix(self.actual_rot[0], self.actual_rot[1], self.actual_rot[2])
        
        # M_rot = np.dot(M_rot.T, self.M_rtab2gmap)        
        # # M_rot = np.dot(M_rot.T, self.M_rtab2gmap.T)        
        # #M_rot = np.dot(M_rot, self.M_rtab2gmap)
        # actual_rot = np.array(euler_from_matrix(M_rot) )
        
        rl = euler_from_matrix(self.M_rtab2gmap)
        actual_rot = np.array([self.actual_rot[0], self.actual_rot[1], self.actual_rot[2] + rl[2] ])

        
        print("Pose in rtabmap ref : ", self.actual_pose)
        print("Pose in gmap ref : ", actual_pose)
        
        print("Rot in rtabmap ref : ", self.actual_rot)
        print("Rot in gmap ref : ", actual_rot)
        
        # prepare the message that needs to be sent

        msg = PoseWithCovarianceStamped()
        msg.header.stamp = self.rtab_time
        msg.header.frame_id = "map_gmap"
        
        msg.pose.pose.position.x = actual_pose[0]
        msg.pose.pose.position.y = actual_pose[1]        
        msg.pose.pose.position.z = actual_pose[2]
        
        q_angle = quaternion_from_euler(actual_rot[0], actual_rot[1], actual_rot[2], axes='sxyz')
        msg.pose.pose.orientation.x = q_angle[0]
        msg.pose.pose.orientation.y = q_angle[1]
        msg.pose.pose.orientation.z = q_angle[2]
        msg.pose.pose.orientation.w = q_angle[3]
        
        covx = min(self.rtab_cov, np.sqrt(abs(self.x_amcl-self.actual_pose[0])) )
        covy = min(self.rtab_cov, np.sqrt(abs(self.y_amcl-self.actual_pose[1])) )
        covt = min(self.rtab_cov, abs(self.tht_amcl-self.actual_rot[2]) )
        
        msg.pose.covariance = np.array([covx, 0.0, 0.0, 0.0 ,0.0, 0.0,
                                        0.0, covy, 0.0, 0.0 ,0.0, 0.0,
                                        0.0, 0.0, 0.0, 0.0 ,0.0, 0.0,
                                        0.0, 0.0, 0.0, 0.0 ,0.0, 0.0,
                                        0.0, 0.0, 0.0, 0.0 ,0.0, 0.0,
                                        0.0, 0.0, 0.0, 0.0 ,0.0, covt])
        self.pub_amcl.publish(msg)

    def main(self):
        """
        Manage the publication of goals, and the events associated with /move_base
        (goal reached or not). 

        Returns
        -------
        None.

        """
        tst= rospy.ServiceProxy('get_transfo', transfo)
        rospy.sleep(3.) # make sure that subscribers have seen this node before sending a goal
            
        v= 0 
        resp1 = None
        transf = np.array([])
        while not rospy.is_shutdown() and resp1 == None :
            try : 
                msg = tst(True)
                Ltransf = msg.transf
                transf = np.array([list(Ltransf)])
                resp1 = True
                            
            except rospy.ServiceException as e :
                if v%100 == 0:
                    print("Service call failed : %s"%e)
                if (v) > 1e6:
                    print("Something's wrong...")
                    resp1 = False
                v+=1        
        
        if resp1:
            if self.try_transf:
                M = transf.reshape(3,3)
                self.M_gmap2rtab[:2,:2] = M[:2,:2]
                self.M_gmap2rtab[:2,3] = M[:2,2]
                self.M_rtab2gmap = np.linalg.inv(self.M_gmap2rtab)

            try:
                os.mkdir(self.data_path+"/waypointManager")
            except OSError:
                print("Folder"+self.data_path+"/waypointManager" +" already existing.")
            try:
                os.mkdir(self.data_path+"/waypointManager"+"/"+self.metric_eval)
            except OSError:
                print("Folder"+self.data_path+"/waypointManager"+"/"+self.metric_eval +" already existing.")
            tdate = datetime.utcnow()+timedelta(hours=2)
            ndate = str(tdate.date())+ "-" + str(tdate.hour) + "-"+ str(tdate.minute)
            self.folder_path = self.data_path+"/waypointManager"+"/"+self.metric_eval+"/Experiment_%s"%ndate 
            try:
                os.mkdir(self.folder_path)
            except OSError:
                print("Folder"+self.folder_path+" already existing.")
                
            self.begin = rospy.Time.now().to_sec()
            self.sub_amcl_pose = rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped,self.callback_amcl_pose )

            self.sub_tf = rospy.Subscriber("/tf",tfMessage,self.callback_tf)
            l = 0
            while l < (self.repeat):
                random.shuffle(self.waypoints)
                                        
                k = 0
                ntry = 0        
                while k < (len(self.waypoints)):
                    rospy.loginfo("Go to point %i : x : %f ; y : %f ; tht : %f",k,self.waypoints[k][0],self.waypoints[k][1],self.waypoints[k][2])
                    
                    msg = self.move_base_goal_cb(self.waypoints[k])
                    rospy.sleep(2.) # make sure that subscribers have seen the node before sending the goal

                    self.move_base.send_goal(msg)
                    result = self.move_base.wait_for_result()
                    print("Result obtained for % i : "%k,result)
                    if result == True:
                        print("Details : ",self.move_base.get_goal_status_text() )
                        details = self.move_base.get_state()
                        print("code : ", details)
                        if details > 3 :
                            # do the recovery process
                            if self.recov_rts == False:
                                rospy.loginfo("Relocalisation as all the recovery strategies have failed.")
                                self.rtabmap_relocalization("Relocalisation as all the recovery strategies have failed.")
                                ntry +=1
                            if self.ntry > ntry :
                                k-=1
                                print("-------- Retry ... -----------")
                            else:
                                # if move_base has failed too many times, go to another objectif.
                                print("Impossible to go to objectif after %i tests. Skip the waypoint."%self.ntry)
                                self.final = "Impossible to go to objectif after %i tests. Skip the waypoint."%self.ntry
                                
                                name = self.folder_path+"/waypoint%i.csv"%(k+l*len(self.waypoints))
                                self.save_data(name,k)
                                self.clean()
                                ntry = 0

                        else:
                            # means that the waypoint has been reached
                            self.final = "Waypoint reached!"
                            name = self.folder_path+"/waypoint%i.csv"%(k+l*len(self.waypoints))
                            self.save_data(name,k)
                            self.clean()
                            ntry = 0
                    k+=1
                    if self.number_eval > self.number_eval_required:
                        # if enough jumps have been recorded, stop the algorithm
                        if ntry != 0:
                            self.final = "Impossible to go to objectif after %i tests. Skip the waypoint."%ntry
                            name = self.folder_path+"/waypoint%i.csv"%(k+l*len(self.waypoints))
                            self.save_data(name,k)
                            self.clean()
                            ntry = 0
                            
                        k = len(self.waypoints)
                        l = self.repeat
                        print("--------   Enough %s recorded. End of algorithm. ------------"%self.metric_eval)

                if l < self.repeat:
                    # if all the waypoints have been reached, do another time the
                    # path to the waypoints, into a different order
                    print("--------   All waypoints sent! ------------")
                    l +=1
            os.system("rosnode kill --all")

    def clear_costmap(self):
        tst= rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
        v= 0 
        resp1 = None
        while not rospy.is_shutdown() and resp1 == None :
            try : 
                msg = tst()
                resp1 = True
                            
            except rospy.ServiceException as e :
                if v%100 == 0:
                    print("Service call failed : %s"%e)
                if (v) > 1e6:
                    print("Something's wrong...")
                    resp1 = False
                v+=1        
        if resp1:
            rospy.loginfo("Clear_costmaps done.")
            
        # it seems that the clear_unknown_space service does not exist anymore.
        # tst= rospy.ServiceProxy('/move_base/clear_unknown_space', Empty)        
        # resp1 = None
        # while not rospy.is_shutdown() and resp1 == None :
        #     try : 
        #         msg = tst()
        #         resp1 = True
                            
        #     except rospy.ServiceException as e :
        #         if v%100 == 0:
        #             print("Service call failed : %s"%e)
        #         if (v) > 1e6:
        #             print("Something's wrong...")
        #             resp1 = False
        #         v+=1      
        # if resp1:
        #     rospy.loginfo("Clear_unknown_space done.")            

    def save_data(self,name,k):
        """
        
        Save the datas about the path to the given waypoint into a .csv file.

        Parameters
        ----------
        name : string
            Complete path to the csv file.
        k : int
            Number of the waypoint of the path.

        Returns
        -------
        None.

        """
        f = open(name,'w')
        with f:
            
            fnames = ["Trans_err_amcl","Rot_err_amcl","Associated_time_amcl",
                       "Trans_err_rts","Rot_err_rts","Associated_time_rts",
                       "Trans_err_odom","Rot_err_odom","Associated_time_odom",
                       "Real_pose", "Real_rot",
                       "Huge change in Gazebo","Relocalisation event"]
            num_col = len(fnames)

            writer = csv.writer(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
            
            L = ["Waypoint number %i :"%k,"x",self.waypoints[k][0],"y",self.waypoints[k][1],"tht",self.waypoints[k][2]]
            
            writer.writerow(L + [" " for l in range(max(0,num_col - len(L) ))])
            writer.writerow([" " for l in range(num_col)])
            writer.writerow(fnames)
            
    
            length = len(self.L_diff_norm_trans_amcl)
            for l in range(length):
                L = [self.L_diff_norm_trans_amcl[l],  self.L_diff_norm_rot_amcl[l], self.L_time_amcl[l],
                     self.L_diff_norm_trans_rts[l],  self.L_diff_norm_rot_rts[l], self.L_time_rts[l],
                     self.L_diff_norm_trans_odom[l],  self.L_diff_norm_rot_odom[l], self.L_time_odom[l],
                     "[%f | %f | %f]"%(self.L_true_trans[l][0],self.L_true_trans[l][1],self.L_true_trans[l][2]),
                     "[%f | %f | %f]"%(self.L_true_rot[l][0],self.L_true_rot[l][1],self.L_true_rot[l][2]),
                     self.L_gazebo_jump[l], self.L_relocation_event[l] ]
                writer.writerow(L)
            writer.writerow([" " for l in range(num_col)])
            writer.writerow([self.final] + [" " for l in range(num_col-1)])
        return(True)        


    def clean(self):
        """
        Purge every list containing datas about a path.

        Returns
        -------
        None.

        """
        self.L_diff_norm_rot_rts = []
        self.L_diff_norm_trans_rts = []
        self.L_time_rts = []
        self.L_time_amcl = []
        self.L_time_odom = []
        self.L_diff_norm_rot_amcl = []
        self.L_diff_norm_trans_amcl = []
        self.L_diff_norm_rot_odom= []
        self.L_diff_norm_trans_odom = []        
        self.L_gazebo_jump = []
        self.L_relocation_event = []
        self.L_true_trans=  []
        self.L_true_rot = []

    def move_base_goal_cb(self, waypoint):
        """
        
        Function called when entering the MOVE_BASE state for the definition of the goal.
        
        Parameters
        ----------
        waypoint : numpy array ( np.array([x, y, tht]) )
            Coordinates of the waypoint to be reached.
        
        """
        nav_goal = MoveBaseGoal()
        nav_goal.target_pose.header.frame_id = 'map_gmap'

        position = Point(waypoint[0], waypoint[1], 0.22)
        q_angle = quaternion_from_euler(0, 0, waypoint[2], axes='sxyz')
        orientation = Quaternion(*q_angle)

        nav_goal.target_pose.pose.position = position
        nav_goal.target_pose.pose.orientation = orientation

        return nav_goal
        


if __name__ == '__main__':
    
    waypoints = [                
                [7, -0.6, np.pi],
        
                [3.7, -0.6, np.pi],
                [3.7, 2.8, np.pi],
                 [3.7, 4.7, np.pi],


                 [3.7, -2.7, np.pi],

                 
                 [8.5, -2.7, np.pi],
                 [8.6, -0.6, np.pi],
                 [8.6, 2.8, np.pi],
                 [8.6, 4.7, np.pi],
                 [7, 4.7, np.pi],
                 [7, 2.8, np.pi],
                 [7, -0.6, np.pi],
                 [7, -2.7, np.pi],
                 [5.4, -2.7, np.pi],                 
                 [5.4, -0.6, np.pi],
                 [5.4, 2.8, np.pi],
                 [5.4, 4.7, np.pi],

                 
                 ]
    
    data_path = "/home/tnmx0798/Documents/ROSBot_simu/data"
    
    cm = bareboneWaypointsManager(waypoints,data_path,18,"relocalization",300,6)
    