#!/usr/bin/env python
# coding: utf-8

# Import python
import json
# Import ros
import rospy
from tf import TransformListener, LookupException, ConnectivityException, ExtrapolationException
from tf.transformations import euler_from_quaternion
from std_msgs.msg import String
#  Import service
from database_manager import DatabaseManager


class WaypointsManager:

    # -- PARAM --
    # Timeout to get transform message
    GET_TRANSFORM_TIMEOUT = 3
    # Instance of DatabaseManager
    db_manager = None

    # TF Listener
    listener = None
    map_frame = None
    base_frame = None

    # -- Subscriber --
    wp_creation_sub = None

    def __init__(self, init_node=True, node_name="waypoints_manager", debug=False):
        # Init node
        if init_node and debug:
            rospy.init_node(node_name, anonymous=False, log_level=rospy.DEBUG)
        elif init_node:
            rospy.init_node(node_name, anonymous=False, log_level=rospy.INFO)

        # Init subscriber
        self.wp_creation_sub = rospy.Subscriber(node_name+'/create_waypoint', String, self.cb_parse_topic_data)

        # Get ROS Param
        self.map_frame = rospy.get_param("~map-frame", "map")
        self.base_frame = rospy.get_param("~base-frame", "base-frame")

        # Init tf listener
        self.listener = TransformListener()

        # Init database manager
        self.db_manager = DatabaseManager.get_instance()

        rospy.loginfo("[WaypointsManager] Ready")
        rospy.on_shutdown(self.on_shutdown)
        rospy.spin()

    """ Callback to parse order from /waypoint_manager/order
    """
    def cb_parse_topic_data(self, data):
        # Convert JSON string to Python dictionary
        print data.data
        data = json.loads(data.data)
        # Check message construction
        if "name" in data and "group" in data and "map" in data:
            self.create_waypoint(
                data["name"],
                data["map"],
                data["group"]
            )

    def create_waypoint(self, name, map, group):
        transform_get = False
        timeout = rospy.Time.now().to_sec() + self.GET_TRANSFORM_TIMEOUT
        pose = []

        # On récupère la position du robot via la tf entre la map et la base du robot
        while not transform_get and timeout > rospy.Time.now().to_sec():
            try:
                (trans, rot) = self.listener.lookupTransform(self.map_frame, self.base_frame, rospy.Time(0))
            except (LookupException, ConnectivityException, ExtrapolationException):
                rospy.logwarn("[WaypointsManager] Failed to get transform")
            else:
                transform_get = True

                # On transforme le quaternion pour obtenir l'angle en radian
                y = euler_from_quaternion(rot, axes='sxyz')[2]
                pose = [trans[0], trans[1], y]

                rospy.loginfo("[WaypointsManager] Robot position: "+str(pose))

                #  Envoie du waypoint sur la base de données
                wp_data = {
                    "point": pose,
                    "name": name,
                    "map": map,
                    "group": group
                }
                self.db_manager.send_waypoint(wp_data)

    def on_shutdown(self):
        self.wp_creation_sub.unregister()
        rospy.loginfo("[WaypointsManager] Stopped")


if __name__ == "__main__":
    wpm = WaypointsManager()
