import math
import numpy as np
import matplotlib.pyplot as plt
from icp import icp


if __name__ == '__main__':
    # set seed for reproducible results
    np.random.seed(12345)

    # create a set of points to be the reference for ICP
    xs = np.random.random_sample((50, 1))
    ys = np.random.random_sample((50, 1))
    reference_points = np.hstack((xs, ys))

    # transform the set of reference points to create a new set of
    # points for testing the ICP implementation

    # 1. remove some points
    points_to_be_aligned = reference_points#[1:47]

    # 2. apply rotation to the new point set
    theta = math.radians(12)
    c, s = math.cos(theta), math.sin(theta)
    rot = np.array([[c, -s],
                    [s, c]])
    points_to_be_aligned = np.dot(points_to_be_aligned, rot)

    # 3. apply translation to the new point set
    tr1 = np.random.random_sample()
    tr2 = np.random.random_sample()
    
    trans = np.array([tr1,tr2])
    
    points_to_be_aligned += trans

    # run icp
    transformation_history, aligned_points = icp(reference_points, points_to_be_aligned, verbose=True)

    # show results
    M = np.eye( 3,3 )
    Mt = np.eye(3,3)
    for k in range(len(transformation_history)):
        Mt[:2,:2] = np.dot(transformation_history[k][:2,:2].T, Mt[:2,:2])
        Mt[:2,2] = np.dot(Mt[:2,2],transformation_history[k][:2,:2].T)+transformation_history[k][:2,2]

        # transformation_history[k][:2,:2] = transformation_history[k][:2,:2].T
        # transformation_history[k][:2,2] = -transformation_history[k][:2,2]
        # transformation_history[k] = np.vstack( (transformation_history[k],np.array([[0,0,1]])) )
        # M = np.dot(transformation_history[k],M)

    # Test propre
    Mt[:2,:2] = (Mt[:2,:2].T)    
    M = np.linalg.inv(Mt)

    print("Rot ori (ref -> p2b): ", rot)
    
    print("Trans ori (ref -> p2b): ", trans)    

    points_icp = np.dot(reference_points, M[:2,:2].T)
    points_icp +=M[:2,2]   
    print("Rot ref -> to be aligned : ", M[:2,:2].T)
    print("Trans ref -> to be aligned : ", M[:2,2])

    
    M2 = np.linalg.inv(M)

    print("Rot to be aligned -> ref: ", M2[:2,:2].T)

    print("Trans to be aligned -> ref: ", M2[:2,2])
    
    points_icp2 = np.dot(points_to_be_aligned, M2[:2,:2].T)
    points_icp2 += M2[:2,2]    
    
    
    plt.plot(reference_points[:, 0], reference_points[:, 1], 'rx', label='reference points')
    plt.plot(points_to_be_aligned[:, 0], points_to_be_aligned[:, 1], 'b1', label='points to be aligned')
    plt.plot(aligned_points[:, 0], aligned_points[:, 1], 'g+', label='aligned points')
    plt.scatter(points_icp[:, 0], points_icp[:, 1], color = 'orange', marker='*', label='reference to tobealigned')
    plt.scatter(points_icp2[:, 0], points_icp2[:, 1], color = 'cyan', marker='*', label='tobealigned to reference')
    plt.legend()
    plt.show()


