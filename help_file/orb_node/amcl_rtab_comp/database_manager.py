#!/usr/bin/env python
# coding: utf-8

# Import python package
import os
import json
import logging
from pymongo import MongoClient

# Import ros package
import rospy

# Import project package


class DatabaseManager:
    """
    L'objectif de cette classe est de pouvoir (re)initialiser la base de données Mongo

    !! Classe singleton !!
    L'appel direct au constructeur n'est pas autorisé !
    Pour obtenir l'instance de cette classe, utilisez DatabaseManager.getinstance()
    """

    # Instance de ManagerGeko (pattern singleton)
    __instance = None

    @staticmethod
    def get_instance():
        if DatabaseManager.__instance is None:
            DatabaseManager()
        return DatabaseManager.__instance

    # Info sur la base
    database_name = None
    database = None

    # Path to files
    waypoints_file = None

    def __init__(self):
        if DatabaseManager.__instance is not None:
            raise Exception("This class is a Singleton! Please call DatabaseManager.getInstance()")
        else:
            # Initialisation du client Mongo
            self.database_name = rospy.get_param("/database", "robotic")
            self.database = MongoClient()[self.database_name]

            # Initialisation des fichiers de données
            self.waypoints_file = rospy.get_param("/waypoints_file", os.path.dirname(os.path.realpath(__file__))
                                                  + "/../data/waypoints.json")

            rospy.loginfo("[DatabaseManager] init")

            DatabaseManager.__instance = self

    def send_waypoints_file(self, waypoints_file=None):
        if waypoints_file is None:
            waypoints_file = self.waypoints_file

        with open(waypoints_file, 'r') as data:
            waypoints_list = json.load(data)["waypoints"]

        for waypoint in waypoints_list:
            self.send_waypoint(waypoint)

    def send_waypoint(self, wp_data):
        """
        Envoie d'un waypoint en base de donnée

        :param wp_data: Données du waypoint: {point: [<float>, <float>, <float>], name: <str>, map: <str>, group: <str>}
        :type wp_data: dict
        :return: "success" en cas de succès | "failed: <error msg>" en cas d'échec
        :rtype: str
        """
        db_waypoint = self.database.waypoint

        if db_waypoint.find_one({"name": wp_data["name"]}) is None:
            waypoint_id = db_waypoint.insert_one(wp_data).inserted_id
            rospy.loginfo("[DatabaseManager] send_waypoint :: Waypoint named \""+wp_data["name"]+"\" insert with id "
                          + str(waypoint_id))
            return "success"
        else:
            rospy.loginfo(
                "[DatabaseManager] send_waypoint :: Waypoint named \"" + wp_data["name"]
                + "\" already exist in database")
            return "failed: Waypoint named \"" + wp_data["name"] + "\" already exist in database"


if __name__ == "__main__":
    DatabaseManager()
