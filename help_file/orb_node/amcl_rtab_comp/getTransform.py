#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 14:50:10 2020

@author: tnmx0798
"""

import math
import numpy as np
import matplotlib.pyplot as plt
from icp import icp

import rospy
from nav_msgs.msg import OccupancyGrid, MapMetaData

from rosbot_gazebo.srv import transfo,boolean


class getTransform():
    
    def __init__(self):
        rospy.init_node('getTransform', anonymous=True)
        self.waypoints= []
        


        self.max_size = 60
        self.min_size = 20
        
        self.marker_size = 50
        gmap_name = rospy.get_param("~gmap_name")


        self.sub_map_gmap = rospy.Subscriber(gmap_name,OccupancyGrid,self.callback_occupancy_grid_gmap)        
        self.sub_map_rtab = rospy.Subscriber("/rtabmap1/grid_prob_map",OccupancyGrid,self.callback_occupancy_grid_rtab) 

        self.origin_rtab = np.array([])

        self.height_rtab,self.width_rtab = 0,0
        self.resolution_rtab = 0
        
        self.map_rtab = np.array([])
        
        self.origin_gmap = np.array([])

        self.height_gmap,self.width_gmap = 0,0
        self.resolution_gmap = 0
        
        self.map_gmap = np.array([])
        
        self.M_gmap2rtab = np.array([])        

        while not rospy.is_shutdown() and (self.origin_rtab.shape[0] == 0 or self.origin_gmap.shape[0] == 0):
            pass

        # print("gmap reso : ",self.resolution_gmap)
        # print("rtab reso : ",self.resolution_rtab)      

        self.pc_gmap = self.map2pc(self.map_gmap,self.origin_gmap,self.resolution_gmap)
        
        self.pc_rtab = self.map2pc(self.map_rtab,self.origin_rtab,self.resolution_rtab)

        # ref : map_gmap
        # points to be aligned : map_rtab
        
        # on cherche donc à avoir la transformation ref -> p2b == map_gmap -> map_rtab
        
        transformation_history, aligned_points = icp(self.pc_gmap, self.pc_rtab, verbose=True, distance_threshold=0.6)
        M = np.eye( 3,3 )
        Mt = np.eye(3,3)
        for k in range(len(transformation_history)):
            Mt[:2,:2] = np.dot(transformation_history[k][:2,:2].T, Mt[:2,:2])
            Mt[:2,2] = np.dot(Mt[:2,2],transformation_history[k][:2,:2].T)+transformation_history[k][:2,2]
    
            # transformation_history[k][:2,:2] = transformation_history[k][:2,:2].T
            # transformation_history[k][:2,2] = -transformation_history[k][:2,2]
            # transformation_history[k] = np.vstack( (transformation_history[k],np.array([[0,0,1]])) )
            # M = np.dot(transformation_history[k],M)
    
        # Test propre
        
        Mt[:2,:2] = (Mt[:2,:2].T)    
        M = np.linalg.inv(Mt)
        
        self.M_gmap2rtab = M        

        self.display_service = rospy.Service('display_transfo', boolean, self.display_transfo)
        self.display_max_err = rospy.Service('get_transfo', transfo, self.get_transfo)
        rospy.spin()
    
    def get_transfo(self,msg):  
        L2 = self.M_gmap2rtab.flatten()
        return( [L2 ])
    
    def display_transfo(self,msg):
        # points icp : from gmap to rtab
        # points icp2 : from rtab to gmap 
        
        M = self.M_gmap2rtab
        points_icp = np.dot(self.pc_gmap, M[:2,:2].T)
        points_icp +=M[:2,2]   
        print("Rot ref -> to be aligned : ", M[:2,:2].T)
        print("Trans ref -> to be aligned : ", M[:2,2])
    
        
        M2 = np.linalg.inv(M)
    
        print("Rot to be aligned -> ref: ", M2[:2,:2].T)
    
        print("Trans to be aligned -> ref: ", M2[:2,2])
        
        points_icp2 = np.dot(self.pc_rtab, M2[:2,:2].T)
        points_icp2 += M2[:2,2]    
        
        
        plt.plot(self.pc_gmap[:, 0], self.pc_gmap[:, 1], 'rx', label='reference points (gmap points on gmap ref)')
        plt.plot(self.pc_rtab[:, 0], self.pc_rtab[:, 1], 'b1', label='points to be aligned (rtabmap points on rtabmap ref)')
        #plt.scatter(points_icp[:, 0], points_icp[:, 1], color = 'orange', marker='*', label='reference to tobealigned')
        plt.scatter(points_icp2[:, 0], points_icp2[:, 1], color = 'cyan', marker='*', label='rtabmap points on gmap ref')
        plt.legend()
        plt.show()        
        return(True)
        

        
    def map2pc(self,mapp,origin,resolution):
    
        transx = []
        transy = []
        
        for i in range(mapp.shape[0]):
            for j in range(mapp.shape[1]):
                if mapp[i,j] > 50:
                    transx.append(origin[0] + i*resolution)
                    transy.append(origin[1] + j*resolution)
        #print("transx : ", transx)
        Mtx = np.array(transx).reshape(len(transx),1)
        Mty = np.array(transy).reshape(len(transy),1)
        
        pc = np.hstack((Mty,Mtx))
        
        return(pc)


    def callback_occupancy_grid_gmap(self,msg):
        print("Got message gmap!")
        first_origin = np.array([msg.info.origin.position.x,msg.info.origin.position.y,msg.info.origin.position.z])

        self.height_gmap = msg.info.height
        self.width_gmap = msg.info.width
        self.resolution_gmap = msg.info.resolution
        
        first_map = np.array([msg.data]).reshape(self.height_gmap,self.width_gmap)
        print(first_map.shape)
        
        i_top = -1
        j_top = -1
        
        top_left = np.array([])
        bottom_right = np.array([])
        
        i = 0        
        while i < (first_map.shape[0]):
            if np.any(first_map[i,:] != -1):
                i_top = i
                i = first_map.shape[0]
            i+=1
        
        j = 0
        while j < (first_map.shape[1]):
            if np.any(first_map[:,j] != -1):
                j_top = j
                j = first_map.shape[1]
            j+=1

        top_left = np.array([first_origin[0] + i_top*self.resolution_gmap , first_origin[1] + j_top*self.resolution_gmap,first_origin[2] ])

        print("Top left : ", top_left)
        
        i_bot = -1
        j_bot = -1
        
        i = first_map.shape[0]-1
        while i> 0:
            if np.any(first_map[i,:] != -1):
                i_bot = i
                i = 0
            i-=1
            
        j = first_map.shape[1]-1
        while j> 0:
            if np.any(first_map[:,j] != -1):
                j_bot = j
                j = 0
            j-=1                

        bottom_right = np.array([first_origin[0] + i_bot*self.resolution_gmap , first_origin[1] + j_bot*self.resolution_gmap,first_origin[2] ])

        print("Bottom right : ", bottom_right)
        
        print("i_top, j_top : {%i,%i}"%(i_top,j_top))
        print("i_bot, j_bot : {%i,%i}"%(i_bot,j_bot))        
        
        self.map_gmap = first_map[ i_top : i_bot , j_top:j_bot ]
        self.origin_gmap = top_left
        
        #print(self.map)



    def callback_occupancy_grid_rtab(self,msg):
        print("Got message rtab!")
        first_origin = np.array([msg.info.origin.position.x,msg.info.origin.position.y,msg.info.origin.position.z])

        self.height_rtab = msg.info.height
        self.width_rtab = msg.info.width
        self.resolution_rtab = msg.info.resolution
        
        first_map = np.array([msg.data]).reshape(self.height_rtab,self.width_rtab)
        print(first_map.shape)
        
        i_top = -1
        j_top = -1
        
        top_left = np.array([])
        bottom_right = np.array([])
        
        i = 0        
        while i < (first_map.shape[0]):
            if np.any(first_map[i,:] != -1):
                i_top = i
                i = first_map.shape[0]
            i+=1
        
        j = 0
        while j < (first_map.shape[1]):
            if np.any(first_map[:,j] != -1):
                j_top = j
                j = first_map.shape[1]
            j+=1

        top_left = np.array([first_origin[0] + i_top*self.resolution_rtab , first_origin[1] + j_top*self.resolution_rtab,first_origin[2] ])

        print("Top left : ", top_left)
        
        i_bot = -1
        j_bot = -1
        
        i = first_map.shape[0]-1
        while i> 0:
            if np.any(first_map[i,:] != -1):
                i_bot = i
                i = 0
            i-=1
            
        j = first_map.shape[1]-1
        while j> 0:
            if np.any(first_map[:,j] != -1):
                j_bot = j
                j = 0
            j-=1                

        bottom_right = np.array([first_origin[0] + i_bot*self.resolution_rtab , first_origin[1] + j_bot*self.resolution_rtab,first_origin[2] ])

        print("Bottom right : ", bottom_right)
        
        print("i_top, j_top : {%i,%i}"%(i_top,j_top))
        print("i_bot, j_bot : {%i,%i}"%(i_bot,j_bot))        
        
        self.map_rtab = first_map[ i_top : i_bot , j_top:j_bot ]
        self.origin_rtab = top_left
        
        #print(self.map)        
        
if __name__ == "__main__":
    cm = getTransform()