#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy

from bson import ObjectId
from smach import State, StateMachine, Iterator
from smach_ros import ServiceState, SimpleActionState

from service_datacenter.srv import RequestSaveData
from std_msgs.msg import Float64
from geometry_msgs.msg import Point, Quaternion
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from tf.transformations import quaternion_from_euler

from utils import logger, database, CurrentModeManager as cmm

class Patrol(StateMachine):
    def __init__(self):
        StateMachine.__init__(self,
                              outcomes=['succeeded', 'lost', 'aborted', 'preempted'],
                              input_keys=['log_operating_mode'],
                              output_keys=['log_operating_mode'])

        with self:
            # ===== PATROL_MANAGER State =====
            StateMachine.add('PATROL_MANAGER', PatrolManager(),
                             transitions={'navigation': 'NAVIGATION',
                                          'aborted': 'aborted',
                                          'preempted': 'preempted'})

            # ===== NAVIGATION State =====
            StateMachine.add('NAVIGATION', Navigation(),
                             transitions={'succeeded': 'succeeded',
                                          'lost': 'lost',
                                          'aborted': 'aborted',
                                          'preempted': 'preempted'})


class PatrolManager(State):
    def __init__(self):
        State.__init__(self,
                       outcomes=['navigation', 'aborted', 'preempted'],
                       input_keys=['log_operating_mode'],
                       output_keys=['log_operating_mode'])

    def execute(self, ud):
        """Called when executing the state. """
        # Find mission in the database
        try:
            # Get mission = list of waypoint ids
            mission = database.mission.find_one({"_id": ud.log_operating_mode["mission_id"]})
            path = []
            # For each waypoint id find the waypoint infos in the database
            for wp_id in mission["path"]:
                wp = database.waypoint.find_one({"_id": wp_id})
                if wp:
                    path.append(wp)
                else:
                    raise Exception
        except (TypeError, KeyError, Exception):
            logger.error("The mission '" + str(ud.log_operating_mode["mission_id"]) + "' has not been found or has problems with its structure")
            return 'aborted'

        # Reposition the mast directed to the left side of the robot
        pub = rospy.Publisher('/geko/pan_position_controller/command', Float64, queue_size=5)
        rospy.sleep(1.0)
        pub.publish(Float64(1.57))

        if self.preempt_requested():
            self.service_preempt()
            return 'preempted'

        # Set path key that will be used in Navigation state.
        ud.log_operating_mode["path"] = path

        # Go to Navigation state.
        return 'navigation'


class Navigation(Iterator):
    def __init__(self):
        """The Iterator container inherits functionality from smach.StateMachine and adds
        some auto-generated transitions that create a sequence of states from the
        order in which said states are added to the container.

        @type outcomes: list of string
        @param outcomes: The potential outcomes of this container.

        @type it: iterable
        @param iteritems: Items to iterate over on each cycle.

        @type it_label: string
        @param iteritems_label: The label that the item in the current
        iteration will be given when it is put into the container's local
        userdata.
        """
        Iterator.__init__(self,
                          outcomes=['succeeded', 'lost', 'preempted', 'aborted'],
                          input_keys=['log_operating_mode'],
                          it=[],
                          output_keys=['log_operating_mode'],
                          it_label='waypoint_index',
                          exhausted_outcome='succeeded')

        # Add a termination callback to the Navigation Iterator state.
        self.register_start_cb(self.set_iterator_list)

        # Counts how many navigation cycles have been aborted consecutively.
        self._count_aborted = 0
        # Limit of consecutive navigation abortions.
        self._limit_count_aborted = 3

        with self:
            self.sm_nav = StateMachine(outcomes=['succeeded', 'preempted', 'aborted', 'lost', 'continue'],
                                       input_keys=['waypoint_index', 'log_operating_mode'],
                                       output_keys=['log_operating_mode'])

            # Adds a start callback to the FOLLOW_PATH state machine.
            self.sm_nav.register_start_cb(self.save_waypoint_log)
            # Adds a termination callback to the FOLLOW_PATH state machine.
            self.sm_nav.register_termination_cb(self.update_waypoint_log)

            with self.sm_nav:
                """ The SimpleActionState represent an actionlib as a state in a state machine.
                
                @type goal_cb: callable 
                @param goal_cb: If the goal for this action needs to be generated at 
                object. The callback is passed two parameters: 
                    - userdata 
                    - current stored goal 
                The callback  returns a goal message. 
 
                @type result_cb: callable 
                @param result_cb: If result information from this action needs to be 
                stored or manipulated on reception of a result from this action, a 
                callback can be stored which is passed this information. The callback 
                is passed three parameters: 
                    - userdata (L{UserData<smach.user_data.UserData>})
                    - result status (C{actionlib.GoalStatus})
                    - result (actionlib result msg)
                """
                StateMachine.add('MOVE_BASE',
                                 SimpleActionState('move_base',
                                                   MoveBaseAction,
                                                   outcomes=['lost', 'succeeded', 'aborted', 'preempted'],
                                                   input_keys=['waypoint_index', 'log_operating_mode'],
                                                   goal_cb=self.move_base_goal_cb,
                                                   result_cb=self.move_base_result_cb),
                                 transitions={'succeeded': 'SAVE_DATA',
                                              'aborted': 'continue',
                                              'lost': 'lost'})

                """The ServiceState is a state for calling a service"""
                self.save_data_service = ServiceState('make_data_acquisition',
                                                      RequestSaveData,
                                                      input_keys=['waypoint_index', 'log_operating_mode'],
                                                      response_cb=self.save_data_response_cb)
                StateMachine.add('SAVE_DATA',
                                 self.save_data_service,
                                 transitions={'succeeded': 'continue',
                                              'aborted': 'continue'})

            # Close the sm_nav machine and add it to the iterator
            Iterator.set_contained_state('FOLLOW_PATH',
                                         self.sm_nav,
                                         loop_outcomes=['continue'])

    def set_iterator_list(self, userdata, initial_states):
        """Called when the NAVIGATION Iterator state is entered.
        It set the list for the iterator to iterate over.
        """
        with self:
            waypoints = userdata.log_operating_mode['path']
            Iterator.set_iteritems(range(0, len(waypoints)), 'waypoint_index')

    def save_data_response_cb(self, userdata, response):
        """Function called when leaving the SAVE_DATA state to retrieve the service response. """
        if self.save_data_service.preempt_requested():
            self.save_data_service.service_preempt()
            return 'preempted'

        # Add the input field in the document logmission
        database.logmission.update_one({"_id": userdata.log_operating_mode['_id']},
                                       {
                                           "$set": {
                                               "data." + str(userdata.waypoint_index) + ".input": ObjectId(response.input_id)
                                           }
                                       })

        if response.input_id == 'error':
            logger.warn('Error saving data.')

        return 'succeeded'  # SAVE_DATA state outcome

    def move_base_goal_cb(self, userdata, goal):
        """Function called when entering the MOVE_BASE state for the definition of the goal. """
        nav_goal = MoveBaseGoal()
        nav_goal.target_pose.header.frame_id = 'map'
        waypoint = userdata.log_operating_mode['path'][userdata.waypoint_index]["point"]

        position = Point(waypoint[0], waypoint[1], 0.0)
        q_angle = quaternion_from_euler(0, 0, waypoint[2], axes='sxyz')
        orientation = Quaternion(*q_angle)

        nav_goal.target_pose.pose.position = position
        nav_goal.target_pose.pose.orientation = orientation

        return nav_goal

    def move_base_result_cb(self, userdata, status, result):
        """Function called when leaving the MOVE_BASE state to retrieve the action result. """
        waypoint_index = str(userdata.log_operating_mode['path'][userdata.waypoint_index]["_id"])
        if status != 3:  # if not succeeded
            self._count_aborted += 1
            logger.warn("Error navigating to waypoint '" + waypoint_index + "'.")
            if self._count_aborted > self._limit_count_aborted:
                cmm.set(cmm.LOST)
                logger.critical("Unable to obtain a plan for multiple waypoints, the robot may be lost or blocked.")
                return 'lost'
        else:
            rospy.sleep(4.0)
            self._count_aborted = 0

    def save_waypoint_log(self, ud, initial_states):
        """Called when the FOLLOW_PATH state machine is entered.
        Add waypoint log in the logmission document
        """
        database.logmission.update_one({"_id": ud.log_operating_mode['_id']},
                                       {
                                           "$set": {
                                               "current_waypoint": ud.waypoint_index,
                                               "data." + str(ud.waypoint_index) + ".waypoint": ud.log_operating_mode['path'][ud.waypoint_index]["_id"],
                                               "data." + str(ud.waypoint_index) + ".status": 'active'
                                           }
                                       })

    def update_waypoint_log(self, userdata, terminal_states, container_outcome):
        """Called when the FOLLOW_PATH state machine is left.
        Update status waypoint log in the logmission document.
        """
        status = 'aborted'
        if container_outcome == 'continue' or container_outcome == 'succeeded':
            if terminal_states == ['MOVE_BASE']:
                status = 'aborted'
            else:
                status = 'succeeded'

        database.logmission.update_one({"_id": userdata.log_operating_mode['_id']},
                                               {
                                              "$set": {
                                                  "data." + str(userdata.waypoint_index) + ".status": status
                                              }
                                          })
