#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 16:27:07 2020

@author: tnmx0798
"""

import csv

import rospy

import numpy as np

import yaml

from nav_msgs.msg import OccupancyGrid, MapMetaData



import glob, os

import matplotlib.pyplot as plt

class createStats():
    
    def __init__(self,data_path):
        rospy.init_node('createStats', anonymous=True)
        self.waypoints= []
        
        self.data_path = data_path
        

        self.max_size = 60
        self.min_size = 20
        
        self.marker_size = 50


        self.sub_map = rospy.Subscriber("/map_stats",OccupancyGrid,self.callback_occupancy_grid)        
        self.all_diff_norm_rot_rts = []
        self.all_diff_norm_trans_rts = []
        self.all_time_rts = []
        self.all_time_amcl = []
        self.all_time_odom = []
        self.all_diff_norm_rot_amcl = []
        self.all_diff_norm_trans_amcl = []
        self.all_diff_norm_rot_odom= []
        self.all_diff_norm_trans_odom = []        
        self.all_gazebo_jump = []
        self.all_relocation_event = []
        self.all_true_trans=  []
        self.all_true_rot = []
        self.reach = []

        self.origin = np.array([])

        self.height,width = 0,0
        self.resolution = 0
        
        
        self.map = np.array([])
        

        while not rospy.is_shutdown() and (self.map.shape[0] == 0 or self.origin.shape[0] == 0):
            pass
        if not rospy.is_shutdown():
            self.read_all_csv(self.data_path)
            self.display_path()
            self.display_spatial_error()
            self.display_time_error()
            
            try:
                os.mkdir(self.data_path+"/Resume")
            except OSError:
                print("Folder"+self.data_path+ "/Resume" +" already existing.")
            
            self.save_statistics(self.data_path+"/Resume/Resume_of_experiment.csv")
            
            plt.show()
    # montrer carte de coût selon la position réelle et la précision des algorithmes
    # montrer carte d'itinéraire du robot selon réalité
    # indiquer points où relocalisation, points où relocalisation positive ou non
    
    # courbe avec précision, en indiquant kidnapping et relocalisation
    

    def write_space(self,writer,n):
        for k in range(n):
            writer.writerow([' '])


    def save_statistics(self,filename):
        # moyenne, médiane, std pour err_trans / err_rot amcl, rtabmap, odometry
        # prendre indices sur lequel on a relocalisation 
        # voir si gain en précision pour amcl ou non
        # si gain -> succès; sinon -> échec
        all_transx = []
        all_transy = []
        
        global_trans_err_amcl = []
        global_rot_err_amcl = []
        
        global_trans_err_rts = []
        global_rot_err_rts = []        
        
        global_trans_err_odom = []
        global_rot_err_odom = []
        
        global_time_odom = []
        global_time_amcl = []
        global_time_rts = []
        
        global_num_gazebo_teleport = 0

        global_num_relocalization_stop = 0
        global_num_relocalization_cov = 0
        
        
        global_pos_amcl_terr_reloc_bef = []
        global_pos_amcl_terr_reloc_aft = []    
        global_pos_rts_terr_reloc = []
        global_pos_terr = []

        global_pos_amcl_rerr_reloc_bef = []
        global_pos_amcl_rerr_reloc_aft = []  
        global_pos_rts_rerr_reloc = []
        global_pos_rerr = []
                
        global_neg_amcl_terr_reloc_bef = []
        global_neg_amcl_terr_reloc_aft = []    
        global_neg_rts_terr_reloc = []
        global_neg_terr = []

        global_neg_amcl_rerr_reloc_bef = []
        global_neg_amcl_rerr_reloc_aft = []  
        global_neg_rts_rerr_reloc = []        
        global_neg_rerr = []
        
        global_num_data = 0
        with open(filename,'w') as f:
            
            writer = csv.writer(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)        
            for k in range(len(self.waypoints)):
    
                transx = [self.all_true_trans[k][i][0] for i in range(len(self.all_true_trans[k])) ]
                transy = [self.all_true_trans[k][i][1] for i in range(len(self.all_true_trans[k])) ]

                num_data = len(transx)
                all_transx += transx
                all_transy += transy
                trans_err_amcl = self.all_diff_norm_trans_amcl[k]
                rot_err_amcl = self.all_diff_norm_rot_amcl[k]
                
                trans_err_odom = self.all_diff_norm_trans_odom[k]
                rot_err_odom = self.all_diff_norm_rot_odom[k]
                
                trans_err_rts = self.all_diff_norm_trans_rts[k]
                rot_err_rts = self.all_diff_norm_rot_rts[k]
                
                # noter aussi : 
                    # temps total expérimentation
                    # capacité de amcl à s'en sortir après téléportation via:
                    # erreur après relocalisation positive : mean,med,std
                    # erreur après relocalisation négative, mean, med, std
                
                # noter ensuite pour chaque passage entre deux waypoints : 
                    # Waypoint départ
                    # Waypoint arrivée
                    # Temps parcours
                         # "Number of relocalizations",
                         # "Number of relocalizations because of a stuck issue",
                         # "Number of relocalizations because of a covariance issue",
                         # "Number of positive relocalization",
                         # "Number of negative relocalization",
                    # Pourcentage
                    # Pourcentage par rapport à valeurs globales (nombre total données, nombre total reloc,
                    # nombre total téléportations)
                    # Erreur amcl, odom, rtab
                    # erreur après relocalisation positive : mean,med,std
                    # erreur après relocalisation négative, mean, med, std

                num_relocalization_stop = 0
                num_relocalization_cov = 0
                
                pos_amcl_terr_reloc_bef = []
                pos_amcl_terr_reloc_aft = []    
                pos_rts_terr_reloc = []
                pos_terr = []

                pos_amcl_rerr_reloc_bef = []
                pos_amcl_rerr_reloc_aft = []  
                pos_rts_rerr_reloc = []
                pos_rerr = []
                
                neg_amcl_terr_reloc_bef = []
                neg_amcl_terr_reloc_aft = []    
                neg_rts_terr_reloc = []
                neg_terr = []
                
                neg_amcl_rerr_reloc_bef = []
                neg_amcl_rerr_reloc_aft = []  
                neg_rts_rerr_reloc = []              
                neg_rerr = []                

                for l in range(1,len(transx)):

                    if ( ( len(self.all_relocation_event[k][l]) > 43 and  self.all_relocation_event[k][l][:44] == "Relocalisation since the robot has not moved")
                        or self.all_relocation_event[k][l] == "Relocalisation as all the recovery strategies have failed."):
                        num_relocalization_stop += 1
                        if self.all_diff_norm_trans_amcl[k][l-1] - self.all_diff_norm_trans_amcl[k][l] > 0:
                            
                            pos_terr.append(self.all_diff_norm_trans_amcl[k][l-1] - self.all_diff_norm_trans_amcl[k][l])
                            pos_amcl_terr_reloc_bef.append(self.all_diff_norm_trans_amcl[k][l-1])
                            pos_amcl_terr_reloc_aft.append(self.all_diff_norm_trans_amcl[k][l])
                            pos_rts_terr_reloc.append(transx[l])
   
                        else:
                            neg_terr.append(self.all_diff_norm_trans_amcl[k][l] - self.all_diff_norm_trans_amcl[k][l-1])
                            neg_amcl_terr_reloc_bef.append(self.all_diff_norm_trans_amcl[k][l-1])
                            neg_amcl_terr_reloc_aft.append(self.all_diff_norm_trans_amcl[k][l])                           
                            neg_rts_terr_reloc.append(transx[l])                            

                        if self.all_diff_norm_rot_amcl[k][l-1] - self.all_diff_norm_rot_amcl[k][l] > 0:
                            
                            pos_rerr.append(self.all_diff_norm_rot_amcl[k][l-1] - self.all_diff_norm_rot_amcl[k][l])
                            pos_amcl_rerr_reloc_bef.append(self.all_diff_norm_rot_amcl[k][l-1])
                            pos_amcl_rerr_reloc_aft.append(self.all_diff_norm_rot_amcl[k][l])
                            pos_rts_rerr_reloc.append(self.all_diff_norm_rot_rts[k][l])
   
                        else:
                            neg_rerr.append(self.all_diff_norm_rot_amcl[k][l] - self.all_diff_norm_rot_amcl[k][l-1])
                            neg_amcl_rerr_reloc_bef.append(self.all_diff_norm_rot_amcl[k][l-1])
                            neg_amcl_rerr_reloc_aft.append(self.all_diff_norm_rot_amcl[k][l])                           
                            neg_rts_rerr_reloc.append(self.all_diff_norm_rot_rts[k][l])    
                    
                    elif (  len(self.all_relocation_event[k][l]) > 48 and self.all_relocation_event[k][l][:49] == "Relocalisation as the robot has a huge covariance"):
                        num_relocalization_cov +=1
                        if self.all_diff_norm_trans_amcl[k][l-1] - self.all_diff_norm_trans_amcl[k][l] > 0:
                            
                            pos_terr.append(self.all_diff_norm_trans_amcl[k][l-1] - self.all_diff_norm_trans_amcl[k][l])
                            pos_amcl_terr_reloc_bef.append(self.all_diff_norm_trans_amcl[k][l-1])
                            pos_amcl_terr_reloc_aft.append(self.all_diff_norm_trans_amcl[k][l])
                            pos_rts_terr_reloc.append(transx[l])
   
                        else:
                            neg_terr.append(self.all_diff_norm_trans_amcl[k][l] - self.all_diff_norm_trans_amcl[k][l-1])
                            neg_amcl_terr_reloc_bef.append(self.all_diff_norm_trans_amcl[k][l-1])
                            neg_amcl_terr_reloc_aft.append(self.all_diff_norm_trans_amcl[k][l])                           
                            neg_rts_terr_reloc.append(transx[l])                            

                        if self.all_diff_norm_rot_amcl[k][l-1] - self.all_diff_norm_rot_amcl[k][l] > 0:
                            
                            pos_rerr.append(self.all_diff_norm_rot_amcl[k][l-1] - self.all_diff_norm_rot_amcl[k][l])
                            pos_amcl_rerr_reloc_bef.append(self.all_diff_norm_rot_amcl[k][l-1])
                            pos_amcl_rerr_reloc_aft.append(self.all_diff_norm_rot_amcl[k][l])
                            pos_rts_rerr_reloc.append(self.all_diff_norm_rot_rts[k][l])
   
                        else:
                            neg_rerr.append(self.all_diff_norm_rot_amcl[k][l] - self.all_diff_norm_rot_amcl[k][l-1])
                            neg_amcl_rerr_reloc_bef.append(self.all_diff_norm_rot_amcl[k][l-1])
                            neg_amcl_rerr_reloc_aft.append(self.all_diff_norm_rot_amcl[k][l])                           
                            neg_rts_rerr_reloc.append(self.all_diff_norm_rot_rts[k][l]) 

                writer.writerow([' Path to waypoint %i'%k])
                
                writer.writerow([' Position of waypoint : ','x : ',self.waypoints[k][0] ,
                                 'y :', self.waypoints[k][1]  ,
                                 'tht : ',self.waypoints[k][2]])                

                writer.writerow([' Start : ','x : ',self.all_true_trans[k][0][0] ,
                                 'y :', self.all_true_trans[k][0][1]  ,
                                 'tht : ',self.all_true_rot[k][0][2]])

                writer.writerow([' End : ','x : ',self.all_true_trans[k][-1][0] ,
                                 'y :', self.all_true_trans[k][-1][1]  ,
                                 'tht : ',self.all_true_rot[k][-1][2]])  
                
                self.write_space(writer,2)

                duration = np.max([self.all_time_odom[k][-1]- self.all_time_odom[k][0],
                                   self.all_time_amcl[k][-1] -  self.all_time_amcl[k][0],
                                   self.all_time_rts[k][-1]- self.all_time_rts[k][0] ])
                

                fname = ["Number of data recorded", 
                         "Duration",
                         "Number of teleportations",
                         "Number of relocalizations",
                         "Number of relocalizations because of a stuck issue",
                         "Number of relocalizations because of a covariance issue"
                         ]
                
                writer.writerow(fname)
            
                writer.writerow([num_data, 
                                 duration,
                                 np.sum(self.all_gazebo_jump[k]), 
                                 num_relocalization_stop + num_relocalization_cov, 
                                 num_relocalization_stop,
                                 num_relocalization_cov
                                 ])
                self.write_space(writer,1)
    
                writer.writerow([' ',' ',
                                 'Percent of teleportations on all datas',
                                 'Percent of relocalizations on all datas',
                                 'Percent of stop on all relocalizations',
                                 'Percent of covariance on all relocalizations'])  
                
                writer.writerow([' ',' ',
                                 (np.sum(self.all_gazebo_jump[k])*100.0/num_data),
                                 (num_relocalization_stop + num_relocalization_cov)*100.0/num_data, 
                                 (num_relocalization_stop)*100.0/(max(num_relocalization_stop + num_relocalization_cov,1) ),
                                 (num_relocalization_cov)*100.0/(max(num_relocalization_stop + num_relocalization_cov,1) )])  
                
                self.write_space(writer,3)           
                writer.writerow(["Number of positive translation relocalization",
                         'Percent of positive on all relocalizations',
                         ' ', 
                          "Number of negative translation relocalization",
                         'Percent of negative on all relocalizations'])
                writer.writerow([ len(pos_terr), 
                                 len(pos_terr)*100.0/(max(num_relocalization_stop + num_relocalization_cov,1) ), 
                                 ' ', 
                                 len(neg_terr),                              
                                 len(neg_terr)*100.0/max(num_relocalization_stop + num_relocalization_cov,1)
                                 ])
                self.write_space(writer,1)
        
                writer.writerow(["Trans_correction_on_amcl (mean)","Trans_correction_on_amcl (median)","Trans_correction_on_amcl (std)",
                                 "Trans_err_on_amcl (mean)","Trans_err_on_amcl (median)","Trans_err_on_amcl (std)"])
                
                writer.writerow([np.mean(pos_terr),np.median(pos_terr),np.std(pos_terr),
                                 np.mean(neg_terr),np.median(neg_terr),np.std(neg_terr)])
                self.write_space(writer,1)                
                writer.writerow(["Mean_corr > Mean_err","Median_corr > Median_err","Std_corr > Std_err"])
                
                if len(pos_terr) == 0:
                    writer.writerow([False,
                                 False,
                                 False])                            
                elif len(neg_terr) == 0:
                    writer.writerow([True,True,True])                            
                else:
                    writer.writerow([np.mean(pos_terr) > np.mean(neg_terr),
                                 np.median(pos_terr) > np.median(neg_terr),
                                 np.std(pos_terr) >np.std(neg_terr)])                
                
    
                self.write_space(writer,3)           
                writer.writerow(["Number of positive rotation relocalization",
                         'Percent of positive on all relocalizations',
                         ' ',
                          "Number of negative rotation relocalization",
                         'Percent of negative on all relocalizations'])
                writer.writerow([ len(pos_rerr), 
                                 len(pos_rerr)*100.0/max(num_relocalization_stop + num_relocalization_cov,1) , 
                                 ' ', 
                                 len(neg_rerr),                              
                                 len(neg_rerr)*100.0/max(num_relocalization_stop + num_relocalization_cov,1) 
                                 ])
                self.write_space(writer,1)
        
                writer.writerow(["Rot_correction_on_amcl (mean)","Rot_correction_on_amcl (median)","Rot_correction_on_amcl (std)",
                                 "Rot_err_on_amcl (mean)","Rot_err_on_amcl (median)","Rot_err_on_amcl (std)"])
                
                writer.writerow([np.mean(pos_rerr),np.median(pos_rerr),np.std(pos_rerr),
                                 np.mean(neg_rerr),np.median(neg_rerr),np.std(neg_rerr)])
                self.write_space(writer,1)                
                writer.writerow(["Mean_corr > Mean_err","Median_corr > Median_err","Std_corr > Std_err"])
                if len(pos_rerr) == 0:
                    writer.writerow([False,
                                 False,
                                 False])                            
                elif len(neg_rerr) == 0:
                    writer.writerow([True,True,True])         
                else:
                    writer.writerow([np.mean(pos_rerr) > np.mean(neg_rerr),
                                 np.median(pos_rerr) > np.median(neg_rerr),
                                 np.std(pos_rerr) >np.std(neg_rerr)])              
    
                
                self.write_space(writer,3)          
                writer.writerow(["Trans_err_odom (mean)","Trans_err_amcl (mean)","Trans_err_rtab (mean)",
                                 "Trans_err_odom (median)","Trans_err_amcl (median)","Trans_err_rtab (median)",
                                 "Trans_err_odom (std)" ,"Trans_err_amcl (std)","Trans_err_rtab (std)"])
                
                
                writer.writerow([np.mean(trans_err_odom),np.mean(trans_err_amcl),np.mean(trans_err_rts),
                                 np.median(trans_err_odom),np.median(trans_err_amcl),np.median(trans_err_rts),
                                 np.std(trans_err_odom) ,np.std(trans_err_amcl),np.std(trans_err_rts)])            
                
                self.write_space(writer,3)
                writer.writerow(["Rot_err_odom (mean)","Rot_err_amcl (mean)","Rot_err_rtab (mean)",
                                 "Rot_err_odom (median)","Rot_err_amcl (median)","Rot_err_rtab (median)",
                                 "Rot_err_odom (std)" ,"Rot_err_amcl (std)","Rot_err_rtab (std)"])
                
                
                writer.writerow([np.mean(rot_err_odom),np.mean(rot_err_amcl),np.mean(rot_err_rts),
                                 np.median(rot_err_odom),np.median(rot_err_amcl),np.median(rot_err_rts),
                                 np.std(rot_err_odom) ,np.std(rot_err_amcl),np.std(rot_err_rts)])            

                self.write_space(writer,5)
                            
                global_pos_amcl_terr_reloc_bef += pos_amcl_terr_reloc_bef
                global_pos_amcl_terr_reloc_aft += pos_amcl_terr_reloc_aft  
                global_pos_rts_terr_reloc += pos_rts_rerr_reloc
                global_pos_terr += pos_terr
                
                global_pos_amcl_rerr_reloc_bef += pos_amcl_rerr_reloc_bef
                global_pos_amcl_rerr_reloc_aft += pos_amcl_rerr_reloc_aft  
                global_pos_rts_rerr_reloc += pos_rts_rerr_reloc
                global_pos_rerr += pos_rerr
                
                global_neg_amcl_terr_reloc_bef += neg_amcl_terr_reloc_bef
                global_neg_amcl_terr_reloc_aft += neg_amcl_terr_reloc_aft  
                global_neg_rts_terr_reloc += neg_rts_rerr_reloc
                global_neg_terr += neg_terr
                
                global_neg_amcl_rerr_reloc_bef += neg_amcl_rerr_reloc_bef
                global_neg_amcl_rerr_reloc_aft += neg_amcl_rerr_reloc_aft  
                global_neg_rts_rerr_reloc += neg_rts_rerr_reloc
                global_neg_rerr += neg_rerr                                 
                
                global_num_relocalization_stop += num_relocalization_stop
                global_num_relocalization_cov += num_relocalization_cov

                global_trans_err_amcl += self.all_diff_norm_trans_amcl[k]
                global_rot_err_amcl += self.all_diff_norm_rot_amcl[k]
                
                global_trans_err_odom += self.all_diff_norm_trans_odom[k]
                global_rot_err_odom += self.all_diff_norm_rot_odom[k]
                
                global_trans_err_rts += self.all_diff_norm_trans_rts[k]
                global_rot_err_rts += self.all_diff_norm_rot_rts[k]
                
                global_num_gazebo_teleport += np.sum(self.all_gazebo_jump[k])
                global_num_data += num_data

                global_time_odom += self.all_time_odom[k]
                global_time_amcl += self.all_time_amcl[k]
                global_time_rts += self.all_time_rts[k]

        
            duration = np.max([global_time_odom[-1]- global_time_odom[0],
                               global_time_amcl[-1] -  global_time_amcl[0],
                               global_time_rts[-1]- global_time_rts[0] ])
            
            writer.writerow([' TOTAL '])
            
            fname = ["Number of data recorded", 
                     "Duration",
                     "Number of teleportations",
                     "Number of relocalizations",
                     "Number of relocalizations because of a stuck issue",
                     "Number of relocalizations because of a covariance issue"
                     ]
            
            writer.writerow(fname)
        
            writer.writerow([global_num_data, 
                             duration,
                             global_num_gazebo_teleport, 
                             global_num_relocalization_stop + global_num_relocalization_cov, 
                             global_num_relocalization_stop,
                             global_num_relocalization_cov
                             ])
            self.write_space(writer,1)

            writer.writerow([' ',' ',
                             'Percent of teleportations on all datas',
                             'Percent of relocalizations on all datas',
                             'Percent of stop on all relocalizations',
                             'Percent of covariance on all relocalizations'])  
            
            writer.writerow([' ',' ',
                             (global_num_gazebo_teleport*100.0/global_num_data),
                             (global_num_relocalization_stop + global_num_relocalization_cov)*100.0/global_num_data, 
                             (global_num_relocalization_stop)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov),
                             (global_num_relocalization_cov)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov)])  
            
            self.write_space(writer,3)           
            writer.writerow(["Number of positive translation relocalization",
                     'Percent of positive on all relocalizations',
                     ' ', 
                      "Number of negative translation relocalization",
                     'Percent of negative on all relocalizations'])
            writer.writerow([ len(global_pos_terr), 
                             len(global_pos_terr)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov), 
                             ' ', 
                             len(global_neg_terr),                              
                             len(global_neg_terr)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov)
                             ])
            self.write_space(writer,1)
    
            writer.writerow(["Trans_correction_on_amcl (mean)","Trans_correction_on_amcl (median)","Trans_correction_on_amcl (std)",
                             "Trans_err_on_amcl (mean)","Trans_err_on_amcl (median)","Trans_err_on_amcl (std)"])
            
            writer.writerow([np.mean(global_pos_terr),np.median(global_pos_terr),np.std(global_pos_terr),
                             np.mean(global_neg_terr),np.median(global_neg_terr),np.std(global_neg_terr)])
            
            self.write_space(writer,1)                
            writer.writerow(["Mean_corr > Mean_err","Median_corr > Median_err","Std_corr > Std_err"])
            if len(global_pos_terr) == 0:
                writer.writerow([False,
                                 False,
                                 False])                            
            elif len(global_neg_terr) == 0:
                writer.writerow([True,True,True])         
            else:
                writer.writerow([np.mean(global_pos_terr) > np.mean(global_neg_terr),
                                 np.median(global_pos_terr) > np.median(global_neg_terr),
                                 np.std(global_pos_terr) >np.std(global_neg_terr)])    
                
            self.write_space(writer,3)           
            writer.writerow(["Number of positive rotation relocalization",
                     'Percent of positive on all relocalizations',
                     ' ',
                      "Number of negative rotation relocalization",
                     'Percent of negative on all relocalizations'])
            writer.writerow([ len(global_pos_rerr), 
                             len(global_pos_rerr)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov), 
                             ' ', 
                             len(global_neg_rerr),                              
                             len(global_neg_rerr)*100.0/(global_num_relocalization_stop + global_num_relocalization_cov)
                             ])
            self.write_space(writer,1)
    
            writer.writerow(["Rot_correction_on_amcl (mean)","Rot_correction_on_amcl (median)","Rot_correction_on_amcl (std)",
                             "Rot_err_on_amcl (mean)","Rot_err_on_amcl (median)","Rot_err_on_amcl (std)"])
            
            writer.writerow([np.mean(global_pos_rerr),np.median(global_pos_rerr),np.std(global_pos_rerr),
                             np.mean(global_neg_rerr),np.median(global_neg_rerr),np.std(global_neg_rerr)])

            self.write_space(writer,1)                
            writer.writerow(["Mean_corr > Mean_err","Median_corr > Median_err","Std_corr > Std_err"])
            if len(global_pos_rerr) == 0:
                writer.writerow([False,
                                 False,
                                 False])                            
            elif len(global_neg_rerr) == 0:
                writer.writerow([True,True,True])         
            else:
                writer.writerow([np.mean(global_pos_rerr) > np.mean(global_neg_rerr),
                                 np.median(global_pos_rerr) > np.median(global_neg_rerr),
                                 np.std(global_pos_rerr) >np.std(global_neg_rerr)])    

            
            self.write_space(writer,3)          
            writer.writerow(["Trans_err_odom (mean)","Trans_err_amcl (mean)","Trans_err_rtab (mean)",
                             "Trans_err_odom (median)","Trans_err_amcl (median)","Trans_err_rtab (median)",
                             "Trans_err_odom (std)" ,"Trans_err_amcl (std)","Trans_err_rtab (std)"])
            
            
            writer.writerow([np.mean(global_trans_err_odom),np.mean(global_trans_err_amcl),np.mean(global_trans_err_rts),
                             np.median(global_trans_err_odom),np.median(global_trans_err_amcl),np.median(global_trans_err_rts),
                             np.std(global_trans_err_odom) ,np.std(global_trans_err_amcl),np.std(global_trans_err_rts)])            
            
            self.write_space(writer,3)
            writer.writerow(["Rot_err_odom (mean)","Rot_err_amcl (mean)","Rot_err_rtab (mean)",
                             "Rot_err_odom (median)","Rot_err_amcl (median)","Rot_err_rtab (median)",
                             "Rot_err_odom (std)" ,"Rot_err_amcl (std)","Rot_err_rtab (std)"])
            
            
            writer.writerow([np.mean(global_rot_err_odom),np.mean(global_rot_err_amcl),np.mean(global_rot_err_rts),
                             np.median(global_rot_err_odom),np.median(global_rot_err_amcl),np.median(global_rot_err_rts),
                             np.std(global_rot_err_odom) ,np.std(global_rot_err_amcl),np.std(global_rot_err_rts)])            
            
        


    def callback_occupancy_grid(self,msg):
        print("Got message!")
        first_origin = np.array([msg.info.origin.position.x,msg.info.origin.position.y,msg.info.origin.position.z])

        self.height = msg.info.height
        self.width = msg.info.width
        self.resolution = msg.info.resolution
        
        first_map = np.array([msg.data]).reshape(self.height,self.width)
        print(first_map.shape)
        
        i_top = -1
        j_top = -1
        
        top_left = np.array([])
        bottom_right = np.array([])
        
        i = 0        
        while i < (first_map.shape[0]):
            if np.any(first_map[i,:] != -1):
                i_top = i
                i = first_map.shape[0]
            i+=1
        
        j = 0
        while j < (first_map.shape[1]):
            if np.any(first_map[:,j] != -1):
                j_top = j
                j = first_map.shape[1]
            j+=1

        top_left = np.array([first_origin[0] + i_top*self.resolution , first_origin[1] + j_top*self.resolution,first_origin[2] ])

        print("Top left : ", top_left)
        
        i_bot = -1
        j_bot = -1
        
        i = first_map.shape[0]-1
        while i> 0:
            if np.any(first_map[i,:] != -1):
                i_bot = i
                i = 0
            i-=1
            
        j = first_map.shape[1]-1
        while j> 0:
            if np.any(first_map[:,j] != -1):
                j_bot = j
                j = 0
            j-=1                

        bottom_right = np.array([first_origin[0] + i_bot*self.resolution , first_origin[1] + j_bot*self.resolution,first_origin[2] ])

        print("Bottom right : ", bottom_right)
        
        print("i_top, j_top : {%i,%i}"%(i_top,j_top))
        print("i_bot, j_bot : {%i,%i}"%(i_bot,j_bot))        
        
        self.map = first_map[ i_top : i_bot , j_top:j_bot ]
        self.origin = top_left
        
        #print(self.map)
        
 
    def display_map(self,ax):
    
        transx = []
        transy = []
        
        for i in range(self.map.shape[0]):
            for j in range(self.map.shape[1]):
                if self.map[i,j] > 1:
                    transx.append(self.origin[0] + i*self.resolution)
                    transy.append(self.origin[1] + j*self.resolution)
        #print("transx : ", transx)
        ax.scatter(transy,transx,color="black",s = 5)
        return(ax)
    
    def display_time_error(self):
        # fenêtre évolution erreur en fonction du temps, avec amcl / rtab / odom [erreur trans ; erreur rot]
        figt = plt.figure()
        axt = figt.add_subplot(1,1,1)
        
        figr = plt.figure()
        axr = figr.add_subplot(1,1,1)        
        all_transx = []
        all_transy = []
        
        global_trans_err_amcl = []
        global_rot_err_amcl = []
        
        global_trans_err_rts = []
        global_rot_err_rts = []        
        
        global_trans_err_odom = []
        global_rot_err_odom = []
        
        global_teleport = []
        
        global_relocalization_stop = []
        
        global_relocalization_cov = []

        indrs = []
        indrc = []        
        indgt = []
        
        val = 0
        
        
        for k in range(len(self.waypoints)):
            
            transx = [self.all_true_trans[k][i][0] for i in range(len(self.all_true_trans[k])) ]
            transy = [self.all_true_trans[k][i][1] for i in range(len(self.all_true_trans[k])) ]
            
            all_transx += transx
            all_transy += transy
            
            global_trans_err_amcl += self.all_diff_norm_trans_amcl[k]
            global_rot_err_amcl += self.all_diff_norm_rot_amcl[k]
            
            global_trans_err_odom += self.all_diff_norm_trans_odom[k]
            global_rot_err_odom += self.all_diff_norm_rot_odom[k]
            
            global_trans_err_rts += self.all_diff_norm_trans_rts[k]
            global_rot_err_rts += self.all_diff_norm_rot_rts[k]
            
            trans_teleport = []

            start_recov_stop = False
            start_recov_cov = False

            for l in range(len(transx)):
                if self.all_gazebo_jump[k][l] == True:
                    global_teleport += [0,1,0] 
                    indgt += [val,val,val]

                else:
                     global_teleport += [0]
                     indgt += [val]
                
                #print(self.all_relocation_event[k][l])
                

                if ( ( len(self.all_relocation_event[k][l]) > 69 and self.all_relocation_event[k][l][25:69] == "Relocalisation since the robot has not moved")
                    or self.all_relocation_event[k][l][25:] == "Relocalisation as all the recovery strategies have failed."):
                    start_recov_stop = True
                    global_relocalization_stop += [0,1]
                    indrs += [val,val]                    
                    
                if (  len(self.all_relocation_event[k][l]) > 73 and self.all_relocation_event[k][l][25:74] == "Relocalisation as the robot has a huge covariance"):
                    start_recov_cov = True
                    global_relocalization_cov += [0,1]
                    indrc += [val,val]                                                     

                if ( ( len(self.all_relocation_event[k][l]) > 43 and  self.all_relocation_event[k][l][:44] == "Relocalisation since the robot has not moved")
                    or self.all_relocation_event[k][l] == "Relocalisation as all the recovery strategies have failed."):
                    
                    if start_recov_stop : 
                        global_relocalization_stop += [1,0]
                        indrs += [val,val] 
                        start_recov_stop = False
                    else:
                        global_relocalization_stop += [0,1,0]
                        indrs += [val,val,val]
                
                else:
                    if start_recov_stop :
                        global_relocalization_stop += [1]
                    else:
                        global_relocalization_stop += [0]
                    indrs+= [val]
                    
                
                if (  len(self.all_relocation_event[k][l]) > 48 and self.all_relocation_event[k][l][:49] == "Relocalisation as the robot has a huge covariance"):
                    
                    if start_recov_cov:
                        global_relocalization_cov += [1,0]
                        indrc += [val,val]           
                        start_recov_cov = False
                    else:
                        global_relocalization_cov += [0,1,0]
                        indrc += [val,val,val]
                else:
                    if start_recov_cov:
                        global_relocalization_cov += [1]                    
                    else:
                        global_relocalization_cov += [0]
                    indrc +=[val]

                val+=1
       
        MaxT = np.max( [ np.max(global_trans_err_odom)/2, np.max(global_trans_err_amcl)/2, np.max(global_trans_err_rts)/2   ] )
        Mgtt = MaxT*np.array(global_teleport)

        Mgrst = MaxT*np.array(global_relocalization_stop)
        Mgrct = MaxT*np.array(global_relocalization_cov)      
        
        length = len(global_trans_err_rts)
        ind = [ l for l in range(length)]
                
        axt.plot(ind, global_trans_err_odom, label = "Odometry", color = "B" )
        axt.plot(ind, global_trans_err_amcl, label = "AMCL", color = "G" )
        axt.plot(ind, global_trans_err_rts, label = "Rtabmap", color = "black" )

        axt.plot(indgt, Mgtt, label = "Teleports during experiment", color = "fuchsia",  linewidth = 0.2 )   
        axt.plot(indrs, Mgrst, label = "Relocalization after stop", color = "R",  linewidth = 0.2 )      
        axt.plot(indrc, Mgrct, label = "Relocalization after covariance explosion", color = "saddlebrown", alpha = 0.5, linewidth = 0.2 )  
        
        axt.set_xlabel("Index")
        axt.set_ylabel("Translational Error (m)")
        axt.set_title("Translational error during experiment")
        axt.legend()
        
        MaxR = np.max( [ np.max(global_rot_err_odom)/2, np.max(global_rot_err_amcl)/2, np.max(global_rot_err_rts)/2   ] )
        Mgtr = MaxR*np.array(global_teleport)

        Mgrsr = MaxR*np.array(global_relocalization_stop)
        Mgrcr = MaxR*np.array(global_relocalization_cov)      
        
        length = len(global_rot_err_rts)
        ind = [ l for l in range(length)]
        
        axr.plot(ind, global_rot_err_odom, label = "Odometry", color = "B" )
        axr.plot(ind, global_rot_err_amcl, label = "AMCL", color = "G" )
        axr.plot(ind, global_rot_err_rts, label = "Rtabmap", color = "black" )

        axr.plot(indgt, Mgtr, label = "Teleports during experiment", color = "fuchsia", linewidth = 0.2 )   
        axr.plot(indrs, Mgrsr, label = "Relocalization after stop", color = "R", linewidth = 0.2 )      
        axr.plot(indrc, Mgrcr, label = "Relocalization after covariance explosion", color = "saddlebrown" , linewidth = 0.2)  

        axr.set_xlabel("Index")
        axr.set_ylabel("Rotational Error (rad)")
        axr.set_title("Rotational error during experiment")
        axr.legend()
        
        
    def display_spatial_error(self):
        # fenêtre avec position erreur amcl, position erreur rtabmap, position erreur odom, positions kidnappings [x;y]
        
        fig_o,ax_o = plt.subplots(2,1)
        fig_a,ax_a = plt.subplots(2,1)
        fig_r,ax_r = plt.subplots(2,1)
        
        all_transx = []
        all_transy = []
        
        global_trans_err_amcl = []
        global_rot_err_amcl = []
        
        global_trans_err_rts = []
        global_rot_err_rts = []        
        
        global_trans_err_odom = []
        global_rot_err_odom = []
        
        global_gazebo_teleport_x = []
        global_gazebo_teleport_y = []
        
        global_relocalization_stop_x = []
        global_relocalization_stop_y = []
        
        global_relocalization_cov_x = []
        global_relocalization_cov_y = []
        
        
        for k in range(len(self.waypoints)):

            transx = [self.all_true_trans[k][i][0] for i in range(len(self.all_true_trans[k])) ]
            transy = [self.all_true_trans[k][i][1] for i in range(len(self.all_true_trans[k])) ]
            
            all_transx += transx
            all_transy += transy
            
            global_trans_err_amcl += self.all_diff_norm_trans_amcl[k]
            global_rot_err_amcl += self.all_diff_norm_rot_amcl[k]
            
            global_trans_err_odom += self.all_diff_norm_trans_odom[k]
            global_rot_err_odom += self.all_diff_norm_rot_odom[k]
            
            
            global_trans_err_rts += self.all_diff_norm_trans_rts[k]
            global_rot_err_rts += self.all_diff_norm_rot_rts[k]
            
            trans_x_teleport = []
            trans_y_teleport = []
            for l in range(1,len(transx)):
                if self.all_gazebo_jump[k][l] == True:
                    trans_x_teleport += [ transx[l-1],transx[l] ] 
                    trans_y_teleport += [ transy[l-1],transy[l] ]    
                
                #print(self.all_relocation_event[k][l])
                if ( ( len(self.all_relocation_event[k][l]) > 43 and  self.all_relocation_event[k][l][:44] == "Relocalisation since the robot has not moved")
                    or self.all_relocation_event[k][l] == "Relocalisation as all the recovery strategies have failed."):
                    global_relocalization_stop_x.append(transx[l])
                    global_relocalization_stop_y.append(transy[l])
                
                elif (  len(self.all_relocation_event[k][l]) > 48 and self.all_relocation_event[k][l][:49] == "Relocalisation as the robot has a huge covariance"):
                    global_relocalization_cov_x.append(transx[l])
                    global_relocalization_cov_y.append(transy[l])            
            
            global_gazebo_teleport_x += trans_x_teleport
            global_gazebo_teleport_y += trans_y_teleport
        
        
        
        
        for i in range(2):
            ax_o[i,] = self.display_map(ax_o[i,])
            ax_o[i,].scatter(global_gazebo_teleport_x,global_gazebo_teleport_y,c="fuchsia" ,label="Teleports during experiment",s = self.marker_size,marker = "*")
            ax_o[i,].scatter(global_relocalization_stop_x,global_relocalization_stop_y,c= "fuchsia" ,label="Relocalization after stop",s = self.marker_size,marker = "d")
            ax_o[i,].scatter(global_relocalization_cov_x,global_relocalization_cov_y,c= "dimgrey" ,label="Relocalization after covariance explosion",s = self.marker_size,marker = "s")

            ax_r[i,] = self.display_map(ax_r[i,])
            ax_r[i,].scatter(global_gazebo_teleport_x,global_gazebo_teleport_y,c="fuchsia" ,label="Teleports during experiment",s = self.marker_size,marker = "*")
            ax_r[i,].scatter(global_relocalization_stop_x,global_relocalization_stop_y,c="fuchsia" ,label="Relocalization after stop",s = self.marker_size,marker = "d")
            ax_r[i,].scatter(global_relocalization_cov_x,global_relocalization_cov_y,c="dimgrey" ,label="Relocalization after covariance explosion",s = self.marker_size,marker = "s")

            ax_a[i,] = self.display_map(ax_a[i,])
            ax_a[i,].scatter(global_gazebo_teleport_x,global_gazebo_teleport_y,c="fuchsia" ,label="Teleports during experiment",s = self.marker_size,marker = "*")
            ax_a[i,].scatter(global_relocalization_stop_x,global_relocalization_stop_y,c="fuchsia" ,label="Relocalization after stop",s = self.marker_size,marker = "d")
            ax_a[i,].scatter(global_relocalization_cov_x,global_relocalization_cov_y,c="dimgrey" ,label="Relocalization after covariance explosion",s = self.marker_size,marker = "s")


                
        # plot odom  

        maxo = np.max(global_trans_err_odom)
        mino = np.min(global_trans_err_odom)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_trans_err_odom[i]+b for i in range(len(global_trans_err_odom))]
        # colormap from https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html
        pos = ax_o[0,].scatter( all_transx, all_transy, c=global_trans_err_odom, s = sg, cmap =  'jet', alpha = 0.9  )
        cbar = fig_o.colorbar(pos, ax = ax_o[0,])
        cbar.set_label('Translationnal Error (m)')

        ax_o[0,].set_xlabel("X position (m)")
        ax_o[0,].set_ylabel("Y position (m)")
        ax_o[0,].set_title("Odometry translational error during experiment")
        ax_o[0,].legend()   
        
        maxo = np.max(global_rot_err_odom)
        mino = np.min(global_rot_err_odom)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_rot_err_odom[i]+b for i in range(len(global_rot_err_odom))]        
        
        pos = ax_o[1,].scatter( all_transx, all_transy, c=global_rot_err_odom, s= sg, cmap = 'jet', alpha = 0.9  )
        cbar = fig_o.colorbar(pos,ax = ax_o[1,])
        cbar.set_label('Rotationnal Error (rad)')

        ax_o[1,].set_xlabel("X position (m)")
        ax_o[1,].set_ylabel("Y position (m)")
        ax_o[1,].set_title("Odometry rotational error during experiment")
        ax_o[1,].legend()   


        # plot amcl

        maxo = np.max(global_trans_err_amcl)
        mino = np.min(global_trans_err_amcl)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_trans_err_amcl[i]+b for i in range(len(global_trans_err_amcl))]

        pos = ax_a[0,].scatter( all_transx, all_transy, c=global_trans_err_amcl, s = sg, cmap = 'jet', alpha = 0.9  )
        cbar = fig_a.colorbar(pos, ax = ax_a[0,])
        cbar.set_label('Translationnal Error (m)')

        ax_a[0,].set_xlabel("X position (m)")
        ax_a[0,].set_ylabel("Y position (m)")
        ax_a[0,].set_title("AMCL translational error during experiment")
        ax_a[0,].legend()   
            
        maxo = np.max(global_rot_err_amcl)
        mino = np.min(global_rot_err_amcl)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_rot_err_amcl[i]+b for i in range(len(global_rot_err_amcl))]        
        
        pos = ax_a[1,].scatter( all_transx, all_transy, c=global_rot_err_amcl, s = sg, cmap = 'jet', alpha = 0.9  )
        cbar = fig_a.colorbar(pos,ax = ax_a[1,])
        cbar.set_label('Rotationnal Error (rad)')

        ax_a[1,].set_xlabel("X position (m)")
        ax_a[1,].set_ylabel("Y position (m)")
        ax_a[1,].set_title("AMCL rotational error during experiment")
        ax_a[1,].legend()   
        
        
        # # plot rtab
        maxo = np.max(global_trans_err_rts)
        mino = np.min(global_trans_err_rts)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_trans_err_rts[i]+b for i in range(len(global_trans_err_rts))]        
        
        pos = ax_r[0,].scatter( all_transx, all_transy, c=global_trans_err_rts, s = sg, cmap = 'jet', alpha = 0.9  )
        cbar = fig_r.colorbar(pos, ax = ax_r[0,])
        cbar.set_label('Translationnal Error (m)')

        ax_r[0,].set_xlabel("X position (m)")
        ax_r[0,].set_ylabel("Y position (m)")
        ax_r[0,].set_title("Rtabmap translational error during experiment")
        ax_r[0,].legend()   
        
        maxo = np.max(global_rot_err_rts)
        mino = np.min(global_rot_err_rts)
        a = (self.min_size - self.max_size)/(mino-maxo)
        b = (self.max_size*mino - self.min_size*maxo)/(mino-maxo)
        
        sg = [ a*global_rot_err_rts[i]+b for i in range(len(global_rot_err_rts))]             
        pos = ax_r[1,].scatter( all_transx, all_transy, c=global_rot_err_rts, s = sg, cmap = 'jet', alpha = 0.9  )
        cbar = fig_r.colorbar(pos,ax = ax_r[1,])
        cbar.set_label('Rotationnal Error (rad)')

        ax_r[1,].set_xlabel("X position (m)")
        ax_r[1,].set_ylabel("Y position (m)")
        ax_r[1,].set_title("Rtabmap rotational error during experiment")
        ax_r[1,].legend()           

    def display_path(self):
        # fenêtre de parcours, avec position des kidnappings et des buts [x;y]

        colors = ['R','G','B','orange','slateblue','peru','pink','purple',
                      "magenta","cyan","crimson","royalblue","slategrey","firebrick",
                      "slategrey","chocolate","steelblue","navy","olive","deepskyblue"]
        #fig,axs = plt.subplots(1, 1)
        
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        
        ax = self.display_map(ax)
        
        for k in range(len(self.waypoints)):
            # diviser selon temps?
            transx = [self.all_true_trans[k][i][0] for i in range(len(self.all_true_trans[k])) ]
            transy = [self.all_true_trans[k][i][1] for i in range(len(self.all_true_trans[k])) ]
            
            trans_x_teleport = []
            trans_y_teleport = []
            for l in range(1,len(transx)):
                if self.all_gazebo_jump[k][l] == True:
                    trans_x_teleport += [ transx[l-1],transx[l] ] 
                    trans_y_teleport += [ transy[l-1],transy[l] ] 
            
            # print("number teleport : ", len([self.all_gazebo_jump[k][l]for l in range(len(self.all_gazebo_jump[k])) if self.all_gazebo_jump[k][l] == True]))
            # print(len(trans_x_teleport))
            # axs[0,0].scatter(transx,transy,c = colors[k],label="Path of waypoint %i"%k, s = 10,marker="x")
            # axs[0,0].scatter(self.waypoints[k][0],self.waypoints[k][1],c = colors[k],label="Waypoint %i"%k,s = 20,marker = "D")
            if self.reach[k][0]:
                endx = [self.waypoints[k][0]]
                endy = [self.waypoints[k][1]]
            else:
                endx = []
                endy = []
            
            debx = []
            deby = []
            
            if k > 0 and self.reach[k-1][0] :
                debx = [self.waypoints[k-1][0]]
                deby = [self.waypoints[k-1][1]]

            ax.plot(debx+transx+endx,deby+transy+endy,c = colors[k],label="Path of waypoint %i"%k)
                   
            ax.scatter(self.waypoints[k][0],self.waypoints[k][1],c = colors[k],label="Waypoint %i"%k,s = self.marker_size,marker = "D")
            ax.scatter(trans_x_teleport,trans_y_teleport,c = colors[k],label="Teleports during path to waypoint %i"%k,s = 20,marker = "*")
            
        ax.set_xlabel("X position (m)")
        ax.set_ylabel("Y position (m)")
        ax.set_title("Path of the robot during the experiment")
        ax.legend()   

    
    def read_all_csv(self,path):
        
        os.chdir(path)
        L = []
        for filename in glob.glob("*.csv"):
            #print(filename)
            L.append(filename)
        #print(L)   
        L2 = [ int(L[k].split(".")[0][8:]) for k in range(len(L))]
        #print(L2)
        
        L_sort = [ x for _,x in sorted(zip(L2,L))]
        
        #print(L_sort)
        for k in range(len(L_sort)):
            #print(L_sort[k])
            self.read_csv(L_sort[k])
    
    
    def read_csv(self,filename):
        
        ind_w = -1
        
        with open(filename,'rb') as f :
            reader = csv.reader(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
            for row in reader:

                isFloat = True
                try:
                    tst = float(row[0][0])
                except ValueError:
                    isFloat = False
                
                
                if row[0][:-5] == "Waypoint number" or row[0][:-4] == "Waypoint number":
                    pt = np.array([float(row[2]),float(row[4]),float(row[6]) ])
                    #print("Pt : ", pt)
                    List =  [(w==pt).all() for w in self.waypoints ]
                    
                    #if np.any( List ):    
                    # should be tried: display with same color
                    # the different path to the same goal.
                    if False:
                        #print(List)
                        ind_w = List.index( np.max(List) )
                        
                    else:
                        #print("New waypoint added")
                        self.waypoints.append(pt)
                        self.all_diff_norm_rot_rts.append([])
                        self.all_diff_norm_trans_rts.append([])
                        self.all_time_rts.append([])
                        self.all_time_amcl.append([])
                        self.all_time_odom.append([])
                        self.all_diff_norm_rot_amcl.append([])
                        self.all_diff_norm_trans_amcl.append([])
                        self.all_diff_norm_rot_odom.append([])
                        self.all_diff_norm_trans_odom.append([])
                        self.all_gazebo_jump.append([])
                        self.all_relocation_event.append([])
                        self.all_true_trans.append([])
                        self.all_true_rot.append([])
                        self.reach.append([])
                        
                
                elif isFloat ==False and row[0][:10] == "Impossible":
                    self.reach[ind_w].append(False)
                    
                elif isFloat == False and row[0][:8] == "Waypoint":                
                    self.reach[ind_w].append(True)
                
                elif isFloat == False :
                    pass
                
                else:
                    #print(row)
                    self.all_diff_norm_trans_amcl[ind_w].append(float(row[0]))
                    self.all_diff_norm_rot_amcl[ind_w].append(float(row[1]))
                    self.all_time_amcl[ind_w].append(float(row[2]))
                    
                    self.all_diff_norm_trans_rts[ind_w].append(float(row[3]))
                    self.all_diff_norm_rot_rts[ind_w].append(float(row[4]))
                    self.all_time_rts[ind_w].append(float(row[5]))
                    
                    self.all_diff_norm_trans_odom[ind_w].append(float(row[6]))
                    self.all_diff_norm_rot_odom[ind_w].append(float(row[7]))
                    self.all_time_odom[ind_w].append(float(row[8]))
                    
                    real_pose = row[9][1:-1].split(' | ')
                    
                    self.all_true_trans[ind_w].append(np.array([float(real_pose[0]),float(real_pose[1]),float(real_pose[2])]))
                    
                    real_rot = row[10][1:-1].split(' | ')
                    
                    self.all_true_rot[ind_w].append(np.array([float(real_rot[0]),float(real_rot[1]),float(real_rot[2])]))

                    if row[11] == "True":
                        #print("True added for %i"%ind_w)
                        self.all_gazebo_jump[ind_w].append(True)
                    else:
                        self.all_gazebo_jump[ind_w].append(False)

                    self.all_relocation_event[ind_w].append(row[12])


if __name__ == "__main__":
    data_path = "/home/tnmx0798/Documents/ROSBot_simu/data/waypointManager/relocalization/Experiment_2020-08-10-9-4"

    cm = createStats(data_path)