#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np

import matplotlib.pyplot as plt

import image_geometry
import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped


import tf2_geometry_msgs

from geometry_msgs.msg import PoseWithCovarianceStamped
from geometry_msgs.msg import PoseStamped


import time
import geometry_msgs.msg

import tf

import tf.transformations

from tf.msg import tfMessage

from std_msgs.msg import String

from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp_rts, comp_odom, pose_rts

from rosbot_gazebo.srv import boolean, save_name, differences

from rtabmap_ros.msg import MapGraph

from nav_msgs.msg import Odometry

import csv

import os

from datetime import datetime


class transformStampedEuler():
    
    def __init__(self):
        self.time = 0
        self.pos = None
        self.rot = None
    
    def __repr__(self):
        return(self.__str__() )
    
    def __str__(self):
        
        return(" Stamp : %f \n pos : [%f,%f,%f] \n rot : [%f,%f,%f] \n"%(self.time,self.pos[0],self.pos[1],self.pos[2],self.rot[0],self.rot[1],self.rot[2]))


class evaluateNodesRts():
     
    def callback_kidnap(self,msg):
        if self.gazebo_jump == False:
            self.gazebo_jump = True
    
    def callback_get_last_truth2(self,msg):
        
        # time = rospy.get_time()
        # true_pos = msg.trans_tr
        # true_rot = msg.rot_tr    
        if self.gazebo_jump == False and msg.gazebo_jump == True:
            self.gazebo_jump = True
        
        if (self.buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = msg.trans_tr
            tf_true.rot = msg.rot_tr   
            tf_true.time = msg.stamp.to_sec()
            # try:
            #     tf_true.time = msg.stamp.to_sec()
            # except Exception:
            #     tf_true.time = time
            self.buf_tf_true.append(tf_true)
            if (len(self.buf_tf_true) > self.qs_odom):
                del(self.buf_tf_true[0])

            #self.buf_tf_true.append(tf_true)
            # if (len(self.buf_tf_true) > 1):
            #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
    
        elif (self.temp_buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = msg.trans_tr   
            tf_true.rot = msg.rot_tr   
            tf_true.time = msg.stamp.to_sec()
            
            # try:
            #     tf_true.time = msg.stamp.to_sec()
            # except Exception:
            #     tf_true.time = time
            self.temp_buf_tf_true.append(tf_true)
            if (len(self.temp_buf_tf_true) > self.qs_odom):
                del(self.temp_buf_tf_true[0])

        
        return(None)    

    def callback_odom2(self,msg):
        
        if (self.buf_access_bl == True or self.temp_buf_access_bl ==True):
            z = 0
            new_quat = msg.pose.pose.orientation
            if self.compare_3d:
                
                nr = 0
                while not rospy.is_shutdown() and nr < self.ntry:
                    try:
                        transformStamped_odom = geometry_msgs.msg.TransformStamped()
                        transformStamped_odom = self.buffer.lookup_transform('odom_noise', 'base_link', msg.header.stamp, rospy.Duration(self.rate_rts) )
                        # odom_noise
                        time_odom = transformStamped_odom.header.stamp
                        break
                    except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                        #rospy.loginfo("Still searching for odom")
                        nr+=1
                        pass
                if nr !=self.ntry:
                    z = transformStamped_odom.transform.translation.z
                    new_quat = transformStamped_odom.transform.rotation
            
            new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z+z])

            euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
            roll,pitch,yaw = euler[0],euler[1],euler[2]
            new_rot = np.array([roll,pitch,yaw])
            
            bl = transformStampedEuler()
            bl.pos = new_pose
            bl.rot = new_rot
            bl.time = msg.header.stamp.to_sec()
            
            if (self.buf_access_bl):
                self.buf_tf_bl.append(bl)
                if len(self.buf_tf_bl) > self.qs_bl:
                    del(self.buf_tf_bl[0])
                    
            elif (self.temp_buf_access_bl):
                self.temp_buf_tf_bl.append(bl)
                if len(self.temp_buf_tf_bl) > self.qs_bl:
                    del(self.temp_buf_tf_bl[0])
                    
                
    def callback_tf2(self,msg):
        lentot = 0   
        
        if (self.buf_access_map ==True and self.buf_access_bl == True and self.buf_access_truth == True):
            
            lts = []
            for k in range(self.nb_diff):
                if len(self.buf_all_tf_map[k]) >0:
                    try : 
                        lts.append(self.buf_all_tf_map[k][-1].header.stamp.to_sec())
                    except Exception : 
                        lts.append(-1)
                else:
                    lts.append(-1)
            
            
            if (np.all(lts == self.prev_map_synch_size) == False):
                lms = [len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))]
                if (self.synch_tot):
                    lentot1 = min(lms)
                else:
                    lentot1 = max(lms)
                   
                lentot2 = len(self.buf_tf_bl)
                lentot3 = len(self.buf_tf_true)
                    
                #print("current len : %i, %i, %i"%(lentot1,lentot2,lentot3))
                lentot = min(lentot1,lentot2,lentot3)

        if (lentot > 0 and self.buf_access_map ==True):
            self.buf_access_map = False
            rospy.loginfo("Here")
            self.prepare_to_send2()

    def callback_rtab(self,msg):
        name = msg.header.frame_id
        if name[3:] == '':
            i = 0
        else: 
            i = int(name[3:])-1     
        if (i < self.nb_diff):
            if (self.buf_access_map): # and len( self.buf_all_tf_map[i]) == 0 ):
                self.buf_all_tf_map[i].append(msg)
                if len(self.buf_all_tf_map[i]) > self.qs_rts:
                    del(self.buf_all_tf_map[i][0])
                            
            elif (self.temp_buf_access_map): # and len( self.temp_buf_all_tf_map[i]) == 0):
                self.temp_buf_all_tf_map[i].append(msg)    
                if len(self.temp_buf_all_tf_map[i]) > self.qs_rts:
                    del(self.temp_buf_all_tf_map[i][0])        
        
        #print(self.buf_all_tf_map[i])
 

    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec <= prev_ec:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
    
    def find_tol_closest_time_match(self,time,L_time,tol):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec < prev_ec:
                i -= 1
                prev_ec = ec
                if prev_ec < tol:
                    break
            else:
                break
        return(i,prev_ec)
    
    
    def find_rel_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = (L_time[i-1]-time)
            if ec < prev_ec and ec >= 0:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)
        

    def check_synch_bl_L(self,buf,Lt):
        bl_synch = []
        imin = 999
        ntol = 0
        #print("Lt : ", Lt)
        if (buf[-1].time != self.len_buf_bl):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t = Lt[j] #-self.rate_rts
                    i = -1            
                    L_time = [ buf[k].time for k in range(len(buf))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    if ec < self.rate_bl/2+self.tol*(self.rate_bl/self.rate_rts):

                        if (ec > self.rate_bl/2):
                            ntol += (ec- self.rate_bl/2)*(self.rate_rts/self.rate_bl)
                            #ntol += 0.1
                        bl_synch.append(buf[i])
                        self.L_ec_bl[j] = -1
                        self.L_disp_bl[j] = True                        
                        if i < imin:
                            imin = i
                    else:
                        if self.L_disp_bl[j]:
                            print("Err base_link synch : min time difference : ",ec," for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            if (ec) < self.rate_bl:
                                ntol += (ec- self.rate_bl/2)*(self.rate_rts/self.rate_bl)
                                #not += 0.1
                            self.L_disp_bl[j] = False
                            self.L_ec_bl[j] = ec
                        if (self.synch_tot):
                            self.len_buf_bl = buf[-1].time
                            break  
                        else :
                            bl_synch.append(None)
                else:
                    bl_synch.append(None)

        self.tol = min( self.ltot*(self.tol  + ntol)/(self.ltot+self.nb_diff), self.rate_rts/2) #min( (self.tol*self.ltot  + ntol)/(self.ltot+self.nb_diff), self.rate_rts/2) 
        #self.tol = min(ntol+self.tol,self.rate_rts/2)
        return(bl_synch,imin)

    def check_synch_odom_L(self,buf,Lt):
        odom_synch = []
        imin = 999

        ntol = 0
        
        if ( buf[-1].time != self.len_buf_true):
            for j in range(len(Lt)):
                if Lt[j] != -1:
                    t =Lt[j] # Lt[j]-self.rate_rts
                    i = -1
        
                    L_time = [ buf[k].time for k in range(len(buf))]
                    (i,ec) = self.find_closest_time_match(t,L_time)
                    #(i,ec) = self.find_tol_closest_time_match(t,L_time, self.rate_odom+self.tol*(self.rate_odom/self.rate_rts))
                    if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_rts):
                        if (ec > self.rate_odom/2):
                            ntol += (ec- self.rate_odom/2)*(self.rate_rts/self.rate_odom)
                            # ntol += 0.1
                        odom_synch.append(buf[i])
                        self.L_ec_odom[j] = -1
                        self.L_disp_odom[j] = True
                        if i < imin : 
                            imin = i
                    else:
                        #if self.L_disp_odom[j] == False and L_ec_odom[j] != ec:
                        
                        if self.L_disp_odom[j]:
                            print(L_time)
                            print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to map : ", j , " (time : %f )"%t)
                            if (ec) < self.rate_odom:
                                ntol += (ec- self.rate_odom/2)*(self.rate_rts/self.rate_odom)
                                #ntol += 0.1
                            self.L_disp_odom[j] = False
                            self.L_ec_odom[j] = ec
                        
                        if (self.synch_tot):
                            self.len_buf_true = buf[-1].time
                            break
                        else:
                            odom_synch.append(None)

                else:
                    odom_synch.append(None)

        # if (ntol >0):
        #     print("ntol odom : ", ntol)    
        
        self.tol = min( (self.tol + ntol )*(self.ltot)/(self.ltot+self.nb_diff), self.rate_rts/2) #min( (self.tol*self.ltot + ntol )/(self.ltot+self.nb_diff), self.rate_rts/2) # 
        #self.tol = min(self.tol+ntol,self.rate_rts/2)
        return(odom_synch,imin)        
            
            
    def check_synch_map3(self,lms):
        """
        No check of synchronization at all. Only publish if there is a new object.
        """
        l = 0
        L_sync = []
        L_map_synch = []
        LMT = []

        buf_max = max(lms)
        # print("Buf max : ", buf_max)

        if buf_max > 1:
            rospy.loginfo("Try to resynch map ... ")
            #print(self.buf_all_tf_map)
            Lalltime = [self.buf_all_tf_map[k][0].header.stamp.to_sec()  for k in range(self.nb_diff) ]
            time_synch_map = max(Lalltime)
            while l < self.nb_diff:
                L_time = [ self.buf_all_tf_map[l][k].header.stamp.to_sec() for k in range(len(self.buf_all_tf_map[l])) ]
                           
                (i,ec) = self.find_closest_time_match(time_synch_map,L_time)
                if (ec < self.rate_rts/2+self.tol): # and  (self.buf_all_tf_map[l][i].header.stamp.to_sec() - self.L_all_time[l][-1] > self.rate_rts/2-self.tol):
                # try to put out self.tol
                    L_sync.append(i)
                    L_map_synch.append(self.buf_all_tf_map[l][i])
                    LMT.append(L_map_synch[-1].header.stamp.to_sec())
                    self.L_disp_odom[l] = True

                # pos 1
                else:
                    l = self.nb_diff
                    # L_sync.append(0)
                    # L_map_synch.append(self.buf_all_tf_map[l][0])
                    # LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                    # self.L_disp_odom[l] = True
                # pos 2
                # else:
                #     L_sync.append(0)
                #     L_map_synch.append(self.buf_all_tf_map[l][0])
                #     LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                #     self.L_disp_odom[l] = True
                l+=1
            if l == self.nb_diff+1:
                rospy.loginfo("Resynch failed.")
                L_sync = []
                L_map_synch = []
                LMT = []
                l = 0
                while l < self.nb_diff:
                    
                    if len(self.buf_all_tf_map[l]) > 0:
                            L_sync.append(0)
                            L_map_synch.append(self.buf_all_tf_map[l][0])
                            LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                    else:
                            L_sync.append(None)
                            L_map_synch.append(None)
                            LMT.append(-1)
                            if (self.synch_tot):
                                rospy.loginfo("Issue : one element missing : %i",l)
                                l = self.nb_diff
                                L = []
                                for k in range(len(self.buf_all_tf_map)):
                                    if (len(self.buf_all_tf_map[k]) > 0):
                                        L.append(self.buf_all_tf_map[k][-1].header.stamp.to_sec())
                                    else:
                                        L.append(-1)
                                self.prev_map_synch_size = np.array(L)
                    l+=1
            
            else:
                rospy.loginfo("Success of resynch.")
            
        else:
            while l < self.nb_diff:
                
                if len(self.buf_all_tf_map[l]) > 0:
                        L_sync.append(0)
                        L_map_synch.append(self.buf_all_tf_map[l][0])
                        LMT.append(L_map_synch[-1].header.stamp.to_sec())  
                else:
                        L_sync.append(None)
                        L_map_synch.append(None)
                        LMT.append(-1)
                        if (self.synch_tot):
                            rospy.loginfo("Issue : one element missing : %i",l)
                            l = self.nb_diff
                            L = []
                            for k in range(len(self.buf_all_tf_map)):
                                if (len(self.buf_all_tf_map[k]) > 0):
                                    L.append(self.buf_all_tf_map[k][-1].header.stamp.to_sec())
                                else:
                                    L.append(-1)
                            self.prev_map_synch_size = np.array(L)
                l+=1

        
        return(L_map_synch,L_sync,LMT)

    def check_jump(self,current_lin_speed,current_ang_speed,
                   prev_lin_speed,prev_ang_speed,
                   current_lin_pos,current_ang_pos,
                   prev_lin_pos,prev_ang_pos,
                   current_time,prev_time,current_detect_jump):
        """
        
        Principe : 
            - prendre vitesse (current_lin_speed, current_ang_speed) et position odométrique actuelle (current_lin_pos,current_ang_pos)
            - obtenir la vitesse (angulaire et linéaire) estimée, en comparant la position précédente
            à la position actuelle. (inutile maintenant)
            Cela permet d'avoir une estimation de la vitesse réelle estimée du robot.
            - obtenir la position (angulaire et linéaire) estimée, à partir de la vitesse précédente
            et la position réelle précédente. 
            - Evaluer la différence entre les positions estimées à partir des mesures précédentes et les positions actuelles.
            Si trop importante, veut dire qu'il y a une erreur : position actuelle ne colle
            pas par rapport à estimation de la vitesse et position précédente. 
        
        """
        detect_jump = False
        if current_detect_jump == True:
            return(current_detect_jump)
        
        if (type(prev_time) != type(None)):
            delay = current_time - prev_time
            estimated_speed = (current_lin_pos - prev_lin_pos)/delay
            estimated_ang = (current_ang_pos-prev_ang_pos)/delay
            
            estimated_pose = current_lin_speed*delay + prev_lin_pos
            # suppress z component if inferior to threshold
            if estimated_pose[2] < self.th_tlp_pos:
                estimated_pose[2] = 0
            estimated_rot = current_ang_speed*delay + prev_ang_pos
            
            rot_diff  = np.linalg.norm(estimated_rot-current_ang_pos) 
            if (rot_diff > np.pi):
                if (rot_diff//(2*np.pi)==0 ):
                    rot_diff =abs( rot_diff - 2*np.pi)
                else:
                    rot_diff = rot_diff%(2*np.pi)

            if (np.linalg.norm(estimated_pose - current_lin_pos) > self.th_tlp_pos  
                or rot_diff > self.th_tlp_rot):
                detect_jump = True
                # print("speed : ", np.linalg.norm(new_speed - estimated_speed ))
                # print("angular : ", np.linalg.norm(new_ang-estimated_ang))
                #print("speed : ", estimated_speed )
                #print("angular : ", estimated_ang)
                print("diff_pose : ", np.linalg.norm( current_lin_pos - estimated_pose ))
                #print("pose : ",  estimated_pose )
                #print("rotation : ", estimated_rot )      
                print("diff_rot : ", rot_diff )
        
        return(detect_jump)

    def callback_odom_jump(self,msg):
        # ancienne méthode pour estimer les téléportations à partir de l'odométrie. N'est plus utilisée

        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        new_speed = np.array([msg.twist.twist.linear.x, msg.twist.twist.linear.y, msg.twist.twist.linear.z])
        new_ang = np.array([msg.twist.twist.angular.x,msg.twist.twist.angular.y,msg.twist.twist.angular.z])
        timens = msg.header.stamp.to_sec()

        self.odom_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_odom_speed,self.prev_odom_ang,
                                           new_pose,new_rot,
                                           self.prev_odom_pos,self.prev_odom_rot,
                                           timens,self.prev_odom_time,self.odom_jump)

        self.prev_odom_pos = new_pose
        self.prev_odom_rot = new_rot
        self.prev_odom_speed = new_speed
        self.prev_odom_ang = new_ang
        self.prev_odom_time = timens
    
    def callback_initialpose_jump(self,msg):
        new_pose = np.array([msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z])
        new_quat = msg.pose.pose.orientation
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        if (type(self.prev_initialpose_pos) != type(None) and (np.linalg.norm(self.prev_initialpose_pos-new_pose ) > 0 or self.linalg.norm(self.prev_initialpose_rot-new_rot) >0) ):
            if (self.pose_jump == False):
                self.pose_jump = True
        elif type(self.prev_initialpose_pos) == type(None):
            self.pose_jump = True
        self.prev_initialpose_pos = new_pose
        self.prev_initialpose_rot = new_rot

    
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation

        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        true_speed = msg.twist[self.num_rosbot].linear
        true_ang = msg.twist[self.num_rosbot].angular
        new_speed = np.array([true_speed.x, true_speed.y, true_speed.z])
        new_ang = np.array([true_ang.x,true_ang.y,true_ang.z])
        timens = rospy.get_time()
        
        self.gazebo_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_gaz_speed,self.prev_gaz_ang,
                                           new_pose,new_rot,
                                           self.prev_gaz_pos,self.prev_gaz_rot,
                                           timens,self.prev_gaz_time,self.gazebo_jump)
        
        self.prev_gaz_pos = new_pose
        self.prev_gaz_rot = new_rot
        self.prev_gaz_speed = new_speed
        self.prev_gaz_ang = new_ang
        self.prev_gaz_time = timens
        
        
        #print(rospy.get_time() - self.last_time_pose)
        if (rospy.get_time() - self.last_time_pose) > self.rate_odom:
            #print("New_pose!")
            
            self.last_time_pose = rospy.get_time()
            time = self.last_time_pose
            if (self.buf_access):
                tf_true = transformStampedEuler()
                tf_true.pos = new_pose
                tf_true.rot = new_rot
                tf_true.time = time
                self.buf_tf_true.append(tf_true)
                if len(self.buf_tf_true) > self.qs_odom:
                    del(self.buf_tf_true[0])

            elif (self.temp_buf_access):
                tf_true = transformStampedEuler()
                tf_true.pos =new_pose
                tf_true.rot = new_rot
                tf_true.time = time
                self.temp_buf_tf_true.append(tf_true)
                if len(self.temp_buf_tf_true) > self.qs_odom:
                    del(self.temp_buf_tf_true[0])
            
        
        return(None)


    def prepare_to_send(self):
        
        
        self.temp_buf_access_map = True
        self.buf_access_bl = False
        self.temp_buf_access_bl = True
        self.buf_access_truth = False
        self.temp_buf_access_truth = True

        lms = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
        
        L_map_synch,L_synch,LMT = self.check_synch_map3(lms)
        test1 = False
        
        if (self.synch_tot):
            test1  = (len(L_map_synch) == self.nb_diff)
        else : 
            test1 = True
        if test1:
            self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])

            bl_synch, odom_synch = [],[]
            bl_check, odom_check = [],[]
                    
            odom_synch,odom_i = self.check_synch_odom_L(self.buf_tf_true,LMT)
            
            test2 = False
            if (self.synch_tot):
                test2 =  (len(odom_synch) == self.nb_diff)
            else:
                test2 = np.any([odom_synch[k] != None for k in range(len(odom_synch))])
                   
            if test2:
                self.len_buf_true = -1
                odom_check = odom_synch
                bl_synch,bl_i = self.check_synch_bl_L(self.buf_tf_bl,LMT)
                
                
                test3= False
                if (self.synch_tot):
                    test3 =  (len(bl_synch) == self.nb_diff)
                else:
                    test3 = np.any([bl_synch[k] != None for k in range(len(bl_synch))])        
            
                if test3:
                    self.len_buf_bl = -1
                    bl_check = bl_synch

                        
            if (min(len(bl_check),len(odom_check)) > 0 and min([ len(self.buf_all_tf_map[k]) for k in range(self.nb_diff)]) > 0):

                    self.pub_rts_err_L(L_map_synch,bl_check,odom_check)

                    l = 0
                    stol = 0
                    Lt = [LMT[k] for k in range(self.nb_diff) if LMT[k]!= -1]

                    mtime = min(Lt)
                    rospy.loginfo("////////////// Publication at time : %f completed! Indice : %i \\\\\\\\\\\\\\\\\\",mtime,self.ind)
                    rospy.loginfo("Updated tolerance : %f", self.tol)
                    
                    
                    self.ind += 1
                    self.send = True
    
                    for k in range(self.nb_diff):

                        val = L_synch[k]
                        if val!= None and bl_check[k] != None and odom_check[k] != None:

                            try : 
                                    
                                self.buf_all_tf_map[k] = self.buf_all_tf_map[k][val:]
                                if val <= 0:
                                    del(self.buf_all_tf_map[k][val])
                            except IndexError:
                                print("Error Index : ", L_synch[k], len(self.buf_all_tf_map[k]), k)
                                if len(self.buf_all_tf_map[k]) == 0:
                                    pass
                                
                            except Exception:
                                print("Other type of error!")
                        

            
        self.temp_buf_access_map = False

        #add up temporary buffers (for map)
        for k in range(self.nb_diff):
            if (len(self.buf_all_tf_map[k]) > 0):
                test = False
                while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                    #print("Treating suppression")
                    p1 = self.temp_buf_all_tf_map[k][0].mapToOdom.translation    
                    nw_pose = np.array([p1.x, p1.y, p1.z])
                    p2 = self.buf_all_tf_map[k][-1].mapToOdom.translation
                    od_pose = np.array([p2.x, p2.y, p2.z])
                    if (np.all(nw_pose==od_pose) == True) :
                        #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_all_tf_map[k][0])
                    else:
                        test = True

            len_temp_map = len(self.buf_all_tf_map[k])
            self.buf_all_tf_map[k] = self.buf_all_tf_map[k][len_temp_map:]
            self.buf_all_tf_map[k] += self.temp_buf_all_tf_map[k]
            self.temp_buf_all_tf_map[k] = []
                    
        # if (self.send):
        #   print("Bufffer after send : ", self.buf_all_tf_map)
            #self.send = False
        self.buf_access_map = True
        
        
        #add up temporary buffer (for tf /odom --> /bl)
        self.temp_buf_access_bl = False
        if len(self.temp_buf_tf_bl) > 0:
            test= False
            while len(self.temp_buf_tf_bl) > 0 and test == False:
                p1 = self.temp_buf_tf_bl[0].pos
                n_pose = p1
                p2 = self.buf_tf_bl[-1].pos
                o_pose = p2
                if (np.all(n_pose==o_pose) == True):
                    #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                    #print("Because of : ",  self.buf_all_tf_map[k][-1])
                    del(self.temp_buf_tf_bl[0])
                else:
                    test= True
        len_temp_bl = len(self.temp_buf_tf_bl)  
        self.buf_tf_bl = self.buf_tf_bl[len_temp_bl:]        
        self.buf_tf_bl += self.temp_buf_tf_bl
        self.temp_buf_tf_bl  = []

        self.buf_access_bl = True
            
        #add up temporary buffer (for true position)
        self.temp_buf_access_truth = False
        if len(self.temp_buf_tf_true) > 0:
            test= False
            while len(self.temp_buf_tf_true) > 0 and test == False:
                p1 = self.temp_buf_tf_true[0].pos
                n_pose = p1
                p2 = self.buf_tf_true[-1].pos
                o_pose = p2
                if (np.all(n_pose==o_pose) == True):
                    #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                    #print("Because of : ",  self.buf_all_tf_map[k][-1])
                    del(self.temp_buf_tf_true[0])
                else:
                    test= True
                        
                        
        len_temp_true = len(self.temp_buf_tf_true)  
        self.buf_tf_true = self.buf_tf_true[len_temp_true:]
        self.buf_tf_true += self.temp_buf_tf_true
        self.temp_buf_tf_true = []

        self.ltot+=self.nb_diff
        
        self.buf_access_truth = True
        


    def prepare_to_send2(self):
        
        
        # self.temp_buf_access_map = True
        # self.buf_access_bl = False
        # self.temp_buf_access_bl = True
        # self.buf_access_truth = False
        # self.temp_buf_access_truth = True

        lms = np.array([len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))])
        lentot = 1
        
        while not rospy.is_shutdown() and lentot > 0 and self.buf_access_truth == True:
            self.buf_access_truth = False
            self.buf_access_map = False
            self.temp_buf_access_map = True
            self.buf_access_bl = False
            self.temp_buf_access_bl = True

            self.temp_buf_access_truth = True    
            L_map_synch,L_synch,LMT = self.check_synch_map3(lms)
            test1 = False
            
            if (self.synch_tot):
                test1  = (len(L_map_synch) == self.nb_diff)
            else : 
                test1 = True
            if test1:
                self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
    
                bl_synch, odom_synch = [],[]
                bl_check, odom_check = [],[]
                        
                odom_synch,odom_i = self.check_synch_odom_L(self.buf_tf_true,LMT)
                
                test2 = False
                if (self.synch_tot):
                    test2 =  (len(odom_synch) == self.nb_diff)
                else:
                    test2 = np.any([odom_synch[k] != None for k in range(len(odom_synch))])
                       
                if test2:
                    self.len_buf_true = -1
                    odom_check = odom_synch
                    bl_synch,bl_i = self.check_synch_bl_L(self.buf_tf_bl,LMT)
                    
                    
                    test3= False
                    if (self.synch_tot):
                        test3 =  (len(bl_synch) == self.nb_diff)
                    else:
                        test3 = np.any([bl_synch[k] != None for k in range(len(bl_synch))])        
                
                    if test3:
                        self.len_buf_bl = -1
                        bl_check = bl_synch
    
                            
                if (min(len(bl_check),len(odom_check)) > 0 and min([ len(self.buf_all_tf_map[k]) for k in range(self.nb_diff)]) > 0):
    
                        self.pub_rts_err_L(L_map_synch,bl_check,odom_check)
    
                        l = 0
                        stol = 0
                        Lt = [LMT[k] for k in range(self.nb_diff) if LMT[k]!= -1]
    
                        mtime = min(Lt)
                        rospy.loginfo("////////////// Publication at time : %f completed! Indice : %i \\\\\\\\\\\\\\\\\\",mtime,self.ind)
                        rospy.loginfo("Updated tolerance : %f", self.tol)


                        self.ind += 1                            
                        self.send = True
        
                        for k in range(self.nb_diff):
    
                            val = L_synch[k]
                            if val!= None and bl_check[k] != None and odom_check[k] != None:
    
                                try : 
                                        
                                    self.buf_all_tf_map[k] = self.buf_all_tf_map[k][val:]
                                    if val <= 0:
                                        del(self.buf_all_tf_map[k][val])
                                except IndexError:
                                    print("Error Index : ", L_synch[k], len(self.buf_all_tf_map[k]), k)
                                    if len(self.buf_all_tf_map[k]) == 0:
                                        pass
                                    
                                except Exception:
                                    print("Other type of error!")
                            
    
                
            self.temp_buf_access_map = False
    
            #add up temporary buffers (for map)
            for k in range(self.nb_diff):
                if (len(self.buf_all_tf_map[k]) > 0):
                    test = False
                    while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                        #print("Treating suppression")
                        p1 = self.temp_buf_all_tf_map[k][0].mapToOdom.translation    
                        nw_pose = np.array([p1.x, p1.y, p1.z])
                        p2 = self.buf_all_tf_map[k][-1].mapToOdom.translation
                        od_pose = np.array([p2.x, p2.y, p2.z])
                        if (np.all(nw_pose==od_pose) == True) :
                            #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                            #print("Because of : ",  self.buf_all_tf_map[k][-1])
                            del(self.temp_buf_all_tf_map[k][0])
                        else:
                            test = True
    
                len_temp_map = len(self.buf_all_tf_map[k])
                self.buf_all_tf_map[k] = self.buf_all_tf_map[k][len_temp_map:]
                self.buf_all_tf_map[k] += self.temp_buf_all_tf_map[k]
                self.temp_buf_all_tf_map[k] = []
                        
            # if (self.send):
            #   print("Bufffer after send : ", self.buf_all_tf_map)
                #self.send = False
            self.buf_access_map = True
            
            
            #add up temporary buffer (for tf /odom --> /bl)
            self.temp_buf_access_bl = False
            if len(self.temp_buf_tf_bl) > 0:
                test= False
                while len(self.temp_buf_tf_bl) > 0 and test == False:
                    p1 = self.temp_buf_tf_bl[0].pos
                    n_pose = p1
                    p2 = self.buf_tf_bl[-1].pos
                    o_pose = p2
                    if (np.all(n_pose==o_pose) == True):
                        #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_tf_bl[0])
                    else:
                        test= True
            len_temp_bl = len(self.temp_buf_tf_bl)  
            self.buf_tf_bl = self.buf_tf_bl[len_temp_bl:]        
            self.buf_tf_bl += self.temp_buf_tf_bl
            self.temp_buf_tf_bl  = []
    
            self.buf_access_bl = True
                
            #add up temporary buffer (for true position)
            # print("Bef temp : ", [self.temp_buf_tf_true[k].time for k in range(len(self.temp_buf_tf_true))])
            # print("Bef curr : ", [self.buf_tf_true[k].time for k in range(len(self.buf_tf_true))])
            self.temp_buf_access_truth = False

            if len(self.temp_buf_tf_true) > 0:
                test= False
                while len(self.temp_buf_tf_true) > 0 and test == False:
                    p1 = self.temp_buf_tf_true[0].pos
                    n_pose = p1
                    p2 = self.buf_tf_true[-1].pos
                    o_pose = p2
                    if (np.all(n_pose==o_pose) == True):
                        #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_tf_true[0])
                    else:
                        test= True
                            
                            
            len_temp_true = len(self.temp_buf_tf_true)  
            self.buf_tf_true = self.buf_tf_true[len_temp_true:]
            self.buf_tf_true += self.temp_buf_tf_true
            self.temp_buf_tf_true = []
        
            # print("Aft cur : ", [self.buf_tf_true[k].time for k in range(len(self.buf_tf_true))])
    
            self.ltot+=self.nb_diff
            
            self.buf_access_truth = True
            
            lts = []
            lentot = 0
            for k in range(self.nb_diff):
                if len(self.buf_all_tf_map[k]) >0:
                    try : 
                        lts.append(self.buf_all_tf_map[k][-1].header.stamp.to_sec())
                    except Exception : 
                        lts.append(-1)
                else:
                    lts.append(-1)
            
            
            if (np.all(lts == self.prev_map_synch_size) == False):
                lms = [len(self.buf_all_tf_map[l]) for l in range(len(self.buf_all_tf_map))]
                if (self.synch_tot):
                    lentot1 = min(lms)
                else:
                    lentot1 = max(lms)
                   
                lentot2 = len(self.buf_tf_bl)
                lentot3 = len(self.buf_tf_true)
                    
                lentot = min(lentot1,lentot2,lentot3)
            

    def pub_rts_err_L(self,L_map_synch,Lbl_synch,Lodom_synch):
        ######## TRY ONE
        
        # Lind = [k for k in range(self.nb_diff) if Lbl_synch[k]!= None]

        # L_real_odom = [Lodom_synch[Lind[j]] for j in range(len(Lind))]
        # L_real_bl = [Lbl_synch[Lind[j]] for j in range(len(Lind))]
        # L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        # Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # mini = min(Lrt)
        # print("Before : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])
        # print("With times : ", Lrt)
        
        # Li = []
        # i = 1
        # Lrttemp = [ m for m in Lrt]
        # mini = min(Lrt)
        # while len(Li) != len(Lind):

        #     Loi = [Lrttemp[j] - mini -i*self.rate_rts/2 for j in range(len(Lrt))]
        #     Loi_ok = [Loi.index(a) for a in Loi if a < 0]
        #     if len(Loi_ok) > 0:
        #         Lttemp = [ Lrt[a] for a in Loi_ok]
        #         vs = Lrt.index(min(Lttemp))
        #         for l in Loi_ok:
        #             print(L_real_odom[l].time,l,vs,L_real_odom[vs].time)
        #             L_real_odom[l] = L_real_odom[vs]
        #             Li.append(l)
        #             Lrttemp[l] = max(Lrttemp) + (i+1)*self.rate_rts/2
        #     i+=1
                
        # print(Lrttemp)
        # print(  Lrt)
            
        # Li = []
        # i = 1
        # Lrttemp2 = [ m for m in Lrt]
        # #print(Lrttemp2)
        # while len(Li) != len(Lind):
        #     Loi = [Lrttemp2[j] - mini -i*self.rate_rts/2 for j in range(len(Lrt))]
        #     Loi_ok = [Loi.index(a) for a in Loi if a < 0]
        #     if len(Loi_ok) > 0:
        #         #print(Loi)
        #         #print(Loi_ok)
        #         Lttemp = [ Lrt[a] for a in Loi_ok]
        #         vs = Lrt.index(min(Lttemp))
        #         for l in Loi_ok:
        #             L_real_bl[l] = L_real_bl[vs]
        #             #print(L_real_bl[l].header.stamp.to_sec(),l,vs,L_real_bl[vs].header.stamp.to_sec())
        #             Li.append(l)
        #             Lrttemp2[l] = max(Lrttemp2) + (i+1)*self.rate_rts/2
        #     #print("i+1")
        #     i+=1                
        #print("After : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])        
        
        # for k in range(self.nb_diff):
        #     if k in Lind:
        #         Lbl_synch[k] = L_real_bl[k]
        #         Lodom_synch[k] = L_real_odom[k]
                
        #print("After : ", [L_real_bl[k].header.stamp.to_sec() for k in range(len(L_real_bl))])
        
        ########## TRY TWO
        
        # ZBL = geometry_msgs.msg.TransformStamped()
        # ZBL.transform.translation.x = 0
        # ZBL.transform.translation.y = 0
        # ZBL.transform.translation.z = 0
        
        # ZBL.transform.rotation.x = 0
        # ZBL.transform.rotation.y = 0
        # ZBL.transform.rotation.z = 0
        # ZBL.transform.rotation.w = 1
        
        # ZO = transformStampedEuler()
        # ZO.pos = np.array([0,0,0])
        # ZO.rot = np.array([0,0,0])
        
        # Lind = [k for k in range(self.nb_diff) if Lbl_synch[k]!= None]
        # L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        # Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]
        # vs = Lrt.index(min(Lrt))
        # blmin = Lbl_synch[vs]
        
        # for k in range(self.nb_diff):
        #     if (Lbl_synch[k]!=None):
        #         Lodom_synch[k] = ZO
        #         Lbl_synch[k] = blmin
        
        Lind = []
        for k in range(self.nb_diff):
            if (Lbl_synch[k] != None and Lodom_synch[k] != None ):
                Lind.append(k)        

        L_real_odom = [Lodom_synch[Lind[j]] for j in range(len(Lind))]
        L_real_bl = [Lbl_synch[Lind[j]] for j in range(len(Lind))]
        L_real_map = [ L_map_synch[Lind[j]] for j in range(len(Lind))]
        Lrt = [ L_real_map[l].header.stamp.to_sec() for l in range(len(L_real_map)) ]

        try :
            if self.verbose:
                print("BL_deriv : ", str([ round(abs(L_real_bl[k].time - Lrt[k]),6) for k in range(len(L_real_bl))]) )
                print("Odom_deriv : ", str([ round( abs(L_real_odom[k].time - Lrt[k]),6) for k in range(len(L_real_odom))]) )
            rospy.loginfo("Maximum time deriv between maps : %s", str(round(max(Lrt) - min(Lrt),6) ) )
        except AttributeError:
            print("AE!")
            print("BL  : ", L_real_odom)
            print("Odom :", L_real_bl)
            pass
        
        for k in range(self.nb_diff):
            if (L_map_synch[k] != None and Lbl_synch[k] != None and Lodom_synch != None):
                true_pos = Lodom_synch[k].pos
                true_rot = Lodom_synch[k].rot
                pose_bl = Lbl_synch[k]
                

            
                tf_rts = geometry_msgs.msg.TransformStamped()
                tf_tranf = L_map_synch[k]
                
                if self.record_rts_node:
                
                    self.L_all_time[k].append(tf_tranf.header.stamp.to_sec())
                
                
                # POSS 1 : calcul à la main
                
                #x0,y0,z0,w0 
                x0,y0,z0,w0 = tf_tranf.mapToOdom.rotation.x,tf_tranf.mapToOdom.rotation.y,tf_tranf.mapToOdom.rotation.z,tf_tranf.mapToOdom.rotation.w
                
                roll_m,pitch_m,yaw_m = tf.transformations.euler_from_quaternion([x0,y0,z0,w0])
                x_map, y_map, z_map = tf_tranf.mapToOdom.translation.x, tf_tranf.mapToOdom.translation.y, tf_tranf.mapToOdom.translation.z
                

                M_o2bl = tf.transformations.compose_matrix(angles = [pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2]],
                                                           translate = [pose_bl.pos[0],pose_bl.pos[1],pose_bl.pos[2] ])
                
                rospy.loginfo("Map --> Odom z : %f ; Odom --> bl z : %f",tf_tranf.mapToOdom.translation.z , pose_bl.pos[2])
                
                M_m2o = tf.transformations.compose_matrix(angles = [roll_m,pitch_m,yaw_m],
                                                           translate = [x_map,y_map,z_map])

                #M_m2bl = np.dot(M_o2bl,M_m2o)
                M_m2bl = np.dot(M_m2o,M_o2bl)

                trans_map = tf.transformations.translation_from_matrix(M_m2bl)
                M_quat = tf.transformations.quaternion_from_matrix(M_m2bl)


                tf_rts.header.stamp = tf_tranf.header.stamp
                
                # debug : check map -> odom_noise only
                
                # tf_rts.transform.translation.x = x_map
                # tf_rts.transform.translation.y =   y_map
                # tf_rts.transform.translation.z = z_map
                
                # tf_rts.transform.rotation.x = x0
                # tf_rts.transform.rotation.y = y0
                # tf_rts.transform.rotation.z = z0
                # tf_rts.transform.rotation.w = w0

                # debug : check odom_noise -> base_link only
                
                # x1,y1,z1,w1 = tf.transformations.quaternion_from_euler(pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2])

                # tf_rts.transform.translation.x =pose_bl.pos[0]
                # tf_rts.transform.translation.y = pose_bl.pos[1]
                # tf_rts.transform.translation.z =pose_bl.pos[2]
                
                # tf_rts.transform.rotation.x = x1
                # tf_rts.transform.rotation.y = y1
                # tf_rts.transform.rotation.z = z1
                # tf_rts.transform.rotation.w = w1
                
                # normal : map -> base_link
                
                tf_rts.transform.translation.x = trans_map[0]#-0.03
                tf_rts.transform.translation.y =   trans_map[1]-0.03
                tf_rts.transform.translation.z =  trans_map[2]+0.18
                
                tf_rts.transform.rotation.x = M_quat[0]
                tf_rts.transform.rotation.y = M_quat[1]
                tf_rts.transform.rotation.z = M_quat[2]
                tf_rts.transform.rotation.w = M_quat[3]


                # POSS 1B

                #   q_map = tf.transformations.quaternion_from_euler(pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2])

                # # x1,y1,z1,w1
                
                # x1,y1,z1,w1 = q_map[0],q_map[1],q_map[2],q_map[3]
                
                # # M_quat =  tf.transformations.quaternion_multiply(q, tf_tranf.mapToOdom.rotation)         
                       
                # M_quat = np.array((
                #     x1*w0 + y1*z0 - z1*y0 + w1*x0,
                #     -x1*z0 + y1*w0 + z1*x0 + w1*y0,
                #     x1*y0 - y1*x0 + z1*w0 + w1*z0,
                #     -x1*x0 - y1*y0 - z1*z0 + w1*w0), dtype=np.float64)                
                
                # # M_rot_bl = tf.transformations.euler_matrix(pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2])[:3,:3]
                # # #print(M_rot_bl)
                # # # print("Hre : ", np.array([tf_tranf.mapToOdom.translation.x,tf_tranf.mapToOdom.translation.y, tf_tranf.mapToOdom.translation.z]))
               
                # # K = np.array([tf_tranf.mapToOdom.translation.x,tf_tranf.mapToOdom.translation.y, tf_tranf.mapToOdom.translation.z])
                # # print("Correct rts trans : ", K)               
                # # M_transfo_trans = np.dot(M_rot_bl,K)
                
                

                # # tf_rts.header.stamp = tf_tranf.header.stamp
                # # tf_rts.transform.translation.x = M_transfo_trans[0] + pose_bl.pos[0]-0.03
                # # tf_rts.transform.translation.y =  M_transfo_trans[1] + pose_bl.pos[1]
                # # tf_rts.transform.translation.z =  M_transfo_trans[2] + pose_bl.pos[2]+0.18
                
                # # # tf_rts.transform.translation.x = tf_tranf.mapToOdom.translation.x + pose_bl.pos[0]-0.03
                # # # tf_rts.transform.translation.y = tf_tranf.mapToOdom.translation.y + pose_bl.pos[1]
                # # # tf_rts.transform.translation.z = tf_tranf.mapToOdom.translation.z + pose_bl.pos[2]+0.18
                


                        
                # tf_rts.transform.rotation.x = M_quat[0]
                # tf_rts.transform.rotation.y = M_quat[1]
                # tf_rts.transform.rotation.z = M_quat[2]
                # tf_rts.transform.rotation.w = M_quat[3]
                
                
                # POSS 2 :
                
                # trans = [tf_tranf.mapToOdom.translation.x+pose_bl.pos[0]-0.03, 
                #          tf_tranf.mapToOdom.translation.y+pose_bl.pos[1],
                #          tf_tranf.mapToOdom.translation.z+pose_bl.pos[2]+0.18]
                
                # x0,y0,z0,w0 = tf_tranf.mapToOdom.rotation.x,tf_tranf.mapToOdom.rotation.y,tf_tranf.mapToOdom.rotation.z,tf_tranf.mapToOdom.rotation.w
                # rot = tf.transformations.euler_from_quaternion([x0,y0,z0,w0])
                
                # angl = [rot[0]+pose_bl.rot[0],
                #         rot[1]+pose_bl.rot[1],
                #         rot[2]+pose_bl.rot[2]]
                
                # Mat_transf = tf.transformations.compose_matrix(angles = angl,translate = trans)
                
                # pos_comp = tf.transformations.translation_from_matrix(Mat_transf)
                # rot_comp = tf.transformations.quaternion_from_matrix(Mat_transf)
                
                # tf_rts.header.stamp = tf_tranf.header.stamp
                # tf_rts.transform.translation.x = pos_comp[0]
                # tf_rts.transform.translation.y =  pos_comp[1]
                # tf_rts.transform.translation.z =  pos_comp[2]
                                
                # tf_rts.transform.rotation.x = rot_comp[0]
                # tf_rts.transform.rotation.y = rot_comp[1]
                # tf_rts.transform.rotation.z = rot_comp[2]
                # tf_rts.transform.rotation.w = rot_comp[3]

                
                # POSS 3
                
                # transfo1 =  geometry_msgs.msg.TransformStamped()
                # x0,y0,z0,w0 = tf_tranf.mapToOdom.rotation.x,tf_tranf.mapToOdom.rotation.y,tf_tranf.mapToOdom.rotation.z,tf_tranf.mapToOdom.rotation.w
                # transfo1.transform.rotation.x = x0
                # transfo1.transform.rotation.y = y0
                # transfo1.transform.rotation.z = z0
                # transfo1.transform.rotation.w = w0
                
                # transfo1.transform.translation.x = tf_tranf.mapToOdom.translation.x
                # transfo1.transform.translation.y = tf_tranf.mapToOdom.translation.y
                # transfo1.transform.translation.z = tf_tranf.mapToOdom.translation.z
                
                
                # p1 = tf2_geometry_msgs.do_transform_pose(PoseStamped(),transfo1)
                
                # q = tf.transformations.quaternion_from_euler(pose_bl.rot[0], pose_bl.rot[1], pose_bl.rot[2]-2*np.pi)
                # x1,y1,z1,w1 = q[0],q[1],q[2],q[3]
                
                # transfo2 =  geometry_msgs.msg.TransformStamped()                
                # transfo2.transform.rotation.x = x1
                # transfo2.transform.rotation.y = y1
                # transfo2.transform.rotation.z = z1
                # transfo2.transform.rotation.w = w1
                
                # transfo2.transform.translation.x = pose_bl.pos[0]
                # transfo2.transform.translation.y =  pose_bl.pos[1]
                # transfo2.transform.translation.z =  pose_bl.pos[2]
                
                # p2 = tf2_geometry_msgs.do_transform_pose(p1,transfo2)                
                
                # #print(p2)
                
                
                # tf_rts.header.stamp = tf_tranf.header.stamp
                # tf_rts.transform.translation.x = p2.pose.position.x-0.03
                # tf_rts.transform.translation.y =   p2.pose.position.y
                # tf_rts.transform.translation.z =  p2.pose.position.z+0.18
                
                # tf_rts.transform.translation.x = tf_tranf.mapToOdom.translation.x + pose_bl.pos[0]-0.03
                # tf_rts.transform.translation.y = tf_tranf.mapToOdom.translation.y + pose_bl.pos[1]
                # tf_rts.transform.translation.z = tf_tranf.mapToOdom.translation.z + pose_bl.pos[2]+0.18
                
                
                
                #print("Map --> Odom z : %f ; Odom --> bl z : %f"%(tf_tranf.mapToOdom.translation.z , pose_bl.pos[2]))
                

                        
                # tf_rts.transform.rotation.x = p2.pose.orientation.x
                # tf_rts.transform.rotation.y = p2.pose.orientation.y
                # tf_rts.transform.rotation.z = p2.pose.orientation.z
                # tf_rts.transform.rotation.w = p2.pose.orientation.w
                

                self.evaluateProximityToTruth_rts(true_pos,true_rot,tf_rts,k)
                
                
        true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
        rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
        self.pose_jump = False
        self.prev_gaz_pos  = true_trans_tr
        self.prev_gaz_rot = rot_tr
        self.gazebo_jump = False
        self.odom_jump = False
        

            

    def evaluateProximityToTruth_rts(self,true_pos,true_rot,transformStamped_rts,k):  
        """
        Ici, on prend le lien map_gazebo --> base_link (exprimé par true_pos et true_rot), et on y rajoute
        le lien base_link --> camera2 pour pouvoir le comparer avec la transformation map --> camera_link2
        
        k : transformation rtabmap k testée
        """
        # debug purpose, to check whether the values of the topics and of the tf tree are equals.
        
        # nr = 0
        # while not rospy.is_shutdown() and nr < self.ntry:
        #     try:
        #         transformStamped_odom = geometry_msgs.msg.TransformStamped()
        #         transformStamped_odom = self.buffer.lookup_transform('map', 'camera_link2', transformStamped_rts.header.stamp, rospy.Duration(self.rate_rts) )
        #         # odom_noise
        #         time_odom = transformStamped_odom.header.stamp
        #         break
        #     except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
        #         #rospy.loginfo("Still searching for odom")
        #         nr+=1
        #         pass

        # if nr == self.ntry:
        #     self.pose_jump = False
        #     print("No map...")
        #     if (self.type_data == "raw"):
        #         trans_tr = np.array([true_pos.x,true_pos.y-0.03,true_pos.z+0.18]) #+0.14
        #         euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
        #         roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]

        #         true_trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
        #         rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
                
        #     elif (self.type_data == "record"):
        #           trans_tr = np.array([true_pos[0],true_pos[1]-0.03,true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
        #           true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
        #           rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
        # else:
        #     self.pose_jump = True
        #     print("Got map!")
        #     euler_tr = tf.transformations.euler_from_quaternion((transformStamped_odom.transform.rotation.x,
        #                                                           transformStamped_odom.transform.rotation.y,
        #                                                           transformStamped_odom.transform.rotation.z,
        #                                                           transformStamped_odom.transform.rotation.w))
        #     roll_tr, pitch_tr, yaw_tr= euler_tr[0],euler_tr[1],euler_tr[2]
                        
        #     trans_tr = np.array([transformStamped_odom.transform.translation.x,transformStamped_odom.transform.translation.y,transformStamped_odom.transform.translation.z])           
        #     rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
        
        
        if (self.type_data == "raw"):
            # trans_tr = np.array([true_pos.x,true_pos.y-0.03,true_pos.z+0.18]) #+0.14
            # euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            # roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]

            # true_trans_tr = np.array([true_pos.x,true_pos.y,true_pos.z])
            # rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
            
            trans_tr = np.array([true_pos[0],true_pos[1]-0.03,true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
            true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
            rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
            
                
        elif (self.type_data == "record"):
              trans_tr = np.array([true_pos[0],true_pos[1]-0.03,true_pos[2]+0.18]) # [0,0,+0.137] pr gtsam    
              true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])                 
              rot_tr = np.array([true_rot[0],true_rot[1],true_rot[2]])
        
        if True: 

            
            # obtain the norm of translation and rotation
            euler_rts = tf.transformations.euler_from_quaternion((transformStamped_rts.transform.rotation.x,
                                                                 transformStamped_rts.transform.rotation.y,
                                                                 transformStamped_rts.transform.rotation.z,
                                                                 transformStamped_rts.transform.rotation.w))
            roll_rts, pitch_rts, yaw_rts= euler_rts[0],euler_rts[1],euler_rts[2]

            
            trans_rts = np.array([transformStamped_rts.transform.translation.x,transformStamped_rts.transform.translation.y,transformStamped_rts.transform.translation.z])           
            rot_rts = np.array([roll_rts,pitch_rts,yaw_rts])

            if self.compare_3d == False:   
                trans_rts[2] = 0.0
                rot_rts[0] = 0.0
                rot_rts[1] = 0.0
                
                trans_tr[2] = 0.0
                rot_tr[0] = 0.0
                rot_tr[1] = 0.0

            trans_rtsTr = trans_tr-trans_rts
            
            rot_rtsTr = rot_tr-rot_rts
            for l in range(3):  
                if (rot_tr[l]*rot_rts[l]<0):
                    rot_rtsTr[l] = min([abs(rot_rts[l]-rot_tr[l]),abs(rot_rts[l]+rot_tr[l]),abs(rot_tr[l]-rot_rts[l]),abs(rot_tr[l]+rot_rts[l])])



            diff_norm_trans = np.linalg.norm(trans_rtsTr)
            diff_norm_rot = np.linalg.norm(rot_rtsTr)
            
            
            
            # publish it
            msg = comp_rts()
            msg.stamp = transformStamped_rts.header.stamp
            msg.trans_rts = list(trans_rts)
            msg.trans_tr = list(trans_tr)
            msg.rot_rts = list(rot_rts)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot
            msg.pose_jump = self.pose_jump
            msg.gazebo_jump = self.gazebo_jump
            self.L_pub_comp[k].publish(msg)
            
            msg = pose_rts()
            msg.stamp = transformStamped_rts.header.stamp
            msg.trans_rts = list(trans_rts)
            msg.rot_rts = list(rot_rts)       
            self.L_pub_rts[k].publish(msg)
            
            if self.record_rts_node:
                self.L_all_trans_err[k].append(diff_norm_trans)
                self.L_all_rot_err[k].append(diff_norm_rot)


        return(None)
    
    def display_errors(self,msg):
        if (msg.true_false):
            # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
            colors = ['R','G','B','orange','black','peru','pink','purple',
                      "magenta","cyan","crimson","royalblue","slategrey","firebrick",
                      "slategrey","chocolate","steelblue","navy","olive","deepskyblue"]
            fig,axs = plt.subplots(2, 1)
            print([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            #ori_time = min([self.L_all_time[k][0] for k in range(self.nb_diff)])
            # ori_time = self.L_time_synch[0]
            # Ltime = [self.L_time_synch[l]-ori_time for l in range(length)]
            Lind = [l for l in range(length)]
            for k in range(self.nb_diff):
                #Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                # print(k)
                # print(len(Lind))
                # print(len(self.L_all_trans_err[k][:length]))
                
                #print(self.L_time[:length])

        
                #print(colors[k])
                axs[0,].plot(Lind,self.L_all_trans_err[k][:length], label="test nbe %i"%k, color = colors[k])
                axs[1,].plot(Lind,self.L_all_rot_err[k][:length],  label="test nbe %i"%k, color = colors[k])
            # ok
            axs[0,].set_xlabel("Pose index")
            axs[0,].set_ylabel("diff_trans_total (m)")
            axs[0,].set_title("Translational error in time")
            axs[0,].legend()
            axs[1,].set_xlabel("Pose index")
            axs[1,].set_ylabel("diff_rot_total (rad)")
            axs[1,].set_title("Rotational error in time")
            axs[1,].legend()
            plt.show()
        return(True)

    def display_max_errors(self,msg):
        if (msg.true_false):
            
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            ori_time = min([self.L_all_time[k][0] for k in range(len(self.L_all_time) ) if len(self.L_all_time[k]) > 0])
            L_medians_trans = []
            L_stds_trans = []
            L_medians_rot = []
            L_stds_rot = []   
            L_mins_rot = []
            L_mins_trans = []
            L_maxs_rot = []
            L_maxs_trans = []

            for k in range(length):
                Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
                L_medians_trans.append(np.median(self.L_all_trans_err[k]) )
                L_stds_trans.append(np.std(self.L_all_trans_err[k]))
                L_medians_rot.append(np.median(self.L_all_rot_err[k]) )
                L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
                min_rot_k = np.min(self.L_all_rot_err[k])
                t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
                L_mins_rot.append([min_rot_k,t_min_rot_k])
                
                max_rot_k = np.max(self.L_all_rot_err[k])
                t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
                L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
                min_trans_k = np.min(self.L_all_trans_err[k])
                t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
                L_mins_trans.append([min_trans_k,t_min_trans_k])
                
                max_trans_k = np.max(self.L_all_trans_err[k])
                t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
                L_maxs_trans.append([max_trans_k,t_max_trans_k])       
           
            L_class_median_trans = [0 for k in range(self.nb_diff)]
            for i in range(len(L_medians_trans)):
                val_clas = L_medians_trans[i]
                clas = 0
                for j in range(len(L_medians_trans)):
                    if val_clas > L_medians_trans[j]:
                        clas+=1
                L_class_median_trans[i] = clas
                
            L_class_median_rot = [0 for k in range(self.nb_diff)]
            for i in range(len(L_medians_rot)):
                val_clas = L_medians_rot[i]
                clas = 0
                for j in range(len(L_medians_rot)):
                    if val_clas > L_medians_rot[j]:
                        clas+=1
                L_class_median_rot[i] = clas                
               
            print("------------ Max errors -------------")
            print(" Translation Max Error Statistics ")
            Lmt2 = [ L_mins_trans[k][0] for k in range(len(L_mins_trans))]
            LMt2 = [ L_maxs_trans[k][0] for k in range(len(L_maxs_trans))]
            print("By median : %f | By max : %f | By min : %f "%(max(L_medians_trans) - min(L_medians_trans) , max(LMt2) - min(LMt2) , max(Lmt2) - min(Lmt2)  ) )
            print("********************************")
            Lmr2 = [ L_mins_rot[k][0] for k in range(len(L_mins_rot))]
            LMr2 = [ L_maxs_rot[k][0] for k in range(len(L_maxs_rot))]
            print(" Rotational Max Error Statistics ")
            print("By median : %f | By max : %f | By min : %f "%(max(L_medians_rot) - min(L_medians_rot) , max(LMr2) - min(LMr2) , max(Lmr2) - min(Lmr2)             ) )
               
        return(True)   
    
    def statistics_mean(self):
        length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
        
        ori_time = min([self.L_all_time[k][0] for k in range(len(self.L_all_time) ) if len(self.L_all_time[k]) > 0])
        
        L_medians_trans = []
        L_stds_trans = []
        L_medians_rot = []
        L_stds_rot = []   
        L_mins_rot = []
        L_mins_trans = []
        L_maxs_rot = []
        L_maxs_trans = []
        
        for k in range(self.nb_diff):
            Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
            L_medians_trans.append(np.mean(self.L_all_trans_err[k]) )
            L_stds_trans.append(np.std(self.L_all_trans_err[k]))
            L_medians_rot.append(np.mean(self.L_all_rot_err[k]) )
            L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
            min_rot_k = np.min(self.L_all_rot_err[k])
            t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
            L_mins_rot.append([min_rot_k,t_min_rot_k])
                
            max_rot_k = np.max(self.L_all_rot_err[k])
            t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
            L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
            min_trans_k = np.min(self.L_all_trans_err[k])
            t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
            L_mins_trans.append([min_trans_k,t_min_trans_k])
                
            max_trans_k = np.max(self.L_all_trans_err[k])
            t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
            L_maxs_trans.append([max_trans_k,t_max_trans_k])   
        
        L_all_positive_detection = []
        L_all_negative_detection = []
        
        for k in range(self.nb_diff):

            LC_detec_trans = np.array([self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1] for l in range(length -1) if abs(self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1]) > self.trans_tol])
            LCdt2 = (LC_detec_trans < self.trans_tol) # infé --> correction positive; sup --> correction négative
            
            LC_detec_rot = np.array([self.L_all_rot_err[k][l] - self.L_all_rot_err[k][l+1] for l in range(length -1) if abs(self.L_all_rot_err[k][l] - self.L_all_rot_err[k][l+1]) > self.rot_tol])
            LCdr2 = (LC_detec_rot < self.rot_tol) # infé --> correction positive; sup --> correction négative            
            
            pos_detect = np.sum(LCdt2)+np.sum(LCdr2)
            neg_detect = np.shape(LC_detec_trans)[0] + np.shape(LC_detec_rot)[0] - pos_detect
            
            L_all_positive_detection.append(pos_detect)
            L_all_negative_detection.append(neg_detect)
            
        
        return(L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot,
               L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans,
               L_all_positive_detection, L_all_negative_detection)

    def statistics_med(self):
        length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
        
        ori_time = min([self.L_all_time[k][0] for k in range(len(self.L_all_time) ) if len(self.L_all_time[k]) > 0])
        
        L_medians_trans = []
        L_stds_trans = []
        L_medians_rot = []
        L_stds_rot = []   
        L_mins_rot = []
        L_mins_trans = []
        L_maxs_rot = []
        L_maxs_trans = []
        
        for k in range(self.nb_diff):
            Ltime = [self.L_all_time[k][l]-ori_time for l in range(length)]
            L_medians_trans.append(np.median(self.L_all_trans_err[k]) )
            L_stds_trans.append(np.std(self.L_all_trans_err[k]))
            L_medians_rot.append(np.median(self.L_all_rot_err[k]) )
            L_stds_rot.append(np.std(self.L_all_rot_err[k]))   
                
            min_rot_k = np.min(self.L_all_rot_err[k])
            t_min_rot_k = Ltime[self.L_all_rot_err[k].index(min_rot_k)]
            L_mins_rot.append([min_rot_k,t_min_rot_k])
                
            max_rot_k = np.max(self.L_all_rot_err[k])
            t_max_rot_k = Ltime[self.L_all_rot_err[k].index(max_rot_k)]
            L_maxs_rot.append([max_rot_k,t_max_rot_k])
                
            min_trans_k = np.min(self.L_all_trans_err[k])
            t_min_trans_k = Ltime[self.L_all_trans_err[k].index(min_trans_k)]
            L_mins_trans.append([min_trans_k,t_min_trans_k])
                
            max_trans_k = np.max(self.L_all_trans_err[k])
            t_max_trans_k = Ltime[self.L_all_trans_err[k].index(max_trans_k)]
            L_maxs_trans.append([max_trans_k,t_max_trans_k])   
        
        L_all_positive_detection = []
        L_all_negative_detection = []
        
        for k in range(self.nb_diff):
            # LC_detec_trans = np.array([self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1] for l in range(length -1) if abs(self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1]) > 0.5])
            # LCdt2 = (LC_detec_trans < self.trans_tol) # infé --> correction positive; sup --> correction négative
            
            # LC_detec_rot = np.array([self.L_all_rot_err[k][l] - self.L_all_rot_err[k][l+1] for l in range(length -1) if abs(self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1]) > 0.5])
            # LCdr2 = (LC_detec_rot < self.rot_tol) # infé --> correction positive; sup --> correction négative            
            
            # LCdtot = LCdt2*LCdr2
            # pos_detect = np.sum(LCdtot)
            # neg_detect = np.shape(LC_detec_trans)[0] - pos_detect
            
            # pos 2
            LC_detec_trans = np.array([self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1] for l in range(length -1) if abs(self.L_all_trans_err[k][l] - self.L_all_trans_err[k][l+1]) > self.trans_tol])
            LCdt2 = (LC_detec_trans < self.trans_tol) # infé --> correction positive; sup --> correction négative
            
            LC_detec_rot = np.array([self.L_all_rot_err[k][l] - self.L_all_rot_err[k][l+1] for l in range(length -1) if abs(self.L_all_rot_err[k][l] - self.L_all_rot_err[k][l+1]) > self.rot_tol])
            LCdr2 = (LC_detec_rot < self.rot_tol) # infé --> correction positive; sup --> correction négative            
            
            pos_detect = np.sum(LCdt2)+np.sum(LCdr2)
            neg_detect = np.shape(LC_detec_trans)[0] + np.shape(LC_detec_rot)[0] - pos_detect
            
            L_all_positive_detection.append(pos_detect)
            L_all_negative_detection.append(neg_detect)
            
        
        return(L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot,
               L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans,
               L_all_positive_detection, L_all_negative_detection)

    def display_statistics(self,msg):
        if (msg.true_false):
            L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot, L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans, L_all_positive_detection, L_all_negative_detection   = self.statistics_med()
        else:
            L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot, L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans, L_all_positive_detection, L_all_negative_detection   = self.statistics_mean()

        L_class_median_trans = [0 for k in range(self.nb_diff)]
        for i in range(len(L_medians_trans)):
            val_clas = L_medians_trans[i]
            clas = 0
            for j in range(len(L_medians_trans)):
                if val_clas > L_medians_trans[j]:
                    clas+=1
            L_class_median_trans[i] = clas
                
        L_class_median_rot = [0 for k in range(self.nb_diff)]
        for i in range(len(L_medians_rot)):
            val_clas = L_medians_rot[i]
            clas = 0
            for j in range(len(L_medians_rot)):
                if val_clas > L_medians_rot[j]:
                    clas+=1
            L_class_median_rot[i] = clas        
                
        if (msg.true_false):
            sev = "Median"
        else:            
            sev = "Mean"
        print("------------ Statistics -------------")
        print(" Translation Error Statistics ")
        for k in range(self.nb_diff):
            print("Test nbe : %i | Global %s Translational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,sev,L_medians_trans[k],L_stds_trans[k],L_mins_trans[k][0],L_mins_trans[k][1],L_maxs_trans[k][0],L_maxs_trans[k][1],L_class_median_trans[k]) )
        print("********************************")
        print(" Rotational Error Statistics ")
        for k in range(self.nb_diff):
            print("Test nbe : %i | Global %s Rotational Error : %f (with std : %f ) | Min : %f (at time : %f) ; Max : %f (at time : %f) | ranking : %i "%(k,sev,L_medians_rot[k],L_stds_rot[k],L_mins_rot[k][0],L_mins_rot[k][1],L_maxs_rot[k][0],L_maxs_rot[k][1],L_class_median_rot[k]) )
        print("********************************")
        print(" Positive / Negative Loop Closure Detection ")
        for k in range(self.nb_diff):
            print("Test nbe : %i | Position detection : %i ; Negative Detection : %i "%(k,L_all_positive_detection[k],L_all_negative_detection[k]) )

        return(True)    
    

    def get_statistics(self,msg):
        if msg.median : 
            L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot, L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans, L_all_positive_detection, L_all_negative_detection   = self.statistics_med()
            print("Got median!")
        else:
            L_medians_trans, L_stds_trans, L_medians_rot, L_stds_rot, L_mins_rot, L_mins_trans, L_maxs_rot, L_maxs_trans, L_all_positive_detection, L_all_negative_detection   = self.statistics_mean()
            print("Got mean!")
        return(L_medians_trans,L_stds_trans,L_medians_rot,L_stds_rot,L_all_positive_detection,L_all_negative_detection,[self.trans_tol,self.rot_tol], len(self.L_all_trans_err[0]))
           
        


    def save_data_service(self,msg):
        name = msg.path + "/" + msg.map_name + ".csv"
        return(self.save_data(name))


    def save_data(self,name):

        f = open(name,'w')
        with f:
            
            fnames = []
            for k in range(1,self.nb_diff+1):
                fnames += ["Trans_err_%i"%k,"Rot_err_%i"%k,"Associated_time_%i"%k," "]
            fnames += ["Max difference in time between everyone"]
            
            
            writer = csv.writer(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
            writer.writerow(fnames)
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            for l in range(length):
                Lt = [ self.L_all_time[i][l] for i in range(len(self.L_all_time))]
                diff = max(Lt)-min(Lt)
                L = []
                for k in range(self.nb_diff):
                    L += [self.L_all_trans_err[k][l],self.L_all_rot_err[k][l],self.L_all_time[k][l]," "]
                L+= [diff]
                writer.writerow(L)
            
        return(True)        
    

    
    def reboot_service(self,msg):
        
        self.buf_access = False        
        self.buf_access_map = False
        self.buf_access_truth = False
        self.buf_access_bl = False
        print("Saveorload : ", msg.save_or_load)
        if msg.save_or_load == 0:
            name = msg.path + "/" + msg.map_name + ".csv"
            self.save_data(name)
        if self.type_data == "raw":            
            self.gazeboTopic.unregister()
        elif self.type_data == "record":
            self.truthTopic.unregister()
        self.sub_tf.unregister()
        self.initialPoseTopic.unregister()
        self.odom_topic.unregister()
        
        if self.compare_3d:
            self.buffer.clear()
            self.listenertf.buffer.clear()
            # del(self.buffer)
            # del(self.listenertf)
        for k in range(self.nb_diff):
            self.L_sub_rtab[k].unregister()

        self.reboot()
        print("Reboot completed in evaluateNodeRts!")
            
        return(True)
    
    
    def reboot(self):
        
        self.ntry = 4
        
        ## Parameters for callback_gazebo
        self.detect_name = False
        self.num_rosbot = -1
        self.prev_gaz_pos = None
        self.prev_gaz_rot = None
        self.prev_gaz_speed = None
        self.prev_gaz_ang = None
        self.prev_gaz_time = None
        
        ## Parameters for callback_initialpose_jump      
        self.prev_initialpose_pos = None
        self.prev_initialpose_rot = None   
        self.pose_jump = False

        ## Parameters for check_jump  
        self.th_tlp_pos = 100 #0.175
        self.th_tlp_rot = 100 #0.1396

        # Parameters for evaluateProximityToTruth_rts
        self.odom_jump = False
        self.gazebo_jump = False

        # Parameters for buffer and buffer access
        self.buf_access = True
        self.temp_buf_access = False
        
        self.buf_access_map = True
        self.temp_buf_access_map = False

        self.buf_access_truth = True
        self.temp_buf_access_truth = False

        self.buf_access_bl = True
        self.temp_buf_access_bl = False

        self.buf_tf_true = []
        self.buf_all_tf_map = []
        self.buf_tf_bl = []
        
        self.temp_buf_tf_true = []
        self.temp_buf_all_tf_map = []
        self.temp_buf_tf_bl = []        

        # Parameters to save datas 
        
        self.L_all_trans_err = []
        self.L_all_rot_err = []
        self.L_all_time = []
        self.L_time_synch = []

        
        # Parameters to manage the publication of rts data

        if self.navig:
            self.rate_rts = 0.5+1e-3
            self.acc = 1.0 #1.0
        else:
            self.rate_rts = 2.0
            self.acc = 3.0 #1.0
        
        self.rate_odom = 0.1+1e-3
        self.rate_bl = 0.1+1e-3
        self.tol = self.rate_rts/2
        self.ltot = self.nb_diff
        self.trans_tol = 0.1
        self.rot_tol = 0.05
        
        self.qs_bl = int((self.nb_diff + self.rate_rts)*self.acc/(self.rate_bl))
        self.qs_odom = int((self.nb_diff + self.rate_rts)*self.acc/(self.rate_odom))
        self.qs_rts = int((self.nb_diff + self.rate_rts)*self.acc/self.rate_rts)
        

        # Parameters to manage the progression of the synchrnization process

        self.prev_map_synch_size = np.array([-1 for k in range(self.nb_diff)])
        self.len_buf_true = -1
        self.len_buf_bl = -1
   
        # Parameters to manage the publication of informations about the synchronization process
        self.ind = 0
        
        self.send = False
        
        self.L_disp_odom = []
        self.L_disp_bl = []
        self.L_ec_odom = []
        self.L_ec_bl = []
        
        for k in range(1,self.nb_diff+1):
            self.buf_all_tf_map.append([])
            self.temp_buf_all_tf_map.append([])              
            self.L_all_trans_err.append([])
            self.L_all_time.append([])
            self.L_all_rot_err.append([])
            self.L_disp_odom.append(True)
            self.L_disp_bl.append(True)
            self.L_ec_odom.append(-1)
            self.L_ec_bl.append(-1)
        
        if self.type_data == "raw":            
            self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
        elif self.type_data == "record":
            self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth2)

        self.initialPoseTopic = rospy.Subscriber("/rtabmap1/initialpose", PoseWithCovarianceStamped, self.callback_initialpose_jump)

        self.kidnap_topic = rospy.Subscriber("/default/kidnap_event", String, self.callback_kidnap)

        #self.odom_topic = rospy.Subscriber("/odom",Odometry, self.callback_odom2)
        
        self.odom_topic = rospy.Subscriber("/odom_noise",Odometry, self.callback_odom2)
        
        self.L_pub_comp = []
        self.L_map_name = []
        self.L_sub_rtab = []
        self.L_pub_rts = []

        self.sub_tf = rospy.Subscriber("/tf",tfMessage,self.callback_tf2)
        for k in range(1,self.nb_diff+1):
            self.L_map_name.append("map%i"%k)
            self.L_pub_comp.append(rospy.Publisher("comp_rts_%i"%(k), comp_rts) )
            self.L_pub_rts.append(rospy.Publisher("pose_rts_%i"%(k), pose_rts) )
            self.L_sub_rtab.append(rospy.Subscriber("rtabmap%i/mapGraph"%k,MapGraph,self.callback_rtab))
        
        


    def __init__(self,rate):

        """
        
        Constructor of the evaluateNodesRts class 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        """
        
        rospy.init_node("evaluateNodesRts", anonymous=False, log_level=rospy.INFO)
        
        rospy.loginfo("INIT")

        self.period = 1.0/rate

        self.verbose = False

        self.type_data= rospy.get_param("~type_data")
        self.nb_diff =rospy.get_param("~nb_diff")
        self.navig =rospy.get_param("~navig")
        self.synch_tot = True
        self.compare_3d = rospy.get_param("~compare_3d")
        self.record_rts_node = rospy.get_param("~record_rts_node")
        

        self.display_service = rospy.Service('display_errors', boolean, self.display_errors)
        self.display_max_err = rospy.Service('display_max_errors', boolean, self.display_max_errors)
        self.display_stats = rospy.Service('display_statistics', boolean, self.display_statistics)
        self.get_stats = rospy.Service('get_statistics', differences, self.get_statistics)
        self.go_reboot = rospy.Service('reboot', save_name, self.reboot_service)
        self.save_dta = rospy.Service('save_data',save_name , self.save_data_service)
        
        if self.compare_3d:
            self.buffer = tf2.Buffer(rospy.Duration(60)) # prend 60s de tf 
            self.listenertf = tf2.TransformListener(self.buffer)


        self.reboot()

        self.rate = rospy.Rate(rate)
        rospy.spin()

        return None



if __name__ == "__main__":
    cm = evaluateNodesRts(1.0)