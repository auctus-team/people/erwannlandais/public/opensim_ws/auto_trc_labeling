#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt


from sensor_msgs.msg import Image as ImageMSG
from sensor_msgs.msg import CameraInfo as CamInfoMSG
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2
import image_geometry
import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped

from geometry_msgs.msg import PoseStamped

import time
import geometry_msgs.msg

import tf

from dynamic_reconfigure.msg import Config as ConfigMsg
from dynamic_reconfigure.msg import ConfigDescription as ConfigDescrMsg

from rosbot_gazebo.srv import save_name, boolean, booleanResponse, save_nameResponse

import os


class fixMap():
    
    def load_save_map(self,msg):
        obj = save_nameResponse()
        obj.success = False
        if msg.save_or_load == 0:
            # save
            name = msg.map_name[:-4]
            f = open(msg.path+"/"+name+".txt","w+")
            f.write(str(self.L_gmap2orb) )
            f.close()
            obj.success= True
        elif msg.save_or_load == 1:
            # load
            self.loadmap = True
            self.reboot_map += 1
            f = open(msg.path+"/"+msg.map_name+".txt","r+")
            fl = f.readlines()
            data = fl[0]
            data = data[1:-1].split(',')
            self.L_gmap2orb = [float(data[k]) for k in range(len(data))]
            obj.success= True
            self.reboot_map_ori = self.reboot_map
        
        return(obj)
    
    def getDynamicState(self,config):
        """
        Method checking, thanks to the dynamic parameters, if there is any need
        to reboot the tf between the origin of the gmapping map and the origin 
        of the orb_slam map
        
        Currently, there is two possibilities for rebooting this tf : 
            
            - nothing have change between the dynamic parameters from
            the previous message and from the current message. It corresponds 
            to a bug recorded for the parameter reset_map : as this parameter
            is reset to "False" when the user put it to "True", there is no
            way to record it with other topics.
            So, when this case occurs : 
                --> means that reset_map has been activated
                --> autorize the map reboot

                
            - one dynamic bool linked to a remapping has been activated (the names of
            the booleans realated to this case is stored into L_authorized)
                --> autorize the map reboot

        Parameters
        ----------
        config : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        L_actu_names = []
        L_actu_values = []
        L_actu_all_values = []
        for k in range(len(config.bools)):
            name = str(config.bools[k].name)
            L_actu_names.append(name)
            L_actu_values.append(config.bools[k].value)
        
        L_actu_all_values = L_actu_values
        
        try : 
            for k in range(len(config.strs)):
                L_actu_all_values.append(config.strs[k].value)
        except Exception :
            print("No strs added")
            pass
        
        try : 
            for k in range(len(config.ints)):
                L_actu_all_values.append(config.ints[k].value)
        except Exception : 
            print("No ints added")
            pass 
        
        try : 
            for k in range(len(config.doubles)):
                L_actu_all_values.append(config.doubles[k].value)
        except Exception : 
            print("No doubles added")
            pass        
        goReboot = False
        # always autorize the reboot at the first time
        if self.L_values == []:
            print("First start")
            goReboot = True            
        else:
            comp = np.array(L_actu_values) == np.array(self.L_values)
            compall = np.array(L_actu_all_values) == np.array(self.L_all_values)
            #all elements equals between the two calls --> case where reset_map has been activated
            if ( all(compall) ):
                print("None elements have changed")
                goReboot = True
            elif (any(comp)):
                for k in range(len(L_actu_names)):
                    if (comp[k] == False and L_actu_names[k] in self.L_autorized):
                        print("Found : ", L_actu_names[k], " as changed : it is now ", L_actu_values[k] )
                        goReboot = True
                        break
                        
        self.L_values = L_actu_values
        self.L_all_values = L_actu_all_values
        return(goReboot)
            
    
    def callback(self,config):
        """
        
        How the reboot works : 
            
            If there's a need for a reboot : 
                
                - reboot_map is increased
                
                - reboot_map is not the same value as reboot_map_ori :
                    --> goto mapCamInWorld, quit the loop updating the tf between the two maps   
                    --> goto callbackORB_Active, the estimated pose of the camera by ORB_SLAM is now searched
                        ( it seems to be just an artifact of a previous method to get the tf between the gmapping map 
                         and the camera position estimated by gmapping. )
                                                                    
                - init : return into mapCamInWorld (as rospy is not shutdown)
                
                - search the tf between the gmapping map and the camera position estimated by gmapping.
                
                - exploit this tf to create the tf between the ORB_SLAM map and the gmapping map.
                    --> as the initial camera pose is the same that the origin of the map made by ORB_SLAM, the tf between the
                 the initial camera pose and the origin of the gmapping map is the same that the origin of the ORB_SLAM map and
                 the origin of the gmapping map.
                 
                 - a new tf is created : end of the reboot.
                     --> reboot_map_ori gets the same value as reboot_map. 
                     --> enter the loop broadcasting the tf between the two maps.
                     --> goto callbackORB_Active, do not update anymore the estimated pose of the
                     camera made by ORB_SLAM.
                 
                 
        Parameters
        ----------
        config : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        if (self.reboot_map == self.reboot_map_ori):
            value_bool = self.getDynamicState(config)
            if value_bool:
                self.reboot_map += 1
        
    
    
    def callbackORB_Active(self,pose):
        """
        Temporary callback : only take once the camera_info matrix, then
        unsubscribe from the concerned topic.

        ------
        Input : 
            
            cami : Camera Info Message : ROS message containing all the intrinsic
            parameters of the camera (fx, fy, distortion coefficients ...)
                
        """
        if ( (self.reboot_map != self.reboot_map_ori) or self.ORB_active == False):
            self.ORB_active = True
            self.depthMsg = pose
            print("Reboot called")
            if self.type_cam == 1:
                if (self.npose == 0):
                    self.last_time_pose = rospy.get_time()
                self.npose +=1
                if (self.npose == self.npose_trigger and (rospy.get_time() - self.last_time_pose < self.time_between_pose) ):
                    print("Trigger autorized")
                elif ( (rospy.get_time() - self.last_time_pose > self.time_between_pose) and self.npose <= self.npose_trigger ):
                    print("Trigger unautorized. Waiting for new initialization.")
                    self.npose = 0

        return None
    
    
	    
    def mapCamInWorld(self):   
        
        broadcaster = tf2.StaticTransformBroadcaster()
        static_transformStamped = geometry_msgs.msg.TransformStamped()
        if (self.loadmap == False):
            while (self.type_cam >0 and self.npose <= self.npose_trigger) and not rospy.is_shutdown() and self.loadmap == False:
                pass
            if self.loadmap == True:
                return(None)
            rospy.loginfo("New pose detected, goto tf search")
            self.reboot_map_ori = self.reboot_map
  
            while not rospy.is_shutdown() :
                try:
                    transformStamped_gmap = geometry_msgs.msg.TransformStamped()
                    transformStamped_gmap = self.buffer.lookup_transform('map2', 'base_link', rospy.Time(0) , rospy.Duration(0.5) )
                    break              
                except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                    rospy.loginfo("Still here")
                    pass
            
            if (self.simu == 1):
                offsetz = 0.18
            else:
                offsetz = 0.18
            
            trans = [transformStamped_gmap.transform.translation.x-0.03,
                     transformStamped_gmap.transform.translation.y,
                     transformStamped_gmap.transform.translation.z+offsetz]
            rot = [transformStamped_gmap.transform.rotation.x,
                   transformStamped_gmap.transform.rotation.y,
                   transformStamped_gmap.transform.rotation.z,
                   transformStamped_gmap.transform.rotation.w]
        else:
            while self.reboot_map != self.reboot_map_ori:
                pass
            trans = self.L_gmap2orb[0:3]
            rot = self.L_gmap2orb[3:]
            self.loadmap = False

        rospy.loginfo("Map2 to Map found")
        print(trans,rot)
        self.reboot_map_ori = self.reboot_map
        self.npose = 0
        
        static_transformStamped.header.stamp = rospy.Time.now()
        static_transformStamped.header.frame_id = "map2"
        static_transformStamped.child_frame_id = "map"
                
        static_transformStamped.transform.translation.x = trans[0]
        static_transformStamped.transform.translation.y = trans[1]
        static_transformStamped.transform.translation.z = trans[2]
        
     
        static_transformStamped.transform.rotation.x = rot[0]
        static_transformStamped.transform.rotation.y = rot[1]
        static_transformStamped.transform.rotation.z = rot[2]
        static_transformStamped.transform.rotation.w = rot[3]
    

        broadcaster.sendTransform(static_transformStamped)
        self.L_gmap2orb = [trans[0],trans[1], trans[2],rot[0],rot[1],rot[2],rot[3]]
        
        while (self.reboot_map == self.reboot_map_ori and not rospy.is_shutdown() ):
            rospy.sleep(0.5)

        print("Change of referential")
        self.last_time_pose = -1

        return(None)
    
    def __init__(self,rate):

        """
        
        Constructor of the CrackMap class. 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        Parameters : 
            
            PicID : int, associated with the ID of the last depth message
            recorded.
            
            cur_PicID : int, associated with the ID of the currently processed
            depth message (by the checkCracks function)
            
            CIFrontColor : Camera Info object, containing all the intrinsic parameters
            of the camera.
            
            depthMsg : Image Message : ROS message containing the last message published by 
            the topic /phantomx/crack_image (a depth picture containing the depth of 
            all the detected cracks on a picture).
            
            depthPic : OpenCV Matrix, containing the last depth picture published by
            the topic /phantomx/crack_image.
            
            allCracksInCave : list, containing all the cracks detected since the beginning
            of the simulation.
            
            nb_cracks : int, containing the number of cracks detected since the beginning of the
            simulation.
            
            dist_center_cracks : float, indicating the minimum 3D distance between the barycenters
            of the cracks.
            
            R_rpic2rcam : rotationnal matrix from the picture frame to the camera sensor frame
            
            R_rcam2rpic : rotationnal matrix from the camera sensor frame to the picture frame
            
            buffer : BufferInterface object, stores the last transformation matrices between frames for
            a certain time (60 seconds here).
            
            listener : TransformListener object, used to express the coordinates of a pixel from the 
            camera sensor frame into the cave frame.
            
        """
        rospy.loginfo("INIT")
        self.bridge = CvBridge()

        
        self.L_autorized = ["reset_map"]

        self.ORB_active = False
        self.depthMsg = None
        self.depthPic = None
    
        self.last_time_pose = -1
        self.time_between_pose = 5
        
        self.npose = 0
        self.npose_trigger = 3

        
        self.reboot_map = 0
        self.reboot_map_ori = self.reboot_map
        
        self.type_cam = rospy.get_param("~type_cam_fm")
        self.simu= rospy.get_param("~simu")
        
        # recall :  recall : 0 : rgbd | 1 : mono | 2 : mono + odometers
        
        if (self.type_cam == 0):
            self.client = rospy.Subscriber("/orb_slam2_rgbd/parameter_updates", ConfigMsg, self.callback)
            self.Posetopic = rospy.Subscriber("/orb_slam2_rgbd/pose", PoseStamped,self.callbackORB_Active)      
        
        elif (self.type_cam == 1):
            self.client = rospy.Subscriber("/orb_slam2_mono/parameter_updates", ConfigMsg, self.callback)
            self.Posetopic = rospy.Subscriber("/orb_slam2_mono/pose", PoseStamped,self.callbackORB_Active)      
            
        self.L_values = []
        self.L_all_values = []
        
        self.buffer = tf2.Buffer(rospy.Duration(60)) # prend 60s de tf 
        self.listenertf = tf2.TransformListener(self.buffer)
        
        save_map_service = rospy.Service('save_map', save_name, self.load_save_map)
        #self.listenertf = tf.TransformListener()
    
        self.L_gmap2orb = []

        self.loadmap = False

        self.rate = rospy.Rate(rate)
        
        while not rospy.is_shutdown():
            while self.ORB_active ==False:
                pass
            rospy.loginfo("Ready")
            self.mapCamInWorld()
        
        
        return None


if __name__ == "__main__":
    rospy.init_node("fixMap", anonymous=False, log_level=rospy.DEBUG)
    cm = fixMap(0.5)