#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np

import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped


import time
import geometry_msgs.msg

import tf
import tf.transformations

from gazebo_msgs.msg import LinkStates
from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp, comp_odom
from rosbot_gazebo.srv import boolean

import matplotlib.pyplot as plt



class evaluateNodesOrb():
          
    
    def callback_get_last_truth(self,msg):

        true_pos = msg.trans_tr
        true_rot = msg.rot_tr      
        self.evaluateProximityToTruth_orb(true_pos,true_rot)     
        return(None)
    
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation


        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            self.evaluateProximityToTruth_orb(true_pos,true_rot)

        
        return(None)   
    

    def evaluateProximityToTruth_orb(self,true_pos,true_rot):     
        #rospy.loginfo("New pose detected, goto tf search")
        ng = 0
        no = 0
        # get transform between origin (by gmap) and pose of camera for gmap
        while not rospy.is_shutdown() and ng < self.ntry:
            try:
                transformStamped_gmap = geometry_msgs.msg.TransformStamped()
                transformStamped_gmap = self.buffer.lookup_transform('map2', 'camera_link2', rospy.Time(0), rospy.Duration(0.5) )
                time_gmap = transformStamped_gmap.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for gmap")
                ng+=1
                pass
        # get transform between origin (by gmap) and pose of camera for orb_slam
        while not rospy.is_shutdown() and no < self.ntry and ng < self.ntry:
            try: 
                transformStamped_orb = geometry_msgs.msg.TransformStamped()
                transformStamped_orb = self.buffer.lookup_transform('map2', 'camera_link', time_gmap, rospy.Duration(0.5) )
                break              
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                rospy.loginfo("Still searching for orb")
                no +=1
                pass
        if (ng==self.ntry):
            #pass
            print("TF gmap not found. Retry.")

        elif (no==self.ntry):
            print("TF orb not found. Retry.")        
        else:
            
            # obtain the norm of translation and rotation
            euler_gm = tf.transformations.euler_from_quaternion((transformStamped_gmap.transform.rotation.x,
                                                                 transformStamped_gmap.transform.rotation.y,
                                                                 transformStamped_gmap.transform.rotation.z,
                                                                 transformStamped_gmap.transform.rotation.w))
            roll_gm, pitch_gm, yaw_gm= euler_gm[0],euler_gm[1],euler_gm[2]
            
            euler_orb = tf.transformations.euler_from_quaternion((transformStamped_orb.transform.rotation.x,
                                                                                     transformStamped_orb.transform.rotation.y,
                                                                                     transformStamped_orb.transform.rotation.z,
                                                                                     transformStamped_orb.transform.rotation.w))
            roll_orb,pitch_orb,yaw_orb = euler_orb[0],euler_orb[1],euler_orb[2]
            
            euler_tr = tf.transformations.euler_from_quaternion( (true_rot.x,true_rot.y,true_rot.z,true_rot.w))
            
            roll_tr,pitch_tr,yaw_tr = euler_tr[0],euler_tr[1],euler_tr[2]
            
            trans_gm = np.array([transformStamped_gmap.transform.translation.x,transformStamped_gmap.transform.translation.y,transformStamped_gmap.transform.translation.z])
            trans_orb = np.array([transformStamped_orb.transform.translation.x,transformStamped_orb.transform.translation.y,transformStamped_orb.transform.translation.z])
            trans_tr = np.array([true_pos.x-0.03,true_pos.y,true_pos.z+0.18])
            
            rot_gm = np.array([roll_gm,pitch_gm,yaw_gm])
            rot_orb = np.array([roll_orb,pitch_orb,yaw_orb])
            rot_tr = np.array([roll_tr,pitch_tr,yaw_tr])
            
            trans_gmTr = trans_tr-trans_gm
            trans_orbTr = trans_tr-trans_orb
            
            rot_gmTr = rot_tr-rot_gm
            rot_orbTr = rot_tr-rot_orb
            
            if np.linalg.norm(rot_gmTr) > np.linalg.norm(rot_orbTr):
                closest_rot = "orb"
            else:
                closest_rot = "gmap"
            if np.linalg.norm(trans_gmTr) > np.linalg.norm(trans_orbTr):
                closest_trans = "orb"
            else:
                closest_trans = "gmap"            
            
            
            # publish it
            msg = comp()
            msg.trans_gm = list(trans_gm)
            msg.trans_orb= list(trans_orb)
            msg.trans_tr = list(trans_tr)
            msg.rot_gm  = list(rot_gm)
            msg.rot_orb  = list(rot_orb)
            msg.rot_tr  = list(rot_tr)
            msg.closest_rot = closest_rot
            msg.closest_trans = closest_trans
            self.pub_comp.publish(msg)        

        return(None)    

    def __init__(self,rate):
        
        """
        
        Constructor of the EvaluateNodesOrb class. 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        Parameters : 
            

            
        """
        rospy.loginfo("INIT")
        
        self.type_data= rospy.get_param("~type_data")
        self.nb_diff =rospy.get_param("~nb_diff")

        if self.type_data == "raw":            
            self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
        elif self.type_data == "record":
            self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth)
            
        self.detect_name = False
        self.num_rosbot = -1
        
        self.ntry =4 #3
        self.last_time_pose = -1
        
        self.period = 1.0/rate
        
        
        self.buffer = tf2.Buffer(rospy.Duration(60)) # prend 60s de tf 
        self.listenertf = tf2.TransformListener(self.buffer)

        self.pub_comp = rospy.Publisher("comp_orb_gmap", comp)

        self.rate = rospy.Rate(rate)
        
        rospy.spin()
        
        return None


if __name__ == "__main__":
    rospy.init_node("evaluateNodesOrb", anonymous=False, log_level=rospy.DEBUG)
    cm = evaluateNodesOrb(1.0)