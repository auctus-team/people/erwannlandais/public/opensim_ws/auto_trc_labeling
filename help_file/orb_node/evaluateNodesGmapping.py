#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:29:44 2020

@author: tnmx0798
"""

import rospy
import rospkg
import numpy as np

import tf2_ros as tf2
from tf2_geometry_msgs import PointStamped

import time
import geometry_msgs.msg 

from geometry_msgs.msg import PoseWithCovarianceStamped

import tf
import tf.transformations

from gazebo_msgs.msg import LinkStates
from gazebo_msgs.msg import ModelStates

from rosbot_gazebo.msg import comp, comp_rts, comp_odom
from rosbot_gazebo.srv import boolean, save_name

import matplotlib.pyplot as plt

from evaluateNodesRts import transformStampedEuler

from tf.msg import tfMessage

import csv

import os


class evaluateNodesGmap():

    def check_jump(self,current_lin_speed,current_ang_speed,
                   prev_lin_speed,prev_ang_speed,
                   current_lin_pos,current_ang_pos,
                   prev_lin_pos,prev_ang_pos,
                   current_time,prev_time,current_detect_jump):
        """
        
        Principe : 
            - prendre vitesse (current_lin_speed, current_ang_speed) et position odométrique actuelle (current_lin_pos,current_ang_pos)
            - obtenir la vitesse (angulaire et linéaire) estimée, en comparant la position précédente
            à la position actuelle. (inutile maintenant)
            Cela permet d'avoir une estimation de la vitesse réelle estimée du robot.
            - obtenir la position (angulaire et linéaire) estimée, à partir de la vitesse précédente
            et la position réelle précédente. 
            - Evaluer la différence entre les positions estimées à partir des mesures précédentes et les positions actuelles.
            Si trop importante, veut dire qu'il y a une erreur : position actuelle ne colle
            pas par rapport à estimation de la vitesse et position précédente. 
        
        """
        detect_jump = False
        if current_detect_jump == True:
            return(current_detect_jump)
        
        if (type(prev_time) != type(None)):
            delay = current_time - prev_time
            estimated_speed = (current_lin_pos - prev_lin_pos)/delay
            estimated_ang = (current_ang_pos-prev_ang_pos)/delay
            
            estimated_pose = current_lin_speed*delay + prev_lin_pos
            # suppress z component if inferior to threshold
            if estimated_pose[2] < self.th_tlp_pos:
                estimated_pose[2] = 0
            estimated_rot = current_ang_speed*delay + prev_ang_pos
            
            rot_diff  = np.linalg.norm(estimated_rot-current_ang_pos) 
            if (rot_diff > np.pi):
                if (rot_diff//(2*np.pi)==0 ):
                    rot_diff =abs( rot_diff - 2*np.pi)
                else:
                    rot_diff = rot_diff%(2*np.pi)

            if (np.linalg.norm(estimated_pose - current_lin_pos) > self.th_tlp_pos  
                or rot_diff > self.th_tlp_rot):
                detect_jump = True
                # print("speed : ", np.linalg.norm(new_speed - estimated_speed ))
                # print("angular : ", np.linalg.norm(new_ang-estimated_ang))
                #print("speed : ", estimated_speed )
                #print("angular : ", estimated_ang)
                print("diff_pose : ", np.linalg.norm( current_lin_pos - estimated_pose ))
                #print("pose : ",  estimated_pose )
                #print("rotation : ", estimated_rot )      
                print("diff_rot : ", rot_diff )
        
        return(detect_jump)
         
    def callback_amcl(self,msg):
        i = 0
        if (i < self.nb_diff):
            if (self.buf_access_map): # and len( self.buf_all_tf_map[i]) == 0 ):
                self.buf_all_tf_map[i].append(msg)
                if len(self.buf_all_tf_map[i]) > self.qs_amcl:
                    del(self.buf_all_tf_map[i][0])
                            
            elif (self.temp_buf_access_map): # and len( self.temp_buf_all_tf_map[i]) == 0):
                self.temp_buf_all_tf_map[i].append(msg)    
                if len(self.temp_buf_all_tf_map[i]) > self.qs_amcl:
                    del(self.temp_buf_all_tf_map[i][0])        
        
        #print(self.buf_all_tf_map[i])

        
    def find_closest_time_match(self,time,L_time):
        ec = 9999
        prev_ec = ec
        i = 0
        while abs(i-1) <= len(L_time):
            ec = abs(time - L_time[i-1])
            if ec <= prev_ec:
                i -= 1
                prev_ec = ec
            else:
                break
        return(i,prev_ec)    
    
    def callback_get_last_truth2(self,msg):
        
        # time = rospy.get_time()
        # true_pos = msg.trans_tr
        # true_rot = msg.rot_tr   
        
        if self.gazebo_jump == False and msg.gazebo_jump == True:
            self.gazebo_jump = True
        
        if (self.buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = msg.trans_tr
            tf_true.rot = np.array([msg.rot_tr[0],msg.rot_tr[1], msg.rot_tr[2]])
            tf_true.time = msg.stamp.to_sec()
            # try:
            #     tf_true.time = msg.stamp.to_sec()
            # except Exception:
            #     tf_true.time = time
            self.buf_tf_true.append(tf_true)
            if (len(self.buf_tf_true) > self.qs_odom):
                del(self.buf_tf_true[0])

            #self.buf_tf_true.append(tf_true)
            # if (len(self.buf_tf_true) > 1):
            #     self.rate_odom = self.buf_tf_true[-1].time - self.buf_tf_true[-2].time
    
        elif (self.temp_buf_access_truth):
            tf_true = transformStampedEuler()
            tf_true.pos = msg.trans_tr   
            tf_true.rot = msg.rot_tr   
            tf_true.time = msg.stamp.to_sec()
            # try:
            #     tf_true.time = msg.stamp.to_sec()
            # except Exception:
            #     tf_true.time = time
            self.temp_buf_tf_true.append(tf_true)
            if (len(self.temp_buf_tf_true) > self.qs_odom):
                del(self.temp_buf_tf_true[0])

        return(None)    
    
    def callback_get_last_truth(self,msg):
        if self.gazebo_jump == False and msg.gazebo_jump == True:
            self.gazebo_jump = True
        true_pos = msg.trans_tr
        true_rot = msg.rot_tr      
        true_time = msg.stamp        
        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            self.evaluateProximityToTruth_gmapping(true_pos,true_rot,true_time)
            self.last_time_pose = rospy.get_time()
            #print("LTP : ", self.last_time_pose)

        return(None)
    
    def callback_gazebo(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation
        true_time = rospy.Time(0)
        
        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])

        if (rospy.get_time() - self.last_time_pose) > self.period:
            #print("New_pose!")
            self.evaluateProximityToTruth_gmapping(new_pose,new_rot,true_time)
            self.last_time_pose = rospy.get_time()
            #print("LTP : ", self.last_time_pose)

        
        return(None)   

    def callback_gazebo2(self,msg):
        if (self.detect_name== False):
            for k in range(len(msg.name)):
                if (msg.name[k] == "rosbot"):
                    self.num_rosbot = k
                    break
            self.last_time_pose = rospy.get_time()
            self.detect_name = True
        true_pos = msg.pose[self.num_rosbot].position
        true_rot = msg.pose[self.num_rosbot].orientation

        new_pose = np.array([true_pos.x, true_pos.y, true_pos.z])
        new_quat = true_rot
        euler = tf.transformations.euler_from_quaternion( (new_quat.x,new_quat.y,new_quat.z,new_quat.w))
        roll,pitch,yaw = euler[0],euler[1],euler[2]
        new_rot = np.array([roll,pitch,yaw])
        
        true_speed = msg.twist[self.num_rosbot].linear
        true_ang = msg.twist[self.num_rosbot].angular
        new_speed = np.array([true_speed.x, true_speed.y, true_speed.z])
        new_ang = np.array([true_ang.x,true_ang.y,true_ang.z])
        timens = rospy.get_time()
        
        self.gazebo_jump = self.check_jump(new_speed,new_ang,
                                           self.prev_gaz_speed,self.prev_gaz_ang,
                                           new_pose,new_rot,
                                           self.prev_gaz_pos,self.prev_gaz_rot,
                                           timens,self.prev_gaz_time,self.gazebo_jump)
        
        self.prev_gaz_pos = new_pose
        self.prev_gaz_rot = new_rot
        self.prev_gaz_speed = new_speed
        self.prev_gaz_ang = new_ang
        self.prev_gaz_time = timens
        
        
        #print(rospy.get_time() - self.last_time_pose)
        if (rospy.get_time() - self.last_time_pose) > self.rate_odom:
            #print("New_pose!")
            
            self.last_time_pose = rospy.get_time()
            time = self.last_time_pose
            if (self.buf_access_truth):
                tf_true = transformStampedEuler()
                tf_true.pos = new_pose
                tf_true.rot = new_rot
                tf_true.time = timens
                self.buf_tf_true.append(tf_true)
                if len(self.buf_tf_true) > self.qs_odom:
                    del(self.buf_tf_true[0])

            elif (self.temp_buf_access_truth):
                tf_true = transformStampedEuler()
                tf_true.pos = new_pose
                tf_true.rot = new_rot
                tf_true.time = timens
                self.temp_buf_tf_true.append(tf_true)
                if len(self.temp_buf_tf_true) > self.qs_odom:
                    del(self.temp_buf_tf_true[0])
            
        
        return(None)

    def callback_tf2(self,msg):
        lentot = 0   
        
        if (self.buf_access_map ==True and self.buf_access_truth == True):
            
            lentot = min(len(self.buf_all_tf_map[0]),len(self.buf_tf_true) )

        if (lentot > 0 and self.buf_access_map ==True):
            self.buf_access_map = False
            self.temp_buf_access_map = True
            #print("Here_gmap")
            self.prepare_to_send2()    

    def check_synch_truth(self,buf,t):
        odom_synch = []
        i = -1
        if (buf[-1].time != self.len_buf_true):
            L_time = [ buf[k].time for k in range(len(buf))]
            (i,ec) = self.find_closest_time_match(t,L_time)
            if ec < self.rate_odom/2+self.tol*(self.rate_odom/self.rate_amcl):
                odom_synch.append(buf[i])
                self.err_odom = False
            else:
                print("Err true_odom synch : min time difference : ",ec, " for time : ", L_time[i], " compared to : ", t)
                if ec > self.rate_odom or self.err_odom:
                    # No matching is then possible : cancel the synchronization for this topic
                    self.map_time_synch = -1
                    if self.err_odom:
                        print("Stop odom blocking!")
                        self.len_buf_true = buf[-1].time
                    self.err_odom = False
                else:
                    self.err_odom = True
                self.len_buf_true = buf[-1].time
        return(odom_synch,i)   


    def prepare_to_send2(self):
        
        
        # self.temp_buf_access_map = True
        # self.buf_access_bl = False
        # self.temp_buf_access_bl = True
        # self.buf_access_truth = False
        # self.temp_buf_access_truth = True

        lentot = min(len(self.buf_all_tf_map[0]),len(self.buf_tf_true) )
        
        while not rospy.is_shutdown() and lentot > 0 and self.buf_access_truth == True:
            self.buf_access_map = False
            self.temp_buf_access_map = True
            self.buf_access_truth = False
            self.temp_buf_access_truth = True

            L_map_synch = [self.buf_all_tf_map[0][0]]

            true_synch= []
            true_synch,true_i = self.check_synch_truth(self.buf_tf_true,L_map_synch[0].header.stamp.to_sec())
                    
            if len(true_synch) > 0 :
                true_check = true_synch
    
                self.evaluateProximityToTruth_gmapping2(true_check[0],L_map_synch[0])

            else:
                print("No synch with map")
            self.temp_buf_access_map = False
    
            #add up temporary buffers (for map)
            for k in range(self.nb_diff):
                if (len(self.buf_all_tf_map[k]) > 0):
                    test = False
                    while len(self.temp_buf_all_tf_map[k]) > 0 and  test == False :
                        #print("Treating suppression")
                        p1 = self.temp_buf_all_tf_map[k][0].pose.pose.position   
                        nw_pose = np.array([p1.x, p1.y, p1.z])
                        p2 = self.buf_all_tf_map[k][-1].pose.pose.position   
                        od_pose = np.array([p2.x, p2.y, p2.z])
                        if (np.all(nw_pose==od_pose) == True) :
                            #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                            #print("Because of : ",  self.buf_all_tf_map[k][-1])
                            del(self.temp_buf_all_tf_map[k][0])
                        else:
                            test = True
    
                len_temp_map = len(self.buf_all_tf_map[k])
                self.buf_all_tf_map[k] = self.buf_all_tf_map[k][len_temp_map:]
                self.buf_all_tf_map[k] += self.temp_buf_all_tf_map[k]
                self.temp_buf_all_tf_map[k] = []
                        
            # if (self.send):
            #   print("Bufffer after send : ", self.buf_all_tf_map)
                #self.send = False
            self.buf_access_map = True
            
            
                
            #add up temporary buffer (for true position)
            # print("Bef temp : ", [self.temp_buf_tf_true[k].time for k in range(len(self.temp_buf_tf_true))])
            # print("Bef curr : ", [self.buf_tf_true[k].time for k in range(len(self.buf_tf_true))])
            self.temp_buf_access_truth = False

            if len(self.temp_buf_tf_true) > 0:
                test= False
                while len(self.temp_buf_tf_true) > 0 and test == False:
                    p1 = self.temp_buf_tf_true[0].pos
                    n_pose = p1
                    p2 = self.buf_tf_true[-1].pos
                    o_pose = p2
                    if (np.all(n_pose==o_pose) == True):
                        #print("Suppress : ", self.temp_buf_all_tf_map[k][0])
                        #print("Because of : ",  self.buf_all_tf_map[k][-1])
                        del(self.temp_buf_tf_true[0])
                    else:
                        test= True
                            
                            
            len_temp_true = len(self.temp_buf_tf_true)  
            self.buf_tf_true = self.buf_tf_true[len_temp_true:]
            self.buf_tf_true += self.temp_buf_tf_true
            self.temp_buf_tf_true = []
    
            self.buf_access_truth = True

            lentot = min(len(self.buf_all_tf_map[0]),len(self.buf_tf_true) )
            
    def evaluateProximityToTruth_gmapping2(self,true_check,transformStamped_gmap):     

            
        # obtain the norm of translation and rotation
            
        #print(true_check)
        true_pos = true_check.pos
        true_rot = true_check.rot
        
        #print(true_pos)
            
        euler_gm = tf.transformations.euler_from_quaternion((transformStamped_gmap.pose.pose.orientation.x,
                                                                 transformStamped_gmap.pose.pose.orientation.y,
                                                                 transformStamped_gmap.pose.pose.orientation.z,
                                                                 transformStamped_gmap.pose.pose.orientation.w))
        roll_gm, pitch_gm, yaw_gm= euler_gm[0],euler_gm[1],euler_gm[2]
        rot_gm = np.array([roll_gm,pitch_gm,yaw_gm])

        trans_gm = np.array([transformStamped_gmap.pose.pose.position.x,transformStamped_gmap.pose.pose.position.y,transformStamped_gmap.pose.pose.position.z])
        
        true_trans_tr = np.array([true_pos[0],true_pos[1],true_pos[2]])     
        trans_tr = np.array([true_pos[0],true_pos[1]-0.03,true_pos[2]+0.18])
        rot_tr = np.array([true_rot[0],true_rot[1], true_rot[2]])
            
        

        # here, we'll guess that odometry on z is perfect. 
        # if self.z_ori == -1 and abs(trans_tr[2]-0.22) < 0.01:
        #     self.z_ori = trans_tr[2]
                
                
        # trans_gm[2] = self.z_ori
        
        if self.compare_3d == False:   
            trans_gm[2] = 0.0
            rot_gm[0] = 0.0
            rot_gm[1] = 0.0
                
            trans_tr[2] = 0.0
            rot_tr[0] = 0.0
            rot_tr[1] = 0.0
        
        
        else:
            # as there is normally no way for amcl to get 3d informations, those informations
            # are replaced from the guess from the informations of the robot
            trans_gm[2] = 0.22
            rot_gm[0] = 0.0
            rot_gm[1] = 0.0
        
        # print("tg : ", trans_gm)
        # print("rg : ", rot_gm)
        # print("tr : ", trans_tr)
        # print("rr : ", rot_tr)        
        
        trans_gmTr = trans_tr-trans_gm
        rot_gmTr = rot_tr-rot_gm
        for l in range(3):  
            if (rot_tr[l]*rot_gm[l]<0):
                rot_gmTr[l] = min([abs(rot_gm[l]-rot_tr[l]),abs(rot_gm[l]+rot_tr[l]),abs(rot_tr[l]-rot_gm[l]),abs(rot_tr[l]+rot_gm[l])])

        diff_norm_trans = np.linalg.norm(trans_gmTr)
        diff_norm_rot = np.linalg.norm(rot_gmTr)
            
        self.L_all_trans_err[0].append(diff_norm_trans)
        self.L_all_rot_err[0].append(diff_norm_rot)
        self.L_all_time[0].append(transformStamped_gmap.header.stamp.to_sec())            
            
            # publish it
        msg = comp_rts()
        msg.stamp = transformStamped_gmap.header.stamp
        msg.trans_rts = list(trans_gm)
        msg.trans_tr = list(trans_tr)
        msg.rot_rts = list(rot_gm)
        msg.rot_tr  = list(rot_tr)
        msg.diff_norm_trans = diff_norm_trans
        msg.diff_norm_rot =  diff_norm_rot

        msg.gazebo_jump = self.gazebo_jump
        
        self.pub_comp.publish(msg)        
        self.pose_jump = False
        self.prev_gaz_pos  = true_trans_tr
        self.prev_gaz_rot = rot_tr
        self.gazebo_jump = False
        self.odom_jump = False
        
        return(None)  
   

    def evaluateProximityToTruth_gmapping(self,true_pos,true_rot,true_time):     
        #rospy.loginfo("New pose detected, goto tf search")
        ng = 0
        # get transform between origin (by gmap) and pose of camera for gmap
        while not rospy.is_shutdown() and ng < self.ntry:
            try:
                transformStamped_gmap = geometry_msgs.msg.TransformStamped()
                transformStamped_gmap = self.buffer.lookup_transform('map_gmap', 'camera_link2', rospy.Time(0), rospy.Duration(0.5) )
                # tester true_time
                time_gmap = transformStamped_gmap.header.stamp
                break
            except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
                #rospy.loginfo("Still searching for gmap")
                ng+=1
                pass

        if (ng==self.ntry):
            #pass
            print("TF gmap not found. Retry.")

        # elif (no==self.ntry):
        #     print("TF orb not found. Retry.")        
        else:
            
            # obtain the norm of translation and rotation
            euler_gm = tf.transformations.euler_from_quaternion((transformStamped_gmap.transform.rotation.x,
                                                                 transformStamped_gmap.transform.rotation.y,
                                                                 transformStamped_gmap.transform.rotation.z,
                                                                 transformStamped_gmap.transform.rotation.w))
            roll_gm, pitch_gm, yaw_gm= euler_gm[0],euler_gm[1],euler_gm[2]
            rot_gm = np.array([roll_gm,pitch_gm,yaw_gm])

            trans_gm = np.array([transformStamped_gmap.transform.translation.x,transformStamped_gmap.transform.translation.y,transformStamped_gmap.transform.translation.z])

            trans_tr = np.array([true_pos[0],true_pos[1]-0.03,true_pos[2]+0.18])
            rot_tr = true_rot
            
            # here, we'll guess that odometry on z is perfect. 
            if self.z_ori == -1 and abs(trans_tr[2]-0.22) < 0.01:
                self.z_ori = trans_tr[2]
                
                
            trans_gm[2] = self.z_ori
            
            
            trans_gmTr = trans_tr-trans_gm
            rot_gmTr = rot_tr-rot_gm
            for l in range(3):  
                if (rot_tr[l]*rot_gm[l]<0):
                    rot_gmTr[l] = min([abs(rot_gm[l]-rot_tr[l]),abs(rot_gm[l]+rot_tr[l]),abs(rot_tr[l]-rot_gm[l]),abs(rot_tr[l]+rot_gm[l])])

            diff_norm_trans = np.linalg.norm(trans_gmTr)
            diff_norm_rot = np.linalg.norm(rot_gmTr)
            
            self.L_all_trans_err[0].append(diff_norm_trans)
            self.L_all_rot_err[0].append(diff_norm_rot)
            self.L_all_time[0].append(transformStamped_gmap.header.stamp.to_sec())
            
            # publish it
            msg = comp_rts()
            msg.stamp = transformStamped_gmap.header.stamp
            msg.trans_rts = list(trans_gm)
            msg.trans_tr = list(trans_tr)
            msg.rot_rts = list(rot_gm)
            msg.rot_tr  = list(rot_tr)
            msg.diff_norm_trans = diff_norm_trans
            msg.diff_norm_rot =  diff_norm_rot

            msg.gazebo_jump = self.gazebo_jump
            self.pub_comp.publish(msg)        

        return(None)    



    def save_data_service(self,msg):
        name = msg.path + "/" + msg.map_name + ".csv"
        return(self.save_data(name))


    def save_data(self,name):

        f = open(name,'w')
        with f:
            
            fnames = []
            for k in range(1,self.nb_diff+1):
                fnames += ["Trans_err_%i"%k,"Rot_err_%i"%k,"Associated_time_%i"%k," "]
            fnames += ["Max difference in time between everyone"]
            
            
            writer = csv.writer(f,delimiter=',',quotechar =',', quoting = csv.QUOTE_MINIMAL)
            writer.writerow(fnames)
            length = min([len(self.L_all_trans_err[l]) for l in range(len(self.L_all_trans_err))])
            for l in range(length):
                Lt = [ self.L_all_time[i][l] for i in range(len(self.L_all_time))]
                diff = max(Lt)-min(Lt)
                L = []
                for k in range(self.nb_diff):
                    L += [self.L_all_trans_err[k][l],self.L_all_rot_err[k][l],self.L_all_time[k][l]," "]
                L+= [diff]
                writer.writerow(L)
            
        return(True)        
    




    def __init__(self,rate):
        
        """
        
        Constructor of the EvaluateNodesOrb class. 
        
        
        ------
        
        Input : 
            
            rate : the rate associated to rospy.
        
        Parameters : 
            

            
        """
        rospy.loginfo("INIT")
        
        self.type_data= rospy.get_param("~type_data")
        self.nb_diff =rospy.get_param("~nb_diff")
        self.navig =rospy.get_param("~navig")        
        
        # boolean to control whether we want to use tf tree or not
        self.no_tf = True

        if self.no_tf :
            self.sub_tf = rospy.Subscriber("/tf",tfMessage,self.callback_tf2)
            if self.type_data == "raw":            
                self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo2 )
    
            elif self.type_data == "record":
                self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth2)
        
        
        else:
            if self.type_data == "raw":            
                self.gazeboTopic = rospy.Subscriber("/gazebo/model_states", ModelStates, self.callback_gazebo )
    
            elif self.type_data == "record":
                self.truthTopic = rospy.Subscriber("/comp_odom", comp_odom, self.callback_get_last_truth)
         
        self.save_dta = rospy.Service('save_data_gmap',save_name , self.save_data_service)       
        
        self.compare_3d= rospy.get_param("~compare_3d")
        
        
        self.L_all_trans_err = [[]]
        self.L_all_rot_err = [[]]
        self.L_all_time = [[]]

        
        self.detect_name = False
        self.num_rosbot = -1
        
        self.ntry =4 #3
        self.last_time_pose = -1
        
        self.z_ori = -1
        
        self.rate_amcl = 0.1+1e-3
        self.rate_odom = 0.1+1e-3
        
        self.tol = self.rate_amcl/4
        
        self.period = self.rate_amcl

        self.nb_diff = 1
        
        
        self.qs_amcl = int((self.nb_diff+self.rate_amcl)/(self.rate_amcl))
        self.qs_odom = int((self.nb_diff+self.rate_odom)/(self.rate_amcl))        
        
        
        self.buf_access_map = True
        self.temp_buf_access_map = False
        
        self.buf_access_truth = True
        self.temp_buf_access_truth = False

        ## Parameters for check_jump  
        self.th_tlp_pos = 100 #0.175
        self.th_tlp_rot = 100 #0.095
        # Parameters for evaluateProximityToTruth_rts
        self.odom_jump = False
        self.gazebo_jump = False
        ## Parameters for callback_gazebo
        self.detect_name = False
        self.num_rosbot = -1
        self.prev_gaz_pos = None
        self.prev_gaz_rot = None
        self.prev_gaz_speed = None
        self.prev_gaz_ang = None
        self.prev_gaz_time = None

        self.err_odom = False

        self.len_buf_true = -1

        self.buf_tf_true = []
        self.buf_all_tf_map = [[]]
        
        self.temp_buf_tf_true = []
        self.temp_buf_all_tf_map = [[]]

        self.buffer = tf2.Buffer(rospy.Duration(60)) # prend 60s de tf 
        self.listenertf = tf2.TransformListener(self.buffer)
        
        if self.navig:
            self.pub_comp = rospy.Publisher("comp_amcl", comp_rts)
            if self.no_tf :
                self.compare_topic = rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, self.callback_amcl )
            
        else:
            self.pub_comp = rospy.Publisher("comp_gmap", comp_rts)

        self.rate = rospy.Rate(rate)
        
        rospy.spin()
        
        return None


if __name__ == "__main__":
    rospy.init_node("evaluateNodesGmap", anonymous=False, log_level=rospy.DEBUG)
    cm = evaluateNodesGmap(1.0)