#!/bin/bash

# Launch orb and gmapping at the same time.

source container_variables.sh
cd ${root_path}
source devel/setup.bash
roslaunch optitrack_full neutral_pose_expe.launch
