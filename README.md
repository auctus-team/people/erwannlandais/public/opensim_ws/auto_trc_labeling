# How to use

First, use it through dogi.

To create a container : 

~~~
dogi run os_ros --name ros_doc
~~~

To join a container (from another topic)

~~~
dogi exec ros_doc
~~~

Launch code through one of the launchfiles.

# Usefull commands

Display all current associations : 

~~~
rosservice call /mocap_expe/print_assos 1
~~~

Pause (= 1) or continue reading : 

~~~
rosservice call /mocap_expe/pause_continue [0/1]
~~~

Set up reading speed (bigger = faster) : 

~~~
rosservice call /mocap_expe/reading_speed [float value]
~~~

Suppress current associations (body parts, markers) :

~~~
rosservice call /to_clean_assos ["hand_l","hand_r","T10"]
~~~

Suppress ALL associations for currently optitrack markers of associated body parts : 

~~~
rosservice call /to_clean_assos ["hand_l","hand_r","FORCE"]
~~~

Add new association to marker (here, T10 to 215), suppress all other associations for the original optitrack marker : 

~~~
rosservice call /to_clean_assos ["T10=215","FORCE"]
~~~
